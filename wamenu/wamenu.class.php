<?php
/**
* -
*
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include dell'interfaccia i_waMenuView
* @ignore
*/
include_once __DIR__ . "/i_waMenuView.interface.php";

//***************************************************************************
//****  classe waMenu *******************************************************
//***************************************************************************
/**
* waMenu
*
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waMenu 
	{
	/**
	* view-object del menu
	*
	* l'oggetto view è è quello che implementa l'interfaccia i_waMenuView
	* e al quale saranno passati i dati per la costruzione dell'interfaccia 
	* utente
	* 
	* se non definito viene usato il default 
	* 
	* \waLibs\views\waMenu\waMenu_default\waMenuView
	* 
	* contenuto in uis/wamenu_default/view/wamenu.php
	* 
	* @var i_waMenuView
	*/	
	public $view 	= null;
	
	/**
	* nome del menu
	*
	* importante solo quando ci sono piu' menu all'interno della stassa pagina
	* @var string
	*/	
	public $name	= 'waMenu';
	
	/**
	 * titolo del menu che puo' essere utilizzato come si vuole nel {@link view view-object}
	 *
	 * @var string
	 */
	public $title;
		
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language = 'it';
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale = 'it-IT';
	
	/**
	* @ignore
	* @access protected
	*/	
	protected	$sectionCounter	= 0;
	
	/**
	* @ignore
	* @access protected
	*/	
	protected	$currLevel		= 0;
	
	/**
	* @ignore
	* @access protected
	*/	
	protected $openedSections = array();
	
	/**
	* @ignore
	* @access protected
	*/	
	protected $openSection = null;
	
	/**
	* struttura utilizzata per contenere i dati da passare al {@link view view-object}
	*
	* @ignore
	* @access protected
	* @var waMenuData
	*/	
	protected $data; 
	
	/**
	* array che contiene le voci cn cui costruire il menu;
	* non serve a niente, se non a renderlo documentabile in modo automatico 
	* (waDocumentation)
	*
	* @ignore
	* @access protected
	*/	
	var	$items; 
	
	

	//***********************************************************************
	/**
	* costruttore
	*
	* inizializza il menu.
	* @param object  view-object  {@link view}
	* @return void 
	*/
	public function __construct($view = null)
		{
		$this->view = $view;
		}
		
	//***********************************************************************
	/**
	* apre il menu
	*
	* @return void 
	*/
	public function open()
		{
	 	$this->data = new waMenuData();
 		$this->data->name = $this->name;
 		$this->data->waMenuPath = $this->GetMyDir();
 		$this->data->title = $this->title;
		$this->data->language = $this->language;
		$this->data->locale = $this->locale;

 		$this->data->items = array();
		$this->openSection = $this->data;
		$this->openedSections[] = $this->openSection;
		
		}
	
	//***********************************************************************
	/**
	* apre una sezione del menu
	*
	* @param string $label etichetta (label/caption) della sezione
	* @param string $url href eventuale link
	* @param string $target e' il target HTML (_blank, _current, ecc.)
	* in cui aprire l'eventuale link; in realtà e' possibile scrivere qualsiasi 
	 * cosa, perche' e' il {@link view view-object} che si occupa di interpretare
	 * il parametro
	* @param stdClass $options oggetto che può contenere valori a piacere da passare alla view (badge, classe...)
	* @return boolean indica che la voce di menu e' selezionata, ossia che 
	 * la pagina corrente corrisponde alla voce di menu passata
	*/
	public function openSection($label, $url = '', $target = '', $options = null)
	    {
    	$id = $this->name .  "_" . $this->sectionCounter;

		$item = new waMenuDataItem();
	    $item->id = $this->name .  "_" . $this->sectionCounter;
	    $item->label = $label;
	    $item->url = $url;
	    $item->target = $target;
	    $item->options = $options;
	    $item->level = $this->currLevel;
	    $item->selected = $this->_isCurrent($url) ? 1 : 0;
		$item->items = array();

		$this->openSection->items[] = $item;
		$this->openSection = $this->openSection->items[count($this->openSection->items) - 1];
		$this->openedSections[] = $this->openSection;
		
    	$this->sectionCounter++;
	    $this->currLevel++;
	    
	    return $item->selected;
	    }
	    
	//*****************************************************************************
	/**
	*
	* chiude una sezione del menu precedentemente aperta con {@link openSection}
	* @return void
	*/
	public function closeSection()
	    {
	    array_pop($this->openedSections);
		$this->openSection = $this->openedSections[count($this->openedSections) - 1];
	    $this->currLevel--;
	    }

	//*****************************************************************************
	/**
	* aggiunge una voce di menu alla sezione correntemente aperta
	*
	* @param string $label etichetta (label/caption) della sezione
	* @param string $url href eventuale link
	* @param string $target e' il target HTML (_blank, _current, ecc.)
	* in cui aprire l'eventuale link; in realtà e' possibile scrivere qualsiasi 
	 * cosa, perche' e' il {@link view view-object} che si occupa di interpretare
	 * il parametro
	* @param stdClass $options oggetto che può contenere valori a piacere da passare alla view (badge, classe...)
	* @return boolean indica che la voce di menu e' selezionata, ossia che la pagina
	*			corrente corrisponde alla voce di menu
	*/
	public function addItem($label, $url = '', $target = '', $options = null)
	    {
		
		$item = new waMenuDataItem();
	    $item->label = $label;
	    $item->url = $url;
	    $item->target = $target;
	    $item->options = $options;
	    $item->level = $this->currLevel;
	    $item->selected = $this->_isCurrent($url) ? 1 : 0;
			
		$this->openSection->items[] = $item;
    	
	    if ($item->selected)
	    	{
			$this->openSection->selected = 1;
	    	}

	    return $item->selected;
	    
	    }
	    
    //***********************************************************************
	/**
	* chiude il menu
	*
	* @return void
	*/
	public function close()
		{
	 	
		}
		 
	//***************************************************************************
	/**
	* mostra
	*
	* deve essere invocato invocato al termine della costruzione
	* del menu per produrne l'output. Dopo l'invocazione di questo metodo non
	* ha piu' alcun senso operare sul menu lato server: il controllo passa
	* alla parte client che provvedera' con il comportamento previsto dal 
	 * {@link view view-object}.
	 * 
	* @param boolean $return se false, allora viene immediatamente effettuato
	* l'output del menu; altrimenti la funzione ritorna il buffer di output 
	* del menu stesso
	 * 
	* @return void|string
	*/
	public function show($return = false)
		{

		ob_start();
		
		// se non è stato passato definisce l'oggetto view di default
		$this->setView();
		$this->view->transform($this->data);
		
		if ($return)
			{
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
			}	
			
		ob_end_flush();
		}
		
	//***************************************************************************
	/**
	* ritorna l'oggetto contenente i dati da inviare al view-object
	 * 
	 * da usare in fase di debug per ispezionare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	* 
	* @return waMenuData
	*/
	public function getData()
		{
		return $this->data;
		}
		
	//***************************************************************************
	/**
	* da usare in fase di debug per mostrare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	 * 
	* @return void
	*/
	public function showJSON()
		{
		header("Content-Type: application/json; charset=utf-8");			
		echo json_encode($this->data, JSON_PRETTY_PRINT);
		}
		
    //***********************************************************************
	/**
	* verifica se la voce di menu corrente corrisponde alla pagina corrente
	*
	* @ignore
	* @access protected
	*/
	protected function _isCurrent($href)
		{
		list($mn_path, $mn_qs) = explode('?', $href);
		//list($srv_path, $srv_qs) = explode('?', $_SERVER['REQUEST_URI']);
		// if ($mn_path != $srv_path)
		if ($mn_path != $_SERVER['SCRIPT_NAME'])
			{
			return false;
			}

		$prms = explode("&", $mn_qs);
		foreach($prms as $prm)
			{
			list($n, $v) = explode("=", $prm);
			$mn_prms[$n] = $v;
			}

		//$prms = explode("&", $srv_qs);
		$prms = explode("&", $_SERVER['QUERY_STRING']);
		foreach($prms as $prm)
			{
			list($n, $v) = explode("=", $prm);
			$srv_prms[$n] = $v;
			}
		
		// eliminiamo gli elementi vuoti e eventuali parametri waTabella
		foreach($mn_prms as $n => $v)
			{
			if ($n == "watable_params" || $n == "")
				{
				unset($mn_prms[$n]);
				}
			}
		foreach($srv_prms as $n => $v)
			{
			if ($n == "watable_params" || $n == "")
				{
				unset($srv_prms[$n]);
				}
			}
		if (count($srv_prms) != count($mn_prms))
			{
			return false;
			}
		foreach($srv_prms as $n => $v)
			{
			if ($mn_prms[$n] != $v)
				{
				return false;
				}
			}
			
	    return true;
		}
	
	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function GetMyDir()
		{
		if (strpos(__FILE__, "\\") !== false)
			{
			// siamo sotto windows
			$thisFile = strtolower(str_replace("\\", "/", __FILE__));
			$dr = strtolower(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']));
			}
		else
			{
			$thisFile = __FILE__;
			$dr = $_SERVER['DOCUMENT_ROOT'];
			}
		
		if (substr($dr, -1) == "/")
			{
			$dr = substr($dr, 0, -1);
			}
		if ($dr != substr($thisFile, 0, strlen($dr)))
			{
			// quando la document root non e' in comune con la path del file corrente, 
			// allora significa che siamo in ambiente di sviluppo, e si includono
			// i file da un link simbolico; in questo caso la libreria deve essere 
			// posta immediatamente al di sotto della document root; se non si puo'
			// fare, occorre copiare la lib dove si ritiene opportuno
			$toret = "/" . basename(dirname(dirname($thisFile))) . "/" . basename(dirname($thisFile));
			}
		else
			{
			$toret = substr(dirname($thisFile), strlen($dr));
			}

		return $toret;		
		}
	
	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function setView()
		{
		if ($this->view)
			{
			return;
			}

		// non è stato definito un trasformer: andiamo con quello di default
		include_once  __DIR__ . "/uis/wamenu_default/view/wamenu.php";
		$this->view = new \waLibs\views\waMenu\waMenu_default\waMenuView();
		}

		
	//***********************************************************************
	}
	
