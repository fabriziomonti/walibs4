<?php
/**
* -
*
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include della struttura dati da passare al view object
* @ignore
*/
include_once __DIR__ . "/wamenu_data.class.php";

//***************************************************************************
//****  interfaccia i_waMenuView ********************************************
//***************************************************************************
/**
* i_waMenuView
*
* interfaccia che tutti i {@link waMenu::view view-object} utiizzati da 
 * waMenu devono rispettare
*
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
interface i_waMenuView
{
	/**
	 * trasforma i dati passati in input nella UI prevista dal view-object
	 * 
	 * @param waMenuData $data i dati da trasformare
	 */
     public function transform(waMenuData $data);
}