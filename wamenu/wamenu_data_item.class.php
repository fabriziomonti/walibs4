<?php
/**
* -
*
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waMenuDataItem ***********************************************
//***************************************************************************
/**
* waMenuDataItem
*
* struttura dati di un item di menu da passare al view-object
* 
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waMenuDataItem 
	{
	/**
	 * identificativo della voce 
	 * 
	 * @var string
	 */
	public $id 	= "";
	
	/**
	 * eventuali sotto-voci
	 * 
	 * @var array
	 */
	public $items 	= array();
	
	/**
	 * etichetta
	 * 
	 * @var string
	 */
	public $label 	= "";
	
	/**
	 * livello della voce 
	 * 
	 * @var int
	 */
	public $level 	= 0;
	
	/**
	 * flag di voce attiva
	 * 
	 * @var bool
	 */
	public $selected = 0;
	
	/**
	 * target (nuova finestra, ecc.)
	 * 
	 * @var string
	 */
	public $target 	= "";
	
	/**
	 * options (valori a piacere passati alla view)
	 * 
	 * @var \stdClass
	 */
	public $options = null;
	
	/**
	 * url della voce 
	 * 
	 * @var string
	 */
	public $url 	= "";
	}
	
