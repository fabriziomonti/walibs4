<?php
/**
* -
*
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include della struttura dati di un item
* @ignore
*/
include_once __DIR__ . "/wamenu_data_item.class.php";

//***************************************************************************
//****  classe waMenuData ***************************************************
//***************************************************************************
/**
* waMenuData
*
* struttura dati da passare al view-object
* 
* @package waMenu
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waMenuData 
	{
	/**
	 * nome del menu
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * path relativa alla document_root della libreria waMenu
	 * 
	 * @var string
	 */
	public $waMenuPath;
	
	/**
	 * titolo del menu
	 * 
	 * @var string
	 */
	public $title;
	
	/**
	 * items del menu
	 * 
	 * @var array
	 */
	public $items;
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language;
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale;
	
	
	}
	
