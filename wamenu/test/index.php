<?php
error_reporting(E_ERROR|E_WARNING);
include "../wamenu.class.php";

// main flow ******

// intestazione della pagina html...
$outputBuffer = creaTop();

// output del menu 1
$outputBuffer .= dammiMenu()->show(true);

// corpo della pagina
$outputBuffer .= dammiCorpoPagina();

// chiusura della pagina html
$outputBuffer .= creaFooter();

// risistema il codice html rendendolo "strict"
$outputBuffer = fammeloStrict($outputBuffer);

// output del buffer contenente la pagina 
exit($outputBuffer);

//*****************************************************************************
function creaTop()
	{
	// intestazione della pagina html...
	header("Content-Type: text/html; charset=UTF-8");
	$buffer = "<html>";

	$buffer .= "<head><title>test waMenu</title><meta http-equiv='Content-Type' content='text/html;charset=utf-8' /></head>\n";
	$buffer .= "<body><div class='container-fluid'>\n";
	
	return $buffer;
	}
	
//*****************************************************************************
function creaFooter()
	{
	
	// chiusura della pagina html
	$buffer .= "</div></body></html>";

	return $buffer;
	}
	
//*****************************************************************************
function fammeloStrict($buffer)
	{
	// spostiamo eventuali "<link ...>" nell'head per essere "strict"
	list ($pre_head, $resto) = explode("<head>", $buffer, 2);
	list ($head, $resto) = explode("</head>", $resto, 2);
	list ($tra_head_e_body, $body) = explode("<body", $resto, 2);
	while (true)
		{
		list ($prima, $dopo) = explode("<link ", $body, 2);
		if (!$dopo)
			break;
		list ($link, $dopo) = explode(">", $dopo, 2);
		$head .= "<link $link" . (substr(rtrim($link), -1) == "/" ? '' : " /") . ">\n";
		$body = "$prima$dopo";
		}

	$buffer = "$pre_head\n<head>\n$head</head>$tra_head_e_body\n<body$body";

	return $buffer;
	}

//*****************************************************************************
/**
 * 
 * @return waMenu
 */
function dammiMenu($view = "", $name = "")
	{

	// crea il menu
	$m = new waLibs\waMenu($view);
	$m->name = $name ? $name : $m->name;
	$m->open();

	$m->openSection("WebAppls");
		$m->addItem("Sito Istituzionale",  "http://www.webappls.com");
		$m->addItem("Area Clienti", "http://www.webappls.com/extranet.php");
		$m->addItem("Intranet", "http://www.webappls.com/intranet.php");
		$m->openSection("Sottomenu");
			$m->addItem("chi siamo",  "http://www.webappls.com/chi_siamo.php");
			$m->openSection("Sottomenu");
				$m->addItem("chi siamo",  "http://www.webappls.com/chi_siamo.php");
				$m->addItem("certificazioni",  "http://www.webappls.com/certificazioni.php");
				$m->addItem("tecnologie",  "http://www.webappls.com/tecnologie.php");
				$m->addItem("progetti",  "http://www.webappls.com/progetti.php");
				$m->openSection("Sottomenu");
					$m->addItem("chi siamo",  "http://www.webappls.com/chi_siamo.php");
					$m->addItem("certificazioni",  "http://www.webappls.com/certificazioni.php");
					$m->addItem("tecnologie",  "http://www.webappls.com/tecnologie.php");
					$m->addItem("progetti",  "http://www.webappls.com/progetti.php");
					$m->openSection("Sottomenu");
						$m->addItem("chi siamo",  "http://www.webappls.com/chi_siamo.php");
						$m->addItem("certificazioni",  "http://www.webappls.com/certificazioni.php");
						$m->addItem("tecnologie",  "http://www.webappls.com/tecnologie.php");
						$m->addItem("progetti",  "http://www.webappls.com/progetti.php");
						$m->openSection("Sottomenu");
							$m->addItem("chi siamo",  "http://www.webappls.com/chi_siamo.php");
							$m->addItem("certificazioni",  "http://www.webappls.com/certificazioni.php");
							$m->addItem("tecnologie",  "http://www.webappls.com/tecnologie.php");
							$m->addItem("progetti",  "http://www.webappls.com/progetti.php");
							$m->addItem("clienti",  "http://www.webappls.com/clienti.php");
							$m->addItem("partner",  "http://www.webappls.com/partner.php");
						$m->closeSection();
						$m->addItem("clienti",  "http://www.webappls.com/clienti.php");
						$m->addItem("partner",  "http://www.webappls.com/partner.php");
					$m->closeSection();
					$m->addItem("clienti",  "http://www.webappls.com/clienti.php");
					$m->addItem("partner",  "http://www.webappls.com/partner.php");
				$m->closeSection();
				$m->addItem("clienti",  "http://www.webappls.com/clienti.php");
				$m->addItem("partner",  "http://www.webappls.com/partner.php");
			$m->closeSection();
			$m->addItem("certificazioni",  "http://www.webappls.com/certificazioni.php");
			$m->addItem("tecnologie",  "http://www.webappls.com/tecnologie.php");
			$m->addItem("progetti",  "http://www.webappls.com/progetti.php");
			$m->addItem("clienti",  "http://www.webappls.com/clienti.php");
			$m->addItem("partner",  "http://www.webappls.com/partner.php");
		$m->closeSection();
	$m->closeSection();

	$m->openSection("walibs test");
		$m->addItem("waDB",  "/wadb/test/index.php");
		$m->addItem("waMenu",  "/wamenu/test/index.php");
		$m->addItem("waForm",  "/waform/test/index.php");
		$m->addItem("waTable",  "/watable/test/index.php");
		$m->addItem("waApplication",  "/waapplication/test/index.php");
	$m->closeSection();
	$m->openSection("Logout", "http://www.playboy.com/");
	$m->closeSection();
	$m->close();
	if ($_GET['json'])
		{
		$m->showJson();
		exit();
		}
		
	return $m;
	}

//*****************************************************************************
function dammiCorpoPagina()
	{

	return "

	<p style='margin-top: 50px'>
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	il mattino ha l'oro in bocca il mattino ha l'oro in bocca il mattino ha l'oro in bocca <br />
	</p>
	<p>
	(Jack <b>\"Wendy, sono a casa tesoro\"</b> Nicholson, Shining...)
	</p>
	";
	
	}