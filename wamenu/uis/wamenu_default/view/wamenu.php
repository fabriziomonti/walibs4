<?php
namespace waLibs\views\waMenu\waMenu_default;

//******************************************************************************
class waMenuView implements \waLibs\i_waMenuView
	{

	//**************************************************************************
	public function transform(\waLibs\waMenuData $data)
		{
		?>
		<!-- Bootstrap core CSS -->
		<link href='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet'>

		<!-- SmartMenus jQuery Bootstrap Addon CSS -->
		<link href='//cdnjs.cloudflare.com/ajax/libs/jquery.smartmenus/1.0.0/addons/bootstrap/jquery.smartmenus.bootstrap.min.css' rel='stylesheet'>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
		<script src='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js'></script>

		<!-- SmartMenus jQuery plugin -->
		<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery.smartmenus/1.0.0/jquery.smartmenus.min.js'></script>

		<!-- SmartMenus jQuery Bootstrap Addon -->
		<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery.smartmenus/1.0.0/addons/bootstrap/jquery.smartmenus.bootstrap.min.js'></script>		

		<!-- Navbar -->
		<div class='navbar navbar-default' role='navigation'>
			<div class='navbar-header'>
				<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
					<span class='sr-only'>Toggle navigation</span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
				</button>
			</div>
			<div class="navbar-collapse collapse">

				<!-- nav -->
				<ul class="nav navbar-nav">
					
				<?php 
				foreach ($data->items as $item)
					{
					$this->showItem($item);
					}
				?>

				</ul>
			</div><!--/.nav-collapse -->
		</div>

		<?php
		}

	//**************************************************************************
	private function showItem(\waLibs\waMenuDataItem $item)
		{

		$myurl = $item->url ? $item->url : "#";
		$target = $item->target ? "target='$item->target'" : "";
		$class = $item->selected ? "class='active'" : "";
		?>
		<li id='<?=$item->id ?>' <?=$class?> >
			<a href='<?=$myurl?>' <?=$target?> >
				<?=$item->label?>
				<?php if (count($item->items)) :?> 
					<span class="caret"></span>
				<?php endif;?>
			</a>
			<?php
			if (count($item->items))
				{
				?>
				<ul class='dropdown-menu'>
					
				<?php
				foreach ($item->items as $item)
					{
					$this->showItem($item);
					}
				?>

				</ul>
				<?php
				}
			
			?>
		</li>

		<?php
		}

//******************************************************************************
	}

//******************************************************************************


