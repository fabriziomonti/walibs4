<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* -
*
* funzione che restituisce l'oggetto waDBConnection a partire dai parametri
 * del file di configurazione passato come argomento
 * 
* @param mixed $configParams : string -> path del file di configurazione ; waDBPparams -> parametri in memoria
* @return waDBConnection
*/
function wadb_getConnection($configParams = '') {
	
	if (is_object($configParams) && get_class($configParams) == 'waLibs\waDBPparams') {
		$WADB_DBTYPE = $configParams->DBTYPE;
		$WADB_HOST = $configParams->HOST;
		$WADB_DBNAME = $configParams->DBNAME;
		$WADB_USERNAME = $configParams->USERNAME;
		$WADB_PASSWORD = $configParams->PASSWORD;
		$WADB_PORT = $configParams->PORT;
		$WADB_LOGNAME = $configParams->LOGNAME;
		$WADB_LOG_CALLBACK_FNC = $configParams->LOG_CALLBACK_FNC;

	}
	else {
		$configFileName = empty($configParams) ? __DIR__ . "/wadb.config.php" : $configParams;
		include $configFileName;
	}
	
	// inclusione del file contenente il driver specifico definito nel file
	// di configurazione
	include_once(__DIR__ . "/wadbdriver_$WADB_DBTYPE.class.php");
	$driver = __NAMESPACE__ . "\\waDBConnection_$WADB_DBTYPE";
	$dbconn = new $driver($WADB_DBTYPE, $WADB_HOST, $WADB_DBNAME, 
							$WADB_USERNAME, $WADB_PASSWORD, $WADB_PORT,
							$WADB_LOGNAME, $WADB_LOG_CALLBACK_FNC);
	$esito = $dbconn->connect();
	return $esito === false ? null : $dbconn;
	}

//*****************************************************************************







