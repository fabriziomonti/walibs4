<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

//***************************************************************************
//****  classe waRecordset **********************************************
//***************************************************************************
/**
* waRecordset
*
* Classe per la gestione di un insieme di righe di database (recordset).
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waRecordset
	{
	/**
	* -
	* Oggetto di classe {@link waDBConnection} che mantiene la connessione al db
	* @var waDBConnection
	*/
	public $dbConnection = null;

	/**
	* -
	* Array di oggetti di classe {@link waRecord} che compongono il recordset
	* @var array
	*/
	public $records = array();

	/**
	* -
	* Matrice ad ordinale numerico dei campi convertiti per l'utilizzo da PHP
	* la proprieta' e' pubblica, ma non documentata, perche' ha senso che
	* venga interrogata solo dalla classe waRecord
	* @var array
	* @ignore
	*/
	var $values = array();

	/**
	* -
	* Matrice ad ordinale numerico dei campi cosi' come ritornati dal db
	* la proprieta' e' pubblica, ma non documentata, perche' ha senso che
	* venga interrogata solo dalla classe waRecord
	* @var array
	* @ignore
	*/
	var $rawValues = array();

	/**
	* -
	* Matrice ad ordinale numerico dei campi convertiti per PHP, ma contenente
	* solo i valori originali tornati dal db
	* la proprieta' e' pubblica, ma non documentata, perche' ha senso che
	* venga interrogata solo dalla classe waRecord
	* @var array
	* @ignore
	*/
	var $originalValues = array();

	/**
	* -
	* Array associativo delle informazioni relative alle columns
	* la proprieta' e' pubblica, ma non documentata, perche' ha senso che
	* venga interrogata solo dalla classe waRecord
	* @var array
	* @ignore
	*/
	var $columns = array();

	/**
	* -
	* Array numerico delle informazioni relative alle columns
	* la proprieta' e' pubblica, ma non documentata, perche' ha senso che
	* venga interrogata solo dalla classe waRecord
	* @var array
	* @ignore
	*/
	var $ordColumns = array();

	/**
	* -
	* Array numerico contenente lo stato dei record presenti nel recordset
	* la proprieta' e' pubblica, ma non documentata, perche' ha senso che
	* venga interrogata solo dalla classe waRecord
	* @var array
	* @ignore
	*/
	var $recordsStatus = array();

	/**
	* -
	* Nr. di record che soddisfano la condizione di una query indipendetemente
	* dal limit impostato
	* @ignore
	* @var int
	*/
	var $noLimitRecordsNr = 0;

	//***************************************************************************
	/**
	* -
	*
	* Costruttore.
	* @param waDBConnection $dbConnection istanza della classe per la
	*  connessione al database
	*/
	public function __construct($dbConnection)
		{
		$this->dbConnection = $dbConnection;
		}

	//***************************************************************************
	/**
	* -
	*
	* Esegue una query SQL, costruisce le strutture dati relative alle colonne
	* e alle righe e restituisce un array di righe waRecord.
	*
	* @param string $sql query SQL da eseguire
	* @param int $recordsNr numero max di righe che la query deve restituire
	* @param int $skip numero di righe di cui effettuare lo skip (offset)
	* @return mixed (boolean | array) : se esito positivo ritorna l'array delle righe restituite
	* dalla query; se esito negativo torna false; per conoscere l'esatto esito
	* della query occorre invocare il metodo {@link errorNr}
	*/
	public function read($sql, $recordsNr = null, $skip = 0)
		{

		$esito = $this->dbConnection->extendedExecute($sql, $recordsNr, $skip);
		if ($esito === false)
			{
			return false;
			}

		list($this->ordColumns, $this->rawValues, $this->noLimitRecordsNr) = $esito;

		// carichiamo le informazioni delle columns
		for ($i = 0; $i < count($this->ordColumns); $i++) 
			{
			$this->columns[strtoupper($this->ordColumns[$i]['name'])] = &$this->ordColumns[$i];
			}

		// carichiamo i records
		for ($i = 0; $i < count($this->rawValues); $i++)
			{
			$this->values[] = $this->convertRecord($this->rawValues[$i]);
			$this->recordsStatus[] = waDB::RECORD_UNMODIFIED;
			$this->records[] = new waRecord($this, $i);
			}
		$this->originalValues = $this->values;

		return $this->records;
		}

	//***************************************************************************
	/**
	* -
	*
	* Ritorna l'ultimo codice di errore restituito dal database.
	* @return string
	*/
	public function errorNr()
		{
		return $this->dbConnection->errorNr();
		}

	//***************************************************************************
	/**
	* -
	*
	* Ritorna l'ultimo messaggio di errore restituito dal database.
	* @return string
	*/
	public function errorMessage()
		{
		return $this->dbConnection->errorMessage();
		}

	//***************************************************************************
	/**
	* -
	*
	* Ritorna il numero di campi (colonne) restituiti dalla query
	* @return int
	*/
	public function fieldNr()
		{
		return count($this->columns);
		}

	//***************************************************************************
	/**
	* -
	*
	* Ritorna il numero di righe che soddisfano la condizione impostata dalla
	* query, indipendentemente da una eventuale richiesta di limitazione delle
	* righe da restituire
	* @return int
	*/
	public function noLimitRecordsNr()
		{
		return $this->noLimitRecordsNr;
		}

	//***************************************************************************
	/**
	* -
	*
	* Dato il nome di un campo tra quelli ritornati dalla query, restituisce
	* l'ordinale della colonna
	* @param string $fieldName nome del campo (case-insensitive)
	* @return int
	*/
	public function fieldIndex($fieldName)
		{
		return $this->columns[strtoupper($fieldName)]['index'];
		}

	//***************************************************************************
	/**
	* -
	*
	* Dato l'indice di una colonna tra quelle ritornate dalla query, restituisce
	* il nome del campo
	* @param int $fieldIndex indice del campo
	* @return string
	*/
	public function fieldName($fieldIndex)
		{
		return $this->ordColumns[$fieldIndex]['name'];
		}


	//***************************************************************************
	/**
	* -
	*
	* Dato, indifferentemente, il nome o l'indice numerico di un campo tra quelli
	* ritornati dalla query, restituisce la lunghezza massima che il campo puo'
	* ospitare
	* @param mixed (string | int) $fieldNameOrIndex nome o indice del campo
	* @return int
	*/
	public function fieldMaxLength($fieldNameOrIndex)
		{
		$colInfo = $this->getColumnInfoFromNameOrIndex($fieldNameOrIndex);
		return $colInfo['maxLength'];
		}

	//***************************************************************************
	/**
	* -
	*
	* Dato, indifferentemente, il nome o l'indice numerico di un campo tra quelli
	* ritornati dalla query, restituisce il tipo del campo nativo sul DB
	* @param mixed (string | int) $fieldNameOrIndex nome o indice del campo
	* @return string
	*/
	public function dbFieldType($fieldNameOrIndex)
		{
		$colInfo = $this->getColumnInfoFromNameOrIndex($fieldNameOrIndex);
		return $colInfo['dbType'];
		}

	//***************************************************************************
	/**
	* -
	*
	* Dato, indifferentemente, il nome o l'indice numerico di un campo tra quelli
	* ritornati dalla query, restituisce il tipo del campo applicativo (si vedano
	* le const relative ai tipi campo applicativi contenute in
	* {@link waDB::STRING wadb.class.php}).
	* @param mixed (string | int) $fieldNameOrIndex nome o indice del campo
	* @return string
	*/
	public function fieldType($fieldNameOrIndex)
		{
		$colInfo = $this->getColumnInfoFromNameOrIndex($fieldNameOrIndex);
		return $colInfo['type'];
		}

	//***************************************************************************
	/**
	* -
	*
	* Ritorna il nome della tabella a cui appartiene un campo
	* 
	* @param mixed (string | int) $fieldNameOrIndex nome o indice del campo
	* @return string
	*/
	public function tableName($fieldNameOrIndex)
		{
		$colInfo = $this->getColumnInfoFromNameOrIndex($fieldNameOrIndex);
		return $colInfo['table'];
		}

	//***************************************************************************
	/**
	* -
	*
	* Ritorna il nome del campo che e' chiave primaria del record
	* attenzione: potrebbero darsi casi in cui esistono piu' chiavi
	* primarie (viste) o in cui non sia stata selezionata nessuna chiave
	* primaria; questo metodo ritorna il nome del primo campo che e'
	* chiave primaria, se esistente. Sta al programmatore calare correttamente
	* la chiamata a questo metodo nel contesto in cui viene chiamato
	* @return string
	*/
	public function primaryKey()
		{
		foreach($this->columns as $info)
			{
			if ($info['primaryKey'])
				return $info['name'];
			}
		}

	//***************************************************************************
	/**
	* -
	*
	* Inserisce una nuova riga vuota nel recordset e ritorna il relativo oggetto
	* di classe waRecord
	* @return waRecord
	*/
	public function add()
		{
		$this->rawValues[] =
			$this->values[] =
			$this->originalValues[] = array_fill(0, $this->fieldNr(), $niente);
		$this->recordsStatus[] = waDB::RECORD_NEW;
		return ($this->records[] = new waRecord($this, count($this->records)));
		}

	//***************************************************************************
	/**
	* -
	*
	* Esegue tutte le operazioni di inserimento, modifica, cancellazione
	* pendenti sull'intero insieme di oggetti di classe waRecord che compongono
	* l'array {@link records}.
	* @return boolean : true = ok; false = errore; per conoscere l'esatto esito
	* dell'operazione occorre invocare il metodo {@link errorNr}
	*/
	public function save($replaceOnInsert = false)
		{
		$retval = true;
		$righeDaAllineare = array();

		for ($i = 0; $i < count($this->records); $i++)
			{
			if ($this->recordsStatus[$i] == waDB::RECORD_MODIFIED)
				{
				$this->updateRecord($i);
				}
			elseif ($this->recordsStatus[$i] == waDB::RECORD_NEW)
				{
				$this->insertRecord($i, $replaceOnInsert);
				}
			elseif ($this->recordsStatus[$i] == waDB::RECORD_TO_DELETE) 
				{
				$this->deleteRecord($i);
				}
				
			if ($this->errorNr())
				{
				$retval = false;
				break;
				}
			else
				{
				$righeDaAllineare[] = $i;
				}
			}

		// dobbiamo allineare lo stato delle righe; possiamo farlo solo dopo
		// che tutte le operazioni sono state eseguite, altrimenti il client
		// perde l'allineamento dell'indice
		foreach ($righeDaAllineare as $recordIndex)
			{
			$this->updateRecordsStatus($recordIndex);
			}

		return $retval;
		}

	//***************************************************************************
	/**
	* -
	*
	* Dato, indifferentemente, il nome o l'indice di un campo tra quelli ritornati
	* dalla query, restituisce l'ordinale della colonna
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecord
	* @param mixed (string | int)  $fieldNameOrIndex nome o indice del campo (case-insensitive)
	* @return int
	* @ignore
	*/
	function getColumnOrdinalFromeNameOrIndex($fieldNameOrIndex)
		{
		return is_numeric($fieldNameOrIndex) ?
				$fieldNameOrIndex :
				$this->columns[strtoupper($fieldNameOrIndex)]['index'];
		}

	//***************************************************************************
	//*****  inizio metodi protected  *********************************************
	//***************************************************************************

	//***************************************************************************
	/**
	* -
	*
	* Una volta eseguite le operazioni pendenti sul recordset con esito positivo,
	* allinea gli stati interni di una riga
	* @param int $recordIndex indice della riga all'intrno del recordset
	* @return void
	* @access protected
	* @ignore
	*/
	protected function updateRecordsStatus($recordIndex)
		{
		switch ($this->recordsStatus[$recordIndex])
			{
			case waDB::RECORD_NEW:
			case waDB::RECORD_MODIFIED:
				$this->originalValues[$recordIndex] = $this->values[$recordIndex];
				// qui manca l'aggiornamento dei values crudi... come facciamo?
				$this->recordsStatus[$recordIndex] = waDB::RECORD_UNMODIFIED;
				break;
			case waDB::RECORD_TO_DELETE:
				array_splice($this->originalValues, $recordIndex, 1);
				array_splice($this->rawValues, $recordIndex, 1);
				array_splice($this->recordsStatus, $recordIndex, 1);
				array_splice($this->records, $recordIndex, 1);
				break;
			}
		}

	//***************************************************************************
	/**
	* -
	*
	* Data una riga, compone la stringa SQL per l'eliminazione del record
	* e la esegue
	* @param int $recordIndex indice della riga all'interno del recordset
	* @return boolean
	* @access protected
	* @ignore
	*/
	protected function deleteRecord($recordIndex)
		{
		// stabiliamo quali sono le tabelle da cui eliminare il record
		$tabelle = array();

		for ($i = 0; $i < $this->fieldNr(); $i++)
			$tabelle[$this->ordColumns[$i]['table']] = $this->ordColumns[$i]['tableAbsName'];

		// per ognuna delle tabelle da cui eliminare il record, creiamo
		// una query sql di DELETE
		foreach ($tabelle as $table => $tableAbsName)
			{
			if ($condizione = $this->getPrimaryKeyClause($table, $recordIndex))
				{
				$sql = "DELETE FROM $tableAbsName  WHERE $condizione";
				if ($this->dbConnection->execute($sql) === false)
					return false;
				}
			}

		return true;
		}

	//***************************************************************************
	/**
	* -
	*
	* Data una riga, compone la stringa SQL per l'inserimento effettivo del record
	* nel db e la esegue
	* @param int $recordIndex indice della riga all'interno del recordset
	* @return boolean
	* @access protected
	* @ignore
	*/
	protected function insertRecord($recordIndex, $replaceOnInsert = false)
		{
		// stabiliamo quali sono le tabelle da aggiornare
		$tabelle = array();
		$riga = & $this->values[$recordIndex];
		for ($i = 0; $i < $this->fieldNr(); $i++)
			{
			if (is_array($riga[$i]))
				{
				for ($j = 0; $j < count($riga[$i]); $j++)
					{
					if ($riga[$i][$j] !== $niente)
						$tabelle[$this->ordColumns[$i]['table']][$this->ordColumns[$i]['name']][$j] = $riga[$i][$j];
					}
				}
			else
				{
				if ($riga[$i] !== $niente)
					$tabelle[$this->ordColumns[$i]['table']][$this->ordColumns[$i]['name']] = $riga[$i];
				}
			}

		// per ognuna delle tabelle che riportano un campo modificato, creiamo
		// una query sql di INSERT
		foreach ($tabelle as $table => $campi)
			{
			// prefissiamo il nome della tabella con il nome del db, nel caso in
			// cui il db sia esterno rispetto alla connessione
			$tableAbsName = $this->columns[strtoupper(key($campi))]['tableAbsName'];
			$sql = ($replaceOnInsert ? "REPLACE" : "INSERT") . " INTO $tableAbsName (";
			$virgola = "";
			foreach ($campi as $nome => $valore)
				{
				$sql .= "$virgola$nome";
				$virgola = ", ";
				}
			$sql .= ") VALUES (";
			$virgola = "";
			foreach ($campi as $nome => $valore)
				{
				$sql .= $virgola . $this->dbConnection->sqlValue($valore, $this->columns[strtoupper($nome)]['dbType']);
				$virgola = ", ";
				}
			$sql .= ")";
			if ($this->dbConnection->execute($sql) === false)
				{
				return false;
				}

			// ci facciamo restituire l'ultimo id eventualmente generato da una
			// chiave primaria autoincrementale, e la andiamo ad inserire
			// nel valore corrispondente, in modo da avere il piu' possibile allineato
			// la riga del recordset con quanto c'e' sul db
			foreach ($this->columns as $nome => $infos)
				{
				if ($infos['table'] == $table &&
						$infos['primaryKey'] &&
						empty($riga[$infos['index']]))
				// questa colonna e' evidentemente una chiave primaria
				// autoincrementale della tabella in esame, altrimenti
				// non avremmo potuto creare il record senza specificarla...
					{
					$riga[$infos['index']] = $this->dbConnection->lastInsertedId();
					}
				}

			}

		return true;
		}

	//***************************************************************************
	/**
	* -
	*
	* Data una riga, compone la stringa SQL per l'update del record
	* e la esegue
	* @param int $recordIndex indice della riga all'interno del recordset
	* @return boolean
	* @access protected
	* @ignore
	*/
	protected function updateRecord($recordIndex)
		{
		// stabiliamo quali sono le tabelle da aggiornare
		$tabelle = array();
		$riga = & $this->values[$recordIndex];
		$originale = & $this->originalValues[$recordIndex];

		for ($i = 0; $i < $this->fieldNr(); $i++)
			{
			if ($riga[$i] !== $originale[$i])
				{
				$tabelle[$this->ordColumns[$i]['table']][$this->ordColumns[$i]['name']] = $riga[$i];
				}
			}

		// per ognuna delle tabelle che riportano un campo modificato, creiamo
		// una query sql di UPDATE
		foreach ($tabelle as $table => $campi)
			{
			// prefissiamo il nome della tabella con il nome del db, nel caso in
			// cui il db sia esterno rispetto alla connessione
			$tableAbsName = $this->columns[strtoupper(key($campi))]['tableAbsName'];
			$sql = "UPDATE $tableAbsName SET ";
			$virgola = "";
			foreach ($campi as $nome => $valore)
				{
				$sql .= "$virgola$nome=" . $this->dbConnection->sqlValue($valore, $this->columns[strtoupper($nome)]['dbType']);
				$virgola = ", ";
				}
			$pkClause =  $this->getPrimaryKeyClause($table, $recordIndex);
			if (!$pkClause)
				{
				// se non abbiamo trovato la chiave primaria, non abbiamo
				// modo di procedere alla modifica
				continue;
				}
			$sql .= " WHERE $pkClause";
			if ($this->dbConnection->execute($sql) === false)
				{
				return false;
				}
			}

		return true;
		}

	//***************************************************************************
	/**
	* -
	*
	* Data una riga, compone la clausola SQL di WHERE per la ricerca di un record
	* @param string $table tabella per cui creare la condizione di where
	* @param int $recordIndex indice della riga all'interno del recordset
	* @return string
	* @access protected
	* @ignore
	*/
	protected function getPrimaryKeyClause($table, $recordIndex)
		{
		// costruisce la condizione di where di una update
		$sql = '';

		$and= "";
		foreach($this->columns as $info)
			{
			if ($info['table'] == $table && $info['primaryKey'])
				{
				$sql .= "$and$info[name]=" .
						$this->dbConnection->sqlValue($this->originalValues[$recordIndex][$info['index']], $info['dbType']);
				$and = " AND ";
				}
			}

		return $sql;
		}


	//***************************************************************************
	/**
	* -
	*
	* Dato, indifferentemente, il nome o l'indice di un campo tra quelli ritornati
	* dalla query, restituisce le informazioni relative alla colonna
	* @param mixed (string | int) $fieldNameOrIndex nome del campo o indice della colonna
	* @return array
	* @access protected
	* @ignore
	*/
	protected function getColumnInfoFromNameOrIndex($fieldNameOrIndex)
		{
		return is_numeric($fieldNameOrIndex) ?
				$this->ordColumns[$fieldNameOrIndex] :
				$this->columns[strtoupper($fieldNameOrIndex)];
		}

	//***************************************************************************
	/**
	* -
	*
	* Data una riga cosi' come arriva dal db, la converte nel formato per
	* essere utilizzata da PHP
	* @param array $riga
	* @return array
	* @access protected
	* @ignore
	*/
	protected function convertRecord($riga)
		{
		$retval = array();
		for ($i = 0; $i < count($riga); $i++)
			{
			$retval[] = $this->dbConnection->fieldConvert($riga[$i], $this->ordColumns[$i]['dbType']);
			}
		return $retval;
		}


	}

//*****************************************************************************





