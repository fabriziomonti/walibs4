<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* @ignore
*/
require_once(__DIR__. '/wadbdriver.class.php');

//***************************************************************************
//****  classe waDBConnection_mysqli ***************************************
//***************************************************************************
/**
* waDBConnection_mysqli
* 
* classe per la connessione fisica ad un database mysql-improved
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* @ignore
*/
class waDBConnection_mysqli extends waDBConnection 
	{
	
	/**
	* @access protected
	* @ignore
	*/ 
	var $DBConn = null;

	//***************************************************************************
	/**
	* -
	* 
	* connette il database.
	* @return boolean per conoscere l'esatto esito del metodo occorre invocare 
	* il metodo {@link errorNr}
	* @ignore
	*/ 
	function connect()
		{
		$this->DBConn = @mysqli_connect($this->WADB_HOST, $this->WADB_USERNAME, $this->WADB_PASSWORD, $this->WADB_DBNAME, $this->WADB_PORT ? $this->WADB_PORT : null);
		if (!$this->DBConn) 
			return false;
		if (! @mysqli_autocommit($this->DBConn, true)) 
			return false;
		mysqli_set_charset($this->DBConn, "utf8");
		return true;
		}
		
	//***************************************************************************
	/**
	* -
	* 
	* disconnette il database (abortisce eventuali transazioni non committate).
	* @return void
	* @ignore
	*/ 
	function disconnect()
		{
		if (!$this->DBConn) 
			return;
		@mysqli_close($this->DBConn);
		$this->DBConn = null;
		}
		
		
	//***************************************************************************
	/**
	* -
	* 
	* ritorna l'ultimo codice di errore restituito dal database.
	* @return string
	* @ignore
	*/ 
	function errorNr()
		{
		if (!$this->DBConn)
			return @mysqli_errno();
		return @mysqli_errno($this->DBConn);
		}
		
	//***************************************************************************
	/**
	* -
	* 
	* ritorna l'ultimo messaggio di errore restituito dal database.
	* @return string
	* @ignore
	*/ 
	function errorMessage()
		{
		if (!$this->DBConn)
			return @mysqli_error();
		return @mysqli_error($this->DBConn);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Esegue un comando SQL sul database connesso.
	* @param string $sql SQL da eseguire
	* @return mixed i dati grezzi ottenuti dalla query o FALSE in caso di errore;
	* per conoscere l'esatto esito del metodo occorre invocare 
	* il metodo {@link errorNr}
	* @ignore
	*/
	function execute($sql)
		{
		$this->logQuery($sql);
		$qid = @mysqli_query($this->DBConn, $sql);
		if (is_bool($qid))
			return $qid;
		$retval = array();
		while ($riga = @mysqli_fetch_assoc($qid))
			$retval[] = $riga;
		@mysqli_free_result($qid);	
		return $retval;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Inizia una transazione.
	* @return void
	* @ignore
	*/ 
	function beginTransaction()
		{
		@mysqli_autocommit($this->DBConn, false);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Conferma una transazione aperta in precedenza con {@link beginTransaction}.
	* @return void
	* @ignore
	*/ 
	function commitTransaction()
		{
		@mysqli_commit($this->DBConn);
		@mysqli_autocommit($this->DBConn, true);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Annulla una transazione aperta in precedenza con {@link beginTransaction}.
	* @return void
	* @ignore
	*/ 
	function rollbackTransaction()
		{
		@mysqli_rollback($this->DBConn);
		@mysqli_autocommit($this->DBConn, true);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* ritorna l'ultimo identifica univoco inserito nel database a fronte di INSERT
	* (posto che la tabella sia dotata di una chiave primaria autoincrementale).
	* @return integer
	* @ignore
	*/ 
	function lastInsertedId()
		{
		return mysqli_insert_id($this->DBConn);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database.
	* @param integer $data data in formato "since-the-epoch"
	* @return string la data in formato SQL
	* @ignore
	*/ 
	function sqlDate($data)
		{
		if (is_null($data) || $data === false || $data === '')
			return $this->sqlNull();
		$retval = date("'Y-m-d'", $data);
		if ($retval === false)
			return $this->sqlNull();
		return $retval;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	* dal database.
	* @param integer $dataOra data/ora in formato "since-the-epoch"
	* @return string la data/ora in formato SQL
	* @ignore
	*/ 
	function sqlDateTime($dataOra)
		{
		if (is_null($dataOra) || $dataOra === false || $dataOra === '')
			return $this->sqlNull();
		$retval = date("'Y-m-d H.i.s'", $dataOra);
		if ($retval === false)
			return $this->sqlNull();
		return $retval;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	* formato SQL TIME richiesto dal database.
	* @param integer $ora ora in formato "since-the-epoch"
	* @return string l'ora in formato SQL
	* @ignore
	*/ 
	function sqlTime($ora)
		{
		if (is_null($ora))
			return $this->sqlNull();
		return "'1980-01-01 " . date("H.i.s'", $ora);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una stringa nel formato SQL richiesto dal database.
	* @param string $stringa  stringa da convertire
	* @return string  stringa convertita
	* @ignore
	*/ 
	function sqlString($stringa)
		{
		if (is_null($stringa))
			return $this->sqlNull();
		return "'" . mysqli_real_escape_string($this->DBConn, $stringa) . "'";
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma un intero nel formato SQL richiesto dal database.
	* @param integer $intero  intero da convertire
	* @return string  stringa convertita
	* @ignore
	*/ 
	function sqlInteger($intero)
		{
		if (is_null($intero) || $intero === '')
			return $this->sqlNull();
		return ((int) $intero) . "";
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma un numero decimale nel formato SQL richiesto dal database.
	* @param float $decimale  idecimale da convertire
	* @return string  stringa convertita
	* @ignore
	*/ 
	function sqlDecimal($decimale)
		{
		if (is_null($decimale) || $decimale === '')
			return $this->sqlNull();
		return ((float) $decimale) . "";
		}
		
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce il valore NULL come richiesto dal db.
	* @return string  
	* @ignore
	*/ 
	function sqlNull()
		{
		return 'NULL';
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $date data in formato "since-the-epoch"
	* @return string la data in formato SQL da utilizzare come stringa
	*/ 
	public function sqlDateAsString($date)
		{
                return substr(substr($this->sqlDate($date), 1), 0, -1);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	* dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $dateTime data/ora in formato "since-the-epoch"
	* @return string la data/ora in formato SQL da utilizzare come stringa
	*/ 
	public function sqlDateTimeAsString($dateTime)
		{
                return substr(substr($this->sqlDateTime($dateTime), 1), 0, -1);
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	* formato SQL TIME richiesto dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $time ora in formato "since-the-epoch"
	* @return string l'ora in formato SQL da utilizzare come stringa
	*/ 
	public function sqlTimeAsString($time)
		{
                return substr(substr($this->sqlTime($time), 1), 0, -1);
		}
		
	//***************************************************************************
	//******* inizio metodi semi-protected ****************************************
	//***************************************************************************
	
	//***************************************************************************
	/**
	* -
	*
	* prende un campo come arriva da PHP e lo converte nel formato SQL
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param mixed $dato dato in formato PHP  da convertire
	* @param string $tipoDB tipo del campo sul database
	* @return string valore da inserire nella query SQL
	* @ignore
	*/ 
	function sqlValue($valore, $tipoDB)
		{
			
		switch ($tipoDB)
			{
			case MYSQLI_TYPE_DATE: 
			case MYSQLI_TYPE_NEWDATE: 
				return $this->sqlDate($valore);
			
			case MYSQLI_TYPE_TIME:
				return $this->sqlTime($valore);
			
			case MYSQLI_TYPE_DATETIME:
			case MYSQLI_TYPE_TIMESTAMP: 
				return $this->sqlDateTime($valore);
			
			case MYSQLI_TYPE_TINY:
			case MYSQLI_TYPE_SHORT: 
			case MYSQLI_TYPE_INT24:
			case MYSQLI_TYPE_LONG: 
			case MYSQLI_TYPE_LONGLONG:
				return $this->sqlInteger($valore);
			
			case MYSQLI_TYPE_FLOAT: 
			case MYSQLI_TYPE_DOUBLE:
			case MYSQLI_TYPE_DECIMAL:
			case MYSQLI_TYPE_NEWDECIMAL:
				return $this->sqlDecimal($valore);
			
			default:
				return $this->sqlString($valore);
			}
		}
		
	//***************************************************************************
	/**
	* -
	*
	* prende un campo come arriva da db e lo converte nel formato usabile da PHP
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param mixed $dato dato in formato DB  da convertire
	* @param string $tipoDB tipo del campo sul database
	* @return mixed  valore PHP
	* @ignore
	*/ 
	function fieldConvert($campo, $tipoDB)
		{
		if (is_null($campo))
			return $campo;
		switch ($tipoDB)
			{
			case MYSQLI_TYPE_DATE: 
			case MYSQLI_TYPE_NEWDATE: 
				if (empty($campo))
					return null;
				if ($campo == "0000-00-00")
					return 0;
				list($anno, $mese, $giorno) = explode("-", $campo);
				return mktime(0,0,0, $mese * 1, $giorno * 1, $anno * 1);
			
			case MYSQLI_TYPE_TIME:
				list($ora, $min, $sec) = explode(":", $campo);
				return mktime($ora * 1, $min * 1, $sec * 1, 1, 1, 1980);
			
			case MYSQLI_TYPE_DATETIME:
			case MYSQLI_TYPE_TIMESTAMP: 
				if (empty($campo))
					return null;
				if ($campo == "0000-00-00 00:00:00")
					return 0;
				list($data, $ore) = explode(" ", $campo);
				list($anno, $mese, $giorno) = explode("-", $data);
				list($ora, $min, $sec) = explode(":", $ore);
				return mktime($ora * 1, $min * 1, $sec * 1, $mese * 1, $giorno * 1, $anno * 1);
			
			case MYSQLI_TYPE_TINY:
			case MYSQLI_TYPE_SHORT: 
			case MYSQLI_TYPE_INT24:
			case MYSQLI_TYPE_LONG: 
			case MYSQLI_TYPE_LONGLONG:
			case MYSQLI_TYPE_FLOAT: 
			case MYSQLI_TYPE_DOUBLE:
			case MYSQLI_TYPE_DECIMAL:
			case MYSQLI_TYPE_NEWDECIMAL:
				return ($campo * 1);
			
			default:
				return $campo;
			}
		}
	
	//***************************************************************************
	/**
	* - 
	*
	* in pratica e' un duplicato di @{link esegui}, salvo che viene utilizzata
	* dal recordset per passare e ricevere piu' informazioni
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param string $sql query sql
	* @param int $nrRighe numero max di records che la query deve restituire
	* @param int $rigaIniziale numero di records di cui effettuare lo skip (offset)
	* @return array : 
	* - nel primo elemento array info colonne; 
	* - nel secondo elemento array dei values cosi' come ritornati dal db (crudi)
	* - nel terzo elemento il nr. di records che soddisfano le condizioni, 
	*   indipendentemente dal limit imposto
	* @ignore
	*/ 
	function extendedExecute($sql, $nrRighe = null, $rigaIniziale = 0)
		{
//echo "$sql<hr>";			
		if ($nrRighe !== null)
			{
			// modifichiamo la query, che ovviamente DEVE essere una select,
			// affinche' mysql possa tornare il nr di records
			// che soddisfano la condizione, indipendentemente dal limit imposto
			$patternSelect="/(select )/i";
//			list($select, $params) = preg_split($patternSelect,trim($sql), 2, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			$elems = preg_split($patternSelect,trim($sql), 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			$select = array_shift($elems);
			$params = implode("", $elems);
			$sql = "$select SQL_CALC_FOUND_ROWS $params LIMIT $rigaIniziale, $nrRighe";
			}
		$qid = @mysqli_query($this->DBConn, $sql);
		if ($qid === false)
			return false;
			
		// carichiamo le informazioni delle columns
		$colInfos = array();
		$nrColonne = mysqli_num_fields($qid);	
		for ($i = 0; $i < $nrColonne; $i++)
			$colInfos[] = $this->setColumnAttribs($qid, $i);
			
		// carichiamo i records
		while ($rigaCruda = @mysqli_fetch_array($qid, MYSQLI_NUM))
			$righeCrude[] = $rigaCruda;
			
		@mysqli_free_result($qid);	
		
		if ($nrRighe !== null)
			{
			// se e' stato richiesto il limit, allora andiamo anche a prelevare
			// il nr di records che soddisfano la condizione
			$qid = mysqli_query($this->DBConn, "SELECT FOUND_ROWS()");
			list($nrRigheNoLimit) = mysqli_fetch_row($qid);
			@mysqli_free_result($qid);	
			}
		else
			$nrRigheNoLimit = count($righeCrude);
		
		return array($colInfos, $righeCrude, $nrRigheNoLimit);
		
		}
		
	//***************************************************************************
	//******* inizio metodi protected *********************************************
	//***************************************************************************
	
	//***************************************************************************
	/**
	* -
	*
	* restituisce il meta-tipo (ossia il tipo applicativo) di un campo del db
	* @param string $tipoDB tipo del campo sul database
	* @return string  meta-tipo
	* @access protected
	* @ignore
	*/ 
	protected function getApplicationFieldType($tipoDB)
		{
		switch ($tipoDB)
			{
			case MYSQLI_TYPE_STRING: 
			case MYSQLI_TYPE_CHAR:
			case MYSQLI_TYPE_VAR_STRING: 
			case MYSQLI_TYPE_ENUM: 
			case MYSQLI_TYPE_SET: 
				return waDB::STRING;
				
			case MYSQLI_TYPE_TINY_BLOB: 
			case MYSQLI_TYPE_BLOB:
			case MYSQLI_TYPE_MEDIUM_BLOB:
			case MYSQLI_TYPE_LONG_BLOB: 
				return waDB::CONTAINER;
				
			case MYSQLI_TYPE_DATE: 
			case MYSQLI_TYPE_NEWDATE: 
				return waDB::DATE;
			
			case MYSQLI_TYPE_TIME:
				return waDB::TIME;
			
			case MYSQLI_TYPE_DATETIME:
			case MYSQLI_TYPE_TIMESTAMP: 
				return waDB::DATETIME;
			
			case MYSQLI_TYPE_TINY:
			case MYSQLI_TYPE_SHORT: 
			case MYSQLI_TYPE_INT24:
			case MYSQLI_TYPE_LONG: 
			case MYSQLI_TYPE_LONGLONG:
				return waDB::INTEGER;
				
			case MYSQLI_TYPE_FLOAT: 
			case MYSQLI_TYPE_DOUBLE:
			case MYSQLI_TYPE_DECIMAL:
			case MYSQLI_TYPE_NEWDECIMAL:
				return waDB::DECIMAL;
				
			default: 
				return WADB_TIPO_SCONOSCIUTO;
			}
			
		}
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce le info di una colonna
	* @return array info colonna
	* @access protected
	* @ignore
	*/ 
	protected function setColumnAttribs($queryId, $colIdx)
		{
		$colInfo = mysqli_fetch_field_direct($queryId, $colIdx);
		$col['tableAbsName'] = "$colInfo->db.$colInfo->table";
		$col['dbName'] = $colInfo->db;
		$col['table'] = $colInfo->table;
		$col['name'] = $colInfo->name;
		$col['index'] = $colIdx;
		$col['dbType'] = strtoupper($colInfo->type);
		$col['primaryKey'] = $colInfo->flags & 2 ? 1 : 0;
		$col['maxLength'] = $colInfo->length;
		$col['type'] = $this->getApplicationFieldType($col['dbType']);

		// problema: se il campo e' di tipo longtext e lavoriamo in UTF8 c'e' 
		// evidentemente un problema di overflow da qualche parte (probabilmente 
		// in PHP, che non riesce a far stare in un intero 2^32*3) e viene 
		// ritornato -1 nella lunghezza massima del campo; in questo 
		// caso interveniamo a mano 
		if ($col['type'] == waDB::CONTAINER && $col['maxLength'] == -1)
			$col['maxLength'] = pow(2, 32) - 1;
		// poiche' lavoriamo in utf8, per i campi stringa ci viene sempre
		// ritornata la lunghezza in bytes, non in caratteri; quindi e' da
		// dividere per 3
		elseif ($col['type'] == waDB::STRING || $col['type'] == waDB::CONTAINER)
			$col['maxLength'] /= 3;
		return $col;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* distruttore
	* @ignore
	*/ 
	function __destruct()
		{
		$this->disconnect();
		}

//***************************************************************************
//****  fine classe waDBConnection_mysqli **********************************
//***************************************************************************
	}	// fine classe 


