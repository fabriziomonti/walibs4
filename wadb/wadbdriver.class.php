<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

//***************************************************************************
/**
* classe "astratta" (non in senso strettamente OO) per la connessione fisica ad un database 
 * 
 * Questa classe non agisce fisicamente su un db, quindi è perfettamente inutile 
 * istanziarne un oggetto; fornisce unicamente l'interfaccia programmatica
 * verso i driver dei rispettivi database e alcuni metodi comuni che, in quanto
 * non legati a uno specifico db-engine, possono essre richiamati anche 
 * staticamente.
 * <br><br>
 * Per istanziare una vera connessione alla base dati è necessario utilizzare
 * la funzione procedurale {@link wadb_getConnection}, la quale restituirà
 * un oggetto che implementerà l'interfaccia documentata da questa classe.
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waDBConnection
	{
	/**
	* 
	* Tipo database; si vedano le const {@link waDB::DBTYPE_MYSQL waDB::DBTYPE_*} in {@link wadb.class.php}
	* @access protected
	*/
	protected $WADB_DBTYPE = '';
	
	/**
	* 
	* Nome o indirizzo IP host di residenza del db
	* @access protected
	*/
	protected $WADB_HOST = '';
	
	/**
	* 
	* Nome utente per l'accesso al db
	* @access protected
	*/
	protected $WADB_USERNAME = '';
	
	/**
	* 
	* Password utente per l'accesso al db
	* @access protected
	*/
	protected $WADB_PASSWORD = '';
	
	/**
	* 
	* Nome del db
	* @access protected
	*/
	protected $WADB_DBNAME = '';
	
	/**
	* 
	* Porta sui cui viene condiviso il db
	* @access protected
	*/
	protected $WADB_PORT = '';
	
	/**
	* 
	* Nome di un file sequenziale dove vengono loggati tutti gli acessi in scrittura al db 
	* (anonimi, salvo l'ip di provenienza)
	* @access protected
	*/
	protected $WADB_LOGNAME = '';
	
	/**
	* 
	* Nome di una funzione callback invocata ad ogni accesso al db in scrittura.
	* Alla funzione, se esistente, viene passato come parametro la stringa sql in esecuzione. E' cosi'
	* possibile per una applicazione definire un proprio logging, che riporti eventuali dati dell'utente
	* che ha invocato la scrittura su db. La variabile puo' anche contenere un metodo: in questo caso sara'
	* un array di tre elementi:
	* o nome della classe, evidentemente singleton, che contiene il metodo
	* o nome di una proprieta' statica della classe che restituisce un' istanza della classe
	* o nome del metodo da invocare
	* @access protected
	*/
	protected $WADB_LOG_CALLBACK_FNC = '';
	
	/**
	 *
	 * @ignore
	 */
	protected $clauses = array("select", "from", "where", "group", "having", "order");

	//***************************************************************************
	/**
	* -
	* 
	* costruttore.
	 * 
	 * @param integer $WADB_DBTYPE vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_HOST vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_DBNAME vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_USERNAME vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_PASSWORD vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_DBNAME vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_PORT vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string $WADB_LOGNAME vedi parametri file di configurazione {@link wadb.config.php}
	 * @param string|array $WADB_LOG_CALLBACK_FNC vedi parametri file di configurazione {@link wadb.config.php}
	*/ 
	public function __construct($WADB_DBTYPE, $WADB_HOST, $WADB_DBNAME, 
							$WADB_USERNAME, $WADB_PASSWORD, $WADB_PORT,
							$WADB_LOGNAME, $WADB_LOG_CALLBACK_FNC)
		{
		$this->WADB_DBTYPE = $WADB_DBTYPE;
		$this->WADB_HOST = $WADB_HOST;
		$this->WADB_USERNAME = $WADB_USERNAME;
		$this->WADB_PASSWORD = $WADB_PASSWORD;
		$this->WADB_DBNAME = $WADB_DBNAME;
		$this->WADB_PORT = $WADB_PORT;
		$this->WADB_LOGNAME = $WADB_LOGNAME;
		$this->WADB_LOG_CALLBACK_FNC = $WADB_LOG_CALLBACK_FNC;
		}
		
	//***************************************************************************
	/**
	* Connette il database.
	 * 
	* @return boolean per conoscere l'esatto esito del metodo occorre invocare
	* il metodo {@link errorNr}
	*/ 
	public function connect()
		{
		}
		
	//***************************************************************************
	/**
	* -
	* 
	* disconnette il database (abortisce eventuali transazioni non committate).
	* @return void
	*/ 
	public function disconnect()
		{
		}
		
		
	//***************************************************************************
	/**
	* -
	* 
	* ritorna l'ultimo codice di errore restituito dal database.
	* @return string
	*/ 
	public function errorNr()
		{
		}
		
	//***************************************************************************
	/**
	* -
	* 
	* ritorna l'ultimo messaggio di errore restituito dal database.
	* @return string
	*/ 
	public function errorMessage()
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Esegue un comando SQL sul database connesso.
	* 
	* @param string $sql SQL da eseguire
	* @return mixed i dati grezzi ottenuti dalla query o FALSE in caso di errore;
	* per conoscere l'esatto esito del metodo occorre invocare 
	* il metodo {@link errorNr}
	*/
	public function execute($sql)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Inizia una transazione.
	* @return void
	*/ 
	public function beginTransaction()
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Conferma una transazione aperta in precedenza con {@link beginTransaction}.
	* @return void
	*/ 
	public function commitTransaction()
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Annulla una transazione aperta in precedenza con {@link beginTransaction}.
	* @return void
	*/ 
	public function rollbackTransaction()
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* ritorna l'ultimo identificativo univoco inserito nel database a fronte di INSERT
	* (posto che la tabella sia dotata di una chiave primaria autoincrementale).
	* @return integer
	*/ 
	public function lastInsertedId()
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database.
	* @param integer $date data in formato "since-the-epoch"
	* @return string la data in formato SQL
	*/ 
	public function sqlDate($date)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	* dal database.
	* @param integer $dateTime data/ora in formato "since-the-epoch"
	* @return string la data/ora in formato SQL
	*/ 
	public function sqlDateTime($dateTime)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	* formato SQL TIME richiesto dal database.
	* @param integer $time ora in formato "since-the-epoch"
	* @return string l'ora in formato SQL
	*/ 
	public function sqlTime($time)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una stringa nel formato SQL richiesto dal database.
	* @param string $string  stringa da convertire
	* @return string  stringa convertita
	*/ 
	public function sqlString($string)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma un intero nel formato SQL richiesto dal database.
	* @param integer $intero  intero da convertire
	* @return string  stringa convertita
	*/ 
	public function sqlInteger($intero)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma un numero decimale nel formato SQL richiesto dal database.
	* @param float $decimale  idecimale da convertire
	* @return string  stringa convertita
	*/ 
	public function sqlDecimal($decimale)
		{
		}
		
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce il valore NULL come richiesto dal db.
	* @return string  
	*/ 
	public function sqlNull()
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $date data in formato "since-the-epoch"
	* @return string la data in formato SQL da utilizzare come stringa
	*/ 
	public function sqlDateAsString($date)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	* dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $dateTime data/ora in formato "since-the-epoch"
	* @return string la data/ora in formato SQL da utilizzare come stringa
	*/ 
	public function sqlDateTimeAsString($dateTime)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	* formato SQL TIME richiesto dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $time ora in formato "since-the-epoch"
	* @return string l'ora in formato SQL da utilizzare come stringa
	*/ 
	public function sqlTimeAsString($time)
		{
		}
		
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* splitSql 
	 * 
	 * data una query sql ritorna un oggetto le cui proprietà corrispondono
	 * ai singoli componenti della query (select, from, where, ...)
	 * 
	 * @param string $sql  la stringa sql da parsare
	 * @return stdClass
	*
	*/
	public function splitSql($sql)
		{
		if (preg_match_all('/(^|\s|\()select\s/i', $sql) == 1)
			{
			return $this->oldSplitSql($sql);
			}

		return $this->newSplitSql($sql);
		}
	
	//***************************************************************************
	//******* inizio metodi semi-protected **************************************
	// (metodi che vengono richiamati da altre classi del package, ma che non
	//  ha molto senso che vengano richiamati dall'esterno a queste)
	//***************************************************************************
	
	//***************************************************************************
	/**
	* -
	*
	* prende un campo come arriva da PHP e lo converte nel formato SQL
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param mixed $dato dato in formato PHP  da convertire
	* @param string $tipoDB tipo del campo sul database
	* @return string valore da inserire nella query SQL
	* @ignore
	*/ 
	function sqlValue($valore, $tipoDB)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* prende un campo come arriva da db e lo converte nel formato usabile da PHP
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param mixed $dato dato in formato DB  da convertire
	* @param string $tipoDB tipo del campo sul database
	* @return mixed  valore PHP
	* @ignore
	*/ 
	function fieldConvert($campo, $tipoDB)
		{
		}
	
	//***************************************************************************
	/**
	* - 
	*
	* in pratica e' un duplicato di @{link esegui}, salvo che viene utilizzata
	* dal recordset per passare e ricevere piu' informazioni
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param string $sql query sql
	* @param int $recordNr numero max di record che la query deve restituire
	* @param int $skip numero di record di cui effettuare lo skip (offset)
	* @return array : 
	* - nel primo elemento array info colonne; 
	* - nel secondo elemento array dei values cosi' come ritornati dal db (crudi)
	* - nel terzo elemento il nr. di records che soddisfano le condizioni, 
	*   indipendentemente dal limit imposto
	* @ignore
	*/ 
	function extendedExecute($sql, $recordNr = null, $skip = 0)
		{
		}
		
	//***************************************************************************
	/**
	* ping
	 * 
	 * riconnette il db se la connessione è andata giù
	 * 
	 * attenzione: non so cosa succeda in caso di transazione (immagino un disastro)
	*/ 
	function ping()
		{
		}
		
	//***************************************************************************
	/**
	* getConnectionId
	 * 
	 * restituisce l'id della connessione al db
	*/ 
	function getConnectionId()
		{
		}
		
	//***************************************************************************
	//******* inizio metodi protected *********************************************
	//***************************************************************************
	
	//***************************************************************************
	/**
	* -
	*
	* restituisce il meta-tipo (ossia il tipo applicativo) di un campo del db
	* @param string $tipoDB tipo del campo sul database
	* @return string  meta-tipo
	* @access protected
	* @ignore
	*/ 
	protected function getApplicationFieldType($tipoDB)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce le info di una colonna
	* @return array info colonna
	* @access protected
	* @ignore
	*/ 
	protected function setColumnAttribs($queryId, $colIdx)
		{
		}
		
	//***************************************************************************
	/**
	* -
	*
	* logga una query in scrittura su db; se valorizzata, chiama anche 
	* la callback function
	* @access protected
	* @ignore
	*/ 
	protected function logQuery($sql, $prepared_params = null)
		{

		if (!$this->WADB_LOGNAME && !$this->WADB_LOG_CALLBACK_FNC)
			{
			return;
			}

		list($comando, $resto) = explode(" ", strtoupper(trim($sql)), 2);
		if ($comando != "INSERT" && $comando != "UPDATE" && $comando != "DELETE" && $comando != "DROP" && $comando != "TRUNCATE")
			{
			return;
			}

		// verifichiamo se c'e' una rientranza
		$stack = debug_backtrace();
		for ($i = 1; $i < count($stack); $i++)
			{
			if ($stack[$i]['class'] == __CLASS__ && $stack[$i]['function'] == __FUNCTION__)
				{
				return;
				}
			}

		if ($this->WADB_LOGNAME)
			{
		    $riga = date("Y-m-d H:i:s") . " ||| " .
		    		str_pad($_SERVER['REMOTE_ADDR'], 15) . " ||| " .
		    		str_pad($_SERVER['PHP_SELF'], 40) . " ||| " .
		    		$sql . "\r\n";
		    $fp = fopen($this->WADB_LOGNAME, "a");
			flock($fp, LOCK_EX);
		    fwrite($fp, $riga);
			flock($fp, LOCK_UN);
		    fclose($fp);
			}			
			
		if ($this->WADB_LOG_CALLBACK_FNC)
			{ 
			if (is_string($this->WADB_LOG_CALLBACK_FNC) && function_exists($this->WADB_LOG_CALLBACK_FNC))
				{
				call_user_func($this->WADB_LOG_CALLBACK_FNC, $sql, $prepared_params);
				}
			elseif (is_array($this->WADB_LOG_CALLBACK_FNC))
				{
				list($applClass, $varClassInst, $method) = $this->WADB_LOG_CALLBACK_FNC;
				$applInstance = eval("return $applClass::$varClassInst;");
				if (method_exists($applInstance, $method))
					{
					call_user_func(array($applInstance, $method), $sql, $prepared_params);
					}
				}
			}
			
		}
		
	//***************************************************************************
	/**
	* divide la query sql nelle varie clausole (nuova versione con parser seio e 
	 * lento)
	 * 
	* @access protected
	* @ignore
	*/	
	protected function newSplitSql($sql)
		{
		include_once(__DIR__ . "/waPHPSQLParser.php");
		
		$parsed = array();
		foreach ((new waPHPSQLParser($sql, true))->parsed as $key => &$val)
			{
			$parsed[strtolower($key)] = &$val;
			}
		
		$retval = new stdClass();
		foreach ($this->clauses as $idx => $clause)
			{
			if ($parsed[$clause])
				{
				$clauseStart = $parsed[$clause]["position"];
				$clauseEnd = null;
				for ($li = $idx + 1; $li < count($this->clauses); $li++)
					{
					$nextClause = $this->clauses[$li];
					if ($parsed[$nextClause])
						{
						$clauseEnd = $parsed[$nextClause]["position"] - 1;
						break;
						}
					}
				
				$clauseLen = $clauseEnd ? $clauseEnd - $clauseStart : null;
				if ($clauseLen)
					{
					$retval->$clause = substr($sql, $clauseStart, $clauseLen);
					}
				else
					{
					$retval->$clause = substr($sql, $clauseStart);
					}
				}
			
			}
		
		return $retval;
		
		}
		
	//***************************************************************************
	/**
	* divide la query sql nelle varie clausole (vecchia versione fatta in casa)
	* @access protected
	* @ignore
	*/	
	protected function oldSplitSql($sql)
		{
		$retval = new stdClass();
		foreach ($this->clauses as $clause)
			{
			$retval->$clause = $this->getClause($sql, $clause);
			}
		
		return $retval;
		}
		
	//***************************************************************************
	/**
	* @access protected
	* @ignore
	*/	
	protected function getClause($sql, $keyword)
		{
		$keyword = strtolower($keyword);
		if ($keyword == "select")
			{
			$inizio = 0;
			}
		else
			{
			$inizio = strripos($sql, " $keyword ");
			}
			
		if ($inizio === false)
			{
			return '';
			}
			
		$clauses = & $this->clauses;
		for ($i = 0; $i < count($clauses); $i++)
			{
			if ($keyword == $clauses[$i])
				{
				break;
				}
			}
		for ($i += 1; $i < count($clauses); $i++)
			{
			$fine = strripos($sql, " $clauses[$i] ");
			if ($fine)
				{
				break;
				}
			}
			
		if ($fine)
			{
			$quanti = $fine - $inizio;
			}
		else
			{
			$quanti = strlen($sql) - $inizio;
			}
			
		return trim(substr($sql, $inizio, $quanti));
		}
		
	//***************************************************************************
	/**
	* -
	*
	* distruttore
	 * @ignore
	*/ 
	function __destruct()
		{
		}

//***************************************************************************
//****  fine classe waDBConnection *****************************************
//***************************************************************************
	}	// fine classe 









