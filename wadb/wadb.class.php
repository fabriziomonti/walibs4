<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* include utility package
* @ignore
*/
require_once(__DIR__ . '/wadbparams.php');

/**
* include utility package
* @ignore
*/
require_once(__DIR__ . '/wadb.utilities.php');

/**
* include classe waRecordset
* @ignore
*/
require_once(__DIR__ . '/warecordset.class.php');

/**
* include classe waRecord
* @ignore
*/
require_once(__DIR__ . '/warecord.class.php');


//***************************************************************************
/**
* classe waDB
 * 
 * Questa classe contiene solamente le const del package
 
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waDB
	{
	
	//******************* tipi database **************************
	/**
	* 
	* Tipo database MySQL
	*/
	const DBTYPE_MYSQL =		"mysql";

	/**
	* 
	* Tipo database MySQL-Improved
	*/
	const DBTYPE_MYSQLI =		"mysqli";

	/**
	* 
	* Tipo database MySQL PDO
	*/
	const DBTYPE_MYSQL_PDO =		"mysql_pdo";

	/**
	* 
	* Tipo database MySQL PDO2 (count al posto di sql_calc_found_rows)
	*/
	const DBTYPE_MYSQL_PDO2 =		"mysql_pdo2";

	/**
	* 
	* Tipo database MS SQL Server
	*/
	const DBTYPE_MSSQL =		"mssql";

	/**
	* 
	* Tipo database ODBC
	*/
	const DBTYPE_ODBC =			"odbc";

	/**
	* 
	* Tipo database ORACLE
	*/
	const DBTYPE_ORACLE =		"oracle";

	/**
	* 
	* Tipo database As a servrice
	*/
	const DBTYPE_ASAS =		"asas";


	//******************* tipi campo applicativi **************************
	/**
	* 
	* Tipo campo applicativo stringa
	*/
	const STRING =				"WADB_STRING";

	/**
	* 
	* Tipo campo applicativo numerico intero
	*/
	const INTEGER =				"WADB_INTEGER";

	/**
	* 
	* Tipo campo applicativo contenitore (text, longtext, container, ecc)
	*/
	const CONTAINER =			"WADB_CONTAINER";

	/**
	* 
	* Tipo campo applicativo data
	*/
	const DATE =				"WADB_DATE";

	/**
	* 
	* Tipo campo applicativo ora
	*/
	const TIME =				"WADB_TIME";

	/**
	* 
	* Tipo campo applicativo data/ora
	*/
	const DATETIME =			"WADB_DATETIME";

	/**
	* 
	* Tipo campo applicativo numerico decimale
	*/
	const DECIMAL =				"WADB_DECIMAL";

	/**
	* 
	* Tipo campo applicativo sconosciuto
	*/
	const UNKNOWNTYPE =			"WADB_UNKNOWNTYPE";


	//******************* stati dei record **************************
	/**
	* @ignore
	*/
	const RECORD_UNMODIFIED =	0;
	/**
	* @ignore
	*/
	const RECORD_MODIFIED =		1;
	/**
	* @ignore
	*/
	const RECORD_NEW =			2;
	/**
	* @ignore
	*/
	const RECORD_TO_DELETE =	3;


//***************************************************************************
//****  fine classe waDBConnection *****************************************
//***************************************************************************
	}	// fine classe 









