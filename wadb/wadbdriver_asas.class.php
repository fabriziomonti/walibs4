<?php

/**
 * -
 *
 * @package waDB
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */

namespace waLibs;

use stdClass;

/**
 * @ignore
 */
require_once(__DIR__ . '/wadbdriver.class.php');

/* todo
  - limit
  - record che soddisfano...
  - autocommit - gestione transazione
  - tabella di appartenenza della colonna
  - flag chiave primaria


 */

//***************************************************************************
//****  classe waDBConnection_asas **********************************************
//***************************************************************************
/**
 * waDBConnection_asas
 * 
 * classe per la connessione fisica ad un database "as a service"
 * 
 * <b>SPERIMENTALE!</b>
 *
 * @package waDB
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 * 
 * @ignore
 */
class waDBConnection_asas extends waDBConnection {

	/**
	 * @access protected
	 * @ignore
	 */
	protected $DBConn = false;

	/**
	 * @access protected
	 * @ignore
	 */
	protected $response_code_ok = 0;

	/**
	 * @access protected
	 * @ignore
	 */
//	protected $oracleVersion = '';

	/**
	 * @access protected
	 * @ignore
	 */
	protected $last_error_code = "";
	protected $last_error_msg = "";

	//***************************************************************************

	/**
	 * -
	 * 
	 * connette il database.
	 * @return boolean per conoscere l'esatto esito del metodo occorre invocare 
	 * il metodo {@link errorNr}
	 * @ignore
	 */
	function connect() {
		$ret = true;
		$this->DBConn = false;
		$params = new stdClass();
		$params->host = $this->WADB_HOST;
		$params->port = $this->WADB_PORT;
		$params->username = $this->WADB_USERNAME;
		$params->password = $this->WADB_PASSWORD;
		$params->database = $this->WADB_DBNAME;

		$response = $this->callService("get_connection", $params);
		if ($response->responseCode === $this->response_code_ok) {
			$this->DBConn = $response->connectionId;
			$this->execute("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
//			$retval = $this->execute('SELECT * FROM v$version');
//			list($niente, $niente, $this->oracleVersion) = explode(" ", $retval[0]["BANNER"]);
			$a = 0;
		} else {
			$ret = false;
		}

		return $ret;
	}

	//***************************************************************************

	/**
	 * -
	 * 
	 * disconnette il database (abortisce eventuali transazioni non committate).
	 * @return void
	 * @ignore
	 */
	function disconnect() {
		if ($this->DBConn === false) {
			return;
		}
		$params = new stdClass();
		$params->connectionId = $this->DBConn;
		$response = $this->callService("close_connection", $params);
		if ($response->responseCode === $this->response_code_ok) {
			$this->DBConn = false;
		}
	}

	//***************************************************************************

	/**
	 * -
	 * 
	 * ritorna l'ultimo codice di errore restituito dal database.
	 * @return string
	 * @ignore
	 */
	function errorNr() {
		return $this->last_error_code;
	}

	//***************************************************************************

	/**
	 * -
	 * 
	 * ritorna l'ultimo messaggio di errore restituito dal database.
	 * @return string
	 * @ignore
	 */
	function errorMessage() {
		return $this->last_error_msg;
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * Esegue un comando SQL sul database connesso.
	 * @param string $sql SQL da eseguire
	 * @return mixed i dati grezzi ottenuti dalla query o FALSE in caso di errore;
	 * per conoscere l'esatto esito del metodo occorre invocare 
	 * il metodo {@link errorNr}
	 * @ignore
	 */
	function execute($sql) {
		$params = new stdClass();
		$params->connectionId = $this->DBConn;
		$params->sql = $sql;
		$response = $this->callService("execute", $params);
		if ($response->responseCode !== $this->response_code_ok) {
			return false;
		}

		$retval = [];
		foreach ($response->data as $inputRow) {
			$outputRow = [];
			foreach ($response->metaData as $idx => $colInfo) {
				$outputRow[$colInfo->name] = $inputRow[$idx];
			}
			$retval[] = $outputRow;
		}

		return $retval;
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * Inizia una transazione.
	 * @return void
	 * @ignore
	 */
	function beginTransaction() {
		$this->execute('SET AUTOCOMMIT=0');
		$this->execute('BEGIN');
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * Conferma una transazione aperta in precedenza con {@link beginTransaction}.
	 * @return void
	 * @ignore
	 */
	function commitTransaction() {
		$this->execute('COMMIT');
		$this->execute('SET AUTOCOMMIT=1');
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * Annulla una transazione aperta in precedenza con {@link beginTransaction}.
	 * @return void
	 * @ignore
	 */
	function rollbackTransaction() {
		$this->execute('ROLLBACK');
		$this->execute('SET AUTOCOMMIT=1');
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * ritorna l'ultimo identifica univoco inserito nel database a fronte di INSERT
	 * (posto che la tabella sia dotata di una chiave primaria autoincrementale).
	 * @return integer
	 * @ignore
	 */
	function lastInsertedId() {
		// tbd
		return asas_insert_id($this->DBConn);
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database.
	 * @param integer $data data in formato "since-the-epoch"
	 * @return string la data in formato SQL
	 * @ignore
	 */
	function sqlDate($data) {
		if (is_null($data)) {
			return $this->sqlNull();
		}
		return "to_date('" . date("Y-m-d H:i:s", $data) . "', 'YYYY-MM-DD HH24:MI:SS')";
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	 * dal database.
	 * @param integer $dataOra data/ora in formato "since-the-epoch"
	 * @return string la data/ora in formato SQL
	 * @ignore
	 */
	function sqlDateTime($dataOra) {
		if (is_null($dataOra)) {
			return $this->sqlNull();
		}
		return "to_timestamp('" . date("Y-m-d H:i:s", $dataOra) . "', 'YYYY-MM-DD HH24:MI:SS')";
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	 * formato SQL TIME richiesto dal database.
	 * @param integer $ora ora in formato "since-the-epoch"
	 * @return string l'ora in formato SQL
	 * @ignore
	 */
	function sqlTime($ora) {
		if (is_null($ora)) {
			return $this->sqlNull();
		}
		return "to_date('1980-01-01 " . date("H:i:s", $ora) . "', 'YYYY-MM-DD HH24:MI:SS')";
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * trasforma una stringa nel formato SQL richiesto dal database.
	 * @param string $stringa  stringa da convertire
	 * @return string  stringa convertita
	 * @ignore
	 */
	function sqlString($stringa) {
		if (is_null($stringa)) {
			return $this->sqlNull();
		}
		// da sistemare!!!!!
		return "'" . str_replace("'", "''", $stringa) . "'";
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * trasforma un intero nel formato SQL richiesto dal database.
	 * @param integer $intero  intero da convertire
	 * @return string  stringa convertita
	 * @ignore
	 */
	function sqlInteger($intero) {
		if (is_null($intero) || $intero === '') {
			return $this->sqlNull();
		}
		return $intero . "";
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * trasforma un numero decimale nel formato SQL richiesto dal database.
	 * @param float $decimale  idecimale da convertire
	 * @return string  stringa convertita
	 * @ignore
	 */
	function sqlDecimal($decimale) {
		if (is_null($decimale) || $decimale === '') {
			return $this->sqlNull();
		}
		return $decimale . "";
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * restituisce il valore NULL come richiesto dal db.
	 * @return string  
	 * @ignore
	 */
	function sqlNull() {
		return 'NULL';
	}

	//***************************************************************************
	//******* inizio metodi semi-protected ****************************************
	//***************************************************************************
	//***************************************************************************

	/**
	 * -
	 *
	 * prende un campo come arriva da PHP e lo converte nel formato SQL
	 * il metodo non e' documentato perche' ha senso se chiamato solo dalla
	 * classe waRecordset
	 * @param mixed $dato dato in formato PHP  da convertire
	 * @param string $tipoDB tipo del campo sul database
	 * @return string valore da inserire nella query SQL
	 * @ignore
	 */
	function sqlValue($valore, $tipoDB) {

		switch ($tipoDB) {
			case 'DATE':
			case 'TIMESTAMP':
				return $this->sqlDateTime($valore);

			case 'NUMBER':
			case 'NUMERIC':
			case 'DEC':
			case 'DECIMAL':
			case 'BINARY_DOUBLE':
			case 'BINARY_FLOAT':
				// qui forse occorre fare una verifica dello scale...
				return $this->sqlDecimal($valore);

			case 'CHAR':
			case 'NCHAR':
			case 'RAW':
			case 'VARCHAR2':
			case 'NVARCHAR2':
			case 'BLOB':
			case 'CLOB':
			case 'NCLOB':
			case 'BFILE':
			case 'MDSYS.SDO_GEOMETRY':
			default:
				return $this->sqlString($valore);
		}
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * prende un campo come arriva da db e lo converte nel formato usabile da PHP
	 * il metodo non e' documentato perche' ha senso se chiamato solo dalla
	 * classe waRecordset
	 * @param mixed $dato dato in formato DB  da convertire
	 * @param string $tipoDB tipo del campo sul database
	 * @return mixed  valore PHP
	 * @ignore
	 */
	function fieldConvert($campo, $tipoDB) {
		if (is_null($campo))
			return $campo;
		switch ($tipoDB) {
			case 'DATE':
			case 'TIMESTAMP':
				list($data, $ore) = explode(" ", $campo);
				list($anno, $mese, $giorno) = explode("-", $data);
				list($ora, $min, $sec) = explode(":", $ore);
				return mktime($ora * 1, $min * 1, $sec * 1, $mese * 1, $giorno * 1, $anno * 1);

			case 'NUMBER':
			case 'NUMERIC':
			case 'DEC':
			case 'DECIMAL':
			case 'BINARY_DOUBLE':
			case 'BINARY_FLOAT':
				return ($campo * 1);

			default:
				return $campo;
		}
	}

	//***************************************************************************

	/**
	 * - 
	 *
	 * in pratica e' un duplicato di @{link esegui}, salvo che viene utilizzata
	 * dal recordset per passare e ricevere piu' informazioni
	 * il metodo non e' documentato perche' ha senso se chiamato solo dalla
	 * classe waRecordset
	 * @param string $sql query sql
	 * @param int $nrRighe numero max di records che la query deve restituire
	 * @param int $rigaIniziale numero di records di cui effettuare lo skip (offset)
	 * @return array : 
	 * - nel primo elemento array info colonne; 
	 * - nel secondo elemento array dei values cosi' come ritornati dal db (crudi)
	 * - nel terzo elemento il nr. di records che soddisfano le condizioni, 
	 *   indipendentemente dal limit imposto
	 * @ignore
	 */
	function extendedExecute($sql, $nrRighe = null, $rigaIniziale = 0) {
//		if (strpos($this->oracleVersion, "11") !== false) {
//			return $this->extendedExecute11($sql, $nrRighe, $rigaIniziale);
//		}
		return $this->extendedExecute12($sql, $nrRighe, $rigaIniziale);
	}
	
	//**************************************************************************
	function extendedExecute12($sql, $nrRighe = null, $rigaIniziale = 0) {

		$params = new stdClass();
		$params->connectionId = $this->DBConn;
		$params->sql = $sql;
		if ($nrRighe !== null) {
			// modifichiamo la query, che ovviamente DEVE essere una select,
			// affinche' asas possa tornare il nr di records
			// che soddisfano la condizione, indipendentemente dal limit imposto
			$params->sql .= " OFFSET $rigaIniziale ROWS FETCH NEXT $nrRighe ROWS ONLY";
		}

		$response = $this->callService("execute", $params);
		if ($response->responseCode !== $this->response_code_ok) {
//exit($sql);			
			return false;
		}

		// carichiamo le informazioni delle columns
		$colInfos = array();
		$nrColonne = count($response->metaData);
		for ($i = 0; $i < $nrColonne; $i++) {
			$colInfos[] = $this->setColumnAttribs($response->metaData[$i], $i);
		}

		// carichiamo i records
		$righeCrude = [];
		for ($i = 0; $i < count($response->data); $i++) {
			$righeCrude[] = $response->data[$i];
		}

		if ($nrRighe === null) {
			$nrRigheNoLimit = count($response->metaData);
		}
		elseif ($nrRighe !== 0) {
			// contiamo i record
			$params->sql = " SELECT count(*) as wadb_found_rows FROM ($sql)";
			$response = $this->callService("execute", $params);
			if ($response->responseCode !== $this->response_code_ok) {
				return false;
			}
			$nrRigheNoLimit = $response->data[0][0];
		}
		
		return array($colInfos, $righeCrude, $nrRigheNoLimit);
	}

	//**************************************************************************
	function extendedExecute11($sql, $nrRighe = null, $rigaIniziale = 0) {
		if ($nrRighe !== null) {
			// modifichiamo la query, che ovviamente DEVE essere una select,
			// affinche' asas possa tornare il nr di records
			// che soddisfano la condizione, indipendentemente dal limit imposto
			$sql = "SELECT * FROM (" .
					" SELECT  v1.*, rownum wadb_rn FROM (" .
					" SELECT  v0.*, count(*) over () wadb_found_rows FROM ($sql) v0" .
					") v1" .
					" WHERE rownum<=" . ($rigaIniziale + $nrRighe) .
					") v2" .
					" WHERE v2.wadb_rn > $rigaIniziale" .
					" ORDER BY wadb_rn";
		}


		$params = new stdClass();
		$params->connectionId = $this->DBConn;
		$params->sql = $sql;
		$response = $this->callService("execute", $params);
		if ($response->responseCode !== $this->response_code_ok) {
			return false;
		}

		// carichiamo le informazioni delle columns
		$colInfos = array();
		$nrColonne = count($response->metaData) - ($nrRighe !== null ? 2 : 0);
		for ($i = 0; $i < $nrColonne; $i++) {
			$colInfos[] = $this->setColumnAttribs($response->metaData[$i], $i);
		}

		// carichiamo i records
		$righeCrude = [];
		for ($i = 0; $i < count($response->data); $i++) {
			$rigaCruda = $response->data[$i];
			if ($nrRighe !== null) {
				$nrRigheNoLimit = $rigaCruda[$nrColonne];
				array_splice($rigaCruda, $nrColonne);
			}
			$righeCrude[] = $rigaCruda;
		}

		return array($colInfos, $righeCrude, $nrRigheNoLimit);
	}

	//***************************************************************************
	//******* inizio metodi protected *********************************************
	//***************************************************************************
	//***************************************************************************

	/**
	 * -
	 *
	 * restituisce il meta-tipo (ossia il tipo applicativo) di un campo del db
	 * @param string $tipoDB tipo del campo sul database
	 * @return string  meta-tipo
	 * @access protected
	 * @ignore
	 */
	protected function getApplicationFieldType($tipoDB) {
		switch ($tipoDB) {

			case 'BLOB':
			case 'CLOB':
			case 'NCLOB':
			case 'BFILE':
				return waDB::CONTAINER;

			case 'DATE':
			case 'TIMESTAMP':
				return waDB::DATETIME;

			case 'NUMBER':
			case 'NUMERIC':
			case 'DEC':
			case 'DECIMAL':
			case 'BINARY_DOUBLE':
			case 'BINARY_FLOAT':
				return waDB::DECIMAL;

			case 'CHAR':
			case 'NCHAR':
			case 'RAW':
			case 'VARCHAR2':
			case 'NVARCHAR2':
			case 'MDSYS.SDO_GEOMETRY':
			default:
				return waDB::STRING;
		}
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * restituisce le info di una colonna
	 * @return array info colonna
	 * @access protected
	 * @ignore
	 */
	protected function setColumnAttribs($colInfo, $colIdx) {
		$col['tableAbsName'] = $colInfo->table;
		$col['table'] = $colInfo->table;
		$col['dbName'] = $this->WADB_DBNAME;

		$col['name'] = $colInfo->name;
		$col['index'] = $colIdx;
		$col['dbType'] = $colInfo->type_name;

		$col['primaryKey'] = strtoupper($colInfo->name) == "ID" ? 1 : 0; // maialata...
		$col['maxLength'] = $colInfo->display_size;
		$col['type'] = $this->getApplicationFieldType($col['dbType']);
		if ($col['type'] == waDB::DECIMAL) {
			$scale = $colInfo->scale;
			if ($scale <= 0) {
				$col['type'] = waDB::INTEGER;
			}
		}
		return $col;
	}

	//**************************************************************************

	/**
	 * -
	 *
	 * effettua la chiamata al servizio
	 * @return array info colonna
	 * @access protected
	 * @ignore
	 */
	protected function callService($endpoint, $postData) {
		$this->resetLastError();
		$data_string = json_encode($postData);
		$ch = curl_init("http://localhost:15220/api/$endpoint");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=UTF-8',
			'Content-Length: ' . strlen($data_string))
		);
		$responseString = curl_exec($ch);
		if ($responseString === false) {
			exit ("callService error");
		}

		$response = (Object) json_decode($responseString);
		$this->setLastError($response);
		return $response;
	}

	//**************************************************************************

	/**
	 * -
	 *
	 * @access protected
	 * @ignore
	 */
	protected function resetLastError() {
		$this->last_error_code = $this->last_error_msg = "";
	}

	//**************************************************************************

	/**
	 * -
	 *
	 * @access protected
	 * @ignore
	 */
	protected function setLastError($response) {
		if ($response->responseCode !== $this->response_code_ok) {
			$this->last_error_code = $response->responseCode;
			$this->last_error_msg = $response->responseMessage;
			if (strpos($response->responseMessage, "ORA-") !== false) {
				$elems = explode("ORA-", $response->responseMessage);
				$elems = explode(":", $elems[1]);
				$this->last_error_code = "ORA-" . $elems[0];
			}
		}
	}

	//***************************************************************************

	/**
	 * -
	 *
	 * distruttore
	 * @ignore
	 */
	function __destruct() {
		$this->disconnect();
	}

//***************************************************************************
//****  fine classe waDBConnection_asas *****************************************
//***************************************************************************
}

// fine classe 