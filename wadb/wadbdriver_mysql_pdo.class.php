<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;
use PDO;
use PDOStatement;

/**
* @ignore
*/
require_once(__DIR__. '/wadbdriver.class.php');

//***************************************************************************
//****  classe waDBConnection_pdo_mysql ***************************************
//***************************************************************************
/**
* waDBConnection_pdo_mysql
* 
* classe per la connessione fisica ad un database mysql-improved
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* @ignore
*/
class waDBConnection_mysql_pdo extends waDBConnection 
	{
	
	/**
	* @access protected
	* @ignore
	 * @type \PDO
	*/ 
	protected $pdo = null;
	
	/**
	* @access protected
	* @ignore
	 * @type \PDOStatement
	*/ 
	protected $statement = null;
	
	protected $prepare_placeholder_start = "___wadb_prepare_placeholder_start___";
	protected $prepare_placeholder_end = "___wadb_prepare_placeholder_end___";
	protected $last_prepared_sql = "";

	//***************************************************************************
	/**
	* -
	* 
	* connette il database.
	* @return boolean per conoscere l'esatto esito del metodo occorre invocare 
	* il metodo {@link errorNr}
	* @ignore
	*/ 
	function connect()
		{
		try
			{
			$this->pdo = new \PDO("mysql:host=$this->WADB_HOST;port=$this->WADB_PORT;dbname=$this->WADB_DBNAME;charset=utf8", $this->WADB_USERNAME, $this->WADB_PASSWORD);
                        $this->pdo-> exec ("SET sql_mode = (SELECT REPLACE (@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);

			return true;
			}
		catch (\PDOException $pe)
			{
			return false;
			}

		}
		
	//***************************************************************************
	/**
	* -
	* 
	* disconnette il database (abortisce eventuali transazioni non committate).
	* @return void
	* @ignore
	*/ 
	function disconnect()
		{
		$this->pdo = null;
		}
		
		
	//***************************************************************************
	/**
	* -
	* 
	* ritorna l'ultimo codice di errore restituito dal database.
	* @return string
	* @ignore
	*/ 
	function errorNr()
		{
		if (!$this->pdo)
			{
			return -1;
			}
		return $this->statement && $this->statement->errorInfo()[1] ? $this->statement->errorInfo()[1] : $this->pdo->errorInfo()[1];
		}
		
	//***************************************************************************
	/**
	* -
	* 
	* ritorna l'ultimo messaggio di errore restituito dal database.
	* @return string
	* @ignore
	*/ 
	function errorMessage()
		{
		if (!$this->pdo)
			{
			return "no connection";
			}
		return $this->statement && $this->statement->errorInfo()[2] ? $this->statement->errorInfo()[2] : $this->pdo->errorInfo()[2];
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Esegue un comando SQL sul database connesso.
	* @param string $sql SQL da eseguire
	* @return mixed i dati grezzi ottenuti dalla query o FALSE in caso di errore;
	* per conoscere l'esatto esito del metodo occorre invocare 
	* il metodo {@link errorNr}
	* @ignore
	*/
	function execute($sql)
		{
		// crea i parametri per il prepared statement
		list($sql, $params) = $this->prepare($sql);

		$this->logQuery($sql, $params);
		
		if (!$this->statement || $this->statement->queryString != $sql)
			{
			$this->statement = $this->pdo->prepare($sql);
			if ($this->statement === false)
				{
				return false;
				}
			}
		$exec_result = $this->statement->execute($params);
		if ($exec_result === false)
			{
			return false;
			}
		
		// carichiamo i records
		$this->statement->setFetchMode(PDO::FETCH_ASSOC);			
		$retval = $this->statement->fetchAll();
		$this->statement->closeCursor();
		
		return $retval;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Inizia una transazione.
	* @return void
	* @ignore
	*/ 
	function beginTransaction()
		{
		$this->pdo->beginTransaction();
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Conferma una transazione aperta in precedenza con {@link beginTransaction}.
	* @return void
	* @ignore
	*/ 
	function commitTransaction()
		{
		$this->pdo->commit();
		}
		
	//***************************************************************************
	/**
	* -
	*
	* Annulla una transazione aperta in precedenza con {@link beginTransaction}.
	* @return void
	* @ignore
	*/ 
	function rollbackTransaction()
		{
		$this->pdo->rollBack();;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* ritorna l'ultimo identifica univoco inserito nel database a fronte di INSERT
	* (posto che la tabella sia dotata di una chiave primaria autoincrementale).
	* @return integer
	* @ignore
	*/ 
	function lastInsertedId()
		{
		return $this->pdo->lastInsertId();
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database.
	* @param integer $data data in formato "since-the-epoch"
	* @return string la data in formato SQL
	* @ignore
	*/ 
	function sqlDate($data)
		{
		if (strpos($data, $this->prepare_placeholder_start) !== false || strpos($data, $this->prepare_placeholder_end) !== false )
			{
			return $this->sqlNull();
			}
		if (is_null($data) || $data === false || $data === '')
			{
			return $this->sqlNull();
			}
		$retval = date("Y-m-d", $data);
		if ($retval === false)
			{
			return $this->sqlNull();
			}
		return $this->prepare_placeholder_start . $retval . $this->prepare_placeholder_end;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	* dal database.
	* @param integer $dataOra data/ora in formato "since-the-epoch"
	* @return string la data/ora in formato SQL
	* @ignore
	*/ 
	function sqlDateTime($dataOra)
		{
		if (strpos($dataOra, $this->prepare_placeholder_start) !== false || strpos($dataOra, $this->prepare_placeholder_end) !== false )
			{
			return $this->sqlNull();
			}
		if (is_null($dataOra) || $dataOra === false || $dataOra === '')
			{
			return $this->sqlNull();
			}
		$retval = date("Y-m-d H.i.s", $dataOra);
		if ($retval === false)
			{
			return $this->sqlNull();
			}
		return $this->prepare_placeholder_start . $retval . $this->prepare_placeholder_end;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	* formato SQL TIME richiesto dal database.
	* @param integer $ora ora in formato "since-the-epoch"
	* @return string l'ora in formato SQL
	* @ignore
	*/ 
	function sqlTime($ora)
		{
		if (strpos($ora, $this->prepare_placeholder_start) !== false || strpos($ora, $this->prepare_placeholder_end) !== false )
			{
			return $this->sqlNull();
			}
		if (is_null($ora))
			{
			return $this->sqlNull();
			}
		return $this->prepare_placeholder_start . "1980-01-01 " . date("H.i.s", $ora) . $this->prepare_placeholder_end;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una stringa nel formato SQL richiesto dal database.
	* @param string $stringa  stringa da convertire
	* @return string  stringa convertita
	* @ignore
	*/ 
	function sqlString($stringa)
		{
		if (strpos($stringa, $this->prepare_placeholder_start) !== false || strpos($stringa, $this->prepare_placeholder_end) !== false )
			{
			return $this->sqlNull();
			}
		if (is_null($stringa))
			{
			return $this->sqlNull();
			}
		return $this->prepare_placeholder_start . $stringa . $this->prepare_placeholder_end;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma un intero nel formato SQL richiesto dal database.
	* @param integer $intero  intero da convertire
	* @return string  stringa convertita
	* @ignore
	*/ 
	function sqlInteger($intero)
		{
		if (strpos($intero, $this->prepare_placeholder_start) !== false || strpos($intero, $this->prepare_placeholder_end) !== false )
			{
			return $this->sqlNull();
			}
		if (is_null($intero) || $intero === '')
			{
			return $this->sqlNull();
			}
		return $this->prepare_placeholder_start .  ((int) $intero) . $this->prepare_placeholder_end;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma un numero decimale nel formato SQL richiesto dal database.
	* @param float $decimale  idecimale da convertire
	* @return string  stringa convertita
	* @ignore
	*/ 
	function sqlDecimal($decimale)
		{
		if (strpos($decimale, $this->prepare_placeholder_start) !== false || strpos($decimale, $this->prepare_placeholder_end) !== false )
			{
			return $this->sqlNull();
			}
		if (is_null($decimale) || $decimale === '')
			{
			return $this->sqlNull();
			}
		return $this->prepare_placeholder_start .  ((float) $decimale) . $this->prepare_placeholder_end;
		}
		
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce il valore NULL come richiesto dal db.
	* @return string  
	* @ignore
	*/ 
	function sqlNull()
		{
		return $this->prepare_placeholder_start .  "DBNullValue" . $this->prepare_placeholder_end;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $date data in formato "since-the-epoch"
	* @return string la data in formato SQL da utilizzare come stringa
	*/ 
	public function sqlDateAsString($date)
		{
                return str_replace($this->prepare_placeholder_end, "", str_replace($this->prepare_placeholder_start, "", $this->sqlDate($date)));
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
	* dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $dateTime data/ora in formato "since-the-epoch"
	* @return string la data/ora in formato SQL da utilizzare come stringa
	*/ 
	public function sqlDateTimeAsString($dateTime)
		{
                return str_replace($this->prepare_placeholder_end, "", str_replace($this->prepare_placeholder_start, "", $this->sqlDateTime($dateTime)));
		}
		
	//***************************************************************************
	/**
	* -
	*
	* trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
	* formato SQL TIME richiesto dal database
         * per essere utilizzato in una ricerca generica di tipo stringa.
	* @param integer $time ora in formato "since-the-epoch"
	* @return string l'ora in formato SQL da utilizzare come stringa
	*/ 
	public function sqlTimeAsString($time)
		{
                return str_replace($this->prepare_placeholder_end, "", str_replace($this->prepare_placeholder_start, "", $this->sqlTime($time)));
		}
		
	//***************************************************************************
	//******* inizio metodi semi-protected ****************************************
	//***************************************************************************
	
	//***************************************************************************
	/**
	* -
	*
	* prende un campo come arriva da PHP e lo converte nel formato SQL
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param mixed $dato dato in formato PHP  da convertire
	* @param string $tipoDB tipo del campo sul database
	* @return string valore da inserire nella query SQL
	* @ignore
	*/ 
	function sqlValue($valore, $tipoDB)
		{
			
		switch (strtoupper($tipoDB))
			{
			case "DATE": 
			case "NEWDATE": 
				return $this->sqlDate($valore);
			
			case "TIME":
				return $this->sqlTime($valore);
			
			case "DATETIME":
			case "TIMESTAMP": 
				return $this->sqlDateTime($valore);
			
			case "TINY":
			case "SHORT": 
			case "INT24":
			case "LONG": 
			case "LONGLONG":
				return $this->sqlInteger($valore);
			
			case "FLOAT": 
			case "DOUBLE":
			case "DECIMAL":
			case "NEWDECIMAL":
				return $this->sqlDecimal($valore);
			
			default:
				return $this->sqlString($valore);
			}
		}
		
	//***************************************************************************
	/**
	* -
	*
	* prende un campo come arriva da db e lo converte nel formato usabile da PHP
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param mixed $dato dato in formato DB  da convertire
	* @param string $tipoDB tipo del campo sul database
	* @return mixed  valore PHP
	* @ignore
	*/ 
	function fieldConvert($campo, $tipoDB)
		{
		if (is_null($campo))
			{
			return $campo;
			}
		switch (strtoupper($tipoDB))
			{
			case "DATE": 
			case "NEWDATE": 
				if (empty($campo))
					{
					return null;
					}
				if ($campo == "0000-00-00")
					{
					return 0;
					}
				list($anno, $mese, $giorno) = explode("-", $campo);
				return mktime(0,0,0, $mese * 1, $giorno * 1, $anno * 1);
			
			case "TIME":
				list($ora, $min, $sec) = explode(":", $campo);
				return mktime($ora * 1, $min * 1, $sec * 1, 1, 1, 1980);
			
			case "DATETIME":
			case "TIMESTAMP": 
				if (empty($campo))
					{
					return null;
					}
				if ($campo == "0000-00-00 00:00:00")
					{
					return 0;
					}
				list($data, $ore) = explode(" ", $campo);
				list($anno, $mese, $giorno) = explode("-", $data);
				list($ora, $min, $sec) = explode(":", $ore);
				return mktime($ora * 1, $min * 1, $sec * 1, $mese * 1, $giorno * 1, $anno * 1);
			
			case "TINY":
			case "SHORT": 
			case "INT24":
			case "LONG": 
			case "LONGLONG":
			case "FLOAT": 
			case "DOUBLE":
			case "DECIMAL":
			case "NEWDECIMAL":
				return ($campo * 1);
			
			default:
				return $campo;
			}
		}
	
	//***************************************************************************
	/**
	* - 
	*
	* in pratica e' un duplicato di @{link esegui}, salvo che viene utilizzata
	* dal recordset per passare e ricevere piu' informazioni
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param string $sql query sql
	* @param int $nrRighe numero max di records che la query deve restituire
	* @param int $rigaIniziale numero di records di cui effettuare lo skip (offset)
	* @return array : 
	* - nel primo elemento array info colonne; 
	* - nel secondo elemento array dei values cosi' come ritornati dal db (crudi)
	* - nel terzo elemento il nr. di records che soddisfano le condizioni, 
	*   indipendentemente dal limit imposto
	* @ignore
	*/ 
	function extendedExecute($sql, $nrRighe = null, $rigaIniziale = 0)
		{
		// crea i parapmetri per il prepared statement
		list($sql, $params) = $this->prepare($sql);
		
		if ($nrRighe !== null)
			{
			if ($nrRighe) 
				{
				// modifichiamo la query, che ovviamente DEVE essere una select,
				// affinche' mysql possa tornare il nr di records
				// che soddisfano la condizione, indipendentemente dal limit imposto
				$patternSelect="/(select )/i";
	//			list($select, $params) = preg_split($patternSelect,trim($sql), 2, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
				$elems = preg_split($patternSelect,trim($sql), 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
				$select = array_shift($elems);
				$query = implode("", $elems);
				$sql = "$select SQL_CALC_FOUND_ROWS $query LIMIT $rigaIniziale, $nrRighe";
				}
			else
				{
				$sql .= " LIMIT 0";
				}
			}
 		$this->statement = $this->pdo->prepare($sql);
		if ($this->statement === false)
			{
			return false;
			}
		$exec_result = $this->statement->execute($params);
		if ($exec_result === false)
			{
			return false;
			}

		// carichiamo le informazioni delle columns
		$colInfos = array();
		$nrColonne = $this->statement->columnCount();	
		for ($i = 0; $i < $nrColonne; $i++)
			{
			$colInfos[] = $this->setColumnAttribs($this->statement, $i);
			}

		// carichiamo i records
		$this->statement->setFetchMode(PDO::FETCH_NUM);			
		$righeCrude = $this->statement->fetchAll();
		$this->statement->closeCursor();
		
		if ($nrRighe !== null)
			{
			if ($nrRighe) 
				{
				// se e' stato richiesto il limit, allora andiamo anche a prelevare
				// il nr di records che soddisfano la condizione
				$this->statement = $this->pdo->query("SELECT FOUND_ROWS()");
				$this->statement->setFetchMode(PDO::FETCH_NUM);
				list($nrRigheNoLimit) = $this->statement->fetch();
				$nrRigheNoLimit *= 1;
				$this->statement->closeCursor();
				}
			else
				{
				$nrRigheNoLimit = 0;
				}
			}
		else
			{
			$nrRigheNoLimit = count($righeCrude);
			}

		return array($colInfos, $righeCrude, $nrRigheNoLimit);
		
		}
		
	//***************************************************************************
	//******* inizio metodi protected *********************************************
	//***************************************************************************
	
	//***************************************************************************
	/**
	* -
	*
	* restituisce il meta-tipo (ossia il tipo applicativo) di un campo del db
	* @param string $tipoDB tipo del campo sul database
	* @return string  meta-tipo
	* @access protected
	* @ignore
	*/ 
	protected function getApplicationFieldType($tipoDB)
		{
		switch (strtoupper($tipoDB))
			{
			case "STRING": 
			case "CHAR":
			case "VAR_STRING": 
			case "ENUM": 
			case "SET": 
				return waDB::STRING;
				
			case "TINY_BLOB": 
			case "BLOB":
			case "MEDIUM_BLOB":
			case "LONG_BLOB": 
				return waDB::CONTAINER;
				
			case "DATE": 
			case "NEWDATE": 
				return waDB::DATE;
			
			case "TIME":
				return waDB::TIME;
			
			case "DATETIME":
			case "TIMESTAMP": 
				return waDB::DATETIME;
			
			case "TINY":
			case "SHORT": 
			case "INT24":
			case "LONG": 
			case "LONGLONG":
				return waDB::INTEGER;
				
			case "FLOAT": 
			case "DOUBLE":
			case "DECIMAL":
			case "NEWDECIMAL":
				return waDB::DECIMAL;
				
			default: 
				return WADB_TIPO_SCONOSCIUTO;
			}
			
		}
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce le info di una colonna
	* @return array info colonna
	* @access protected
	* @ignore
	*/ 
	protected function setColumnAttribs($queryId, $colIdx)
		{
		$colInfo = $queryId->getColumnMeta($colIdx);
		$col['tableAbsName'] = $colInfo["table"];
		$col['dbName'] = "";
		$col['table'] = $colInfo["table"];
		$col['name'] = $colInfo["name"];
		$col['index'] = $colIdx;
		$col['dbType'] = $colInfo["native_type"];
		$col['primaryKey'] = 0;
		foreach ($colInfo["flags"] as $flag)
			{
			if (strtolower($flag) == "primary_key")
				{
				$col['primaryKey'] = 1;
				break;
				}
			}
		$col['maxLength'] = $colInfo["len"];
		$col['type'] = $this->getApplicationFieldType($col['dbType']);

		// problema: se il campo e' di tipo longtext e lavoriamo in UTF8 c'e' 
		// evidentemente un problema di overflow da qualche parte (probabilmente 
		// in PHP, che non riesce a far stare in un intero 2^32*3) e viene 
		// ritornato -1 nella lunghezza massima del campo; in questo 
		// caso interveniamo a mano 
		if ($col['type'] == waDB::CONTAINER && $col['maxLength'] == -1)
			{
			$col['maxLength'] = pow(2, 32) - 1;
			}
		// poiche' lavoriamo in utf8, per i campi stringa ci viene sempre
		// ritornata la lunghezza in bytes, non in caratteri; quindi e' da
		// dividere per 3
		elseif ($col['type'] == waDB::STRING || $col['type'] == waDB::CONTAINER)
			{
			$col['maxLength'] /= 3;
			}
		return $col;
		}
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce le info di una colonna
	* @return array info colonna
	* @access protected
	* @ignore
	*/ 
	protected function prepare($sql)
		{
		// crea il prepared statement
		$elems = explode($this->prepare_placeholder_start, $sql);
		$params = [];
		$prepared_sql = "";
		foreach ($elems as $elem)
			{
			if (strpos($elem, $this->prepare_placeholder_end) !== false )
				{
				list($param, $rest) = explode($this->prepare_placeholder_end, $elem);
				$prepared_sql .= "?$rest";
				$params[] = $param == "DBNullValue" ? null : $param;
				}
			else
				{
				$prepared_sql .= $elem;
				}
			}
		
		return [$prepared_sql, $params];
		}
		
	//***************************************************************************
	/**
	* -
	*
	* ping
	 * 
	 * riconnette il db se la connessione è andata giù
	 * 
	 * attenzione: non so cosa succeda in caso di transazione (immagino un disastro)
	* @ignore
	*/
	function ping()
		{
		try
			{
			$this->pdo->query('SELECT 1');
			}
		catch (PDOException $e)
			{
			$this->connect();
			}
		}

	//***************************************************************************
	function getConnectionId()
		{
		return $this->pdo->query('SELECT CONNECTION_ID() as id')->fetch(PDO::FETCH_ASSOC)["id"];
		}
		
	//***************************************************************************
	/**
	* -
	*
	* distruttore
	* @ignore
	*/ 
	function __destruct()
		{
		$this->disconnect();
		}

//***************************************************************************
//****  fine classe waDBConnection_pdo_mysql **********************************
//***************************************************************************
	}	// fine classe 


