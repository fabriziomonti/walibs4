<?php

/**
 * -
 *
 * @package waDB
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */

namespace waLibs;

use stdClass;

/**
 * @ignore
 */
require_once(__DIR__ . '/wadbdriver.class.php');

/* todo
  - limit
  - record che soddisfano...
  - autocommit - gestione transazione
  - tabella di appartenenza della colonna
  - flag chiave primaria


 */

//***************************************************************************
//****  classe waDBConnection_oracle **********************************************
//***************************************************************************
/**
 * waDBConnection_oracle
 * 
 * classe per la connessione fisica ad un database oracle
 * 
 * <b>SPERIMENTALE!</b>
 *
 * @package waDB
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 * 
 * @ignore
 */
class waDBConnection_oracle extends waDBConnection {

    /**
     * @access protected
     * @ignore
     */
    protected $DBConn = false;

    //***************************************************************************

    /**
     * -
     * 
     * connette il database.
     * @return boolean per conoscere l'esatto esito del metodo occorre invocare 
     * il metodo {@link errorNr}
     * @ignore
     */
    function connect() {
        
        $dbConnString = "$this->WADB_HOST" . ($this->WADB_PORT ? ":$this->WADB_PORT" : "") . "/$this->WADB_DBNAME";
//        $dbConnString = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(Host=$this->WADB_HOST)(Port=$this->WADB_PORT)))(CONNECT_DATA=(SID=$this->WADB_DBNAME)))";
//        $dbConnString = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(Host=$this->WADB_HOST)(Port=$this->WADB_PORT)))(CONNECT_DATA=(SERVICE_NAME=$this->WADB_DBNAME)))";

        $this->DBConn = oci_connect($this->WADB_USERNAME, $this->WADB_PASSWORD, $dbConnString);
        if ($this->DBConn === false) {
            return false;
        }

        $this->execute("SET AUTOCOMMIT=1");
        $this->execute("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD'");
        $this->execute("ALTER SESSION SET NLS_TIMESTAMP_FORMAT ='YYYY-MM-DD HH24:MI:SS'");
        return true;
    }

    //***************************************************************************

    /**
     * -
     * 
     * disconnette il database (abortisce eventuali transazioni non committate).
     * @return void
     * @ignore
     */
    function disconnect() {
        if ($this->DBConn === false) {
            return;
        }
        @oci_close($this->DBConn);
        $this->DBConn = false;
    }

    //***************************************************************************

    /**
     * -
     * 
     * ritorna l'ultimo codice di errore restituito dal database.
     * @return string
     * @ignore
     */
    function errorNr() {
        if (!$this->DBConn) {
            return -1;
        }
        $retval = @oci_error($this->DBConn);
        if ($retval === false) {
            return 0;
        }
        return $retval['code'];
    }

    //***************************************************************************

    /**
     * -
     * 
     * ritorna l'ultimo messaggio di errore restituito dal database.
     * @return string
     * @ignore
     */
    function errorMessage() {
        if (!$this->DBConn) {
            return "no connection";
        }
        $retval = @oci_error($this->DBConn);
        if ($retval === false)
            return '';
        return $retval['message'];
    }

    //***************************************************************************

    /**
     * -
     *
     * Esegue un comando SQL sul database connesso.
     * @param string $sql SQL da eseguire
     * @return mixed i dati grezzi ottenuti dalla query o FALSE in caso di errore;
     * per conoscere l'esatto esito del metodo occorre invocare 
     * il metodo {@link errorNr}
     * @ignore
     */
    function execute($sql) {
        $qid = @oci_parse($this->DBConn, $sql);
        if ($qid === false) {
            return false;
        }
        if (!@oci_execute($qid)) {
            return false;
        }

        $retval = array();
        while ($riga = @oci_fetch_assoc($qid)) {
            $retval[] = $riga;
        }

        @oci_free_statement($qid);
        return $retval;
    }

    //***************************************************************************

    /**
     * -
     *
     * Inizia una transazione.
     * @return void
     * @ignore
     */
    function beginTransaction() {
        $this->execute('SET AUTOCOMMIT=0');
        $this->execute('BEGIN');
    }

    //***************************************************************************

    /**
     * -
     *
     * Conferma una transazione aperta in precedenza con {@link beginTransaction}.
     * @return void
     * @ignore
     */
    function commitTransaction() {
        $this->execute('COMMIT');
        $this->execute('SET AUTOCOMMIT=1');
    }

    //***************************************************************************

    /**
     * -
     *
     * Annulla una transazione aperta in precedenza con {@link beginTransaction}.
     * @return void
     * @ignore
     */
    function rollbackTransaction() {
        $this->execute('ROLLBACK');
        $this->execute('SET AUTOCOMMIT=1');
    }

    //***************************************************************************

    /**
     * -
     *
     * ritorna l'ultimo identifica univoco inserito nel database a fronte di INSERT
     * (posto che la tabella sia dotata di una chiave primaria autoincrementale).
     * @return integer
     * @ignore
     */
    function lastInsertedId() {
        return oracle_insert_id($this->DBConn);
    }

    //***************************************************************************

    /**
     * -
     *
     * trasforma una data "since-the-epoch" nel formato SQL DATE richiesto dal database.
     * @param integer $data data in formato "since-the-epoch"
     * @return string la data in formato SQL
     * @ignore
     */
    function sqlDate($data) {
        if (is_null($data)) {
            return $this->sqlNull();
        }
        return date("'Y-m-d'", $data);
    }

    //***************************************************************************

    /**
     * -
     *
     * trasforma una data/ora "since-the-epoch" nel formato SQL DATETIME richiesto
     * dal database.
     * @param integer $dataOra data/ora in formato "since-the-epoch"
     * @return string la data/ora in formato SQL
     * @ignore
     */
    function sqlDateTime($dataOra) {
        if (is_null($dataOra)) {
            return $this->sqlNull();
        }
        return date("'Y-m-d H.i.s'", $dataOra);
    }

    //***************************************************************************

    /**
     * -
     *
     * trasforma una ora "since-the-epoch" (ignorando anno, mese, giorno) nel 
     * formato SQL TIME richiesto dal database.
     * @param integer $ora ora in formato "since-the-epoch"
     * @return string l'ora in formato SQL
     * @ignore
     */
    function sqlTime($ora) {
        if (is_null($ora)) {
            return $this->sqlNull();
        }
        return "'1980-01-01 " . date("H.i.s'", $ora);
    }

    //***************************************************************************

    /**
     * -
     *
     * trasforma una stringa nel formato SQL richiesto dal database.
     * @param string $stringa  stringa da convertire
     * @return string  stringa convertita
     * @ignore
     */
    function sqlString($stringa) {
        if (is_null($stringa)) {
            return $this->sqlNull();
        }
        // da sistemare!!!!!
        return "'" . str_replace("'", "''", $stringa) . "'";
    }

    //***************************************************************************

    /**
     * -
     *
     * trasforma un intero nel formato SQL richiesto dal database.
     * @param integer $intero  intero da convertire
     * @return string  stringa convertita
     * @ignore
     */
    function sqlInteger($intero) {
        if (is_null($intero) || $intero === '') {
            return $this->sqlNull();
        }
        return $intero . "";
    }

    //***************************************************************************

    /**
     * -
     *
     * trasforma un numero decimale nel formato SQL richiesto dal database.
     * @param float $decimale  idecimale da convertire
     * @return string  stringa convertita
     * @ignore
     */
    function sqlDecimal($decimale) {
        if (is_null($decimale) || $decimale === '') {
            return $this->sqlNull();
        }
        return $decimale . "";
    }

    //***************************************************************************

    /**
     * -
     *
     * restituisce il valore NULL come richiesto dal db.
     * @return string  
     * @ignore
     */
    function sqlNull() {
        return 'NULL';
    }

    //***************************************************************************
    //******* inizio metodi semi-protected ****************************************
    //***************************************************************************
    //***************************************************************************

    /**
     * -
     *
     * prende un campo come arriva da PHP e lo converte nel formato SQL
     * il metodo non e' documentato perche' ha senso se chiamato solo dalla
     * classe waRecordset
     * @param mixed $dato dato in formato PHP  da convertire
     * @param string $tipoDB tipo del campo sul database
     * @return string valore da inserire nella query SQL
     * @ignore
     */
    function sqlValue($valore, $tipoDB) {

        switch ($tipoDB) {
            case 'DATE':
            case 'TIMESTAMP':
                return $this->sqlDateTime($valore);

            case 'NUMBER':
            case 'NUMERIC':
            case 'DEC':
            case 'DECIMAL':
            case 'BINARY_DOUBLE':
            case 'BINARY_FLOAT':
                // qui forse occorre fare una verifica dello scale...
                return $this->sqlDecimal($valore);

            case 'CHAR':
            case 'NCHAR':
            case 'RAW':
            case 'VARCHAR2':
            case 'NVARCHAR2':
            case 'BLOB':
            case 'CLOB':
            case 'NCLOB':
            case 'BFILE':
            case 'MDSYS.SDO_GEOMETRY':
            default:
                return $this->sqlString($valore);
        }
    }

    //***************************************************************************

    /**
     * -
     *
     * prende un campo come arriva da db e lo converte nel formato usabile da PHP
     * il metodo non e' documentato perche' ha senso se chiamato solo dalla
     * classe waRecordset
     * @param mixed $dato dato in formato DB  da convertire
     * @param string $tipoDB tipo del campo sul database
     * @return mixed  valore PHP
     * @ignore
     */
    function fieldConvert($campo, $tipoDB) {
        if (is_null($campo)) {
            return $campo;
        }
        switch ($tipoDB) {
            case 'DATE':
                list($anno, $mese, $giorno) = explode("-", $campo);
                return mktime(0, 0, 0, $mese * 1, $giorno * 1, $anno * 1);
            case 'TIMESTAMP':
                list($data, $ore) = explode(" ", $campo);
                list($anno, $mese, $giorno) = explode("-", $data);
                list($ora, $min, $sec) = explode(":", $ore);
                return mktime($ora * 1, $min * 1, $sec * 1, $mese * 1, $giorno * 1, $anno * 1);

            case 'NUMBER':
            case 'NUMERIC':
            case 'DEC':
            case 'DECIMAL':
            case 'BINARY_DOUBLE':
            case 'BINARY_FLOAT':
                return ($campo * 1);

            default:
                return $campo;
        }
    }

    //***************************************************************************

    /**
     * - 
     *
     * in pratica e' un duplicato di @{link esegui}, salvo che viene utilizzata
     * dal recordset per passare e ricevere piu' informazioni
     * il metodo non e' documentato perche' ha senso se chiamato solo dalla
     * classe waRecordset
     * @param string $sql query sql
     * @param int $nrRighe numero max di records che la query deve restituire
     * @param int $rigaIniziale numero di records di cui effettuare lo skip (offset)
     * @return array : 
     * - nel primo elemento array info colonne; 
     * - nel secondo elemento array dei values cosi' come ritornati dal db (crudi)
     * - nel terzo elemento il nr. di records che soddisfano le condizioni, 
     *   indipendentemente dal limit imposto
     * @ignore
     */
    function extendedExecute($sql, $nrRighe = null, $rigaIniziale = 0) {

        $mySql = $sql;
        if ($nrRighe !== null) {
            // modifichiamo la query, che ovviamente DEVE essere una select,
            // affinche' oracle possa tornare il nr di records
            // che soddisfano la condizione, indipendentemente dal limit imposto
            $mySql .= " OFFSET $rigaIniziale ROWS FETCH NEXT $nrRighe ROWS ONLY";
        }

        $qid = oci_parse($this->DBConn, $mySql);
        if ($qid === false) {
            return false;
        }
        if (!oci_execute($qid)) {
            return false;
        }

        // carichiamo le informazioni delle columns
        $colInfos = array();
        $nrColonne = oci_num_fields($qid);
        for ($i = 0; $i < $nrColonne; $i++) {
            $colInfos[] = $this->setColumnAttribs($qid, $i);
        }

        // carichiamo i records
        $righeCrude = [];
//        while ($rigaCruda = @oci_fetch_array($qid, OCI_NUM | OCI_RETURN_NULLS | OCI_RETURN_LOBS)) {
        while ($rigaCruda = oci_fetch_array($qid, OCI_NUM | OCI_RETURN_NULLS)) {
            $righeCrude[] = $rigaCruda;
        }

        oci_free_statement($qid);

        // contiamo i record
        if ($nrRighe === null) {
            $nrRigheNoLimit = count($righeCrude);
        } elseif ($nrRighe !== 0) {
            $mySql = " SELECT count(*) as wadb_found_rows FROM ($sql)";
            $response = $this->execute($mySql);
            if (!$response) {
                return false;
            }
            $nrRigheNoLimit = $response[0]["WADB_FOUND_ROWS"];
        }

        return array($colInfos, $righeCrude, $nrRigheNoLimit);
    }

    //***************************************************************************
    //******* inizio metodi protected *********************************************
    //***************************************************************************
    //***************************************************************************

    /**
     * -
     *
     * restituisce il meta-tipo (ossia il tipo applicativo) di un campo del db
     * @param string $tipoDB tipo del campo sul database
     * @return string  meta-tipo
     * @access protected
     * @ignore
     */
    protected function getApplicationFieldType($tipoDB) {
        switch ($tipoDB) {
            case 'BLOB':
            case 'CLOB':
            case 'NCLOB':
            case 'BFILE':
                return waDB::CONTAINER;

            case 'DATE':
            case 'TIMESTAMP':
                return waDB::DATETIME;

            case 'NUMBER':
            case 'NUMERIC':
            case 'DEC':
            case 'DECIMAL':
            case 'BINARY_DOUBLE':
            case 'BINARY_FLOAT':
                return waDB::DECIMAL;

            case 'CHAR':
            case 'NCHAR':
            case 'RAW':
            case 'VARCHAR2':
            case 'NVARCHAR2':
            case 'MDSYS.SDO_GEOMETRY':
            default:
                return waDB::STRING;
        }
    }

    //***************************************************************************

    /**
     * -
     *
     * restituisce le info di una colonna
     * @return array info colonna
     * @access protected
     * @ignore
     */
    protected function setColumnAttribs($queryId, $colIdx) {
        // attenzione: non puÃ² funzionare cosÃ¬!
        $col['tableAbsName'] = $colInfo->table;
        $col['table'] = $colInfo->table;
        $col['dbName'] = $this->WADB_DBNAME;

        $col['name'] = oci_field_name($queryId, $colIdx + 1);
        $col['index'] = $colIdx;
        $col['dbType'] = strtoupper(oci_field_type($queryId, $colIdx + 1));

        $col['primaryKey'] = $colInfo->primary_key;
        $col['maxLength'] = oci_field_size($queryId, $colIdx + 1);
        $col['type'] = $this->getApplicationFieldType($col['dbType']);
        if ($col['type'] == waDB::DECIMAL) {
            $scale = oci_field_scale($queryId, $colIdx + 1);
            if ($scale <= 0) {
                $col['type'] = waDB::INTEGER;
            }
        }
        return $col;
    }

    //***************************************************************************

    /**
     * -
     *
     * distruttore
     * @ignore
     */
    function __destruct() {
        $this->disconnect();
    }

//***************************************************************************
//****  fine classe waDBConnection_oracle *****************************************
//***************************************************************************
}

// fine classe 


	
	