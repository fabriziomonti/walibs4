<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* @ignore
*/
require_once __DIR__ . '/waPositionCalculator.php';

/**
* @ignore
*/
class waPHPSQLParser extends \PHPSQLParser\PHPSQLParser
	{

	/**
	 * It parses the given SQL statement and generates a detailled 
	 * output array for every part of the statement. The method can 
	 * also generate [position] fields within the output, which hold 
	 * the character position for every statement part. The calculation 
	 * of the positions needs some time, if you don't need positions in
	 * your application, set the parameter to false.
	 * 
	 * @param String  $sql           The SQL statement.
	 * @param boolean $calcPositions True, if the output should contain [position], false otherwise.
	 * 
	 * @return array An associative array with all meta information about the SQL statement.
	 */
	public function parse($sql, $calcPositions = false)
		{
		parent::parse($sql);
		
		// calc the positions of some important tokens
		if ($calcPositions)
			{
			$calculator = new waPositionCalculator();
			$this->parsed = $calculator->setPositionsWithinSQL($sql, $this->parsed);
			}

		return $this->parsed;
		}

	}
