<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;
use PDO;
use PDOStatement;

/**
* @ignore
*/
require_once(__DIR__. '/wadbdriver_mysql_pdo.class.php');

//***************************************************************************
//****  classe waDBConnection_mysql_pdo2 ***************************************
//***************************************************************************
/**
* waDBConnection_mysql_pdo2
* 
* classe per la connessione fisica ad un database mysql-pdo : versione senza sql_calc_found_rows
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* @ignore
*/
class waDBConnection_mysql_pdo2 extends waDBConnection_mysql_pdo 
	{
	
	//***************************************************************************
	/**
	* - 
	*
	* in pratica e' un duplicato di @{link esegui}, salvo che viene utilizzata
	* dal recordset per passare e ricevere piu' informazioni
	* il metodo non e' documentato perche' ha senso se chiamato solo dalla
	* classe waRecordset
	* @param string $sql query sql
	* @param int $nrRighe numero max di records che la query deve restituire
	* @param int $rigaIniziale numero di records di cui effettuare lo skip (offset)
	* @return array : 
	* - nel primo elemento array info colonne; 
	* - nel secondo elemento array dei values cosi' come ritornati dal db (crudi)
	* - nel terzo elemento il nr. di records che soddisfano le condizioni, 
	*   indipendentemente dal limit imposto
	* @ignore
	*/ 
	function extendedExecute($sql, $nrRighe = null, $rigaIniziale = 0)
		{
		// crea i parapmetri per il prepared statement
		list($sql, $params) = $this->prepare($sql);
		
		if ($nrRighe !== null)
			{
			$sql .= " LIMIT $rigaIniziale, $nrRighe";
			}
 		$this->statement = $this->pdo->prepare($sql);
		if ($this->statement === false)
			{
			return false;
			}
		$exec_result = $this->statement->execute($params);
		if ($exec_result === false)
			{
			return false;
			}

		// carichiamo le informazioni delle columns
		$colInfos = [];
		$nrColonne = $this->statement->columnCount();	
		$mainTable = "";
		for ($i = 0; $i < $nrColonne; $i++)
			{
			$colInfo = $this->setColumnAttribs($this->statement, $i);
			$colInfos[] = $colInfo;
			if ($colInfo["primaryKey"] && !$mainTable) 
				{
				// tabella principale (si spera...) da cui prelevare il nr record 
				// nel caso non sia impostata una condizione di where
				$mainTable = $colInfo["tableAbsName"];
				}
			}

		// carichiamo i records
		$this->statement->setFetchMode(PDO::FETCH_NUM);			
		$righeCrude = $this->statement->fetchAll();
		$this->statement->closeCursor();
		
		if ($nrRighe !== null)
			{
			if ($nrRighe) 
				{
				// se e' stato richiesto il limit, allora andiamo anche a prelevare
				// il nr di records che soddisfano la condizione
				
				// se la query ha una condizione di where allora contiamo i risultati 
				// (se la where non ? basata su chiavi e la tabella ha molti record potrebbe essere lentissimo)
				$do_count = true;
				
				$query_elems = $this->splitSql($sql);
				if (strlen(trim($query_elems->where)) || strlen(trim($query_elems->having) || !$mainTable)) 
					{
					$count_sql = "select count(*) $query_elems->from $query_elems->where $query_elems->group $query_elems->having";
					$this->statement = $this->pdo->prepare($count_sql);
					$exec_result = $this->statement->execute($params);
					$this->statement->setFetchMode(PDO::FETCH_NUM);
					list($nrRigheNoLimit) = $this->statement->fetch();
					$nrRigheNoLimit *= 1;
					$this->statement->closeCursor();
					}
				else 
					{
					$this->statement = $this->pdo->query("show table status where Name='$mainTable'");
					$this->statement->setFetchMode(PDO::FETCH_ASSOC);
					$table_status = $this->statement->fetch();
					$nrRigheNoLimit = $table_status["Rows"] * 1;
					$this->statement->closeCursor();
					}
				}
			else
				{
				$nrRigheNoLimit = 0;
				}
			}
		else
			{
			$nrRigheNoLimit = count($righeCrude);
			}

		return array($colInfos, $righeCrude, $nrRigheNoLimit);
		
		}
		

//***************************************************************************
//****  fine classe waDBConnection_mysql_pdo2 **********************************
//***************************************************************************
	}	// fine classe 


