<?php
/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

//***************************************************************************
//***************************************************************************
//***************************************************************************
/**
* waRecord
* 
* Classe per la gestione di una riga di database
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waRecord
	{
	/**
	*
	* Oggetto {@link waRecordset} che contiene la riga in oggetto
	* @var waRecordset 
	*/ 
	public 	$recordset;

	/**
	*
	* Ordinale della riga all'interno del recordset
	* @var int 
	*/ 
	public $recordIndex;
	
	//***********************************************************************
	/**
	* -
	* 
	* costruttore.
	* @param waRecordset $recordset istanza della classe per la gestione del 
	* recordset che contiene la riga in oggetto
	* @param int $recordIndex ordinale della riga all'interno del recordset
	*  connessione al database
	* @ignore
	*/ 
	public function __construct ($recordset, $recordIndex)
		{
		$this->recordset = $recordset;
		$this->recordIndex = $recordIndex;
		}
		
	//***********************************************************************
	/**
	* -
	* 
	* Dato, indifferentemente, il nome o l'indice di un campo tra quelli ritornati 
	* dalla query, restituisce il valore del campo corrispondente nella riga.
	* @param mixed (string | int) $fieldNameOrIndex nome del campo o indice della colonna
	* @param boolean $asOnDb : se vero torna il value esattamente come restituito
	* dal motore del database, ossia non formattato per il normale utilizzo PHP
	* @return mixed valore del campo
	*/ 
	public function value($fieldNameOrIndex, $asOnDb = false)
		{
		$fieldIndex = $this->recordset->getColumnOrdinalFromeNameOrIndex($fieldNameOrIndex);
		return ($asOnDb ? 
				$this->recordset->rawValues[$this->recordIndex][$fieldIndex] :
				$this->recordset->values[$this->recordIndex][$fieldIndex]);
		}
		
	//***********************************************************************
	/**
	* -
	* 
	* Dato, indifferentemente, il nome o l'indice di un campo tra quelli ritornati 
	* dalla query, restituisce il valore del campo corrispondente nella riga prima
	 * di un eventuale aggiornamento
	* @param mixed (string | int) $fieldNameOrIndex nome del campo o indice della colonna
	* @return mixed valore originale del campo
	*/ 
	public function originalValue($fieldNameOrIndex)
		{
		$fieldIndex = $this->recordset->getColumnOrdinalFromeNameOrIndex($fieldNameOrIndex);
		return $this->recordset->originalValues[$this->recordIndex][$fieldIndex];
		}
		
	//***********************************************************************
	/**
	* -
	* 
	* Dato, indifferentemente, il nome o l'indice di un campo tra quelli ritornati 
	* dalla query, sostituisce nella riga il valore esistente con quello passato.
	*
	* Il valore sara' consolidato sul db a fronte della successiva chiamata al
	* metodo {@link waRecordset::salva()} dell'oggetto che contiene il record
	* @param mixed (string | int) $fieldNameOrIndex nome del campo o indice della colonna
	* @param mixed $value : il nuovo value da inserire
	* @return void
	*/ 
	function insertValue($fieldNameOrIndex, $value)
		{
		$fieldIndex = $this->recordset->getColumnOrdinalFromeNameOrIndex($fieldNameOrIndex);
		if ($this->isValueModified($value, $this->value($fieldIndex), $this->originalValue($fieldIndex)))
			{
			$this->recordset->values[$this->recordIndex][$fieldIndex] = $value;
			if ($this->recordset->recordsStatus[$this->recordIndex] == waDB::RECORD_UNMODIFIED)
				{
				$this->recordset->recordsStatus[$this->recordIndex] = waDB::RECORD_MODIFIED;
				}
			}
		}
		
	//***********************************************************************
	/**
	* -
	*
	* Marca il record per la successiva eliminazione dal db. Il record sara'
	* cancellato effettivamente dal db solo a fronte di successiva chiamata al
	* metodo {@link waRecordset::salva()} dell'oggetto che contiene il record
	* @return void
	*/ 
	function delete()
		{
		return $this->recordset->recordsStatus[$this->recordIndex] = waDB::RECORD_TO_DELETE;
		}
		
	//***********************************************************************
	/**
	* -
	*
	* restituisce true se il record è stato creato
	* @return boolean
	*/ 
	function isNew()
		{
		return $this->recordset->recordsStatus[$this->recordIndex] == waDB::RECORD_NEW;
		}
		
	//***********************************************************************
	/**
	* -
	*
	* restituisce true se il record è stato modificato
	* @return boolean
	*/ 
	function isModified()
		{
		return $this->recordset->recordsStatus[$this->recordIndex] == waDB::RECORD_MODIFIED;
		}
		
	//***********************************************************************
	/**
	* -
	*
	* restituisce true se il record è da cancellare
	* @return boolean
	*/ 
	function isToDelete()
		{
		return $this->recordset->recordsStatus[$this->recordIndex] == waDB::RECORD_TO_DELETE;
		}
		
	//***********************************************************************
	/**
	* -
	* 
	* Dato, indifferentemente, il nome o l'indice di un campo tra quelli ritornati 
	* dalla query, restituisce vero se il campo è stato modificato
	* @param mixed (string | int) $fieldNameOrIndex nome del campo o indice della colonna
	* @return boolean
	*/ 
	public function isFieldModified($fieldNameOrIndex)
		{
		$value = $this->value($fieldNameOrIndex);
		return $this->isValueModified($value, $value, $this->originalValue($fieldNameOrIndex));
		}
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce la lista dei campi modificati di un record
	* @return array
	*/
	public function getChanges()
		{
		$changes = [];
		$field_nr = $this->recordset->fieldNr();
		for ($i = 0; $i < $field_nr; $i++)
			{
			if ($this->isFieldModified($i))
				{
				$change = [];
				$change["name"] = $this->recordset->fieldName($i);
				$change["old_value"] = $this->originalValue($i);
				$change["new_value"] = $this->value($i);
				$changes[] = $change;
				}
			}
		
		return $changes;
		}
		
	//***********************************************************************
	/**
	* -
	*
	* restituisce, se esiste, il value del campo individuato da $fieldName
	 * (equivalente a invocare il medodo {@link value}
	 * 
	 * Attenzione a situazioni particolari in cui un record potrebbe contenere 
	 * lo stesso nome di una delle proprietà della classe (recordIndex, ecc.):
	 * in questo caso ciò che viene restituito è il value della proprietà,
	 * non del campo
	 * 
	* @param string $fieldName nome del campo
	* @return mixed value del campo
	*/ 
	public function __get($fieldName)
		{
		return is_numeric($fieldName) || $this->recordset->fieldIndex($fieldName) !== null ? 
				$this->value($fieldName) : 
				$this->$fieldName;
		}
		
	//***********************************************************************
	/**
	* -
	*
	* inserisce, se esiste, il value dato nel campo individuato da $fieldName
	 * (equivalente a invocare il medodo {@link insertValue}
	 * 
	 * Attenzione a situazioni particolari in cui un record potrebbe contenere 
	 * lo stesso nome di una delle proprietà della classe (recordIndex, ecc.):
	 * in questo caso ciò che viene valorizzato è il value della proprietà,
	 * non del campo
	 * 
	* @param string $fieldName nome del campo
	* @param mixed $value : il nuovo value da inserire
	* @return void
	*/ 
	public function __set($fieldName, $value)
		{
		return is_numeric($fieldName) || $this->recordset->fieldIndex($fieldName) !== null ? 
				$this->insertValue($fieldName, $value) :
				$this->$fieldName = $value;
		}

	//***********************************************************************
	/**
	* -
	* 
	* verifica che due valori siano differenti ai fini della modifica
	* @return boolean
	*/ 
	protected function isValueModified($value, $current_value, $original_value )
		{
		$changed = false;
		if (is_float($original_value) && is_float($value))
			{
			list($int, $dec) = explode(".", (string) $original_value, 2);
			$nr_dec = strlen($dec);
			$changed = bccomp($value, $original_value, $nr_dec);
			}
		else 
			{
			$changed = (string) $value !== (string) $original_value || (string) $value !== (string) $current_value;
			}
			
		return $changed;
		}
		
	}

//*****************************************************************************







