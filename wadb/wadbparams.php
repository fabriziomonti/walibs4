<?php

/**
* -
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
namespace waLibs;

//***************************************************************************
/**
* classe pojo da utilizzare per il passaggio dei parametri di connessione al 
 * driver 
 * 
*
* @package waDB
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waDBPparams {
	var $DBTYPE = "";
	var $HOST = "";
	var $USERNAME = "";
	var $PASSWORD = "";
	var $DBNAME = "";
	var $PORT = "";
	var $LOGNAME = "";
	var $LOG_CALLBACK_FNC;
}
