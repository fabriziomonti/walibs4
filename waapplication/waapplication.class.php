<?php
/**
* waApplication
*
* classe contenenente i metodi e le proprieta' comuni di una applicazione
* standard
* 
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* @ignore
*/
include_once __DIR__ . "/../vendor/autoload.php";
	
/**
* @ignore
*/
include_once  __DIR__ . "/i_waApplicationView.interface.php";

/**
* waApplication
*
* classe contenenente i metodi e le proprieta' comuni di una applicazione
* standard
* 
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waApplication
	{
	//codici modalita' di navigazione
	/**
	* -
	* codice della modalita' di navigazione su singola pagina
	* 
	* comunica alla ui che l'applicazione prevede una navigazione
	* in cui ogni finestra figlia si apra nella stessa
	* "window" della mamma
	* 
	* solamente la UI sa come comportarsi a fronte di questa informazione: il
	* comportamento di waApplication non è in alcun modo modificato da questo
	* valore
	*/
	const NAV_PAGE =	'WAAPPLICATION_NAV_PAGE';

	/**
	* -
	* codice della modalita' di navigazione su finestra esterna alla pagina
	* 
	* comunica alla ui che l'applicazione prevede una navigazione
	* in cui ogni pagina figlia si apra in un 
	* una nuova "window" che sara' figlia della mamma
	* 
	* solamente la UI sa come comportarsi a fronte di questa informazione: il
	* comportamento di waApplication non è in alcun modo modificato da questo
	* valore
	*/
	const NAV_WINDOW =	'WAAPPLICATION_NAV_WINDOW';

	/**
	* -
	* codice della modalita' di navigazione su iframe interno alla pagina
	* 
	* comunica alla ui che l'applicazione prevede una navigazione
	* in cui ogni pagina figlia si apra in un 
	* una nuova "iFrame" che sara' figlia della mamma
	* 
	* solamente la UI sa come comportarsi a fronte di questa informazione: il
	* comportamento di waApplication non è in alcun modo modificato da questo
	* valore
	*/
	const NAV_INNER =	'WAAPPLICATION_NAV_INNER';

	/**
	* dominio http della'applicazione
	* salvo esigenze particolari la proprietà viene determinata automaticamente
	* @var string
	*/	
	public 	$domain;
	
	/**
	* directory di lavoro del file system dell'applicazione
	* salvo esigenze particolari la proprietà viene determinata automaticamente
	* @var string
	*/	
	public 	$cwd;				// directory di lavoro del file system
	
	/**
	* directory di lavoro http dell'applicazione; 
	* salvo esigenze particolari la proprietà viene determinata automaticamente
	* @var string
	*/	
	public 	$httpwd = false;			// directory di lavoro http (non cancellare l'inizializzazione a false!!!)
	
	/**
	* directory del file-system da usare come temporanea dell'applicazione
	* @var string
	*/	
	public 	$directoryTmp;
	
	/**
	* oggetto view dell'applicazione
	* 
	* l'oggetto view è quello che implementa l'interfaccia i_waApplicationView
	* e al quale saranno passati i dati per la costruzione dell'interfaccia 
	* utente
	* 
	* se non definito viene usato il default 
	* 
	* \waLibs\views\waApplication\waApplication_default\waApplicationView
	* 
	* contenuto in uis/waapplication_default/view/waapplication.php
	* 
	* @var object
	*/	
	public 	$view = null;
	
	/**
	* nome programmatico dell'applicazione 
	* qualcosa di breve (max 10 caratteri) senza spazi ne' cartteri strani; 
	* l'idea e' quella di un nome di variabile da usare internamente
	* @var string
	*/	
	public 	$name;
	
	/**
	* titolo dell'applicazione, formale da mostrare all'utente e non per uso interno
	* @var string
	*/	
	public 	$title;
	
	/**
	* versione dell'applicazione
	* @var string
	*/	
	public 	$version;
	
	/**
	* data version applicazione (formato since-the-epoch)
	* @var int
	*/	
	public 	$versionDate;
	
	/**
	* server per invio della posta elettronica
	* @var string
	*/	
	public 	$smtpServer;
	
	/**
	* utente smtp per invio della posta elettronica
	* @var string
	*/	
	public 	$smtpUser;
	
	/**
	* password smtp per invio della posta elettronica
	* @var string
	*/	
	public 	$smtpPassword;
	
	/**
	* protocollo di sicurezza su smtp (ssl/tls)
	* se vuoto nessun encryption
	* @var string
	*/	
	public 	$smtpSecurity;

	/**
	* porta smtp; se vuoto si usa il default (25)
	* @var string
	*/	
	public 	$smtpPort;

	/**
	* indirizzo email per il supporto dell'applicazione
	* @var string
	*/	
	public 	$supportEmail;
	
	/**
	* indirizzo email per le info sull'applicazione
	* @var string
	*/	
	public 	$infoEmail;
	
	/**
	* indica se l'applicazione usa la sessione di PHP
	* 
	* se true, allora
	* e' la classe waApplication stessa che si preoccupa di fare partire
	* la sessione tramite il proprio metodo {@link init}.
	* @var boolean
	*/	
	public 	$useSession = true;
	
	/**
	* -
	* per la classe questa proprieta' non ha alcun significato; si limita
	* semplicemente a passare l'informazione al {@link $view view-object}, il quale, sulla base
	* di questa informazione, puo' decidere in che modalita' far navigare
	* l'utente
	* @var string
	*/	
	public 	$navigationMode = waApplication::NAV_PAGE;
	
	/**
	* oggetto di classe waDocumentation che provvede a creare la documentazione delle pagine visitate.
	* 
	* L'istanziazione dell'oggetto è a carico dell'applicazione; waApplication
	* si limita, qualora l'oggetto sia stato creato, ad effettuare le chiamate 
	* di richiesta documentazione delle pagine visitate.
	* <br/><br/>
	* Ovviamente l'oggetto deve essere istanziato solo in ambiente di sviluppo,
	* mai in produzione, per ovvi motivi di performance.
	* <br/><br/>
	* Si noti che in fase di creazione dell'istanza occorre passare al
	* costruttore un file di configurazione waDB: questo potrà essere il 
	* medesimo utilizzato dall'applicazione, ma in questo caso le tabelle di 
	* documentazione verranno creato all'interno del DB designato dal file di 
	* configurazione, ossia il medesimo dell'applicazione.
	* 
	* @var waDocumentation
	*/
	public $waDoc = null;
	
	/**
	* -
	* proprietà che è possibile valorizzare qualora l'applicazione sia suddivisa
	* in più sezioni (es: gestione, valutazione, certificazione, calendari,
	* pagamenti, ecc.). Ogni sottoclasse di waApplication può definire tramite
	* questa proprietà il proprio titolo. 
	* <br/><br/>
	* La proprietà viene utilizzata dalla classe waDocumentation per 
	* suddividere le pagine dell'applicazione all'interno della propria sezione.
	* <br/><br/>
	* Se non valorizzata, la proprietà assumerà il value della proprietà
	* {@link $title}.
	* 
	* @var string
	*/	
	public 	$sectionTitle = '';
	
	/**
	* -
	* proprietà che è possibile valorizzare qualora l'applicazione sia suddivisa
	* in più sezioni (es: gestione, valutazione, certificazione, calendari,
	* pagamenti, ecc.). Ogni sottoclasse di waApplication può definire tramite
	* questa proprietà la propria sigla (name breve). 
	* <br/><br/>
	* La proprietà viene utilizzata dalla classe waDocumentation per 
	* suddividere le pagine dell'applicazione all'interno della propria sezione.
	* <br/><br/>
	* Se non valorizzata, la proprietà assumerà il value della proprietà
	* {@link $name}
	* 
	* @var string
	*/	
	public 	$sectionName = '';
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language = 'it';
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale = 'it-IT';
	
	//**************************************************************************
	//**************************************************************************
	//**************************************************************************
	
	/**
	* flag che indica se la pagina deve essere chiusa al momento del ritorno
	* oppure deve continuare (verosimilmente con un data-entry)
	* @var boolean
	* @ignore
	*/	
	protected 	$closePage = '';
	
	/**
	* valori che un pagina figlia ritorna alla mamma affinche' si allinei
	* alle modifiche effettuate (al momento una string base64)
	* 
	* ha senso solo per modalità navigazione != waApplication::NAV_PAGE
	* @ignore
	* @var array
	*/	
	protected 	$returnValues = '';
	
	/**
	* array degli oggetti (ma possono anche essere semplici stringhe)
	* che devono essere mandati in output (la sequenza la decide il {@link $view view-object})
	* @ignore
	* @var array
	*/	
	protected 	$items = array();
	
	/**
	* buffer utilizzato per contenere l'output
	*
	* @access protected
	* @ignore
	* @var waApplicationData
	*/	
	protected	$data; 
	
	//*****************************************************************************
	/**
	* inizializza l'applicazione
	* 
	* effettua l'include delle classi standard
	* (waDB, waMenu, waForm, waTable) e fa eventualmente partire la sessione
	* (se {@link $useSession} = true).
	* <br/><br/>
	* Di fatto questo metodo sostituisce il costruttore della classe (costruttore
	* che la classe non possiede), in modo che la chiamata sia sempre esplicita
	* e posizionata nel momento che il programmatore ritiene più opportuno.
	* <br/><br/>
	* Questo metodo deve essere sempre invocato esplicitamente dalla classe 
	* applicazione derivata, anche eventualmente nel proprio costruttore.
	* @return void
	*/
	public function init()
		{
		$this->domain = $_SERVER['HTTP_HOST'];
		//definisce la working-directory http dell'applicazione; se per qualche 
		//motivo viene fuori un risultato sbagliato, allora il programmatore
		// dovrà valorizzare autonomamente la proprietà (prima o dopo la 
		// chiamata a init() non fa differenza)
		$this->httpwd = $this->getHttpWd();
		$this->cwd = $this->getCwd();
		if ($this->useSession)
			{
			session_start();
			}
		$this->includeClasses();	

		}
	
	//***************************************************************************
	/**
	* mostra un messaggio e termina l'esecuzione dello script corrente
	* @param string $title intestazione del messaggio
	* @param string $message testo del message da mostrare
	* @param boolean $back comanda alla UI di mostrare un bottone o analogo per tornare alla pagina precedente
	* @param boolean $close comanda alla UI di mostrare un bottone o analogo per chiudere la finestra corrente
	* @return void
	*/
	public function showMessage($title, $message, $back = true, $close = false)
		{
		// eventuali items già aggiunti vanno eliminati
		$this->items = array();
		
		$this->addItem($title, "title");
		$this->addItem($message, "message");
		if ($back)
			{
			$this->addItem('', "waapplication_action_back");
			}
		if ($close)
			{
			$this->addItem('', "waapplication_action_close");
			}
		$this->show();
	   exit();
		}
		
	//***************************************************************************
	/**
	* mostra un messaggio di errore generato dal database e termina l'esecuzione dello script corrente
	* @param waDBConnection $dbConnection oggetto contenente la connessione al DB
	* @return void
	*/
	public function showDBError($dbConnection)
		{
	   $this->showMessage (
	   				"Errore di sistema",
						"Sono spiacente, ma hai trovato un errore di sistema e non riesco a" .
						" soddisfare la tua richiesta. Per cortesia, riprova piu' tardi, grazie.<P>[" .
						$dbConnection->errorNr() . "] " . 
						$dbConnection->errorMessage());
		}
	
	//***************************************************************************
	/**
	* mostra un messaggio di errore a fronte della violazione di obbligatorietà e termina l'esecuzione dello script corrente
	* @return void
	*/
	public function showMandatoryError()
		{
	   $this->showMessage (
				"Errore di compilazione",
				"Mancano campi obbligatori: non e' possibile eseguire la richiesta.<P>" .
				"I campi contrassegnati dall'asterisco devono essere " .
				"obbligatoriamente compilati, altrimenti la richiesta " .
				"non verra' eseguita");
		}
	
	
	//***************************************************************************
	/**
	* restiuisce una connessione al database 
	* 
	* in caso di errore, termina 
	* l'esecuzione dello script invocando il metodo {@link showDBError}
	* @param string $configFile name del file di configurazione del package waDB; 
	* se non specificato o se vuoto, verrà utilizzato il file di configurazione 
	* di default di waDB, che è vuoto: di fatto, quindi, il parametro deve 
	* essere sempre valorizzato correttamente
	* 
	* @return waDBConnection
	*/
	public function getDBConnection($configFile = null)
		{
		$dbConnection = wadb_getConnection($configFile);
		if ($dbConnection === null || $dbConnection === false)
			{
			$this->showMessage("Fallita connessione db", "Fallita connessione db");
			}
		if ($dbConnection->errorNr())
			{
			$this->showDBError($dbConnection);
			}
		return $dbConnection;
		}		
	
	//***************************************************************************
	/**
	* restituisce un oggetto di classe waRecordset caricato con le righe ritornate dalla query
	* 
	* in caso di errore, termina l'esecuzione dello script
	* invocando il metodo {@link showDBError}
	* @param string $sql query sql da eseguire
	* @param waDBConnection $dbConnection connessione al db; puo' essere vuoto: 
	* in questo caso viene valorizzato dal metodo tramite chiamata a {@link getDBConnection} 
	* e l'oggetto diventa utilizzabile anche dal chiamante
	* @param int $recordsNr numero max di records che la query deve restituire
	* @param int $skip numero di records di cui effettuare lo skip (offset)
	* @return waRecordset oggetto di classe waRecordset caricato con i records ritornate dalla query
	*/
	public function getRecordset($sql, $dbConnection = null, $recordsNr = null, $skip = 0)
		{
		$dbConnection = empty($dbConnection) ? 
							$this->getDBConnection() : 
							$dbConnection;
		$recordset = new waRecordset($dbConnection);
		$recordset->read($sql, $recordsNr, $skip);
		if ($recordset->errorNr())
			{
			$this->showDBError($dbConnection);
			}

		return $recordset;
		}
		
	//*************************************************************************
	/**
	* funzione di esecuzione standard di una query (evidentemente di edit)
	* 
	* in caso di errore, termina l'esecuzione dello script
	* invocando il metodo {@link showDBError}
	* @param string $sql
	* @param waDBConnection $dbConnection
	*/
	public function dbExecute($sql, waDBConnection $dbConnection = null)
		{		
		$dbConnection = empty($dbConnection) ? 
							$this->getDBConnection() : 
							$dbConnection;
		$dbConnection->execute($sql);
		if ($dbConnection->errorNr())
			{
			$this->showDBError($dbConnection);
			}
		}
	
	//***************************************************************************
	/**
	* consolida su db le modifiche avvenute su un recordset waRecordset
	* 
	* in caso di errore, termina l'esecuzione dello script
	* invocando il metodo {@link showDBError}
	* @param waRecordset $recordset recordset da consolidare
	*/
	public function saveRecordset(waRecordset $recordset)
		{
		$recordset->save();
		if ($recordset->errorNr())
			{
			$this->showDBError($recordset->dbConnection);
			}
		}
		
	
	//***************************************************************************
	/**
	* invia un messaggio email
	* 
	* attenzione: necessita della classe phpmailer come previsto dalla 
	* configurazione di composer
	* 
	* @param mixed $to destinatari/o; puo' essere un solo indirizzo email 
	* (string) o 'n' indirizzi email (array di stringhe)
	* @param string $subject subject del messaggio
	* @param string $body body del messaggio
	* @param mixed $from mittente del messaggio; puo' essere una stringa 
	* (indirizzo email) o un array di stringhe in cui il primo elemento e'
	* l'indirizzo email ed il secondo il name; se vuoto, viene utilizzato per
	* l'indirizzo il valore della proprieta' {@link supportEmail} e per il nome
	* il valore della proprieta' {@link title}
	* @param mixed $cc destinatari/o  in carbon-copy; puo' essere un solo indirizzo email 
	* (string) o 'n' indirizzi email (array di stringhe)
	* @param mixed $bcc destinatari/o  in blind-carbon-copy; puo' essere un solo indirizzo email 
	* (string) o 'n' indirizzi email (array di stringhe)
	* @param mixed $attachments file allegato al messaggio; puo' essere un solo name file
	* (string) o 'n' nomi file (array di stringhe)
	* @param boolean $isHtml flag che indica se il messaggio deve essere inviato 
	* in formato HTML
	* @param boolean $returnErrorMessage flag che indica se l'esito deve essere
	 * un semplice boolean o l'eventuale stringa di errore; attenzione perchè
	 * la logica del valore di ritorno viene invertita
	 * @param array $customHeaders array degli header personalizzati
	* @return mixed:  se $returnErrorMessage == false : true = ok; altrimenti "" = ok
	*/
	public function sendMail($to, 
						$subject, 
						$body = "", 
						$from = "", 
						$cc = "", 
						$bcc = "", 
						$attachments = "",
						$isHtml = false,
						$returnErrorMessage = false,
						$customHeaders = [],
						$sender = "")
						
		{
		$mail = new \PHPMailer(true);
		$mail->CharSet = "UTF8";
		$mail->SMTPAutoTLS = false;
		$mail->Timeout = 10;
		
		if (!empty($from))
			{
			if (is_array($from))
				{
				$mail->From = $from[0];
				$mail->FromName = $from[1];
				}
			elseif (is_object($from))
				{
				$mail->From = $from->address;
				$mail->FromName = $from->name;
				}
			else
				{
				$mail->From = $from;
				}
			}
		else
			{
			if (is_array($this->supportEmail))
				{
				$mail->From = $this->supportEmail[0];
				$mail->FromName = $this->supportEmail[1];
				}
			elseif (is_object($this->supportEmail))
				{
				$mail->From = $this->supportEmail->address;
				$mail->FromName = $this->supportEmail->name;
				}
			else
				{
				$mail->From = $this->supportEmail;
				$mail->FromName = $this->title;
				}
			}
			
		$mail->Subject = $subject;
		if (!empty($this->smtpServer))
			{
			$mail->Mailer = "smtp";
			$mail->Host = $this->smtpServer;
			if (!empty($this->smtpUser))
				{
				$mail->SMTPAuth = true;
				$mail->Username = $this->smtpUser;
				$mail->Password = $this->smtpPassword;
				if (!empty($this->smtpSecurity))
					$mail->SMTPSecure = $this->smtpSecurity;
				if (!empty($this->smtpPort))
					$mail->Port = $this->smtpPort;
				}
			}
			
		$this->addMailAddr($mail, $to, 'Address');
		$this->addMailAddr($mail, $cc, 'CC');
		$this->addMailAddr($mail, $bcc, 'BCC');
		$this->addMailAttachment($mail, $attachments);
		
		// TESTO DEL MESSAGGIO IN HTML
		$mail->IsHTML($isHtml);
		
		$mail->Body = $body;
		if (is_array($customHeaders))
			{
			foreach ($customHeaders as $name => $value)
				{
				$mail->addCustomHeader($name, $value);
				}
			}

		if ($sender)
			{
			$mail->Sender = $sender;
			}
		
		try 
			{
			$esito = @$mail->Send();
			return $returnErrorMessage ? $mail->ErrorInfo : $esito;
			}
		catch (\phpmailerException $e)
			{
			return $returnErrorMessage ? $mail->ErrorInfo : false;
			}
		catch (\Exception $e)
			{
			throw $e;
			}
			
		}
		
	//******************************************************************************
	function addMailAttachment($MailObj, $attachments)
		{
		if (!empty($attachments))
			{
			if (is_array($attachments))
				{
				foreach ($attachments as $attachment)
					{
					if (is_array($attachment))
						$MailObj->AddAttachment($attachment[0], $attachment[1]);
					else
						$MailObj->AddAttachment($attachment);
					}
				}
			else 
				{
				if (is_array($attachments))
					$MailObj->AddAttachment($attachments[0], $attachments[1]);
				else
					$MailObj->AddAttachment($attachments);
				}
			}
		}

	//***************************************************************************
	/**
	* manda in output la pagina
	* 
	* manda in output la pagina, compresi gli elementi aggiunti alla pagina 
	* tramite il metodo {@link addItem}, utilizzando il {@link $view view-object} indicato 
	* nella proprietà {@link view}.
	* @param boolean $return se false, allora viene immediatamente effettuato 
	* l'output della pagina; altrimenti la funzione ritorna il buffer di output 
	* @return void|string
	*/
	public function show($return = false)
		{
		// se e' stato istanziato l'oggetto waDoc, crea la documentazione della
		// pagina
		$this->createDoc();

		$this->createData();
		ob_start();
		
		// se non è stato passato definisce l'oggetto view di default
		$this->setView();
		$this->view->transform($this->data);
		
//		if (stripos($html, "<html") !== false)
//			// effettuiamo la correzione "strict" solo se stiamo
//			// effettivamente mandando in output un'intera pagina HTML
//			// (se non è una pagina intera - ad esempio il contenuto di un div -
//			// o se l'output non è html, allora non si fa nessun controllo 
//			// strict)
//			$html = $this->correctHTML($html);

		if ($return)
			{
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
			}	
			
		header("Content-Type: text/html; charset=utf-8");			
		ob_end_flush();
		}
		
	//***************************************************************************
	/**
	* ritorna l'oggetto contenente i dati da inviare al view-object
	 * 
	 * da usare in fase di debug per ispezionare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	* 
	* @return waApplicationData
	*/
	function getData()
		{
		$this->createData();
		return $this->data;
		}
		
	//***************************************************************************
	/**
	* manda in output i dati della pagina in formato JSON
	* 
	* da usare in fase di debug per mostrare i dati in formato JSON che vengono
	* passati al {@link $view view-object} anziche' l'output generato da 
	* {@link $view view-object} stesso 
	* 
	* @return void
	*/
	public function showJSON()
		{
		$this->createData();
		header("Content-Type: application/json; charset=utf-8");			
		echo json_encode($this->data, JSON_PRETTY_PRINT);
		}
		
	//***************************************************************************
	/**
	* ritorna alla pagina che ha chiamato la corrente
	* 
	* metodo da invocare (tipicamente) al termine dell'elaborazione di un submit: 
	* al {@link view view-object} vengono passati i valori  della risposta
	* (un esito, dei valori inputati, ciò che decide la vostra applicazione)
	* e sulla base di questi dati decidereà ooportunamente come continuare la 
	* navigazione
	* 
	* @param array $returnValues valori di ritorno da passare al {@link view view-object}
	* @param boolean $closePage flag che indica se la pagina debba essere chiusa 
	* a fronte della ricezione della presente risposta; in realtà sulla base di
	* questo valore il {@link view view-object} potra' prendere le decisioni che
	* preferisce
	* @param boolean $return anziche' mandare in output il risultato dell'azione del
	* {@link view view-object}, ritorna il buffer contenente l'output
	* 
	*/
	public function response($returnValues = false, $closePage = true, $return = false)
		{
					
		$this->closePage = $closePage ? 1 : 0;
		$returnValues = is_array($returnValues) ? $returnValues : array(0);
		// bicio_??? orrore!!!
		foreach ($returnValues as $k => $v)
			{
			$paramsToRet .= "$k=$v|||";
			}
		$this->returnValues = base64_encode($paramsToRet);
		return $this->show($return);
		}
				
	//***************************************************************************
	/**
	* aggiunge un nuovo elemento da mandare in output
	* 
	* aggiunge un nuovo elemento da mandare in output al momento della chiamata 
	* a {@link show}.
	* <br/><br/>
	* L'item potra' essere uno qualsiasi degli oggetti predefiniti dalle
	* waLibs (waMenu, waForm, waTable), oppure una qualsiasi cosa che il {@link view view-object}
	* specifico dell'applicazione sia in grado di gestire. Ad esempio: se 
	* volete mandare in output un blocco di testo libero, anche con 
	* formattazione html, l'item sara' la stringa che contiene il testo e 
	* il name dell'item potra' essere usato dal {@link view view-object} specifico 
	* dell'applicazione per metterlo in un div con classe "testolibero".
	* @param mixed $item l'oggetto da mandare in output; per gli oggetti
	*			predefiniti (waMenu, waForm, waTable) si passera' l'oggetto
	*			vero e proprio: sara' poi la classe waApplication a invocarne
	*			il metodo {@link show}. I restanti tipi di elementi (stringhe, 
	 *			stdClass, array, ecc.) dovranno essere gestiti  dal {@link view view-object}
	*			dell'applicazione
	* @param string $name e' il name dell'item che potra' essere 
	*			intercettato dal {@link view view-object}, che quindi si comportera' come da 
	*			istruzioni. Per gli oggetti conosciuti delle waLibs non e' 
	*			necessario specificare il parametro: sara' sempre utilizzato il 
	*			valore della proprieta' name dell'oggetto.
	* @param string $type e' il type di item; per gli oggetti conosciuti 
	*			delle waLibs non e' necessario specificare il parametro: sara' 
	*			sempre utilizzato il name della classe dell'oggetto (waForm, 
	*			waMenu, ecc.). Per gli items arbitrari il default è "string"
	* @return void
	*/
	public function addItem($item, $name = '', $type = 'string')
		{
		// costruiamo la struttura da salvare negli items
		$tosav = array("name" => '', "type" => '', "value" => null);
		if (is_object($item))
			{
			$tosav['name'] = $name ? $name : $item->name;
			$tosav['type'] = strtolower(get_class($item));
			}
		else
			{
			$tosav['name'] = $name ;
			$tosav['type'] = $type;
			}
			
		$tosav['value'] = $item;
		$this->items[$tosav['name']] = (object) $tosav;
		}
				
	//***************************************************************************
	/**
	* rimanda il browser ad una nuova pagina
	*
	* rimanda il browser ad una nuova pagina; questa funziona puo' essere invocata
	* solo se non e' stato effettuato alcun output, altrimenti verra' generato
	* un errore da parte dell'engine PHP
	* @param string $iURL indirizzo a cui ridirezionare il browser
	* @return void
	*/
	public function redirect ($iURL)
		{
	   header("Status: 301 Redirect");
	   header("Location: $iURL");
	   exit();
		}
		
	//***************************************************************************
	/**
	* mostra il contenuto di un oggetto o di un array
	*
	* mostra il contenuto di un oggetto o di un array; da usare solo in fase di 
	* debug
	* @param mixed $obj oggetto o array da mostrare
	* @return void
	*/
	function showObject($obj)
		{
		echo "<pre>" . print_r($obj, true) . "</pre><hr>";
		}
	
				
	
	
	//****************************************************************************************
	//****************************************************************************************
	//****************************************************************************************
	//****************************************************************************************
	//******   funzioni protected                                                      
	//****************************************************************************************
	//****************************************************************************************
	//****************************************************************************************
	//****************************************************************************************
	
	//***************************************************************************
	/**
	* crea la documentazione della pagina
	* 
	* se l'oggetto {@link $waDoc} è stato istanziato, allora genera la 
	* documentazione della pagina corrente, ivi compresi menu, moduli e tabelle
	* in questa compresi.
	* @return void
	*/
	protected function createDoc()
		{
		// possono darsi casi in cui un eventuale errore db richiama il metodo
		// showMessage che chiama show che chiama createDoc,
		// generando loop;
		static $rientranza = false;
		
		if (!$this->waDoc || $rientranza)
			{
			return;
			}

		$rientranza = true;
		$this->sectionName = $this->sectionName ? $this->sectionName : $this->name;
		$this->sectionTitle = $this->sectionTitle ? $this->sectionTitle : $this->title;
		$this->waDoc->documentSection($this->sectionName, $this->sectionTitle);
		
		foreach ($this->items as $item) 
			{
			if ($item['type'] == 'string' && $item['name'] == 'title')
				{
				$this->waDoc->documentPage($this->sectionName, $item['value']);
				}
			elseif (is_a($item['value'], 'waTable'))
				{
				$this->waDoc->documentTable($this->sectionName, $item['value']);
				}
			elseif (is_a($item['value'], 'waForm'))
				{
				$this->waDoc->documentForm($this->sectionName, $item['value']);
				}
			elseif (is_a($item['value'], 'waMenu'))
				{
				$this->waDoc->documentMenu($this->sectionName, $item['value']);
				}
			}
			
		$rientranza = false;
		}
		
	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getMyPath()
		{
		if ($_SESSION[get_class() . "_path"])
			{
			// ce l'eravamo già salvata in precedenza
			return $_SESSION[get_class() . "_path"];
			}
		
		if (strpos(__FILE__, "\\") !== false)
			{
			// siamo sotto windows
			$thisFile = strtolower(str_replace("\\", "/", __FILE__));
			$dr = strtolower(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']));
			}
		else
			{
			$thisFile = __FILE__;
			$dr = $_SERVER['DOCUMENT_ROOT'];
			}
		
		if (substr($dr, -1) == "/")
			{
			$dr = substr($dr, 0, -1);
			}
			
		if ($dr != substr($thisFile, 0, strlen($dr)))
			{
			// symlink! cerchiamo la path relativa alla document root
			$relative = "/" . basename(dirname(dirname($thisFile))) . "/" . basename(dirname($thisFile));
			$dirIterator = new \RecursiveDirectoryIterator($dr, \RecursiveDirectoryIterator::FOLLOW_SYMLINKS);
			$iterator = new \ParentIterator($dirIterator);
			$directories = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST, \RecursiveIteratorIterator::CATCH_GET_CHILD);

			foreach ($directories as $path => $object)
				{
                                $path = str_replace("\\", "/", $path);
				if (strpos($path, $relative) !== false)
					{
					$toret = substr($path, strlen($dr));
					break;
					}
				}
			}
		else
			{
			$toret = substr(dirname($thisFile), strlen($dr));
			}
		return $_SESSION[get_class() . "_path"] = $toret;		
		}
	
	//******************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected  function addMailAddr($MailObj, $Addr, $DestType)
		{
		if (empty($Addr))
			{
			return;
			}
			
		$methodName = "Add$DestType";
		
		if (is_array($Addr))
			{
			foreach($Addr as $oneAddress) 
				{
				if (is_object($oneAddress))
					{
					$MailObj->$methodName($oneAddress->address, $oneAddress->name);
					}
				else
					{
					$MailObj->$methodName($oneAddress);
					}
				}
			}
		else
			{
			if (is_object($Addr))
				{
				$MailObj->$methodName($Addr->address, $Addr->name);
				}
			else
				{
				$MailObj->$methodName($Addr);
				}
			}
			
		}

	//****************************************************************************************
	/**
	* cerca di determinare quale e' la directory di lavoro http dell'applicazione
	* (la radice dell'applicazione rispetto alla document root, 
	* non di un'eventuale sottosezione), ricavandolo dalla directory del modulo 
	* che per primo contiene un'istanza di waApplication. Se per qualche motivo il risultato
	* fosse sbagliato (perche' il file che contiene la prima estensione di
	* waApplication non risiede nella radice dell'applicazione come le linee
	* guida prevedono, oppure perche' c'e' una pippa sistemistica), allora
	* il programmatore dovra' definirsi da solo il value della proprieta'
	* httpwd
	* 
	* @ignore
	* @access protected
	*/
	protected function getHttpWd()
		{
		if ($this->httpwd !== false)
			// qualcuno ha già valorizzato la working directory; avra' avuto i 
			// suoi bravi motivi per farlo, quindi non tocchiamo nulla
			return $this->httpwd;
		
		$stacktrace = @debug_backtrace(false);
		if (!$stacktrace[1] || strtolower($stacktrace[1]['class']) != strtolower(get_class()))
			// non esiste neanche una estensione di waApplication????
			// che vuol dire? valorizziamo la working directory come se 
			// corrispondesse alla document-root, perche' non sappiamo che altro
			// fare
			return "";
			
		$dr = $_SERVER['DOCUMENT_ROOT'];
		if (strpos($dr, "\\") !== false)
			// siamo sotto windows
			$dr = strtolower(str_replace("\\", "/", $dr));

		// togliamo un eventuale slash finale dalla definizione della document-root
		if (substr($dr,-1) == "/")
			$dr = substr($dr, 0, -1);
		
		return str_replace("\\", "/", substr(dirname($stacktrace[1]['file']), strlen($dr)));

		}
	
	//****************************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function getCwd()
		{
		$dr = $_SERVER['DOCUMENT_ROOT'];
		if (substr($dr, -1) == "\\" || 	substr($dr, -1) == "/")
			$dr = substr($dr, 0, -1);
		return "$dr$this->httpwd";
		}
	
	//****************************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function includeClasses()
		{
		include_once __DIR__ . "/../wadb/wadb.class.php";
		include_once __DIR__ . "/../waform/waform.class.php";
		include_once __DIR__ . "/../watable/watable_quickedit.class.php";
		include_once __DIR__ . "/../wamenu/wamenu.class.php";
		}
		
	//***************************************************************************
	/**
	* correctHTML
	*
	* mette a posto eventuali scorrettezze xhtml
	* 
	* @ignore
	* @access protected
	* @return string
	*/
	protected function correctHTML($buffer)
		{
		// spostiamo i "<link ...>" nell'head
		list ($pre_head, $resto) = explode("<head>", $buffer, 2);
		list ($head, $resto) = explode("</head>", $resto, 2);
		list ($tra_head_e_body, $body) = explode("<body", $resto, 2);
		while (true)
			{
			list ($prima, $dopo) = explode("<link ", $body, 2);
			if (!$dopo)
				break;
			list ($link, $dopo) = explode(">", $dopo, 2);
			$head .= "<link $link" . (substr(rtrim($link), -1) == "/" ? '' : " /") . ">\n";
			$body = "$prima$dopo";
			}
		
		$buffer = "$pre_head\n<head>\n$head</head>$tra_head_e_body\n<body$body";
		
		return $buffer;
		}
		
	//***************************************************************************
	/**
	* costruisce la struttura dati JSON da passare al {@link view view-object}
	*
	* @access protected
	* @ignore
	*/
	protected function createData()
		{
		$this->data = new waApplicationData();
		$this->data->waApplicationPath = $this->getMyPath();
		$this->data->workingDirectory = $this->httpwd;
		$this->data->domain = $this->domain;
		$this->data->name = $this->name;
		$this->data->title = $this->title;
		$this->data->sectionTitle = $this->sectionTitle;
		$this->data->sectionName = $this->sectionName;
		$this->data->navigationMode = $this->navigationMode;
		$this->data->page = new waApplicationDataPage();
		$this->data->page->uri = $_SERVER['REQUEST_URI'];
		$this->data->page->previousUri = $_SERVER['HTTP_REFERER'];
		$this->data->page->name = substr(basename($_SERVER['PHP_SELF']), 0, -4);
		$this->data->language = $this->language;
		$this->data->locale = $this->locale;

		$this->data->page->items = array();
		foreach ($this->items as $inItem) 
			{
			$outItem = new waApplicationDataPageItem();
			$outItem->name = $inItem->name;
			$outItem->type = $inItem->type;
                        if (is_object($inItem->value) && strtolower(get_class($inItem->value)) != "stdclass" && !is_subclass_of($inItem->value, "stdclass"))
				{
				$outItem->value = $inItem->value->show(true);
				}
			else
				{
				$outItem->value = $inItem->value;
				}

			$this->data->page->items[$outItem->name] = $outItem;
			}
 		
 		if ($this->returnValues)
 			{
			$this->data->page->returnValues = $this->returnValues;
			$this->data->page->close = $this->closePage;
 			}

		} 

	//***********************************************************************
	/**
	 * definisce il {@link view view-object} di default se non già definito
	* @ignore
	* @access protected
	*/	
	protected function setView()
		{
		if ($this->view)
			{
			return;
			}

		// non è stato definito un trasformer: andiamo con quello di default
		include_once  __DIR__ . "/uis/waapplication_default/view/waapplication.php";
		$this->view = new \waLibs\views\waApplication\waApplication_default\waApplicationView();
		}

		
//***************************************************************************
	} 	// fine classe waApplication
	

