<?php
include "testapplication.inc.php";

//*****************************************************************************
/**
* formcorsi 
* 
* questa classe si preoccupera' di mostrare all'utente un form di classe 
* {@link waLibs\waForm} pr la manipolazione di un record corsi gestito dalla 
* nostra applicazione.
* 
* Deriva da {@link applicazioneTest}, dalla quale quindi
* eredita l'interfaccia programmatica (proprieta' e metodi); a questa noi potremo
* aggiungere i nostri metodi applicativi e se necessario modificare il 
* comportamento della classe di default mediante l'override dei metodi.
*/
//*****************************************************************************
class formcorsi extends testApplication
	{
		
	/**
	 * oggetto proprieta'  della pagina che conterra' i dati del form
	 * da mostrare all'utente
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//*****************************************************************************
	/**
	* 
	*/
	function __construct()
		{
		parent::__construct();
		
		// creiamo l'oggetto di classe waLibs\waForm e leggiamo l'eventuale input
		// utente/programma: questo ci dira' quale operazione e' stata richiesta
		$this->createForm();
		
		// eseguiamo l'operazione a seconda della scelta dell' utente o del
		// programma
		if ($this->form->isToCancel())
			// l'utente ha richiesto abort dell'editing
			$this->response();
		elseif ($this->form->isToDelete())
			// l'utente o il programma hanno richiesto l'eliminazione del record 
			// in editing o comunque di quello passato come parametro
			$this->deleteRecord();
		elseif ($this->form->isToUpdate())
			// l'utente ha richiesto update  o insert del record
			$this->updateRecord();
		else
			// non e' stato richiesto di eseguire un'azione, ma semplicemente di
			// mostrare il form all'utente ai fini dell'editing
			$this->showPage();
		
		}
		
	//*****************************************************************************
	/**
	* mostraPagina
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Form corsi", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//*****************************************************************************
	/**
	* creaForm
	* 
	* costruisce  il form 
	* @return waLibs\waForm
	*/
	function createForm()
		{
		// creazione del form...
		$this->form = $this->getForm();
		
		// creazione del recordset da associare al form
		$dbconn = $this->getDBConnection();
	  	$sql = "SELECT * FROM corsi";
	  	if ($_GET['id_corso'])
			// se ci viene passato un identificativo di record restringiamo la
			// ricerca allo specifico corso
	  		$sql .= " WHERE id_corso=" . $dbconn->sqlInteger($_GET['id_corso']);
	  	else 
			// se non ci viene passato un identificativo di record leggiamo un
			// recordset vuoto (zero righe: solo intestazioni); questo velocizza
			// l'accesso al DB e ritorna le informazioni sui campi da editare
			$nrRighe = 0;

		// lettura del recordset e associazione del recordset al form...
		$this->form->recordset = $this->getRecordset($sql, $dbconn, $nrRighe, 0);
		
	 	// inserimento dei controlli all'interno del form
		$ctrl = $this->form->addSelectTypeahead("id_organismo", "Organismo", false, true);
			$ctrl->sql = "select id_organismo, nome from organismi order by nome";
		$ctrl = $this->form->addSelect("id_amministrazione", "Amministrazione"); 	
			$ctrl->list = $this->getList("amministrazioni", "id_amministrazione", "nome");
		$ctrl = $this->form->addText("rifpa", "Rif. P.A.");
		$ctrl = $this->form->addText("nome", "Titolo corso");
		$ctrl = $this->form->addInteger("nr_ore", "Monte ore complessivo");
		$ctrl = $this->form->addDate("data_inizio", "Data inizio");
		$ctrl = $this->form->addDate("data_fine", "Data fine");
		$ctrl = $this->form->addCurrency("importo", "Importo complessivo");
		
		// inserimento bottoni di submit all'interno del form
		$this->addFormButtons($this->form);		

		// leggiamo eventuali values di input (se e' stato fatto submit o 
		// chiesta RPC)
		$this->form->getInputValues();
		}
		
		
	//*****************************************************************************
	/**
	* aggiornaRecord
	* 
	* e' il metodo invocato quando l'utente preme il bottone di submit e che
	* permette l'inserimento o la modifica di un record
	* @return void
	*/
	function updateRecord()
		{
		$this->form->checkMandatory();
		if (!$this->form->save(true))
			$this->showDBError($this->form->recordset->dbConnection);
		
		$this->response();
		}
	
	//*****************************************************************************
	/**
	* eliminaRecord
	* 
	* e' il metodo invocato quando l'utente preme il bottone di submit con
	* richiesta di cancellazione del record, oppure quando viene richiesta la 
	* cancellazione del record a partire da una azione su riga di waTabella
	* @return void
	*/
	function deleteRecord()
		{
		if (!$this->form->delete(true))
			$this->showDBError($this->form->recordset->dbConnection);
		
		$this->response();
		}
		
	}	// fine classe pagina
	
//*****************************************************************************
// istanzia la pagina
$page = new formcorsi();
