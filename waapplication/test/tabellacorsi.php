<?php
include "testapplication.inc.php";

//*****************************************************************************
/**
* tablecorsi 
* 
* questa classe si preoccupera' di mostrare all'utente una table di classe 
* {@link waLibs\waTable} contenente tutti i corsi gestiti dalla nostra applicazione
* 
* Deriva da {@link applicazioneTest}, dalla quale quindi
* eredita l'interfaccia programmatica (proprieta' e metodi); a questa noi potremo
* aggiungere i nostri metodi applicativi e se necessario modificare il 
* comportamento della classe di default mediante l'override dei metodi.
*/
class tablecorsi extends testApplication
	{

	//*****************************************************************************
	/**
	* mostraPagina
	* 
	* costruisce la pagina contenente la table e la manda in output
	* @return void
	*/
	function showPage()
		{
		// costruiamo la table
		$table = $this->createTable();
			
		// prepara la pagina, ossia il contenitore della table
		$this->addItem($this->getMenu());
		$this->addItem("Table corsi", "title");
		$this->addItem($table);
		
		// manda in output l'intera pagina
		$this->show();
		}
		
	//*****************************************************************************
	/**
	* creaTable
	* 
	* costruisce la table 
	* 
	* @return waLibs\waTable
	*/
	function createTable()
		{
		// creazione della table sulla base della query sql
		$sql = "SELECT corsi.*," .
				"organismi.id_organismo, organismi.nome as nomeorgan," .
				" amministrazioni.sigla, amministrazioni.nome as nomeamm" .
				" FROM corsi" .
				" INNER JOIN amministrazioni ON corsi.id_amministrazione=amministrazioni.id_amministrazione".
				" INNER JOIN organismi ON corsi.id_organismo=organismi.id_organismo";
		$table = $this->getTable($sql);
		
		// definizione delle proprieta' di base della table
		$table->title = "corsi";
		$table->formPage = "modulocorsi.php";
		
		// definizione delle azioni della table
		$table->actions['Delete']->enablingFunction = array($this, "checkEnableDelete");
		$table->addAction("Test", true);
		if ($_GET["child"])
			$table->addAction("close");
		
		// definizione delle columns della table e delle relative proprieta'
		$table->addColumn("id_corso", "ID");
		$table->addColumn("rifpa", "Rif. P.A.");
		$col = $table->addColumn("nome", "Corso");
			$col->aliasOf = 'corsi.nome';
		$col = $table->addColumn("id_organismo", "Cod. org.");
			$col->aliasOf = 'corsi.id_organismo';
		$col = $table->addColumn("nomeorgan", "Organismo");
			$col->aliasOf = 'organismi.nome';
		$col = $table->addColumn("sigla", "Amministrazione");
			$col->link = true;
		$col = $table->addColumn("data_inizio", "Data inizio");
		$col = $table->addColumn("importo", "Importo");
			$col->totalizza = true;
		$col = $table->addColumn("eliminabile", "Eliminabile", true, false, false, waLibs\waTable::ALIGN_C);
			$col->computeFunction = array($this, "showDeletability");
		$col = $table->addColumn("nomeamm", "nomeamm", false, false, false);
		
		// se la table fosse destinata anche all'input (post o rpc), questo 
		// sarebbe il punto dove chiamare $table->leggiValoriIngresso()
			
		// lettura dal database dei records che andranno a popolare la table
		if (!$table->loadRows())
			$this->showDBError($table->recordset->dbConnection);
		
		return $table;
		}
		
	//*****************************************************************************
	/**
	* showDeletability
	* 
	* callback function da invocare in corrispondenza della visualizzazione di una
	* cella della colonna <b>ELIMINABILE</b>. All'oggetto di classe {@link waLibs\waTable}, 
	* infatti, viene detto che per quella colonna non si deve mostrare il contenuto
	* del campo, bensi' il valore di ritorno di questo metodo.
	* 
	* Nel nostro caso specifico, all'interno della cella verra' mostrato <b>si</b> se
	* l'importo del corso e' minore di 100.000 euro; <b>no</b> negli altri casi
	* @return string
	*/
	function showDeletability(waLibs\waTable $table)
		{		
		return $table->record->importo <= 100000 ? 'yes' : 'no';
		}
	
	//*****************************************************************************
	/**
	* verificaAbilitaElimina
	* 
	* callback function da invocare per l'abilitazione, o meno, di una azione su
	* record in corrispondenza del record corrente durante la costruzione della
	* table.
	* 
	* Il valore di ritorno di questo metodo dira' alla table se in corrispondenza della visualizzazione di una
	* cella della colonna <b>ELIMINABILE</b>. All'oggetto di classe {@link waLibs\waTable}, 
	* infatti, viene detto che per quella colonna non si deve mostrare il contenuto
	* del campo, bensi il valore di ritorno di questo metodo
	* @return string
	*/
	function checkEnableDelete(waLibs\waTable $table)
		{		
		return $table->record->importo <= 100000;
		}
	
	
	}	// fine classe pagina
	
//*****************************************************************************
// istanzia la pagina
$page = new tablecorsi();
$page->showPage();
	
