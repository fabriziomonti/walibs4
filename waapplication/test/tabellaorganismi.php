<?php
include "testapplication.inc.php";

//*****************************************************************************
/**
* tabellacorsi 
* 
* questa classe si preoccupera' di mostrare all'utente una tabella di classe 
* {@link waTabella} contenente tutti gli organismi gestiti dalla nostra 
* applicazione
* 
* Deriva da {@link applicazioneTest}, dalla quale quindi
* eredita l'interfaccia programmatica (proprieta' e metodi); a questa noi potremo
* aggiungere i nostri metodi applicativi e se necessario modificare il 
* comportamento della classe di default mediante l'override dei metodi.
*/
class tabellaorganismi extends testApplication
	{

	//*****************************************************************************
	/**
	* mostraPagina
	* 
	* costruisce la pagina contenente la tabella e la manda in output
	* @return void
	*/
	function showPage()
		{
		// costruiamo la table
		$table = $this->createTable();
			
		// prepara la pagina, ossia il contenitore della table
		$this->addItem($this->getMenu());
		$this->addItem("Table organismi", "title");
		$this->addItem($table);
		
		// manda in output l'intera pagina
		$this->show();
		}
		
	//*****************************************************************************
	/**
	* creaTabella
	* 
	* costruisce la tabella 
	* 
	* @return waTabella
	*/
	function createTable()
		{
		// creazione della tabella sulla base della query sql
		$sql = "SELECT * FROM organismi";
		$table = $this->getTable($sql);
		
		// definizione delle proprieta' di base della tabella
		$table->title = "organismi";
		$table->formPage = "moduloorganismi.php";
		
		// definizione delle columns della tabella e delle relative proprieta'
		$table->addColumn("id_organismo", "ID", true, false, true, WATABLE_ALLINEA_DX, WATABLE_FMT_INTERO);
		$table->addColumn("NOME", "Nome");
		$col = $table->addColumn("EMAIL", "E-mail");
			$col->link = true;
		$table->addColumn("TELEFONO", "Telefono");
		
		// se la tabella fosse destinata anche all'input (post o rpc), questo 
		// sarebbe il punto dove chiamare $tabella->leggiValoriIngresso()
			
		// lettura dal database dei records che andranno a popolare la tabella
		if (!$table->loadRows())
			$this->showDBError($table->recordset->dbConnection);
		
		return $table;
		}
		
	
	}	// fine classe pagina
	
//*****************************************************************************
// istanzia la pagina
$page = new tabellaorganismi();
$page->showPage();
	
