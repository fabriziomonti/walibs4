<?php
include "testapplication.inc.php";

//*****************************************************************************
/**
* moduloallievi 
* 
* questa classe si preoccupera' di mostrare all'utente un modulo di classe 
* {@link waModulo} pr la manipolazione di un record allievi gestito dalla 
* nostra applicazione.
* 
* Deriva da {@link applicazioneTest}, dalla quale quindi
* eredita l'interfaccia programmatica (proprieta' e metodi); a questa noi potremo
* aggiungere i nostri metodi applicativi e se necessario modificare il 
* comportamento della classe di default mediante l'override dei metodi.
*/
//*****************************************************************************
class moduloallievi extends testApplication
	{
		
	/**
	 * oggetto proprieta'  della pagina che conterra' i dati del modulo
	 * da mostrare all'utente
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//*****************************************************************************
	/**
	* 
	*/
	function __construct()
		{
		parent::__construct();
		
		// creiamo l'oggetto di classe waLibs\waForm e leggiamo l'eventuale input
		// utente/programma: questo ci dira' quale operazione e' stata richiesta
		$this->createForm();
		
		// eseguiamo l'operazione a seconda della scelta dell' utente o del
		// programma
		if ($this->form->isToCancel())
			// l'utente ha richiesto abort dell'editing
			$this->response();
		elseif ($this->form->isToDelete())
			// l'utente o il programma hanno richiesto l'eliminazione del record 
			// in editing o comunque di quello passato come parametro
			$this->deleteRecord();
		elseif ($this->form->isToUpdate())
			// l'utente ha richiesto update  o insert del record
			$this->updateRecord();
		else
			// non e' stato richiesto di eseguire un'azione, ma semplicemente di
			// mostrare il form all'utente ai fini dell'editing
			$this->showPage();
		
		}
		
	//*****************************************************************************
	/**
	* showPage
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Form allievi", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//*****************************************************************************
	/**
	* creaModulo
	* 
	* costruisce  il modulo 
	* @return waModulo
	*/
	function createForm()
		{
		// creazione del form...
		$this->form = $this->getForm();
		
		// creazione del recordset da associare al form
		$dbconn = $this->getDBConnection();
	  	$sql = "SELECT * FROM allievi";
	  	if ($_GET['id_allievo'])
			// se ci viene passato un identificativo di record restringiamo la
			// ricerca allo specifico allievo
	  		$sql .= " WHERE id_allievo=" . $dbconn->sqlInteger($_GET['id_allievo']);
	  	else 
			// se non ci viene passato un identificativo di record leggiamo un
			// recordset vuoto (zero righe: solo intestazioni); questo velocizza
			// l'accesso al DB e ritorna le informazioni sui campi da editare
			$nrRighe = 0;

		// lettura del recordset e associazione del recordset al modulo...
		$this->form->recordset = $this->getRecordset($sql, $dbconn, $nrRighe, 0);
		
	 	// inserimento dei controlli all'interno del modulo
		$ctrl = $this->form->addInteger("id_allievo", "Identificativo", true);
		$ctrl = $this->form->addSelect("id_corso", "Corso", false, true);
			$ctrl->list = $this->getList("corsi", "id_corso", "nome");
	 	
		
		$ctrl = $this->form->addText("NOME", "Nome", false, true);
		$ctrl = $this->form->addText("INDIRIZZO", "Indirizzo");
		$ctrl = $this->form->addInteger("CAP", "CAP");
		$ctrl = $this->form->addText("PROVINCIA", "Provincia");
		$ctrl = $this->form->addText("CITTA", "Città");
		$ctrl = $this->form->addText("CODICE_FISCALE", "Codice fiscale");
		$ctrl = $this->form->addPassword("PASSWORD", "Password");
		$ctrl = $this->form->addDateTime("DATAORA_COLLOQUIO", "Data/Ora colloquio");
			$ctrl->value = time();
		$ctrl = $this->form->addBoolean("FLAG_AMMISSIONE", "Ammesso");
		$ctrl = $this->form->addBoolean("FLAG_PROMOZIONE", "Promosso");

		$ctrl = $this->form->addUpload("NOME_FILE_CURRICULUM", "Curriculum");
		if (!empty($this->form->recordset->records))
			$ctrl->showPage = "docs/" . $this->form->recordset->records[0]->nome_file_curriculum;
		
		// inserimento bottoni di submit all'interno del form
		$this->addFormButtons($this->form);		

		// leggiamo eventuali values di input (se e' stato fatto submit o 
		// chiesta RPC)
		$this->form->getInputValues();
		}
		
		
	//*****************************************************************************
	/**
	* aggiornaRecord
	* 
	* e' il metodo invocato quando l'utente preme il bottone di submit e che
	* permette l'inserimento o la modifica di un record
	* @return void
	*/
	function updateRecord()
		{
		$this->form->checkMandatory();
		$this->saveDoc($this->form->inputControls["NOME_FILE_CURRICULUM"]);
		if (!$this->form->save(true))
			$this->showDBError($this->form->recordset->dbConnection);
		
		$this->response();
		}

	//*****************************************************************************
	/**
	* eliminaRecord
	* 
	* e' il metodo invocato quando l'utente preme il bottone di submit con
	* richiesta di cancellazione del record, oppure quando viene richiesta la 
	* cancellazione del record a partire da una azione su riga di waTabella
	* @return void
	*/
	function deleteRecord()
		{
		if (!$this->form->delete(true))
			$this->showDBError($this->form->recordset->dbConnection);
		
		$this->response();
		}
		
	//***************************************************************************** 
	// salva l'eventuale documento allegato
	//***************************************************************************** 
	function saveDoc(waUpload $ctrl)
		{
		// salvataggio del documento (se c'e'...)
		if ($ctrl->isToDelete())
			// cancelliamo un eventuale documento esistente
			@unlink("docs/$ctrl->value");
		elseif ($ctrl->getUploadError())			
	    	$this->showMessage("Errore caricamento file", 
	    						"Si e' verificato l'errore " . 
	    						$ctrl->getUploadError() . 
	    						" durante il caricamento del documento $ctrl->name." .
	    						" Si prega di avvertire l'assistenza tecnica.", 
    							false, true);
		elseif ($ctrl->isToSave())
			{
			@unlink("docs/$ctrl->value");
			if (!$ctrl->saveFile("docs/$ctrl->inputValue"))
	    		$this->showMessage("Errore spostamento file", 
	    						"Si e' verificato un errore " . 
	    						" durante lo spostamento del documento $ctrl->name." .
	    						" Si prega di avvertire l'assistenza tecnica.", 
    							false, true);
			}
			
		return true;			
		}
	
	}	// fine classe pagina
	
//*****************************************************************************
// istanzia la pagina
$page = new moduloallievi();

