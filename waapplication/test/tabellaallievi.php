<?php
include "testapplication.inc.php";

//*****************************************************************************
/**
* tabellacorsi 
* 
* questa classe si preoccupera' di mostrare all'utente una tabella di classe 
* {@link waTabella} contenente tutte le amministyrazioni gestite dalla nostra 
* applicazione
* 
* Deriva da {@link applicazioneTest}, dalla quale quindi
* eredita l'interfaccia programmatica (proprieta' e metodi); a questa noi potremo
* aggiungere i nostri metodi applicativi e se necessario modificare il 
* comportamento della classe di default mediante l'override dei metodi.
*/
class tabellaallievi extends testApplication
	{

	//*****************************************************************************
	/**
	* mostraPagina
	* 
	* costruisce la pagina contenente la tabella e la manda in output
	* @return void
	*/
	function showPage()
		{
		// costruiamo la table
		$table = $this->createTable();
			
		// prepara la pagina, ossia il contenitore della table
		$this->addItem($this->getMenu());
		$this->addItem("Table allievi", "title");
		$this->addItem($table);
		
		// manda in output l'intera pagina
		$this->show();
		}
		
	//*****************************************************************************
	/**
	* creaTabella
	* 
	* costruisce la tabella 
	* 
	* @return waTabella
	*/
	function createTable()
		{
		// creazione della tabella sulla base della query sql
		$sql = "SELECT allievi.*, corsi.rifpa, corsi.nome as nomecorso," .
				"organismi.id_organismo, organismi.nome as nomeorgan," .
				"amministrazioni.sigla, amministrazioni.nome as nomeamm" .
				" FROM allievi" .
				" LEFT JOIN corsi ON allievi.id_corso=corsi.id_corso".
				" LEFT JOIN amministrazioni ON corsi.id_amministrazione=amministrazioni.id_amministrazione".
				" LEFT JOIN organismi ON corsi.id_organismo=organismi.id_organismo";
		$table = $this->getTable($sql);
		
		// definizione delle proprieta' di base della tabella
		$table->title = "allievi";
		$table->formPage = "moduloallievi.php";
		
		// definizione delle columns della tabella e delle relative proprieta'
		$table->addColumn("id_allievo", "ID", true, false, true);
		$col = $table->addColumn("nome", "Nome");
			$col->aliasOf = 'allievi.nome';
		$table->addColumn("codice_fiscale", "Codice fiscale");
		$table->addColumn("rifpa", "Rif. P.A.");
		$col = $table->addColumn("nomecorso", "Corso");
			$col->aliasOf = 'corsi.nome';
		$col = $table->addColumn("id_organismo", "Cod. org.");
			$col->aliasOf = 'organismi.id_organismo';
		$col = $table->addColumn("nomeorgan", "Organismo");
			$col->aliasOf = 'organismi.nome';
		$col = $table->addColumn("sigla", "Amministrazione");
		$col = $table->addColumn("flag_ammissione", "Ammesso", true, false, false, waLibs\waTable::ALIGN_C);
			$col->computeFunction = array($this, "mostraAmmissione");
		$col = $table->addColumn("flag_promozione", "Promosso", true, false, false, waLibs\waTable::ALIGN_C);
			$col->computeFunction = array($this, "mostraPromozione");
		$col = $table->addColumn("nome_file_curriculum", "Curriculum");
			$col->link = true;
		
		// se la tabella fosse destinata anche all'input (post o rpc), questo 
		// sarebbe il punto dove chiamare $tabella->leggiValoriIngresso()
			
		// lettura dal database dei records che andranno a popolare la tabella
		if (!$table->loadRows())
			$this->showDBError($table->recordset->dbConnection);
		
		return $table;
		}
		
	//*************************************************************************
	function mostraAmmissione(waLibs\waTable $table)
		{		
		return $table->record->flag_ammissione == 1 ? 'si' : 'no';
		}
	
	//*************************************************************************
	function mostraPromozione(waLibs\waTable $table)
		{		
		return $table->record->flag_promozione == 1 ? 'si' : 'no';
		}
	
	
	}	// fine classe pagina
	
//*****************************************************************************
// istanzia la pagina
$page = new tabellaallievi();
$page->showPage();

