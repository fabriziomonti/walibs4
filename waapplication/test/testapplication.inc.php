<?php
error_reporting(E_ERROR|E_WARNING);

if (!defined('_APPLICATION_CLASS'))
{
/**
* @ignore
*/
define('_APPLICATION_CLASS',1);

/**
* @ignore
*/
include_once (dirname(__FILE__) . "/config.inc.php");
/**
* @ignore
*/
include_once (dirname(__FILE__) . "/../waapplication.class.php");

//*****************************************************************************
/**
* applicazionetest 
* 
* questa classe conterra' tutte le proprieta' e i metodi comuni della nostra 
* specifica applicazione. Deriva da {@link waApplicazione}, dalla quale quindi
* eredita l'interfaccia programmatica (proprieta' e metodi); a questa noi potremo
* aggiungere i nostri metodi applicativi e se necessario modificare il 
* comportamento della classe di default mediante l'override dei metodi.
*/
class testApplication extends waLibs\waApplication
	{
	// file di configurazione della connessione al db dell'applicazione
	var $fileConfigDB;
	
	// struttura in cui salvare le preferenze di navigazione (ma volendo anche
	// altro) dell'utente
	var $userPref;
	
	//*****************************************************************************
	/**
	* costruttore dell'applicazione 
	* 
	* in questo caso l'inizializzazione e' annegata nel costruttore, ma sono
	* frequenti i casi in cui e' meglio tenere separate le due azioni
	* @return void
	*/
	function __construct()
		{
		$this->name = APPL_NAME;
		$this->title = APPL_TITLE;
		$this->version = APPL_REL;
		$this->versionDate = APPL_REL_DATE;
		$this->smtpServer  = APPL_SMTP_SERVER;
		$this->supportEmail = APPL_SUPPORT_ADDRESS;
		$this->infoEmail = APPL_INFO_ADDRESS;
		
		
		$this->fileConfigDB = dirname(__FILE__) . "/dbconfig.inc.php";
		
		$this->init();
		
		$this->userPref = & $_SESSION['user_pref'];
		$this->navigationMode = $this->userPref['navigation'] ? 
										$this->userPref['navigation'] :
										waLibs\waApplication::NAV_PAGE;
		
		}
	
	//*****************************************************************************
	/**
	* costruisce e manda in output il menu dell'applicazione 
	* 
	* @return waLibs\waMenu
	*/
	function getMenu()
	    {
	
		$m = new waLibs\waMenu();
		$m->open();
		
		$m->openSection("amministrazioni", "tabellaamministrazioni.php");
		$m->closeSection();
		$m->openSection("organismi", "tabellaorganismi.php");
		$m->closeSection();
		$m->openSection("corsi", "tabellacorsi.php");
		$m->closeSection();
		$m->openSection("allievi", "tabellaallievi.php");
		$m->closeSection();
		$m->openSection("navigazione");
			$m->addItem("nella stessa pagina", "index.php?navigation=" . waLibs\waApplication::NAV_PAGE);
			$m->addItem("a finestre", "index.php?navigation=" . waLibs\waApplication::NAV_WINDOW);
			$m->addItem("iframe", "index.php?navigation=" . waLibs\waApplication::NAV_INNER);
		$m->closeSection();

		$m->close();
		return $m;
		
	    }
	    
	//***************************************************************************
	/**
	* - 
	* 
	* restiuisce una connessione al database oppure, in caso di errore, termina 
	* l'esecuzione dello script invocando il metodo {@link showDBError}
	* 
	* @return waDBConnection
	*/
	function getDBConnection()
		{
		$dbConnection = waLibs\wadb_getConnection($this->fileConfigDB);
		if ($dbConnection->errorNr())
			$this->showDBError($dbConnection);
		return $dbConnection;
		}		
	
	//*****************************************************************************
	/**
	* restituisce una tabella standard dell'applicazione
	* 
	* questo metodo e' molto comodo in una applicazione, perche' permette di:
	* - definire in un unico punto gli attributi di TUTTE le tabelle dell'applicazione
	* - qualora in fase di lavorazione si decidesse di non utilizzare direttamente la classe {@link waTabella}, bensi' una sua derivata, e' sufficiente intervenire in questo unico punto dell'applicazione, anziche' in tutte le pagine che contengono una tabella
	* 
	* @param string $sql stringa sql che generera' il recordset che popolera'
	* la tabella
	* @return waTabella
	*/
	function getTable($sql)
	    {
		$table = new waLibs\waTable($sql, $this->fileConfigDB);
		
		$table->removeAction("Details");
		return $table;
	    }
	
	//*****************************************************************************
	/**
	* restituisce un form standard dell'applicazione
	* 
	* questo metodo e' molto comodo in una applicazione, perche' permette di:
	* - definire in un unico punto gli attributi di TUTTI i moduli dell'applicazione
	* - qualora in fase di lavorazione si decidesse di non utilizzare direttamente la classe {@link waLibs\waForm}, bensi' una sua derivata, e' sufficiente intervenire in questo unico punto dell'applicazione, anziche' in tutte le pagine che contengono un form
	* 
	* @return waLibs\waForm
	*/
	function getForm()
	    {
		$form = new waLibs\waForm(null, $this);
		$form->width = 800;
		return $form;
	    }
	
	//*****************************************************************************
	/**
	* restituisce una lista da associare a un controllo di classe {@link waSelezione}
	* 
	* partendo da una tabella di database, restituisce un array associativo in cui 
	* la chiave e' l'dentificativo del record e il valore la descrizione del record.
	* 
	* Tipicamente questo array viene associato ad un controllo di classe {@link waSelezione}
	* all'interno di un form nel quale occorre scegliere il valore di un campo
	* foreign-key
	* @param string $table nome della tabella del db
	* @param string $idFieldName nome del campo identificativo univoco del record
	* @param string $descriptionFieldName nome del campo che contiene una descrizione umanamente conprensibile del record
	* @return array
	*/
	function getList($table, $idFieldName, $descriptionFieldName)
		{
		$sql = "SELECT * FROM $table ORDER BY $descriptionFieldName";
		$rs = $this->getRecordset($sql, $dbconn);
		
		$retval = array();
		foreach ($rs->records as $record)
			$retval[$record->$idFieldName] = $record->$descriptionFieldName;
			
		return $retval;
		}
	
	//*****************************************************************************
	/**
	* definisce la bottoniera di submit standard di un form
	* 
	* tipicamente, ogni form di una applicazione ha una bottoniera con azioni
	* ricorrenti (salva, elimina, annulla, ecc.). La chiamata a questo metodo
	* definisce la bottoniera standard per questa applicazione.
	* 
	* @param waLibs\waForm $form form nel quale i bottoni andranno inseriti
	* 
	* @return void
	*/
	//*****************************************************************************
	function addFormButtons(waLibs\waForm $form)
		{
		$butt = $form->addButton("cmdSubmit", "SUBMIT");
		$butt->top += $form->controlsLineSpacing * 2;
		$butt->width = 90;
	  	if ($form->recordset->records)
	  		{
			$butt2 = $form->addButton("cmdDelete", "DELETE");
			$butt2->top = $butt->top;
			$butt2->left = $butt->left + $butt->width;
			$butt2->width = $butt->width;
			$butt2->delete = true;
	  		}
		$butt3 = $form->addButton("cmdCancel", "CANCEL");
		$butt3->top = $butt->top;
		$butt3->left = $butt->left + $butt->width + ($butt2 ? $butt2->width : 0);
		$butt3->width = $butt->width;
		$butt3->cancel = true;
		
		}
	
	//*****************************************************************************
	function show()
		{
		// se ci viene passato il parametro xml in query-string, l'utnte vuole
		// solo vedere l'xml generato per l'oggetto waTable; ovviamente viene 
		// mostrato solo quello della prima tabella della pagina
		if ($_GET['json'])
				{
				exit($this->showJSON());
				}
				
			parent::show();
			}
	
	    
//***************************************************************************
	} 	// fine classe application
	
//***************************************************************************

//***************************************************************************
//******* fine della gnola **************************************************
//***************************************************************************
} //  if (!defined('_APPLICATION_CLASS'))

