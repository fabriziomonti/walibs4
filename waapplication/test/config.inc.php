<?php
if (!defined('__CONFIG_VARS'))
{
	define('__CONFIG_VARS',1);
	
	// parametri di inizializzazione usati da waapplicazione
	define('APPL_NAME', 						'testapplication');
	define('APPL_TITLE', 						'applicazione di prova');
	define('APPL_REL', 							'0.1');
	define('APPL_REL_DATE', 					mktime(0,0,0, 11, 14, 2012));
	define('APPL_SMTP_SERVER', 					'webappls.com');
	define('APPL_SUPPORT_ADDRESS',				'f.monti@webappls.com');
	define('APPL_INFO_ADDRESS', 				'f.monti@webappls.com');
	
} //  if (!defined('__CONFIG_VARS'))

