<?php
/**
* -
*
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include delle sotto-strutture dati di cui è composta la classe
* @ignore
*/
include_once __DIR__ . "/waapplication_data_page.class.php";

//***************************************************************************
//****  classe waApplicationData ***************************************************
//***************************************************************************
/**
* waApplicationData
*
* struttura dati da passare al view-object
* 
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waApplicationData 
	{
	/**
	 * nome programmatico dell'applicazione
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * path relativa alla document_root della libreria waApplication
	 * 
	 * @var string
	 */
	public $waApplicationPath;
	
	/**
	 * directory di lavoro dell'applicazione
	 * 
	 * @var string
	 */
	public $workingDirectory;
	
	/**
	 * dominio dell'applicazione
	 * 
	 * @var string
	 */
	public $domain;
	
	/**
	 * titolo (nome per l'utente) dell'applicazione
	 * 
	 * @var string
	 */
	public $title;
	
	/**
	 * nome della sezione dell'applicazione
	 * 
	 * @var string
	 */
	public $sectionName;
	
	/**
	 * titolo della sezione dell'applicazione
	 * 
	 * @var string
	 */

	/**
	 * modalità di navigazione dell'applicazione (vedi waApplication::NAV_*)
	 * 
	 * @var string
	 */
	public $navigationMode;
	
	/**
	 * dati della pagina {@link waApplicationDataPage}
	 * 
	 * @var waApplicationDataPage
	 */
	public $page;
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language;
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale;
	
	}
	
