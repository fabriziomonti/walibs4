<?php
/**
* -
*
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include della struttura dati da passare al view object
* @ignore
*/
include_once __DIR__ . "/waapplication_data.class.php";

//***************************************************************************
//****  interfaccia i_waApplicationView ********************************************
//***************************************************************************
/**
* i_waApplicationView
*
* interfaccia che tutti i {@link waApplication::view view-object} utiizzati da 
 * waApplication devono rispettare
*
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
interface i_waApplicationView
{
	/**
	 * trasforma i dati passati in input nella UI prevista dal view-object
	 * 
	 * @param stdClass $data i dati da trasformare
	 */
    public function transform(waApplicationData $data);
}