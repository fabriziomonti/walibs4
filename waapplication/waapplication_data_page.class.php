<?php
/**
* -
*
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include delle sotto-strutture dati di cui è composta la classe
* @ignore
*/
include_once __DIR__ . "/waapplication_data_page_item.class.php";

//***************************************************************************
//****  classe waApplicationDataPage ****************************************
//***************************************************************************
/**
* waApplicationDataPage
*
* struttura dati di una pagina da passare al view-object
* 
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waApplicationDataPage 
	{
	/**
	 * nome programmatico della pagina
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * URI della pagina
	 * 
	 * @var string
	 */
	public $uri;
	
	/**
	 * URI della pagina precedente (referrer)
	 * 
	 * @var string
	 */
	public $previousUri;
	
	/**
	 * lista elementi della pagina {@link waApplicationDataPageItem}
	 * 
	 * @var array
	 */
	public $items;
	
	/**
	* valori che un pagina figlia ritorna alla mamma affinche' si allinei
	* alle modifiche effettuate 
	 * 
	 * @var array
	 */
	public $returnValues;
	
	/**
	* flag che indica se la pagina deve essere chiusa al momento del ritorno
	* oppure deve continuare (verosimilmente con un data-entry)
	 * 
	* @var boolean
	 */
	public $close;
	
	}
	
