<?php
/**
* -
*
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waApplicationDataPageItem ************************************
//***************************************************************************
/**
* waApplicationDataPageItem
*
* struttura dati di elemento di una pagina da passare al view-object
* 
* @package waApplication
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waApplicationDataPageItem 
	{
	/**
	 * nome programmatico dell'elemento
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * tipo dell'elemento (string, waMenu, waTable, waForm...)
	 * 
	 * @var string
	 */
	public $type;
	
	/**
	 * valore dell'elemento
	 * 
	 * @var string
	 */
	public $value;
	
	}
	
