<?php 
namespace waLibs\views\waApplication\waApplication_default;

//******************************************************************************
class waApplicationView implements \waLibs\i_waApplicationView
	{
	
	/**
	 * dati in input
	 * @var \waLibs\waApplicationData
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData $data)
		{
		$this->data = $data;

		?>
		
		<!DOCTYPE html>
		<html>
			<head>
				<!-- Bootstrap core CSS -->
				<link href='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet'>

				<link href='<?=$this->data->waApplicationPath?>/uis/waapplication_default/css/waapplication.css' rel='stylesheet'/>

				<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/mootools/1.6.0/mootools-core.min.js'></script>
				<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
				<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js'></script>

				<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
				<script type='text/javascript'>
					jQuery.noConflict();			
				</script>

				<script type='text/javascript' src='<?=$this->data->waApplicationPath?>/uis/waapplication_default/js/strmanage.js'></script>
				<script type='text/javascript' src='<?=$this->data->waApplicationPath?>/uis/waapplication_default/js/waapplication.js'></script>

				<title>
					<?=$this->data->title?>
				</title>
			</head>
			<body >
				<noscript>
					<hr />
					<div style='text-align: center'>
						<b>
							Questa applicazione usa Javascript, ma il tuo browser ha questa funzione
							disabilitata. Sei pregato di abilitare Javascript per il dominio <?=$this->data->domain?>
							e ricaricare la pagina.
						</b>
					</div>
					<hr />
				</noscript>

				<!-- se lavoriamo con navigazione interna creiamo anche l'iframe destinato a contenere la finestra figlia-->
				<?php if ($this->data->navigationMode == \waLibs\waApplication::NAV_INNER) : ?>
					<iframe id='waapplication_child_iframe' class='waapplication_child_iframe' style='visibility:hidden'>
					</iframe>
				<?php endif; ?>

				<!-- creazione degli elementi costitutivi della pagina (titolo, tabelle, moduli, testo libero, ecc.-->
				<?php 
				foreach ($this->data->page->items as $item) : 
					?>
					<div class="waapplication_<?=$item->name?>">
						<?=$item->value?>
					</div>
				<?php endforeach; ?>

				<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
				<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
				<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
				<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
				<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
				<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
				<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->name?>.css' rel='stylesheet'/>
				<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->page->name?>.css' rel='stylesheet'/>
				<script type='text/javascript' src='<?=$this->data->workingDirectory?>/ui/js/<?=$this->data->name?>.js'></script>
				<script type='text/javascript' src='<?=$this->data->workingDirectory?>/ui/js/<?=$this->data->page->name?>.js'></script>

				<!-- se non esiste il file js relativo alla pagina, creiamo un oggetto pagina che ha le proprieta' -->
				<!-- e i metodi di default dell'applicazione. -->
				<!-- In ogni caso diciamo all'applicazione/pagina in che modalita' si dovra' navigare -->
				<!-- e se la pagina deve allineare la mamma e/o eventualmente chiudersi -->
				<script type='text/javascript'>
					if (!document.waPage)
						document.waPage = new waApplication();
					document.waPage.navigationMode = '<?=$this->data->navigationMode?>';
					document.waPage.language = '<?=$this->data->language?>';
					document.waPage.locale = '<?=$this->data->locale?>';
					<?php if ($this->data->page->returnValues) : ?>
						document.waPage.updateParent('<?=$this->data->page->returnValues?>');
						<?php if ($this->data->page->close) : ?>
							document.waPage.closePage();
						<?php endif; ?>
					<?php endif; ?>
				</script>

				<!--modal per alert/confirm-->
				<div  id="waapplication_alert" class="modal fade">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title"><?=$data->title?></h4>
							</div>
							<div class="modal-body">
							</div>
							<div class="modal-footer">
								<button id="waapplication_alert_close" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								<button id="waapplication_confirm_cancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								<button id="waapplication_confirm_confirm" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->				

			</body>
		</html>

		<?php
		}
		
	//**************************************************************************
	}
//******************************************************************************


