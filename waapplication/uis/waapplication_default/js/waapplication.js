//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
 * classe di base delle procedure di controllo di una applicazione waLibs.
 * 
 * Il view-object di default, una volta caricato questo file, cercherà di caricare un 
 * file javascript con nome [nome_pagina].js. Dopodichè verificherà se
 * esiste già un oggetto in document.waPage . Se non esiste, creerà quell'oggetto
 * instanziando la presente classe.
 * 
 * E' così possibile per il programmatore estendere la presente classe nel file 
 * [nome_page].js e sempre all'interno del file istanziarne l'estensione in 
 * document.waPage.
 * 
 * I compiti principali della classe sono:
 * 
 * <ul>
 * <li>gestire la navigazione tra le pagine (apertura/chiusura/aggiornamento)</li>
 * <li>collegare le azioni di eventuali oggetti waTable presenti nella pagina
 *		con metodi implementati  all'interno dell'estensione della classe
 *		(ossia in [nome_pagina].js). Si veda al proposito la nota in 
 *		{@link linkTablesActions}
 * </li>
 * <li>collegare i link di eventuali oggetti waTable presenti nella page
 *		con metodi implementati  all'interno dell'estensione della classe
 *		(ossia in [nome_pagina].js). Si veda al proposito la nota in 
 *		{@link linkTablesLinks}
 * </li>
 * <li>collegare gli eventi di eventuali oggetti contenuti nei waForm presenti 
 *		nella pagina
 *		con metodi implementati  all'interno dell'estensione della classe
 *		(ossia in [nome_pagina].js). Si veda al proposito la nota in 
 *		{@link linkFormsEvents}.
 * </li>
 * </ul>
 *	
 *	Naturalmente è poi possibile che [nome_pagina].js dichiari una classe che
 *	non deriva direttamente da waApplication, ma da una classe intermedia che
 *	derivi da waApplication, e che implementi metodi personalizzati per il 
 *	funzionamento della propria applicazione (calcolo codice fiscale, gestione
 *	help online, ecc.). Difatti il view-object di default cercherà anche di caricare un
 *	file javascript con nome [nome_applicazione].js
 *	
 *	@class waApplication
 */
var waApplication = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	
	/**
	 * dictionary degli oggetti
	 * waTable trovati all'interno della pagina, valorizzato automaticamente 
	 * dalla classe
	 * @memberof waApplication
	 */
	tables		: {},
	
	/**
	 * dictionary (array associativo: in javascript è un oggetto) degli oggetti
	 * waForm trovati all'interno della page, valorizzato automaticamente 
	 * dalla classe
	 * @memberof waApplication
	 */
	forms		: {},
	
	/**
	 * array degli oggetti waTable trovati all'interno della page, 
	 * valorizzato automaticamente dalla classe in ordine di creazione
	 * @memberof waApplication
	 */
	tablesIdx	: [],
	
	/**
	 * array degli oggetti waForm trovati all'interno della page, 
	 * valorizzato automaticamente dalla classe in ordine di creazione
	 * @memberof waApplication
	 */
	formsIdx	: [],
	
	/**
	 * primo oggetto waTable trovato all'interno della page; corrisponde
	 * al primo elemento di {@link tables} e  {@link tablesIdx}.  
	 * @memberof waApplication
	 */
	table 	: null,		// prima o unica table passata al costruttore
	
	/**
	 * primo oggetto waForm trovato all'interno della page; corrisponde
	 * al primo elemento di {@link forms} e  {@link formsIdx}.  
	 * @memberof waApplication
	 */
	form 		: null,		// primo o unico form passato al costruttore
	
	/**
	 * codice della modalità di navigazione da utilizzare, così come passato 
	 * dalla corrispondente classe PHP
	 * @memberof waApplication
	 */
	navigationMode	: '',		// codice della modalita' di navigazione da utilizzare (vedi waapplication.class.php)
	
	childWindow		: null,		// handle della finestra che contiene la page figlia quando si naviga in modalita' finestra
	childIframe		: null,		// oggetto iframe che contiene la page figlia quando si naviga in modalita' interna
	
	/**
	 * metodo da richiamare da parte della page figlia in modo che la page 
	 * parent si allinei alle modifiche apportate dalla figlia.
	 * 
	 * Per default questa proprietà punta al metodo {@link reloadPage}, che,
	 * appunto, si ricarica completamente per riflettere eventuali cambiamenti
	 * apportati dalla page figlia. E' ben possibile utilizzare un altro 
	 * metodo che, ad esempio, decida di recepire i cambiamenti tramite chiamate 
	 * RPC.
	 * 
	 * al metodo viene passato un dictionary di tutti gli elementi che hanno
	 * subito input.
	 * @memberof waApplication
	 */
	updateMethod	: null,	
	
	lastOffsetY	: 0,		// posizione di scroll verticale della page al momento in cui si apre una page figlia (serve per riposizionarsi sul riallineamento in modalita' navigazione interna)

	navigationModePage		: 'WAAPPLICATION_NAV_PAGE',	// vedi const in waApplication.class.php
	navigationModeWindow	: 'WAAPPLICATION_NAV_WINDOW',
	navigationModeInner	 	: 'WAAPPLICATION_NAV_INNER',
	
	// quando si naviga con finestra interna (iframe) non e' possibile chiamare
	// immediatamente il updateMethod di una eventuale nonna, perche'
	// se updateMethod prevede il ricaricamento della page (ed e' assai
	// probabile) cio' farebbe scomparire la mamma; per questo motivo salviamo
	// in un flag che quando la mamma (che ha ricevuto la chiamata a updateMethod)
	// si chiude, allora quello e' il momento di chiamare il updateMethod
	// della propria mamma (ossia la nonna della page che ha chiamato per 
	// prima updateMethod, che si deve trasmettere a cascata a tutte le
	// mamme)
	flagReloadGrandmaInnerNavigation: false,
	
	/**
	* lingua utilizzata
	*
	 * @type string
	 * @memberof waApplication
	*/	
	language : 'it',
	
	/**
	* locale utilizzato
	*
	 * @type string
	 * @memberof waApplication
	*/	
	locale : 'it-IT',

	//-------------------------------------------------------------------------
	/**
	 * inizializzazione (costruttore)
	 * 
	 * @memberof waApplication
	 */
	initialize: function() 
		{
		// definizione iniziale delle proprieta'
		this.updateMethod = this.reloadPage;
		this.childIframe = document.getElementById("waapplication_child_iframe");
		
		// cerca tutti gli oggetti di classe waTable e waForm presenti nella page
		// per associarli all'application
		for (var li = 0; li < document.forms.length; li++)
			this.addObject(document.forms[li]);
			
		// collegamento tra la table e le azioni non comprese da standard; pone
		// le azioni nello scope dell'application anziche' della table.
		this.linkTablesActions();
		
		// collegamento tra la table e le azioni non comprese da standard; pone
		// le azioni nello scope dell'application anziche' della table.
		this.linkTablesLinks();
		
		// dice alle tables che funzione utilizzare per aprire le finestre figlie
		this.linkTablesWindows();

		// collegamento tra il form e le azioni non comprese da standard
		// pone gli eventi nello scope dell'application 
		this.linkFormsEvents();
		
		// sostituisce le implementazioni di alert e confirm di waTable e waForm
		// con quelle contenute in queta classe
		this.overrideMessageFunctions();
	
		// se ci vien passato un parametro di posizionamento verticale proviamo a
		// rimettere la page nella posizione in cui l'utente l'ha lasciata
		// quando ha aperto una page figlia che evidentemente ci chiede di allinearci
		// alle sue modifiche
		var applRetY = this.getQSParam("wa_pry"); 
		if (applRetY)
			{
			try {window.scrollTo(0, applRetY);}
			catch (exception) {}		
			}
				
		},
	
	//-------------------------------------------------------------------------
	// data una form, verifica se e' una di quelle associate a waForm o
	// waTable; se del caso aggiunge gli oggetti a quelli gestiti
	// dall'applicazione
	addObject: function(form) 
		{
		var obj = '';
		var type;
		if (form.waTable)
			{
			type = "tables";
			obj = form.waTable;
			}
		else if (form.waForm)
			{
			type = "forms";
			obj = form.waForm;
			}
		else
			return;			
			
		obj.application = this;
		this[type][obj.name] = obj;
		this[type + "Idx"][this[type + "Idx"].length] = obj;
		
		if (obj.obj.waTable && !this.table)
			this.table = obj;
		else if (obj.obj.waForm && !this.form)
			this.form = obj;
		
		},
		
	//-------------------------------------------------------------------------
	/**
	 * metodo documentato a soli fini illustrativi, ma che non ha alcuna 
	 * necessità di essere invocato dal programmatore, in quanto richiamato
	 * dalla classe in fase di inizializzazione.
	 * 
	 * Questo metodo cerca eventuali metodi definiti all'interno della classe
	 * (e naturalmente sue derivate) che abbiano una corrispondenza con una
	 * delle azioni definite all'interno di un oggetto waTable. Se trova la
	 * corrispondenza, associa all'azione della tabella il metodo della classe,
	 * tale per cui quando l'utente richiede l'azione viene invocato il metodo
	 * della classe. Inoltre pone lo scope del metodo all'interno della classe
	 * derivata da waApplication, anzichè di waTable.
	 * <p>
	 * Ad esempio: supponiamo di avere nella pagina un oggetto waTable il cui
	 * nome è <b>table_ordini</b>, e che per quella tabella sia stato richiesto di
	 * implementare l'azione su riga <b>archivia</b>. 
	 * </p>
	 * Supponiamo ancora che abbiate istanziato in document.waPage un oggetto
	 * di classe derivata da waApplication e che implementi il metodo
	 * </p>
	 * <pre>
	 * action_table_ordini_archivia : function(idRiga)
	 * </pre>
	 * <p>
	 * Ebbene, ogni volta che l'utente premerà il bottone (o qualsiasi altro
	 * meccanismo) "archivia" verrà invocato il metodo da voi definito in 
	 * document.waPage, e lo scope del metodo sarà quello di document.waPage,
	 * ossia <b>this</b> corrisponderà a document.waPage e non a
	 * document.table_ordini.
	 * </p>
	 * <p>
	 * A questo punto voi potrete chiamare il vostro server avvertendolo che
	 * l'utente ha richiesto l'archiviazione della riga; ad esempio (supponendo
	 * che la riga ordine abbia anche una colonna con nome <b>titolo</b>):
	 * </p>
	 * <pre>
	 * {
	 * if (confirm("Confermi archiviazione ordine " + this.tables["table_ordini"].rows[idRiga].fields.titolo + "?"))
	 *	location.href = "?archivia=1&idRiga=" + idRiga;
	 * }
	 * </pre>
	 * @memberof waApplication
	 */
	linkTablesActions: function ()
		{
		var methodName;
		var tableName;
		
		for (methodName in this)
			{
			if (typeof(this[methodName]) == 'function' && methodName.substr(0, ("action_").length) == "action_")
				{
				for (tableName in this.tables)
					{
					if (methodName.substr(0, ("action_" + tableName + "_").length) == "action_" + tableName + "_")
						this.tables[tableName][methodName] = new Function ("id", "return this.application." + methodName + "(id)");
					}
				}
			}
		},

	//-------------------------------------------------------------------------
	/**
	 * metodo documentato a soli fini illustrativi, ma che non ha alcuna 
	 * necessità di essere invocato dal programmatore, in quanto richiamato
	 * dalla classe in fase di inizializzazione.
	 * 
	 * Questo metodo cerca eventuali metodi definiti all'interno della classe
	 * (e naturalmente sue derivate) che abbiano una corrispondenza con uno
	 * dei link definiti all'interno di un oggetto waTable. Se trova la
	 * corrispondenza, associa al link della tabella il metodo della classe,
	 * tale per cui quando l'utente segue il link viene invocato il metodo
	 * della classe. Inoltre pone lo scope del metodo all'interno della classe
	 * derivata da waApplication, anzichè di waTable.
	 * <p>
	 * Ad esempio: supponiamo di avere nella pagina un oggetto waTable il cui
	 * nome è <b>table_ordini</b>, e che per quella tabella sia stato richiesto di
	 * implementare un link sulla colonna <b>referente</b>. 
	 * </p>
	 * Supponiamo ancora che abbiate istanziato in document.waPage un oggetto
	 * di classe derivata da waApplication e che implementi il metodo
	 * </p>
	 * <pre>
	 * link_table_ordini_referente : function(idRiga)
	 * </pre>
	 * <p>
	 * Ebbene, ogni volta che l'utente seguirà il link nella cella della colonna
	 * <b>referente</b> verrà invocato il metodo da voi definito in 
	 * document.waPage, e lo scope del metodo sarà quello di document.waPage,
	 * ossia <b>this</b> corrisponderà a document.waPage e non a
	 * document.table_ordini.
	 * </p>
	 * <p>
	 * A questo punto voi potrete, ad esempio, invocare il client di posta 
	 * elettronica passandogli l'indirizzo email del referente (che avrete 
	 * ovviamente inserito in una delle colonne della tabella, magari non visibile):
	 * </p>
	 * <pre>
	 * {
	 * location.href = "mailto:" + this.tables["table_ordini"].rows[id].fields.emailReferente;
	 * }
	 * </pre>
	 * @memberof waApplication
	 */
	linkTablesLinks: function ()
		{
		var methodName;
		var tableName;
		
		for (methodName in this)
			{
			if (typeof(this[methodName]) == 'function' && methodName.substr(0, ("link_").length) == "link_")
				{
				for (tableName in this.tables)
					{
					if (methodName.substr(0, ("link_" + tableName + "_").length) == "link_" + tableName + "_")
						this.tables[tableName][methodName] = new Function ("id", "return this.application." + methodName + "(id)");
					}
				}
			}
		},

	//-------------------------------------------------------------------------
	// funzione richiamata automaticamente dalla linkFormsEvents
	applicationEvent: function (event)
		{
		// siamo nello scope dell'oggetto fisico che ha innescato l'event;
		// vogliamo andare nello scope di questo (this) oggetto;
		// cerchiamo qual'e' il controllo a cui appartiene l'oggetto che ha
		// scatenato l'event, in modo da ricostruire il nome del metodo da
		// richiamare
		
		var methodName = "event_on" + (window.event || event).type + "_";
		
		// verifichiamo se non sia un event del form
		if (this.elements)
			return this.waForm.application[methodName + this.id + "_" + this.id](event);

		var controlName = '';
		var a;
		for (controlName in this.form.waForm.controls)
			{
			if (this.form.waForm.controls[controlName].isMine(this))
				{
				a = 24;
				break;
				}
			a = 25;
			}
			
		methodName += this.form.id + "_" + this.form.waForm.controls[controlName].name;
		if (this.form.waForm.application[methodName])
			{
			return this.form.waForm.application[methodName](event);
			}
			
		},
		
	//-------------------------------------------------------------------------
	/**
	 * metodo documentato a soli fini illustrativi, ma che non ha alcuna 
	 * necessità di essere invocato dal programmatore, in quanto richiamato
	 * dalla classe in fase di inizializzazione.
	 * 
	 * Questo metodo cerca eventuali metodi definiti all'interno della classe
	 * (e naturalmente sue derivate) che abbiano una corrispondenza con un
	 * evento e uno dei controlli definiti all'interno di un oggetto waForm. Se trova la
	 * corrispondenza, associa l'evento al metodo della classe,
	 * tale per cui quando l'utente genera l'evento viene invocato il metodo
	 * della classe. Inoltre pone lo scope del metodo all'interno della classe
	 * derivata da waApplication, anzichè di waForm/waControl.
	 * <p>
	 * Ad esempio: supponiamo di avere nella pagina un oggetto waForm il cui
	 * nome è <b>form_ordini</b>, e che per quel form vogliate controllare
	 * l'event onblur sul controllo <b>importo</b>
	 * </p>
	 * Supponiamo ancora che abbiate istanziato in document.waPage un oggetto
	 * di classe derivata da waApplication e che implementi il metodo
	 * </p>
	 * <pre>
	 * event_onblur_form_ordini_importo: function (event)
	 * </pre>
	 * <p>
	 * Ebbene, ogni volta che l'utente genererà l'evento verrà invocato il metodo 
	 * da voi definito in 
	 * document.waPage, e lo scope del metodo sarà quello di document.waPage,
	 * ossia <b>this</b> corrisponderà a document.waPage e non a
	 * document.form_ordini.
	 * </p>
	 * <p>
	 * A questo punto voi potrete, ad esempio, verificare il superamento di una 
	 * data soglia:
	 * </p>
	 * <pre>
	 * {
	 * if (this.forms["form_ordini"].controls.importo.get() > 1000)
	 *	alert("Attenzione: superata soglia!");
	 * }
	 * </pre>
	 * @memberof waApplication
	 */
	linkFormsEvents: function ()
		{
		var elems = [];
		var eventName = '';
		var controlName = '';
		var methodName = '';
		var formName = '';
			
		for (methodName in this)
			{
			if (typeof(this[methodName]) == 'function' && methodName.substr(0, ("event_").length) == "event_")
				{
				elems = methodName.split("_", 3);
				eventName = elems[1];
				for (formName in this.forms)
					{
					if (methodName.substr(0, ("event_" + eventName + "_" + formName + "_").length) == 
												"event_" + eventName + "_" + formName + "_")
						{
						controlName = methodName.substr(("event_" + eventName + "_" + formName + "_").length);
						if (this.forms[formName].controls[controlName])
							this.forms[formName].controls[controlName].addEvent(eventName, this.applicationEvent);
						else if (formName == controlName)
							// e' stato richiesto un event sull'intero form (verosimilmente l'overload di onsubmit)
							this.forms[formName].obj[eventName] = this.applicationEvent;
						}
					}
				}
			}
		},

	//-------------------------------------------------------------------------
	// sostituisce le implementazioni di alert e confirm di waTable e waForm
	// con quelle contenute in queta classe
	overrideMessageFunctions: function ()
	    {
	    for (var li in this.tables)
			{
	    	this.tables[li].alert = this.alert;
	    	this.tables[li].confirm = this.confirm;
			}
			
	    for (var li in this.forms)
			{
	    	this.forms[li].alert = this.alert;
	    	this.forms[li].confirm = this.confirm;
			}
	    },
	    
	//-------------------------------------------------------------------------
	// dice alle tabelle che funzione utilizzare per aprire le finestre figlie
	linkTablesWindows: function ()
	    {
	    // ricordati che la classe table ha un reference all'application
	    for (var li in this.tables)
	    	this.tables[li].openPage = function (page, w, h) {return this.application.openPage(page, w, h);}
	    },
	    
	//-------------------------------------------------------------------------
	/**
	 * apre una pagina figlia
	 * 
	 * @param {string} page URL della pagina di destinazione
	 * @param {int} winW larghezza della finestra di destinazione; naturalmente
	 * ha senso solo per la modalità di navigazione a finestre; default 1024
	 * @param {int} winW altezza della finestra di destinazione; naturalmente
	 * ha senso solo per la modalità di navigazione a finestre; default 768
	 * @memberof waApplication
	 */
	openPage: function (page, winW, winH)
	    {
	    if (this.navigationMode == this.navigationModeWindow)
	    	return this.openPageWindow(page, winW, winH);
	    else if (this.navigationMode == this.navigationModeInner)
			return this.openPageInner(page);
		else
		    {
			var qs = this.removeQSParam("wa_pry");
		    var qoe = qs.substr(0, 1) == "?" ? "&" : "?";
		    var returnPage = encodeBase64(document.location.pathname + 
		    					qs + 
		    					qoe + 
		    					"wa_pry=" + this.getOffsetY());
		    qoe = page.indexOf("?") != -1 ? "&" : "?";
		    location.href = page + qoe + "wa_pr=" + returnPage;
		    }
		
	    },
	    
	//-------------------------------------------------------------------------
	openPageWindow: function (page, winW, winH)
	    {
		if (!winW) winW = 1024;
		if (!winH) winH = 768;
		
		this.closeChild();
		this.childWindow = window.open(page,"",
								"width=" + winW +
								",height=" + winH +
								",screenX=0,screenY=0,top=0,left=0" + 
								",bgcolor=white,scrollbars=yes,resizable=yes,status=yes");
	    },
	    
	//-----------------------------------------------------------------------------
	openPageInner: function (page)
		{
		this.lastOffsetY = this.getOffsetY();
		scroll(0,0);
		document.body.style.overflow = 'hidden';
		this.childIframe.style.visibility ='';
	    if (this.childIframe.contentWindow)
	   		this.childIframe.contentWindow.document.location.href = page;
	    else if (document.all)
			document.all[this.childIframe.id].src = page;  
	
	    },
	    
	//-------------------------------------------------------------------------
	/**
	 * chiude la page corrente
	 * @memberof waApplication
	 */
	closePage: function ()
	    {
	    if (this.navigationMode == this.navigationModeWindow)
	    	self.close();
	    else if (this.navigationMode == this.navigationModeInner)
	    	{
			// se una figlia di questo frame ha richiesto il caricamento a 
			// cascata delle mamme, questo e' il momento per dire alla mamma
			// di ricaricarsi
			if (parent.document.waPage.flagReloadGrandmaInnerNavigation && parent.document.waPage.updateMethod)
				parent.document.waPage.updateMethod();
			parent.document.body.style.overflow = '';
			parent.document.waPage.childIframe.style.visibility ='hidden';
		   	document.location.href = "about:blank";
			if (parent.document.waPage.lastOffsetY)
				{
				try {parent.scrollTo(0, parent.document.waPage.lastOffsetY);}
				catch (exception) {}		
				}
	    	}
		else
		    {
			var returnPage = this.getQSParam("wa_pr"); 
			if (returnPage)
				return location.href = decodeBase64(returnPage);
			this.errorMsg("return page not found");
		    }
	    	
	    },
    
	//-------------------------------------------------------------------------
	/**
	 * ritornna il valore di un parametro contenuto nella query-string dell'url corrente
	 * 
	 * @memberof waApplication
	 * @return {string}
	 */
	getQSParam: function (paramName)
		{
		var pairs = (document.location.search.substr(1)).split('&');
		for (var i = pairs.length - 1; i >= 0; i--)
			{
			var kv = pairs[i].split('=');
			if (kv[0] == paramName)
				return kv[1];
			}
		
		return false;
		},
		
	//-------------------------------------------------------------------------
	// restituisce lo posizione di scroll verticale della page, in modo che 
	// quando ne viene chiamato il riallineamento possa posizionarsi nella 
	// posizione dove l'utente l'aveva lasciata
	getOffsetY: function ()
		{
		var y = 0;
		try
			{
			if (document.documentElement && document.documentElement.scrollTop)
				y = document.documentElement.scrollTop;
			else if (document.body && document.body.scrollTop)
				y = document.body.scrollTop;
			else if (window.pageYOffset)
				y = window.pageYOffset;
			}
		catch(exception){}
		return y;
		},
	
	//-------------------------------------------------------------------------
	// metodo richiamato da una finestra figlia quando vuole comunicare alla mamma
	// di allinearsi alle modifiche da lei effettuate
	updateParent: function (inputValues)
	    {
	    if (this.navigationMode == this.navigationModePage)
	    	return;
	    	
		var inputValues = decodeBase64(inputValues);
		var elems = inputValues.split("|||");
		var param;
		inputValues = {};
		for (var i = 0; i < elems.length; i++)
			{
			 param = elems[i].split("=", 2);
			 if (param[0] != "")
			 	inputValues[param[0]] = param[1];
			}
		var mamma = opener ? opener : parent;
		mamma.document.waPage.updateMethod(inputValues);
	    },
    
	//-------------------------------------------------------------------------
	// il default delle funzioni di sincronizzazione che una finestra child 
	// chiama per far si che la mamma si allinei; nel caso piu' semplice
	// (questo), praticamente si ricarica. se avete bisogno di qualcosa di
	// piu' complesso, definite una vostra "updateMethod"
	reloadPage: function (childInputData)
		{
		if (opener)
			{
			if (opener.document.waPage && opener.document.waPage.updateMethod)
				opener.document.waPage.updateMethod(childInputData);
			}			
		else 
			parent.document.waPage.flagReloadGrandmaInnerNavigation = true;
			
		// in caso di ricaricamento di finestra a causa di inserimento/modifica record
		// non dobbiamo chiudere  ne' la finestra di help, ne' quella service, quindi
		// dobbiamo ridefinire l'event onUnload del documento
		if (document.layers)
			window.captureEvents(Event.ONUNLOAD);
		window.onunload = '';
		
		// ricarichiamo la finestra... sarebbe facile fare una reload; e invece no
		// perche' senno' il browser ci chiede l'autorizzazione in caso di precedente
		// submit. 
		// OCCHIO! si perdono eventuali parametri POST! se servono, fatevi 
		// un updateMethod apposta
		var qs = this.removeQSParam("wa_pry");
			
		// aggiungiamo al ricaricamnto il parametro per lo spiazzamento verticale,
		// cosi' l'utente ritrova la page nella posizione dove l'ha lasciata
	    if (this.navigationMode == this.navigationModeWindow)
			qs += (qs == '' ? '?' : '&') + "wa_pry=" + this.getOffsetY();
	    else if (this.navigationMode == this.navigationModeInner)
			qs += (qs == '' ? '?' : '&') + "wa_pry=" + this.lastOffsetY;
		
		document.location.href = document.location.pathname + qs;
		},
	

	//-------------------------------------------------------------------------
	/**
	 * 
	* toglie un parametro dalla query string (location.search) 
	* @memberOf waApplication
	* @return {string} la nuova query string senza il parametro appena rimosso
	*/
	removeQSParam: function (param)
	    {
		var qs = '';
		if (document.location.search.length)
			{
			var kv = new Array();
			var pairs = (document.location.search.substr(1)).split('&');
			for (var i = 0; i < pairs.length; i++)
				{
				kv = pairs[i].split('=');
				if (kv[0] != param)
					qs += (qs == '' ? '?' : '&') + kv[0] + "=" + (kv[1] ? kv[1] : '');
				}
			}
			
		return qs;
			
		},
		
	//-------------------------------------------------------------------------
	// metodo invocato sulla unload del documento; se la navigazione e' a 
	// finestre chiude una eventuale finestra figlia (e a cascata le figlie
	// chiuderanno le figlie)
	closeChild: function ()
	    {
		if (this.childWindow && !this.childWindow.closed)
			{
			this.childWindow.document.waPage.closePage();
			this.childWindow = null;
			}
	    },

	//-------------------------------------------------------------------------
	/**
	 * mostra un messaggio di errore e torna false
	* @memberOf waApplication
	* @return {boolean} sempre false
	 */
	errorMsg: function (msg)
	    {
	    this.alert(msg);
	    return false;
	    },

	//-------------------------------------------------------------------------
	/**
	 * verifica se un dictionary ha elementi al suo interno oppure no
	* @memberof waApplication
	* @return (boolean)
	*/
	isDictionaryEmpty: function (dictionary)
		{
		for (var li in dictionary)
			return false;
		
		return true;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * restituisce il nr di elemnti contenuti in un dictionary 
	 * (comodo quando si hanno liste chiave/valore)
	 * 
	* @memberof waApplication
	* @return (integer)
	*/
	dictionaryLength: function (dictionary)
		{
		var retval = 0;
		
		for (var li in dictionary)
			retval++;
		
		return retval;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * implementazione alert asincrona
	 * 
	 * @param {string} msg testo del messaggio
	 * @memberOf waApplication
	*/
	alert: function(msg)
		{
		var text = msg.replace(/\n/g, "<br>");
		jQuery("#waapplication_alert .modal-body").html(text);
		jQuery("#waapplication_alert_close").show();
		jQuery("#waapplication_confirm_cancel").hide();
		jQuery("#waapplication_confirm_confirm").hide();
		jQuery("#waapplication_alert").modal({backdrop: 'static'});
		},
	
	//-------------------------------------------------------------------------
	/**
	 * implementazione confirm asincrona
	 * 
	 * @param {string} msg testo del messaggio
	 * @param {function} callback - viene richiamata solo a fronte di confirm
	 * @param {object} callbackOptions opzioni da passare a callback
	 * @memberof waApplication
	 */ 
	confirm: function(msg, callback, callbackOptions) 
		{
		var text = msg.replace(/\n/g, "<br>");
		jQuery("#waapplication_alert .modal-body").html(text);
		jQuery("#waapplication_alert_close").hide();
		jQuery("#waapplication_confirm_cancel").show();
		jQuery("#waapplication_confirm_confirm").show();
		jQuery("#waapplication_confirm_confirm").off().on("click", function() {callback(callbackOptions);});
		jQuery("#waapplication_alert").modal({backdrop: 'static'});
		},
	
	//-------------------------------------------------------------------------
	/**
	 * mostra il contenuto di un oggetto (utile in fase di debug per i dizionari)
	* @memberOf waApplication
	 */
	showObject: function (obj)
		{
		var msg = '';
		for (var li in obj)
			msg += li + "=" + obj[li] + "\n";
		alert(msg);
		}

		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
