<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/watable_data_column_header_input_option.class.php";


//***************************************************************************
//****  classe waTableDataColumnHeaderInput *********************************
//***************************************************************************
/**
* waTableDataColumnHeaderInput
*
* struttura dati da passare al view-object: specifiche per l'input all'interno 
* di una colonna
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataColumnHeaderInput 
	{
	/**
	 * tipo input (si vedano \waLibs::INPUT_*)
	 * 
	 * @var string
	 */
	public $type;

	/**
	 * input obbligatorio
	 * 
	 * @var bool
	 */
	public $mandatory;

	/**
	 * lunghezza massima campo (occhio! underscore)
	 * 
	 * @var int
	 */
	public $fieldMaxLen;

	/**
	 * eventuali opzioni (di select o radio...) {@link waTableDataColumnHeaderInpuOption}
	 * 
	 * @var array
	 */
	public $options;

	}
	
