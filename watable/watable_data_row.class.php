<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataRow ***********************************************
//***************************************************************************
/**
* waTableDataRow
*
* struttura dati da passare al view-object: dati di una riga della tabella
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataRow 
	{
	/**
	 * identificativo della riga
	 * 
	 * @var int
	 */
	public $id;

	/**
	 * array delle azioni su record abilitate; ogni elemento dell'array è un 
	 * bool che indica se la corrispondente azione su record definita in 
	 * waTableData::recordActions è abilitata o meno
	 * 
	 * @var array
	 */
	public $enableableActions;

	/**
	 * array dei valori contenuti nelle celle; ogni elemento dell'array corrisponde
	 * posizionalmente alla colonna definita in 
	 * waTableData::columnHeaders
	 * 
	 * @var mixed
	 */
	public $cells;

	}
	
