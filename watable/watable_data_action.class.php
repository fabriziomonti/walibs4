<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataAction *****************************************
//***************************************************************************
/**
* waTableDataAction
*
* struttura dati da passare al view-object: azione
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataAction 
	{
	/**
	 * nome dell'azione
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * etichetta dell'azione
	 * 
	 * @var string
	 */
	public $label;
		
	}
	
