//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	tables		: {},
	forms		: {},
	tablesIdx	: [],
	formsIdx	: [],
	// proprieta' valorizzate e utilizzabili nel caso in cui la pagina gestisca una sola table/form
	table 	: null,		// prima o unica table passata al costruttore
	form 		: null,		// primo o unico form passato al costruttore

	//-------------------------------------------------------------------------
	//initialization
	initialize: function(tables, forms) 
		{
		// definizione iniziale delle proprieta'
		if (tables)
			{
			if (tables instanceof Array)	
				{
				this.tablesIdx = tables;
				for (var i = 0; i < tables.length; i++)
					{
					this.tables[tables[i].name] = tables[i];
					this.tables[tables[i].name].application = this;
					}
				this.table = this.tablesIdx[0];
				}
			else
				this.table = this.tablesIdx[0] = this.tables[tables.name] = tables;
			}

		if (forms)
			{
			if (forms instanceof Array)	
				{
				this.formsIdx = forms;
				for (var i = 0; i < forms.length; i++)
					{
					this.forms[forms[i].name] = forms[i];
					this.forms[forms[i].name].application = this;
					}
				this.form = this.formsIdx[0];
				}
			else
				this.form = this.formsIdx[0] = this.forms[forms.name] = forms;
			}
						
		// collegamento tra la table e le azioni non comprese da standard; pone
		// le azioni nello scope dell'application anziche' della table.
		this.bindTableActions();
		
		// collegamento tra la table e le azioni non comprese da standard; pone
		// le azioni nello scope dell'application anziche' della table.
		this.bindTableLinks();
		
		},
	
	//-------------------------------------------------------------------------
	// collegamento tra la table e i link che possono essere presentinelle colonne; pone
	// i metodi nello scope dell'application anziche' della table.
	bindTableActions: function ()
		{
		var nomeMetodo;
		var tableName;
		
		for (nomeMetodo in this)
			{
			if (typeof(this[nomeMetodo]) == 'function' && nomeMetodo.substr(0, ("action_").length) == "action_")
				{
				for (tableName in this.tables)
					{
					if (nomeMetodo.substr(0, ("action_" + tableName + "_").length) == "action_" + tableName + "_")
						this.tables[tableName][nomeMetodo] = new Function ("id", "return this.application." + nomeMetodo + "(id)");
					}
				}
			}
		},

	//-------------------------------------------------------------------------
	// collegamento tra la table e i link che possono essere presentinelle colonne; pone
	// i metodi nello scope dell'application anziche' della table.
	bindTableLinks: function ()
		{
		var nomeMetodo;
		var tableName;
		
		for (nomeMetodo in this)
			{
			if (typeof(this[nomeMetodo]) == 'function' && nomeMetodo.substr(0, ("link_").length) == "link_")
				{
				for (tableName in this.tables)
					{
					if (nomeMetodo.substr(0, ("link_" + tableName + "_").length) == "link_" + tableName + "_")
						this.tables[tableName][nomeMetodo] = new Function ("id", "return this.application." + nomeMetodo + "(id)");
					}
				}
			}
		},

	//-------------------------------------------------------------------------
	showObject: function (obj)
		{
		var msg = '';
		for (var li in obj)
			msg += li + "=" + obj[li] + "\n";
		this.alert(msg);
		},

	//-------------------------------------------------------------------------
	// verifica se un oggetto dizionario ha elementi al suo interno oppure no
	emptyDictionary: function (dizionario)
		{
		for (var li in dizionario)
			return false;
		
		return true;
		},
		
	//-------------------------------------------------------------------------
	action_table0_Custom: function (id)
		{
		var link = "<a href='javascript:alert(\"" + 
							this.tables.table0.rows[id].fields['importo'] + "\")'>" +
							this.tables.table0.rows[id].getCellContent("rifpa") + 
							"</a>";
							
		this.tables.table0.rows[id].setCellContent("rifpa", link);
		},

	//-------------------------------------------------------------------------
	action_table0_waTable_default: function ()
		{
		location.href = "?type=waTable_default";
		},

	//-------------------------------------------------------------------------
	action_table0_waTable_actions_right: function ()
		{
		location.href = "?type=waTable_actions_right";
		},

	//-------------------------------------------------------------------------
	action_table0_waTable_actions_context: function ()
		{
		location.href = "?type=waTable_actions_context";
		},

	//-------------------------------------------------------------------------
	action_table0_waTable_actions_context_left_click: function ()
		{
		location.href = "?type=waTable_actions_context_left_click";
		},

	//-------------------------------------------------------------------------
	action_table0_waTable_edit: function ()
		{
		location.href = "?type=waTable_edit";
		},

	//-------------------------------------------------------------------------
	action_table0_waTable_quickedit: function ()
		{
		location.href = "?type=waTable_quickedit";
		},

	//-------------------------------------------------------------------------
	link_table0_sigla: function (id)
		{
		this.alert(this.tables.table0.rows[id].fields.nomeamm);
		},
		
	//-------------------------------------------------------------------------
	action_table1_Custom: function (id)
		{
		var link = "<a href='javascript:alert(\"" + 
							this.tables.table1.rows[id].fields['importo'] + "\")'>" +
							this.tables.table1.rows[id].getCellContent("rifpa") + 
							"</a>";
							
		this.tables.table1.rows[id].setCellContent("rifpa", link);
		},

	//-------------------------------------------------------------------------
	action_table1_waTable_default: function ()
		{
		location.href = "?type=waTable_default";
		},

	//-------------------------------------------------------------------------
	action_table1_waTable_actions_right: function ()
		{
		location.href = "?type=waTable_actions_right";
		},

	//-------------------------------------------------------------------------
	action_table1_waTable_actions_context_left_click: function ()
		{
		location.href = "?type=waTable_actions_context_left_click";
		},

	//-------------------------------------------------------------------------
	action_table1_waTable_edit: function ()
		{
		location.href = "?type=waTable_edit";
		},

	//-------------------------------------------------------------------------
	action_table1_waTable_quickedit: function ()
		{
		location.href = "?type=waTable_quickedit";
		},

	//-------------------------------------------------------------------------
	link_table1_sigla: function (id)
		{
		this.alert(this.tables.table1.rows[id].fields.nomeamm);
		}
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
var tables = [];
tables.push(document.table0);
tables.push(document.table1);
var pagina = new waPage(tables);