<?php
error_reporting(E_ERROR|E_WARNING);

//******************************************************************************
/**
 * inclusione dei moduli walibs che ci servono in questa pagina
 */
include("../../waform/waform.class.php");
include("../watable_quickedit.class.php");

// file contenente i parametri di connessione al database
define ("FILE_CONFIG_DB", dirname(__FILE__) . "/dbconfig.inc.php");

// creazione degli oggetti waTable che mostreremo nella pagina
$table0 = dammiTabella("table0");
$table1 = dammiTabella("table1");

// creazione dell'header della pagina html
$outputBuffer = creaTop();

// corpo della pagina contenente le due tabelle
$outputBuffer .= $table0->show(true);
$outputBuffer .=  "<p /><hr /><p />";
$outputBuffer .= $table1->show(true);

// inclusione delle procedure di controllo specifiche di questa pagina
// che verranno utilizzate in interfaccia utente
$outputBuffer .=  "<script type='text/javascript' src='ui/js/index.js'></script>\n";

// chiusura della pagina html
$outputBuffer .= creaFooter();

// risistema il codice html rendendolo "strict"
$outputBuffer = fammeloStrict($outputBuffer);

			
exit($outputBuffer);

//*************************************************************************
/**
 * Enter description here...
 *
 * @param unknown_type $nome
 * @return waLibs\waTable
 */
function dammiTabella($nome)
	{		
	// creazione della tabella sulla base della query sql
	$sql = "SELECT corsi.*, organismi.nome as nomeorgan, amministrazioni.sigla, amministrazioni.nome as nomeamm" .
			" FROM corsi" .
			" LEFT JOIN amministrazioni ON corsi.id_amministrazione=amministrazioni.id_amministrazione".
			" LEFT JOIN organismi ON corsi.id_organismo=organismi.id_organismo" .
			"";
//			" ORDER BY corsi.id_corso";
	$className =  "walibs\\" . ($_GET["type"] == "waTable_quickedit" ? "waTable_quickedit" : "waTable_edit");
	$table = new $className($sql, FILE_CONFIG_DB);
	
	if ($_GET["type"])
		{
		include_once __DIR__ . "/../uis/" . strtolower($_GET["type"]) . "/view/watable.php";
		$class = "\\waLibs\\views\\waTable\\$_GET[type]\\waTableView";
		$table->view = new $class();
		}
	
	$table->name = $nome;
	$table->listMaxRec = 3;
	$table->pdf_orientation = "L";
	
	// definizione delle proprieta' di base della tabella
	$table->title = "corsi";
	$table->formPage = "../../waform/test/index.php";
	
	// definizione delle azioni della tabella
	$table->actions['Delete']->enablingFunction = "checkEnableDelete";
	$table->addAction("waTable_default", false, "Left");
	$table->addAction("waTable_actions_right", false, "Right");
	$table->addAction("waTable_actions_context", false, "Context");
	$table->addAction("waTable_actions_context_left_click", false, "Context left");
	$table->addAction("waTable_edit", false, "Edit");
	$table->addAction("waTable_quickedit", false, "Quick edit");
	$table->addAction("Custom", true, "Cus tom");
	
	// definizione delle columns della tabella e delle relative proprieta'
	$col = $table->addColumn("id_corso", "ID", true, true, true, waLibs\waTable::ALIGN_R, waLibs\waTable::FMT_INTEGER);
		$col->pdfPerc = 5;
	
	// nelle successive 2 columns facciao cose diverse a seconda del fatto che
	// siamo in edit o meno
	if (stripos($_GET["type"], "edit"))
		{
		$col = $table->addColumn("id_organismo", "Org.", true, true, true, waLibs\waTable::ALIGN_C, waLibs\waTable::FMT_INTEGER);
		$col->inputType = waLibs\waTable::INPUT_SELECT;
		$col->inputMandatory = true;
		$col->aliasOf = "organismi.id_organismo";
		$dbconn = waLibs\wadb_getConnection(FILE_CONFIG_DB);
		$rs = new waLibs\waRecordset($dbconn);
		$rs->read("SELECT * FROM organismi ORDER BY nome");
		$col->inputOptions[''] = '';
		foreach ($rs->records as $record)
			$col->inputOptions[$record->id_organismo] = $record->nome;
		$col->pdfPerc = 5;
		}
	else
		{
		$col = $table->addColumn("nomeorgan", "Org.");
		$col->aliasOf = "organismi.nome";
		$col->pdfPerc = 12;
		}		
		
	if (stripos($_GET["type"], "edit"))
		{
		$col = $table->addColumn("id_amministrazione", "Amm.ne");
		$col->inputType = waLibs\waTable::INPUT_SELECT;
		$col->inputMandatory = true;
		$col->aliasOf = "amministrazioni.id_amministrazione";
		$rs = new waLibs\waRecordset($dbconn);
		$rs->read("SELECT * FROM amministrazioni ORDER BY nome");
		$col->inputOptions[''] = '';
		foreach ($rs->records as $record)
			$col->inputOptions[$record->id_amministrazione] = $record->sigla;
		}
	else
		{
		$col = $table->addColumn("sigla", "Amm.ne");
		$col->link = true;
		}		
	$col->pdfPerc = 9;
		
	$col = $table->addColumn("rifpa", "Rif. P.A.");
		$col->inputType = waLibs\waTable::INPUT_TEXT;
		$col->maxCaratteri = 10;
		$col->inputMandatory = true;
		$col->pdfPerc = 8;
	
	$col = $table->addColumn("nome", "Nome");
		$col->aliasOf = "corsi.nome";
		$col->inputType = waLibs\waTable::INPUT_TEXT;
		$col->maxcaratteri = 50;
//		$col->inputMandatory = true;
		$col->pdfPerc = 10;
//		$col->convertiHTML = false;
	
	$col = $table->addColumn("data_inizio", "Data Inizio", true, true, true, waLibs\waTable::ALIGN_C);
		$col->inputType = waLibs\waTable::INPUT_DATE;
//		$col->inputMandatory = true;
		$col->pdfPerc = 10;
	
	$col = $table->addColumn("data_fine", "Data ora fine", true, true, true, waLibs\waTable::ALIGN_C);
		$col->inputType = waLibs\waTable::INPUT_DATETIME;
//		$col->inputMandatory = true;
		$col->pdfPerc = 15;
	
	$col = $table->addColumn("nr_ore", "Nr. ore", true, true, true, waLibs\waTable::ALIGN_R);
		$col->inputType = waLibs\waTable::INPUT_INTEGER;
		$col->pdfPerc = 5;
		$col->totalize = true;
	
	$col = $table->addColumn("importo", "Importo", true, true, true, waLibs\waTable::ALIGN_R);
		$col->totalizza = true;
		$col->inputType = waLibs\waTable::INPUT_CURRENCY;
		$col->pdfPerc = 12;
	
//	$col = $table->addColumn("attivo", "Attivo", true, true, true, waLibs\waTable::ALIGN_C);
//		$col->inputType = WATABLE_INPUT_LOGICO;
//		$col->pdfPerc = 7;
	
	$col = $table->addColumn("eliminabile", "Eliminabile", true, false, false, waLibs\waTable::ALIGN_C);
		$col->computeFunction = "showDeletability";
		$col->pdfPerc = 11;
	
	// una colonna non visibile il cui contenuto viene usato dalle 
	//procedure di controllo della UI
	$col = $table->addColumn("nomeamm", "nomeamm", false, false, false);
	
	// verifica che non sia stato richiesto un eventuale input dati
	// (ovviamente la chiamata ha senso solo se la tabella prevede input)
	$table->getInputValues ();
	if ($table->isToUpdate())
		{
		if (!($table->save (true)))
			exit("Errore su db: " . $table->recordset->errorNr() . " - " . $table->recordset->errorMessage() ."<hr>");
		}

	// lettura dal database dei records che andranno a popolare la tabella
	if (!$table->loadRows())
		exit("Errore su db: " . $table->recordset->errorNr() . " - " . $table->recordset->errorMessage() ."<hr>");
		
	// se ci viene passato il parametro xml in query-string, l'utnte vuole
	// solo vedere l'xml generato per l'oggetto waTable; ovviamente viene 
	// mostrato solo quello della prima tabella della pagina
	if ($_GET['json'])
		exit($table->showJSON());
	
	return $table;
	}

//*************************************************************************
function showDeletability(waLibs\waTable $table)
	{		
	return $table->record->value("importo") <= 100000 ? 'si' : 'no';
	}

//*************************************************************************
function checkEnableDelete(waLibs\waTable $table)
	{		
	return $table->record->value("importo") <= 100000;
	}

//*****************************************************************************
function showArr($val)
	{
	echo "<pre>" . print_r($val, true) . "</pre><hr>";
	}
	
//*****************************************************************************
function creaTop()
	{
	// intestazione della pagina html...
	header("Content-Type: text/html; charset=UTF-8");
	$buffer = "
				<!DOCTYPE html>
				<html>
					<head>
						<meta charset='utf-8'>
						<meta http-equiv='X-UA-Compatible' content='IE=edge'>
						<meta name='viewport' content='width=device-width, initial-scale=1'>
						<title>waTable Test</title>

					</head>
					<body>
					<div class='container-fluid'>
				
				";
	
	return $buffer;
	}
	
//*****************************************************************************
function creaFooter()
	{
	// chiusura della pagina html
	return "</div></body></html>";
	}
	
//*****************************************************************************
function fammeloStrict($buffer)
	{
	// spostiamo eventuali "<link ...>" nell'head per essere "strict"
	list ($pre_head, $resto) = explode("<head>", $buffer, 2);
	list ($head, $resto) = explode("</head>", $resto, 2);
	list ($tra_head_e_body, $body) = explode("<body", $resto, 2);
	while (true)
		{
		list ($prima, $dopo) = explode("<link ", $body, 2);
		if (!$dopo)
			break;
		list ($link, $dopo) = explode(">", $dopo, 2);
		$head .= "<link $link" . (substr(rtrim($link), -1) == "/" ? '' : " /") . ">\n";
		$body = "$prima$dopo";
		}

	$buffer = "$pre_head\n<head>\n$head</head>$tra_head_e_body\n<body$body";

	return $buffer;
	}

