<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include della struttura dati da passare al view object
* @ignore
*/
include_once __DIR__ . "/watable_data.class.php";

//***************************************************************************
//****  interfaccia i_waTableView ********************************************
//***************************************************************************
/**
* i_waTableView
*
* interfaccia che tutti i {@link waTable::view view-object} utiizzati da waTable
* devono rispettare
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
interface i_waTableView
{
	/**
	 * trasforma i dati passati in input nella UI prevista dal view-object
	 * 
	 * @param waTableData $data i dati da trasformare
	 */
    public function transform(waTableData $data);
}