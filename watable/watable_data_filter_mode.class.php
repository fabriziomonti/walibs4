<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataFilterMode ****************************************
//***************************************************************************
/**
* waTableDataFilterMode
*
* struttura dati da passare al view-object: filter mode
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataFilterMode 
	{
	/**
	 * nome del modo di filtro 
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * valore del modo di filtro (lt|le|eq...)
	 * 
	 * @var string
	 */
	public $value;
		
	}
	
