<?php

/**
 * -
 *
 * @package waTable
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */

namespace waLibs;

//******************************************************************************
/**
 * @ignore
 */
include_once(dirname(__FILE__) . '/ya_wam_pdf.php');

//******************************************************************************
/**
 * classettina che dato un oggetto waTable ne produce l'output
 * pdf
 * 
 * @ignore
 */
//******************************************************************************
class watable_pdf extends ya_wam_pdf {

    var $table;
    var $widths = array();
    var $alignments = array();

    //**************************************************************************

    /**
     * 
     */
    function __construct(waTable $table) {
        $this->table = $table;
        parent::__construct($this->table->pdfOrientation);
        $this->AliasNbPages();
    }

    //**************************************************************************

    /**
     *
     */
    function Header() {
        if ($this->page == 1) {
            $this->SetFont("Arial", 'B', 12);
            $this->Cell(0, 8, $this->table->title, 0, 1, 'C');
        }

        $this->SetFillColor(0xcc);
        $this->SetFont("Arial", 'B', 9);
        $cells = array();
        foreach ($this->table->columns as $col) {
            if ($col->show)
                $cells[] = $col->label;
        }
        $this->MultiLineRow($this->widths, 5, $cells, 1, $this->alignments, 1);
    }

    //**************************************************************************

    /**
     * 
     */
    function Footer() {
        $this->SetY(-12);
        $this->Cell(0, 4, 'page ' . $this->PageNo() . " of {nb}", "T", 0, 'C');
    }

    //**************************************************************************

    /**
     * determina l'allineamento di una colonna secondo quanto definito dal
     * programmatore o il default del tipo campo
     *
     */
    function getColumnAlignment(waColumn $col) {
        static $alignment_codes = array(waTable::ALIGN_C => 'C', waTable::ALIGN_R => 'R', waTable::ALIGN_L => 'L');
        if ($col->alignment) {
        // se è stato definito un allineamento, non c'e' bisogno di 
        // desumerlo dal tipo campo
            return $alignment_codes[$col->alignment];
        }

        if ($this->table->matrix) {
            return $alignment_codes[waTable::ALIGN_L];
        }

        switch ($this->table->recordset->fieldType($col->name)) {
            case waDB::DECIMAL:
            case waDB::INTEGER:
                return $alignment_codes[waTable::ALIGN_R];
                break;
            case waDB::DATE:
            case waDB::DATETIME:
            case waDB::TIME:
                return $alignment_codes[waTable::ALIGN_C];
                break;
        }

        return $alignment_codes[waTable::ALIGN_L];
    }

    //**************************************************************************

    /**
     * determina la larghezza di una colonna secondo quanto definito dal
     * programmatore o il default del tipo campo
     *
     */
    function getColumnWidth(waColumn $col, $pageWidth) {
        if ($this->table->matrix) {
            if (!$col->pdfPerc) {
                $col->pdfPerc =  100 / count($this->table->columns);
            }
            return $col->pdfPerc * $pageWidth / 100;
        }

        $rs = $this->table->recordset;

        if ($col->pdfPerc)
            return $col->pdfPerc * $pageWidth / 100;
        elseif ($rs->fieldType($col->name) == waDB::DECIMAL)
            return 40;
        elseif ($rs->fieldType($col->name) == waDB::DATE)
            return 40;
        elseif ($rs->fieldType($col->name) == waDB::DATETIME)
            return 40;
        elseif ($rs->fieldMaxLength($col->name) > 50)
            return 50;
        elseif ($rs->fieldMaxLength($col->name) < 20)
            return 20;

        return $rs->fieldMaxLength($col->name);
    }

    //**************************************************************************

    /**
     * determina euristicamente una larghezza delle columns per 
     * cui la percentuale di larghezza non è stata definita e apre il pdf
     *
     */
    function open() {

        parent::Open($this->table->pdf_orientazione);
        $this->SetTitle($this->table->title, true);
        $rs = $this->table->recordset;
        $pageWidth = $this->w - ($this->lMargin + $this->rMargin);

        // determinazione degli attributi delle columns (allineamento e larghezza
        foreach ($this->table->columns as $col) {
            if ($col->show) {
                $this->alignments[] = $this->getColumnAlignment($col);
                $totlen += $this->widths[] = $this->getColumnWidth($col, $pageWidth);
            }
        }

        for ($i = 0; $i < count($this->widths); $i++)
            $totLarghezze += $this->widths[$i] = intval(floor($this->widths[$i] * $pageWidth / $totlen));

        // aggiungiamo lo sfrido all'ultima colonna 
        $this->widths[count($this->widths) - 1] += intval(floor($pageWidth - $totLarghezze));

        $this->AddPage();
    }

    //**************************************************************************

    /**
     * esporta la table
     *
     */
    function export() {

        $this->SetFillColor(0xff);
        $this->SetFont("Arial", '', 9);

        // si legge da db a blocchi di 100 righe, in modo da non sforare il 
        // memory limit
        while ($this->table->readNextExportBlock()) {
            foreach ($this->table->recordset->records as $this->table->record) {
                $cells = $widths = $alignments = array();
                foreach ($this->table->columns as $col) {
                    if ($col->show) {
                        if (!empty($col->computeFunction))
                            $cells[] = call_user_func($col->computeFunction, $this->table);
                        else {
                            switch ($col->format) {
                                case waTable::FMT_DATE:
                                    $cells[] = $this->table->record->value($col->name) ?
                                            date("d/m/Y", $this->table->record->value($col->name)) :
                                            '';
                                    break;
                                case waTable::FMT_DATETIME:
                                    $cells[] = $this->table->record->value($col->name) ?
                                            date("d/m/Y H:i", $this->table->record->value($col->name)) :
                                            '';
                                    break;
                                case waTable::FMT_TIME:
                                    $cells[] = $this->table->record->value($col->name) ?
                                            date("H:i", $this->table->record->value($col->name)) :
                                            '';
                                    break;
                                case waTable::FMT_DECIMAL:
                                    $cells[] = $this->table->record->value($col->name) !== null && $this->table->record->value($col->name) !== '' ?
                                            number_format($this->table->record->value($col->name), $col->decimalDigitsNr, ",", ".") :
                                            '';
                                    break;
                                case waTable::FMT_STRING:
                                case waTable::FMT_INTEGER:
                                case waTable::FMT_RAW:
                                    $cells[] = $this->table->record->value($col->name);
                                    break;
                                default:
                                    $Index = $this->table->recordset->fieldIndex($col->name);
                                    switch ($this->table->recordset->fieldType($col->name)) {
                                        case waDB::DECIMAL:
                                            $cells[] = $this->table->record->value($col->name) !== null && $this->table->record->value($col->name) !== '' ?
                                                    number_format($this->table->record->value($col->name), $col->decimalDigitsNr, ",", ".") :
                                                    '';
                                            break;
                                        case waDB::DATE:
                                            $cells[] = $this->table->record->value($col->name) ?
                                                    date("d/m/Y", $this->table->record->value($col->name)) :
                                                    '';
                                            break;
                                        case waDB::DATETIME:
                                            $cells[] = $this->table->record->value($col->name) ?
                                                    date("d/m/Y H:i", $this->table->record->value($col->name)) :
                                                    '';
                                            break;
                                        case waDB::TIME:
                                            $cells[] = $this->table->record->value($col->name) ?
                                                    date("H:i", $this->table->record->value($col->name)) :
                                                    '';
                                            break;
                                        default:
                                            $cells[] = $this->table->record->value($col->name);
                                    }
                            }
                        }
                    }
                }
                if (!$this->page)
                    $this->open();

                $this->MultiLineRow($this->widths, 4, $cells, 1, $this->alignments, 0);
            }
        }

        $this->Output($this->table->lettersNumbersOnly($this->table->title) . date("_YmdHis") . ".pdf", 'I');
        exit();
    }

    //**************************************************************************

    /**
     * esporta la table
     *
     */
    function exportFromMatrix() {

        $this->SetFillColor(0xff);
        $this->SetFont("Arial", '', 9);

        foreach ($this->table->matrix as $this->table->record) {
            $cells = $widths = $alignments = array();
            foreach ($this->table->columns as $col) {
                if ($col->show) {
                    if (!empty($col->computeFunction)) {
                        $cells[] = call_user_func($col->computeFunction, $this->table);
                    } 
                    else {
                        $cells[] = $this->table->format($col);
                    }
                }
            }
            if (!$this->page) {
                $this->open();
            }

            $this->MultiLineRow($this->widths, 4, $cells, 1, $this->alignments, 0);
        }

        $this->Output($this->table->lettersNumbersOnly($this->table->title) . date("_YmdHis") . ".pdf", 'I');
        exit();
    }

    //*****************************************************************************
}

//*****************************************************************************
