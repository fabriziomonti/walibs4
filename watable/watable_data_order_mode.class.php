<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataOrderMode *****************************************
//***************************************************************************
/**
* waTableDataOrderMode
*
* struttura dati da passare al view-object: order mode
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataOrderMode 
	{
	/**
	 * nome del modo di ordinamento 
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * valore del modo di ordinamento (asc|desc)
	 * 
	 * @var string
	 */
	public $value;
		
	}
	
