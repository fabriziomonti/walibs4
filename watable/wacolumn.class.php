<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

//***************************************************************************
//****  classe waColumn ****************************************************
//***************************************************************************
/**
* waColumn
*
* contiene le informazioni di una colonna contenuta in un oggetto di classe
* {@link waTable}
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waColumn
	{
	/**
	* nome della colonna
	*
	* nome univoco della colonna all'interno della tabella; deve corrispondere 
	 * al nome della colonna all'interno del cursore restituito dalla query SQL 
	 * (senza prefisso della tabella, quindi), oppure alla chiave dell'array 
	 * associativo che alimentano la classe  {@link waTable}.
	 * 
	 * Qualora la colonna dovesse causare situazioni di ambiguità
	 * tra nomi uguali di tabelle diverse all'interno della query SQL, 
	 * (evidentemente solo in caso di filtro o ordinamento), allora è necessario 
	 * valorizzare con il nome completo di prefisso tabellare  anche la
	 * proprietà {@link $aliasOf}.
	 * 
	* E' buona norma che il nome
	 * segua le regole di naming della programmazione (no spazi, punteggiatura, 
	 * ecc.), perche' e' verosimile che lato client questa proprieta' venga 
	 * utilizzata per accedere ad un oggetto di programmazione.
	 * 
	* @var string
	*/	
	public $name;
	
	/**
	* intestazione della colonna
	*
	* @var string
	*/	
	public $label;
	
	/**
	* indica se la colonna puo' essere sottoposta a ordinamento
	*
	* @var boolean
	*/	
	public $sort = true;
	
	/**
	* indica se la colonna puo' essere sottoposta a filtro
	*
	* @var boolean
	*/	
	public $filter = true;
	
	/**
	* indica se la colonna deve essere mostrata a video
	*
	* La proprietà non ha alcun effetto visivo di per se': è {@link waTable::$view view-object} che si 
	 * comporterà come ritiene opportuno a fronte di questa informazione.
	 * 
	* @var boolean
	*/	
	public $show = true;
	
	/**
	* allineamento della colonna
	*
	* Puo' valere una delle seguenti const:
	* - {@link waTable::ALIGN_L} ->	allineamento a sinistra
	* - {@link waTable::ALIGN_C} ->	allineamento al centro
	* - {@link waTable::ALIGN_R} ->	allineamento a destra
	 * 
	 * si presti attenzione al fatto che non e' necessario
	 * fornire questa informazione; in alternativa e' possibile utilizzare la
	 * proprieta' {@link fieldType}: il {@link waTable::$view view-object}, sulla base del valore di 
	 * questa proprietà, puo' prendere decisioni relative alla formattazione
	 * * @var integer
	*/	
	public $alignment;
	
	/**
	* lunghezza massima di una cella espressa in caratteri;
	*
	* se 0, allora la cella non ha limite di caratteri
	* @var integer
	*/	
	public $maxChar = waTable::CELL_MAX_CHAR;
	
	/**
	* formattazione colonna
	*
	* formattazione da applicare al valore da visualizzare. 
	* Puo' valere una delle seguenti define:
	* - {@link waTable::FMT_DATE}		->	dd/mm/YYYY
	* - {@link waTable::FMT_DATETIME}	->	dd/mm/YYYY HH:ii:ss
	* - {@link waTable::FMT_TIME}		->	HH:ii:ss
	* - {@link waTable::FMT_DECIMAL}	->	#.###,##
	* - {@link waTable::FMT_INTEGER}	->	####
	* - {@link waTable::FMT_STRING}	->	stringa html-encoded
	* - {@link waTable::FMT_RAW}		->	stringa non html-encoded
	* - qualsiasi altra cosa: -> viene applicata la formattazione standard per il tipo rilevato dal database
	 * 
	 * si presti attenzione al fatto che non e' necessario
	 * fornire questa informazione; in alternativa e' possibile utilizzare la
	 * proprieta' {@link fieldType}: il {@link waTable::$view view-object}, sulla base del valore di 
	 * questa proprieta', puo' prendere decisioni relative alla formattazione
	* @var integer
	*/	
	public $format;
	
	/**
	* nr. decimali formattazione
	*
	* nr. di decimali da applicare alla eventuale formattazione di un nr. float
	* @var integer
	*/	
	public $decimalDigitsNr = 2;
	
	/**
	* link
	*
	* se true, l'informazione viene semplicemente passata al 
	 * {@link waTable::$view view-object}, che dovra'
	 * implementare i meccanismi affinche' il link diventi effettivo. 
	 * 
	* @var boolean
	*/	
	public $link;
	
	/**
	* callback function di formattazione/calcolo del valore da mandare in output
	*
	* nome di una funzione/metodo PHP che deve
	* essere chiamata al posto della formattazione standard. Se il valore passato 
	* e' una stringa, allora verra' invocata una funzione procedurale; se e' un array
	* il primo elemento sara' l'oggetto a cui il metodo appartiene, il secondo
	* elemento il nome del metodo. Il valore ritornato dalla funzione/metodo sara' 
	* cio' che viene effettivamente mostrato come contenuto della cella. 
	* 
	* In questo modo
	* e' possibile avere una o piu' columns totalmente gestite dall'applicazione,
	* la quale puo' effettuare le opportune elaborazioni non standard (verifica 
	* di presenza di file sul filesystem, ecc.). Alla funzione/metodo verra' passato
	* come parametro l'oggetto di classe {@link waTable} istanziato ($this).
	* @var mixed (string | array)
	*/	
	public $computeFunction;
	
	/**
	* flag totalizzatore della colonna
	*
	* indica che la colonna deve produrre un totale 
	* che verra' mostrato in una riga aggiuntiva in fondo alla tabella
	* @var boolean
	*/	
	public $totalize = false;
	
	/**
	* -
	*
	* indica che il nome della colonna e' un alias di un altro campo/calcolo, di cui qui
	* va specificato il nome/calcolo, oppure che il nome della colonna potrebbe
	 * entrare in conflitto con quello di un altro  campo del DB appartenente
	 * a una tabella diversa ma coinvolta nella stessa query SQL: in questo
	 * caso la proprietà dovrà contenere il nome del campo prefissato dalla
	 * tabella, ad esempio: <b>fornitori.nome</b> 
	 * 
	 * La valorizzazione della proprietà ha senso solo quando la colonna può
	 * essere sottoposta a filtro o ordinamento.
	 * 
	* @var string
	*/	
	public $aliasOf = '';
	
	/**
	* noACapo
	*
	* indica che la cella deve usare il nowrap; l'XSLT può utilizzare a piacere
	 * questa informazione
	 * 
	* @var boolean
	*/	
	public $noWrap = false;
	
	/**
	* flag di escaping del codice HTML
	*
	* indica se convertire il valore del campo prima di mostrarlo a video affinche' 
	* i caratteri non possano generare sequenze di tag HTML.
	* 
	* Il default e' true, ossia cio' che viene mostrato in corrispondenza della colonna
	* sara' l'eventuale <b>codice</b> HTML, non il suo rendering.
	* 
	* Qualora per particolari motivi fosse necessario mostrare il rendering HTML
	* del valore del campo all'interno della cella, e' necessario porre questa
	* proprieta' a false. Attenzione ovviamente ai casi in cui cio' possa generare 
	 * cross-site-scripting.
	* @var boolean
	*/	
	public $HTMLConversion = true;
	

	/**
	* fieldType
	*
	* indica il tipo di campo contenuto nella colonna. Questa informazione puo' essere utile 
	 * ai fini della formattazione .
	* 
	* In caso di recordset letto da base dati, per default questa proprieta' riporta il risultato di waRecordset::fieldType
	* (e' ovviamente possibile sovrascrivere questo valore e forzarlo). In caso di recordset generato da array sara'
	* compito del programmatore valorizzare opportunamente questa proprieta', qualora servisse lato UI.
	* 
	* Non esiste una codifica predefinita per il tipo, ma al fine di mantenere convenzionalmente una medesima
	* modalita' di lavoro all'interno dell'azienda, si consiglia di utilizzare la stessa codifica utilizzata all'interno
	* di waDB; ad esempio: STRING, INTEGER, DATE, DATETIME, CONTAINER, ecc..
	* 
	* Non esiste il default per questa proprieta'
	* 
	* @var string
	*/	
	public $fieldType = "";
	
	/**
	* pdfPerc
	*
	* percentuale della larghezza della pagina che la colonna deve occupare
	 * in una eventuale esportazione in PDF
	 * 
	* @var integer
	*/	
	public $pdfPerc = 0;
	
	/**
	* totalizzatore
	*
	* variabile d'appoggio che mantiene i parziali per la totalizzazione
	* @ignore
	* @var string
	*/	
	public $totalizer = 0;
	
//***************************************************************************
	}	// fine classe waColumn


