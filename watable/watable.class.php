<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* @ignore
*/
include_once __DIR__ . "/../vendor/autoload.php";
	
/**
* -
* include della classe waColumn
* @ignore
*/
include_once(__DIR__ . "/wacolumn.class.php");

/**
* -
* include della classe waTableAction
* @ignore
*/
include_once(__DIR__ . "/watableaction.class.php");

/**
* -
* include dell'interfaccia i_waTableView
* @ignore
*/
include_once  __DIR__ . "/i_waTableView.interface.php";

/**
* -
* include del package waDb per l'accesso al database
* @ignore
*/
include_once(__DIR__ . "/../wadb/wadb.class.php");

//***************************************************************************
//****  classe waTable **************************************************
//***************************************************************************
/**
* waTable
*
* classe per la gestione di una tabella standard con alimentazione
* proveniente da una query SQL o da una matrice in memoria.
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTable
	{
	/**
	* -
	* lunghezza massima di una cella in caratteri (default)
	*/
	const CELL_MAX_CHAR = 100;

	/**
	* -
	* nr. massimo di righe per pagina (default)
	*/
	const LIST_MAX_REC = 20;

	/**
	* -
	* formato data delle celle
	*/
	const FMT_DATE = 'WATABLE_FMT_DATE';

	/**
	* -
	* formato data/ora delle celle
	*/
	const FMT_DATETIME = 'WATABLE_FMT_DATETIME';

	/**
	* -
	* formato ora delle celle
	*/
	const FMT_TIME = 'WATABLE_FMT_TIME';

	/**
	* -
	* formato decimale delle celle
	*/
	const FMT_DECIMAL = 'WATABLE_FMT_DECIMAL';
	const FMT_CURRENCY = 'WATABLE_FMT_CURRENCY';

	/**
	* -
	* formato intero delle celle
	*/
	const FMT_INTEGER = 'WATABLE_FMT_INTEGER';

	/**
	* -
	* formato stringa delle celle html-encoded
	*/
	const FMT_STRING = 'WATABLE_FMT_STRING';
	const FMT_TEXT = 'WATABLE_FMT_TEXT';

	/**
	* -
	* formato stringa delle celle non html-encoded
	*/
	const FMT_RAW = 'WATABLE_FMT_RAW';
	const FMT_NONE = 'WATABLE_FMT_NONE';

	/**
	* -
	* allineamento cella a sinistra
	*/
	const ALIGN_L = 'WATABLE_ALIGN_L';

	/**
	* -
	* allineamento cella al centro
	*/
	const ALIGN_C = 'WATABLE_ALIGN_C';

	/**
	* -
	* allineamento cella a destra
	*/
	const ALIGN_R = 'WATABLE_ALIGN_R';

	/**
	* -
	* definizione di tipo input areatesto
	* 
	*/
	const INPUT_TEXTAREA = 'WATABLE_INPUT_TEXTAREA';

	/**
	* -
	* definizione di tipo input data
	* 
	*/
	const INPUT_DATE = 'WATABLE_INPUT_DATE';

	/**
	* -
	* definizione di tipo input data/ora
	* 
	*/
	const INPUT_DATETIME = 'WATABLE_INPUT_DATETIME';

	/**
	* -
	* definizione di tipo input ora
	* 
	*/
	const INPUT_TIME = 'WATABLE_INPUT_TIME';

	/**
	* -
	* definizione di tipo input intero
	* 
	*/
	const INPUT_INTEGER = 'WATABLE_INPUT_INTEGER';

	/**
	* -
	* definizione di tipo input logico
	* 
	*/
	const INPUT_BOOLEAN = 'WATABLE_INPUT_BOOLEAN';

	/**
	* -
	* definizione di tipo input selezione
	* 
	*/
	const INPUT_SELECT = 'WATABLE_INPUT_SELECT';

	/**
	* -
	* definizione di tipo input testo
	* 
	*/
	const INPUT_TEXT = 'WATABLE_INPUT_TEXT';

	/**
	* -
	* definizione di tipo input valuta
	* 
	*/
	const INPUT_CURRENCY = 'WATABLE_INPUT_CURRENCY';

	/**
	* -
	* definizione del codice OK per rpc
	* 
	*/
	const RPC_OK = 'WATABLE_RPC_OK';

	/**
	* -
	* definizione del codice NON OK per rpc
	* 
	*/
	const RPC_KO = 'WATABLE_RPC_KO';

	/**
	* -
	* nr di record da leggere in un blocco di esportazione (csv/xls/pdf)
	* 
	*/
	const NR_REC_EXPORT_BLOCK = 100;

	/**
	* view-object della tabella
	*
	* l'oggetto view ?? ?? quello che implementa l'interfaccia {@link i_waTableView}
	* e al quale saranno passati i dati per la costruzione dell'interfaccia 
	* utente
	* 
	* se non definito viene usato il default 
	* 
	* \waLibs\views\waTable\waTable_default\waTableView
	* 
	* contenuto in uis/watable_default/view/watable.php
	* 
	* @var i_waTableView
	*/	
	public $view 			= null;
	
	/**
	* name della tabella
	*
	* E' buona norma che il nome
	 * segua le regole di naming della programmazione (no spazi, punteggiatura, 
	 * ecc.), in modo che questa proprieta' possa essere 
	 * utilizzata per accedere ad un oggetto di programmazione.
	 * 
	* @var string
	*/	
	public $name	= 'waTable';
	
	/**
	* nr. massimo di righe da mostrare all'interno di una pagina
	*
	* se posto a 0, allora non viene gestita la paginazione e vengono
	* mostrati tutti i record che soddisfano le condizioni impostate nella
	* query
	* @var integer
	*/	
	public $listMaxRec = waTable::LIST_MAX_REC;
	
	/**
	 * flag di selezione mutuamente esclusiva
	 *
	 * indica, dal solo punto di vista della logica di programmazione, 
	 * se la selezione dei record e' mutuamente esclusiva, oppure se e'
	 * possibile selezionare piu' di un record alla volta, evidentemente ai fini 
	 * di azioni da compiere su piu' record contemporaneamente.
	 * 
	 * L'implementazione di <i>come</i> cio' avviene e' completamente a carico 
	 * del {@link waTable::$view view-object}
	 * 
	 * @var boolean
	 */	
	public $exclusiveSelection	= true;
	
	/**
	 * titolo della tabella che puo' essere utilizzato come si vuole 
	 * nel {@link waTable::$view view-object}
	 *
	 * @var string
	 */
	public $title;
		
	
	/**
	* modulo per azioni sui record
	*
	* Nome della pagina da invocare a fronte della pressione dei tasti
	* delle actions standard sui record (vedi/modifica/Elimina) nonche' sull'azione,
	 * sempre standard, "Nuovo".
	* 
	* Quando alla tabella viene richiesta una azione standard su un record 
	* (Vedi, Nuovo, Modifica, Elimina), 
	* tipicamente questa azione viene svolta da una pagina contenente
	* un modulo, ossia un oggetto di classe waForm. 
	 * 
	 * Come venga gestita l'apertura della pagina in questione e' completamente 
	 * a carico della UI.
	 * 
	 * @var string
	*/	
	public $formPage = '';
	
	/**
	* eventuale funzione PHP da invocare prima della costruzione di ogni riga
	*
	* Alla funzione verra' passata l'intera tabella ($this).
	* se la funzione ritorna FALSE, allora la riga verra' omessa dalla lista.
	 * 
	 * E' possibile anche richiamare il metodo di un oggetto, anzich?? una 
	 * funzione procedurale; in questo caso la propriet?? assumer?? il valore
	 * di un array, in cui il primo elemento ?? l'oggetto che contiene il metodo
	 * e il secondo elemento il name del metodo.
	 * 
	* @var mixed string|array
	*/	
	public $functionBeforeRow = '';
	
	/**
	* eventuale funzione PHP da invocare dopo la costruzione di ogni riga
	*
	* Alla funzione verra' passata l'intera tabella ($this).
	 * 
	 * E' possibile anche richiamare il metodo di un oggetto, anzich?? una 
	 * funzione procedurale; in questo caso la propriet?? assumer?? il valore
	 * di un array, in cui il primo elemento ?? l'oggetto che contiene il metodo
	 * e il secondo elemento il name del metodo.
	 * 
	* @var mixed string|array
	*/	
	public $functionAfterRow = '';
	
	/**
	* recordset
	*
	* oggetto di classe waRecordset generato dalla query {@link sql} opportunamente
	* limitato per la corretta paginazione e filtrato dai filtri utente
	* @var waRecordset
	*/	
	public	$recordset; 
	
	/**
	* record
	*
	* oggetto di classe waRecord contente il record corrente durante il
	* ciclo di generazione del buffer di output
	* @var waRecord
	*/	
	public	$record; 
	
	/**
	* eventuale array che popola la tabella al posto della stringa sql
	*
	* @var array
	*/	
	public	$matrix; 
	
	/**
	* array delle azioni
	*
	* array associativo che contiene tutte le azioni 
	* applicabili alla tabella, siano esse su un insieme di record o su un 
	* singolo record. Ogni azione corrispondera'
	* ad un bottone (o a qualsiasi altro meccanismo deinito nel {@link waTable::$view view-object}, 
	* il quale inneschera', secondo le modalita' definite nalla UI,
	 *  la funzionalita' ad esso associata.
	* 
	* Ogni azione e' contenuta in
	* un oggetto di classe {@link waTableAction}. Puo' essere indifferentente creato
	* invocando il metodo {@link addAction}, oppure inizializzando direttamente
	* l'elemento dell'array.
	* 
	* L'array e' composto di un nr. di elementi a piacere, in cui la 
	* chiave dell'elemento e' il name dell'azione.
	*
	* La classe fornisce di default le actions standard:
	* - New 
	* - Details
	* - Edit
	* - Delete
	* dove <b>New</b> e' un'azione non {@link onRecord}, mentre le altre lo sono.
	* 
	* Inoltre, a fronte di determinate condizioni:
	* - Filter // se almeno una delle colonne e' ordinabile/filtrabile
	* - noFilter // se il filtro e' correntemente attivo
	*
	* E' possibile non mostrare uno qualsiasi dei bottoni di azione predefiniti
	* semplicemente eliminando l'elemento corrispondente. Ad esempio:
	*
	* <code>
	* $table = new waTable("SELECT * FROM Fornitori");
	* $table->removeAction('New');
	* </code>
	*
	* elimina dalla pagina la possibilita' di inserire un nuovo record.
	*
	* Analogamente e' possibile ridefinire la caption delle actions standard.
	* Ad esempio:
	*
	* <code>
	* $table->actions['New']->label = 'Inserisci';
	* </code>
	*
	* cambia la caption (label)
	* del bottone che per default del metodo {@link addAction} 
	 * e' uguale al nome.
	* 
	* @var array
	*/	
	public $actions	= array();

	/**
	* array delle colonne
	*
	* array associativo che contiene tutte le colonne sotto forma di oggetti
	* {@link waColumn}.
	 * 
	* Ogni colonna (elemento dell'array), puo' essere indifferentente creato
	* invocando il metodo {@link addColumn}, oppure inizializzando direttamente
	* l'elemento dell'array; si tenga pero' presente che essendo un array associativo, 
	* l'ordine delle columns sara' quello di creazione degli elementi,
	* e che la prima colonna della tabella (non necessariamente il primo campo 
	* del record e non necessariamente visibile) DEVE sempre essere l'identificativo 
	* univoco della riga.
	 * 
	 * La chiave dell'array sara' corrispondente alla prorieta' {@link waColumn::$name name} della 
	 * classe {@link waColumn}
	* @var array
	*/	
	public $columns	= array();		

	/**
	* flag di filtro attivo sulla vista
	*
	* readOnly; indica se c'e' un filtro attivo sulla vista (la tabella e' frutto di
	* una selezione effettuata col bottone "Filter")
	* @var boolean
	*/	
	public $isFilterActive = false;
	
	/**
	* orientazione di eventuale esportazione in pdf (P/L)
	*
	* @var string
	*/	
	public $pdfOrientation = 'P';
	
	/**
	* classe personalizzata per la gestione del pdf (e' cosi' possibile 
	 * personalizzare hdr, ftr, font, ecc.). 
	 * 
	 * La classe deve mostrare la medesima interfaccia di watable_pdf (se derivate
	 * andate sicuri...)
	*
	* @var string
	*/	
	public $pdfClassName = 'waLibs\watable_pdf';
	
	/**
	* path completa del file che contiene la classe personalizzata per 
	 * l'esportazione in pdf
	*
	* @var string
	*/	
	public $pdfClassFile = '/watable_pdf.class.php';	// reinizializzato nel costruttore
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language = 'it';
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale = 'it-IT';
	
	//*************************************************************************
	// protected

	/**
	* sql 
	*
	* query sql che alimenta la tabella. Il recordset waRecordset
	* sara' generato dalla classe e restituito sotto forma di proprieta'
	* all'applicazione
	* @ignore
	* @access protected
	* @var string
	*/	
	protected	$_sql; 
	
	/**
	* file configurazione DB 
	*
	* @ignore
	* @access protected
	* @var string
	*/	
	protected	$_dbConfigFile; 
	
	/**
	* connessione DB
	*
	* oggetto di classe wadbConnection che gestisce la connessione
	* @var wadbConnection
	* @access protected
	* @ignore
	*/	
	protected	$dbConnection; 
	
	/**
	* @ignore
	* @access protected
	* @var string
	*/	
	protected $_idName;				// name del campo identificativo univoco del record
								// e' sempre il campo individuato dalla 1a colonna
	
	/**
	* @ignore
	* @access protected
	* @var string
	*/	
	protected $_colsByCntr;			// protected; columns ad accesso sequenziale
	
	/**
	* @ignore
	* @access protected
	* @var string
	*/	
	protected $_currPath;				// path corrente della classe
	
	/**
	* @ignore
	* @access protected
	* @var string
	*/	
	protected $_recordCount;				// path corrente della classe
	
	/**
	* @ignore
	* @access protected
	* @var boolean
	*/	
	protected $_hasTotals;				// indica che almeno una colonna
									// deve essere totalizzata e quindi
									// viene prodotta la riga dei totali
	
	/**
	* oggetto sqlParsed 
	*
	* @var stdClass
	* @ignore
	* @access protected
	*/	
	protected	$sqlParsed; 
	
	/**
	* struttura dati che verr?? passata al {@link view view-object}
	*
	* @ignore
	* @var waTableData
	*/	
	protected	$data; 
	
	/**
	* lista dei modi filtro applicabili alle columns
	*
	* @access protected
	* @ignore
	* @var array
	*/	
	protected	$filterModes = array("lt" => array("operatore" => "<", "name" => "lower"),
									"le" => array("operatore" => "<=", "name" => "lower-equal"), 
									"eq" => array("operatore" => "=", "name" => "equal"), 
									"ge" => array("operatore" => ">=", "name" => "greater-equal"), 
									"gt" => array("operatore" => ">", "name" => "greater"), 
									"ne" => array("operatore" => "<>", "name" => "not equal"), 
									"sw" => array("operatore" => " start ", "name" => "start with"), 
									"like"  => array("operatore" => " like ", "name" => "contains"));

	/**
	* lista dei modi ordinamento applicabili alle columns
	*
	* @access protected
	* @ignore
	* @var array
	*/	
	protected	$orderModes = array("asc" => "Ascending", 
									"desc" => "Descending");

	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* __construct (costruttore)
	*
	* inizializza la tabella.
	 * 
	* @param mixed $sqlOArray : se stringa e' la query sql che sara' passata al database; se e' un array, e' la matrix che verra' utilizzata per popolare la tabella; l'array potra' essere associativo o semplice, salvo che ci sia la corrispondenza tra la chiave/indice della colonna ed il name del campo passato al metodo {@link addColumn}. In caso di array, la tabella non sara' paginata e non sara' possibile utilizzare le funzionalita' relative all'ordinamento e filtro.
	* @param string $dbConfigFile name del file di configurazione della classe waDB da utilizzare per la connessione al DB
	*/
	public function __construct($sqlOArray, $dbConfigFile = null)
		{
		if (is_array($sqlOArray))
			{
			$this->matrix = $sqlOArray;
			$this->listMaxRec = 0;
			$this->_recordCount = count($this->matrix);
			}
		else
			{
			$this->_sql = $sqlOArray;
			}
		$this->_dbConfigFile = $dbConfigFile;

		// creazione actions standard
		$this->addAction('New');
		$this->addAction('Filter');
		$this->addAction('Details', true);
		$this->addAction('Edit', true);
		$this->addAction('Delete', true);
		
		$this->_currPath = $this->getMyPath();
		
		// se messo in fase di dichiarazione si incasina phpdocumentor (???)
		$this->pdfClassFile = __DIR__ . '/watable_pdf.class.php';
		
		}

	//***************************************************************************
	/**
	* aggiunge una azione all'array {@link actions}. 
	 * 
	* @param string $name name dell'azione ({@link waTableAction::$name})
	* @param boolean $onRecord definisce se l'azione e' onRecord o meno ({@link waTableAction::$onRecord})
	* @param string $label caption del bottone; per default uguale al name ({@link waTableAction::$label})
	* @param mixed $enablingFunction callback function di verifica dell'abilitazione dell'azione
	* per la riga corrente ({@link waTableAction::$enablingFunction})
	* @return waTableAction oggetto {@link waTableAction} che contiene i parametri dell'azione
	*/
	public function addAction($name, 
							$onRecord = false, 
							$label = '',
							$enablingFunction = '')
		{
		$label = !$label ? $name : $label;
		$this->actions[$name] = new waTableAction();
		$this->actions[$name]->name = $name;
		$this->actions[$name]->onRecord = $onRecord;
		$this->actions[$name]->label = $label;
		$this->actions[$name]->enablingFunction = $enablingFunction;
		return $this->actions[$name];
		}
	
	//***************************************************************************
	/**
	* elimina una azione
	*
	* elimina una azione dall'array {@link actions}. Ha senso chiamare questo
	* metodo solo per le actions di default (New/Details/Edit/Delete)
	* @param string $name name dell'azione ({@link waTableAction::$name})
	* @return void
	*/
	public function removeAction($name)
		{
		unset($this->actions[$name]);
		}
	
	//***************************************************************************
	/**
	* Aggiunge una colonna alla tabella
	*
	* per la spiegazione approfondita dei parametri si veda le proprieta'
	* della classe {@link waColumn}.
	*
	* @param string $name name colonna
	* @param string $label intestazione colonna
	* @param boolean $show flag di visualizzazione
	* @param boolean $sort flag di ordinamento
	* @param boolean $filter flag di filtro
	* @param integer $alignment alignment colonna
	* @param integer $format format contenuto cella
	* @param integer $decimalDigitsNr nr. decimali da mostrare in caso di format float
	* @param integer $maxChar nr.massimo caratteri visualizzabili dalla cella
	* @param boolean $link indica che il contenuto della cella e' destinato a contenere un link
	* @param string $computeFunction funzione personalizzata di calcolo del valore da mostrare nella cella
	* @param boolean $totalize flag di totalizzazione colonna
	* @param string $aliasOf eventuale name campo di cui la colonna e' alias
	* @param boolean $noWrap indica se usare o meno il wrap all'interno delle celle della colonna
	* @param boolean $HTMLConversion indica se convertire o meno il valore trovato in corrispondenza del campo affinche' venga mostrato, di eventuali sequenze HTML significative, il codice oppure il rendering
	* @return waColumn istanza dell'oggetto {@link waColumn} che viene aggiunta
	* all'array {@link columns}; e' naturalmente possibile modificare gli attributi
	* di questa istanza a piacimento.
	*/
	public function addColumn(	$name, 
								$label, 
								$show = true,
								$sort = true, 
								$filter = true, 
								$alignment = '', 
								$format = '',
								$decimalDigitsNr = 2,
								$maxChar = waTable::CELL_MAX_CHAR,
								$link = false,
								$computeFunction = '',
								$totalize = false,
								$aliasOf = '',
								$noWrap = false,
								$HTMLConversion = true
							)

		{
		$index = count($this->columns);
		$this->columns[$name] = new waColumn();
		$this->columns[$name]->name = $name;
		$this->columns[$name]->label = $label;
		$this->columns[$name]->show = $show;
		$this->columns[$name]->sort = $this->fromSql() ? $sort : false;
		$this->columns[$name]->filter = $this->fromSql() ? $filter : false;
		$this->columns[$name]->alignment = $alignment;
		$this->columns[$name]->format = $format;
		$this->columns[$name]->decimalDigitsNr = $decimalDigitsNr;
		$this->columns[$name]->maxChar = $maxChar;
		$this->columns[$name]->link = $link;
		$this->columns[$name]->computeFunction = $computeFunction;
		$this->columns[$name]->totalize = $totalize;
		$this->columns[$name]->aliasOf = $aliasOf;
		$this->columns[$name]->noWrap = $noWrap;
		$this->columns[$name]->HTMLConversion = $HTMLConversion;
                
		$this->_colsByCntr[$index] = &$this->columns[$name];
		return $this->columns[$name];
		}

	//***************************************************************************
	/**
	* Effettua la visualizzazione della tabella
	*
	* In pratica, e' l'ultimo metodo da invocare. Una volta chiamato questo metodo 
	* la tabella viene visualizzata e il compito della classe e' terminato
	*
	* @param boolean $return se false, allora viene immediatamente effettuato
	* l'output della tabella; altrimenti la funzione ritorna il buffer di output 
	* della tabella stessa
	* @return void|string
	*/
	public function show($return = false)
		{
		$this->createData();
		ob_start();
		
		// se non ?? stato passato definisce l'oggetto view di default
		$this->setView();
		$this->view->transform($this->data);
		
		if ($return)
			{
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
			}	
			
		ob_end_flush();
		} 

	//***************************************************************************
	/**
	* ritorna l'oggetto contenente i dati da inviare al view-object
	 * 
	 * da usare in fase di debug per ispezionare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	* 
	* @return waTableData
	*/
	function getData()
		{
		$this->createData();
		return $this->data;
		}
		
	//***************************************************************************
	/**
	* da usare in fase di debug per mostrare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	 * 
	* @return void
	*/
	function showJSON()
		{
		$this->createData();
		header("Content-Type: application/json; charset=utf-8");			
		echo json_encode($this->data, JSON_PRETTY_PRINT);
		}
		
	//***************************************************************************
	/**
	* costruisce la struttura dati da passare al {@link view view-object}
	*
	* @access protected
	* @ignore
	*/
	protected function createData()
		{
		$this->data = new waTableData();
		$this->data->name = $this->name;
		$this->data->uri = $this->getURI();
		$this->data->waTablePath = $this->_currPath;
		$this->data->title = $this->title;
		$this->data->exclusiveSelection = $this->exclusiveSelection;
		$this->data->formPage = $this->formPage;
		$this->data->language = $this->language;
		$this->data->locale = $this->locale;

		
		// encoding dei parametri da passare al modulo di ordinamento e filtro...
 		$this->createFilterData();
 		$this->createSortData();
 									
		// crea la sezione delle actions (sia su pagina che su record;
		$this->createActionsData();
			
		$this->createNavbarData();
		
		// headers della tabella
		$this->createColHeadersData();
			
		// rows
		$this->createRowsData();			

		// eventuale riga dei totali
		$this->createTotalsRowData();
		
		} 

	//***************************************************************************
	/**
	* crea il recordset {@link $recordset} che verra' utilizzato per alimentare la tabella
	*
	* Il recordset sara' creato partendo dalle condizioni impostate dalla query
	* sql passata al costruttore successivamente raffinato/modificato mediante 
	* le selezioni effettuate dall'utente in fase di ordinamento/filtro.
	* 
	* @return boolean false nel caso si sia verificato un errore nell'esecuzione
	* della query
	*/
	public function loadRows()
		{
                if (!$this->fromSql()) {
                    return $this->loadRowsFromMatrix();
                }
            
		$this->dbConnection = $this->dbConnection ? $this->dbConnection : wadb_getConnection($this->_dbConfigFile);
		if ($this->dbConnection->errorNr())
			{
			return false;
			}

		// divide la query sql nelle varie clausole
		$this->sqlParsed = $this->dbConnection->splitSql($this->_sql);
		// Applica eventuali filtri impostati con i criteri di ricerca
		$this->applyFilter();
		// Applica eventuali ulteriori filtri impostati con la ricerca rapida
		$this->applyQuickSearch();
		
		// Applica eventuali sort order impostati con i criteri di ricerca
		$this->applySortOrder();
		
		// Applica eventuali ulteriori sort order impostati con l'ordinamento rapido
		$this->applyQuickSortOrder();
		
		// se viene richiesta l'esportazione usiamo una paginazione forzata che
		// parte dalla prima riga (ovviamente) e arriva fino alla centesima;
		// in questo modo non rischiamo di sforare il memory limit; saranno poi
		// le procedura di esportazione che si preoccuperanno di leggere i 
		// restanti record
		if ($_GET["watable_export_csv"][$this->name])
			{
			$this->exportCSV();
			}
		elseif ($_GET["watable_export_xls"][$this->name])
			{
			$this->exportXLS();
			}
		elseif ($_GET["watable_export_pdf"][$this->name])
			{
			$this->exportPDF();
			}

		list($nrRighe, $rigaIniziale) = $this->getLimitClause();
		
		$this->recordset = new waRecordset($this->dbConnection);
		$sqlParsed = $this->sqlParsed;
		$sql = "$sqlParsed->select $sqlParsed->from $sqlParsed->where $sqlParsed->group $sqlParsed->having $sqlParsed->order";
		
		$this->recordset->read($sql, $nrRighe, $rigaIniziale);
		if ($this->recordset->errorNr())
			{
			return false;
			}
		$this->_recordCount = $this->recordset->noLimitRecordsNr();
		
		return true;
		}
		
	//***************************************************************************
	/**
	* questo metodo serve a caricare le righe dalla matrice, che è già data,
         * quindi di fatto non fa quello che il suo nome dichiara; ma serve per 
         * poter innescare un eventuale processo di esportazione CSV/XLS/PDF
	*/	
	protected function loadRowsFromMatrix()
		{
		if ($_GET["watable_export_csv"][$this->name])
			{
			$this->exportCSVFromMatrix();
			}
		elseif ($_GET["watable_export_xls"][$this->name])
			{
			$this->exportXLSFromMatrix();
			}
		elseif ($_GET["watable_export_pdf"][$this->name])
			{
			$this->exportPDFFromMatrix();
			}
		return true;
		}
		
	//***************************************************************************
	/**
	* restituisce true se ?? stato richiesta l'esportazione della tabella in 
	 * formato CSV/XLS/PDF
	*/	
	public function isExport()
		{
		return $_GET["watable_export_csv"][$this->name] ||
				$_GET["watable_export_xls"][$this->name] ||
				$_GET["watable_export_pdf"][$this->name];
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function formatVal($col, $val)
		{
		switch($col->format)
			{
			case waTable::FMT_DATE:
				return $this->getDateS($val);
			case waTable::FMT_DATETIME:
				return $this->getDateTimeS($val);
			case waTable::FMT_TIME:
				return $this->getTimeS($val);
			case waTable::FMT_DECIMAL:
				return $this->formatCurrency($val, $col->decimalDigitsNr);
			case waTable::FMT_STRING:
				return $val;
			case waTable::FMT_INTEGER:
				return intval($val);
			case waTable::FMT_RAW:
				return $val;
			default:
				if (!$this->fromSql())
					return $val;
        		$Index = $this->recordset->fieldIndex($col->name);
			    switch ($this->recordset->fieldType($col->name))
			        {
			        case waDB::DECIMAL:
						return $this->formatCurrency($val, $col->decimalDigitsNr);
			        case waDB::DATE:
						return $this->getDateS($val);
			        case waDB::DATETIME:
						return $this->getDateTimeS($val);
			        case waDB::TIME:
						return $this->getTimeS($val);
			        default:
						return $val;
			        }
			}
			
		}
		
	//***************************************************************************
	/**
	* @ignore
	*/	
	 public function format(waColumn $col)
		{
		$val = $this->fromSql() ?
					$this->record->value($col->name) :
					$this->record[$col->name];
		
		return $this->formatVal($col, $val);
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getDateS($tm)
		{
		if ($tm)
			{
			return date("Y-m-d", $tm);
			}
		}
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getTimeS($tm)
		{
		if ($tm)
			{
			return date("H:i:s", $tm);
			}
		}
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getDateTimeS($tm)
		{
		return trim($this->getDateS($tm) . " " . $this->getTimeS($tm));
		}
	
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function formatCurrency ($TheVar, $DecNr = 2)
		{
	    if ($TheVar === null || $TheVar === '')
			{
			return '';
			}
		$TheVar = (float) $TheVar;
	
	    $TheVar = number_format($TheVar, $DecNr, ".", '');
	
	    return $TheVar;
		}
		
	//*****************************************************************************
	// genera i controlli hidden che vengono associati ad ogni riga di una tabella
	/**
	* @ignore
	* @access protected
	*/	
	protected function createRowQualifiersData(waTableDataRow $out)
		{
		// scriviamo un controllo invisibile per ogni azione su record che puo'
		// essere abilitata/disabilitata
		$out->enableableActions = array();
		$idx = 0;
		
		foreach ($this->actions as $azione)
			{
			if ($azione->onRecord)
				{
				$out->enableableActions[$idx] = $azione->enablingFunction ? 
												call_user_func($azione->enablingFunction, $this) :
												true;
				$idx++;
				}
			}
			
		}
			
	//*****************************************************************************
	// costruisce il link da chiamare in caso di click su una intestazione di 
	// colonna, che provoca il sort rapido dei record in visualizzazione
	/**
	* @access protected
	* @ignore
	*/	
	protected function dammiDatiOrdinamentoRapido(waColumn $col)
		{
		return $col->sort && $_GET["watable_qo"][$this->name] == $col->name ? $_GET["watable_qom"][$this->name] : 'no';
		}

	//*****************************************************************************
	// restituisce la uri della pagina corrente
	/**
	* @ignore
	* @access protected
	*/	
	protected function getURI()
		{
		// prendiamo tutti i parametri get e riportiamo solo quelli valorizzati
		$passo = explode("&", $_SERVER['QUERY_STRING']);
		$toret = '';
		$amp = '';
		foreach ($passo as $elem)
			{
			list($k, $v) = explode("=", $elem, 2);
			if (strlen($v))
				{
				$toret .= "$amp$k=$v";
				$amp = "&";
				}
			}
		
		return $toret ? htmlspecialchars("?$toret") : '';
			
		}

	//*****************************************************************************
	// dato un parametro per la costruzione del link, restituisce il valore
	// contenuto nel db/matrix se il parametro corrisponde ad un name campo,
	// altrimenti il parametro e' una costante e quindi viene ritornato
	// esso stesso
	/**
	* @ignore
	* @access protected
	*/	
	protected function dammiValoreParametroLink($param)
		{
		if ($this->fromSql())
			{
			$fieldIndex = $this->recordset->fieldIndex($param);
			$value = isset($fieldIndex) ? $this->record->value($param) : $param;
			}
		else
			{
			$value = array_key_exists($param, $this->record) ?
					$this->record[$param] : $param;
			}

		return $value;
		}
		
	//*****************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function createActionsData()
		{
    	
		$this->createQuickSearchData();
		if ($this->isFilterActive)
			{
			$this->addAction("noFilter", false, 'No Filter');
			}

		// aggiungiamo le actions non "onRecord"
    	$this->data->pageActions = array();
		$idx = 0;
		foreach ($this->actions as $azione)
			{
			if (!$azione->onRecord)
				{
				$this->data->pageActions[$idx] = new waTableDataAction ();
				$this->data->pageActions[$idx]->name = $azione->name;
				$this->data->pageActions[$idx]->label = $azione->label;
				$idx++;
				}
			}
				
		// mostriamo le actions "onRecord"
    	$this->data->recordActions = array();
		$idx = 0;
		foreach ($this->actions as $azione)
			{
			if ($azione->onRecord)
				{
				$this->data->recordActions[$idx] = new waTableDataAction ();
				$this->data->recordActions[$idx]->name = $azione->name;
				$this->data->recordActions[$idx]->label = $azione->label;
				$idx++;
				}
			}
	
		}
	
	//*****************************************************************************
	/**
	 * 
	 * trasforma il valore ricevuto da un controllo di ricerca (filtro o rapida)
	 * a seconda di questi casi:
	 * o se la ricerca e' per 'like' aggiunge i caratteri jolly prima e dopo
	 * o se viene riconosciuta una data, la data viene formattata secondo le regole del db
	 * o in tutti gli altri casi ritorna il valore passato
	 * 
	 * comunque e' da migliorare....
	 * 
	* @ignore
	* @access protected
	*/	
	protected function getSqlValue(waColumn $col, $value, $operatore)
		{
		// non sappiamo a priori quale e' il tipo del valore che ci stanno
		// passando, perche' non avendo ancora acceduto al db non sappiamo di 
		// che tipo e' la colonna data; il client e' pero' tenuto a formattare 
		// secondo le regole standard le date e i decimali; per i decimali non
		// c'e' problema: cosi' come vengono passati vanno bene anche per PHP e
		// quindi per il db; per le date/ore, invece, occorre cercare di 
		// riconoscerle e trasformarle di conseguenza
		if (preg_match('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', $value, $parts)) 
			{
			$time = mktime($parts[4], $parts[5], $parts[6], $parts[2], $parts[3], $parts[1]);
			$value = $this->dbConnection->sqlDateTimeAsString($time);
			}
		elseif (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $value, $parts)) 
			{
			$time = mktime(0,0,0, $parts[2], $parts[3], $parts[1]);
			$value = $this->dbConnection->sqlDateAsString($time);
			}
		elseif (preg_match('/^(\d{2}):(\d{2}):(\d{2})$/', $value, $parts)) 
			{
			$time = mktime($parts[1], $parts[2], $parts[3], 1, 1, 1980);
			$value = $this->dbConnection->sqlTimeAsString($time);
			}
			
		$value = $operatore == ' like ' ? "%$value%" : $value;
		$value = $operatore == ' start ' ? "$value%" : $value;
		return $this->dbConnection->sqlString($value);
		}
		
	//*****************************************************************************
	/**
	 * 
	 * dato l'indice di un filtro ritorna l'operatore utilizzato per l'ordinamento
	 * 
	* @ignore
	* @access protected
	*/	
	protected function getColumnSortMode($idx)
		{
		if (!is_int($idx))
			{
			return;
			}
		if ($this->orderModes[$_GET["watable_om"][$this->name][$idx]])
			{
			return $_GET["watable_om"][$this->name][$idx];
			}
		}
	
	//*****************************************************************************
	/**
	 * 
	 * verifica se una colonna e' stata selezionata per l'ordinamento e se del caso
	 * ritorna l'indice dell'ordinamento
	 * 
	 * attenzione che potrebbe ritornare false e 0, che sono due cose diverse
	 * 
	* @ignore
	* @access protected
	*/	
	protected function getColumnSortIndex(waColumn $col)
		{
		if (!is_array($_GET["watable_of"][$this->name]))
			{
			return false;
			}

		foreach( $_GET["watable_of"][$this->name] as $idx => $fieldName)
			{
			if ($col->name == $fieldName && $col->sort)
				{
				return $idx;
				}
			}
			
		return false;
		}

	//*****************************************************************************
	/**
	 * 
	 * 
	* @ignore
	* @access protected
	*/	
	protected function createSortData()
		{
		$this->data->orderModes = array();
		$idx = 0;
 		foreach ($this->orderModes as $value => $name)
			{
			$this->data->orderModes[$idx] = new waTableDataOrderMode();
			$this->data->orderModes[$idx]->name = $name;
			$this->data->orderModes[$idx]->value = $value;
			$idx++;
			}

		$this->data->orders = array();
		$idx = 0;
		if (is_array( $_GET["watable_of"][$this->name]))
			{
			foreach( $_GET["watable_of"][$this->name] as $fidx => $fieldName)
				{
				if ($this->columns[$fieldName] && $this->columns[$fieldName]->sort)
					{
					$this->data->orders[$idx] = new waTableDataOrder();
					$this->data->orders[$idx]->index = $fidx;
					$this->data->orders[$idx]->field = $fieldName;
					$this->data->orders[$idx]->mode = $this->getColumnSortMode($fidx);
					$idx++;
					}
				}
			}
			
		
		}
	
	//*****************************************************************************
	/**
	 * 
	 * 
	* @ignore
	* @access protected
	*/	
	protected function createFilterData()
		{
		$this->data->filterModes = array();
		$idx = 0;
 		foreach ($this->filterModes as $value => $modo)
			{
			$this->data->filterModes[$idx] = new waTableDataFilterMode();
			$this->data->filterModes[$idx]->name = $modo["name"];
			$this->data->filterModes[$idx]->value = $value;
			$idx++;
			}
			
		$this->data->filters = array();
		$idx = 0;
		if (is_array( $_GET["watable_ff"][$this->name]))
			{
			foreach( $_GET["watable_ff"][$this->name] as $fidx => $fieldName)
				{
				if ($this->columns[$fieldName] &&
						$this->columns[$fieldName]->filter &&
						($modo = $this->getColumnFilterMode($fidx, true)) &&
						($value = $this->getColumnFilterValue($fidx, true)) !== '')
					{
					$this->data->filters[$idx] = new waTableDataFilter();
					$this->data->filters[$idx]->index = $fidx;
					$this->data->filters[$idx]->field = $fieldName;
					$this->data->filters[$idx]->mode = $modo;
					$this->data->filters[$idx]->value = $value;
					$idx++;
					}
				}
			}
			
		
		}
	
	//*****************************************************************************
	/**
	 * 
	 * dato l'indice di un filtro ritorna l'operatore utilizzato per il filtro
	 * 
	* @ignore
	* @access protected
	*/	
	protected function getColumnFilterMode($idx, $mask = false)
		{
		if (!is_int($idx))
			{
			return;
			}
		if ($operatore = $this->filterModes[ $_GET["watable_fm"][$this->name][$idx]]['operatore'])
			{
			if ($mask)
				{
				return $_GET["watable_fm"][$this->name][$idx];
				}
			else
				{
				return $operatore;
				}
			}

		}
	
	//*****************************************************************************
	/**
	 * 
	 * dato l'indice di un filtro ritorna il valore utilizzato per il filtro
	 * 
	* @ignore
	* @access protected
	*/	
	protected function getColumnFilterValue($idx, $mask = false)
		{
		if (!is_int($idx))
			{
			return;
			}
		if ($mask)
			{
			return $_GET["watable_fv"][$this->name][$idx];
			}
		return  $_GET["watable_fv"][$this->name][$idx];
		}
	
	//*****************************************************************************
	/**
	 * 
	 * verifica se una colonna e' stata selezionata per il filtro e se del caso
	 * ritorna l'indice del filtro
	 * 
	 * attenzione che potrebbe ritornare false e 0, che sono due cose diverse
	 * 
	* @ignore
	* @access protected
	*/	
	protected function getColumnFilterIndex(waColumn $col)
		{
		if (!is_array($_GET["watable_ff"][$this->name]))
			{
			return false;
			}

		foreach( $_GET["watable_ff"][$this->name] as $idx => $fieldName)
			{
			if ($col->name == $fieldName &&
					$col->filter &&
					$this->getColumnFilterMode($idx) &&
					$this->getColumnFilterValue($idx) !== '')
				{
				return $idx;
				}
			}
			
		return false;
		}
	
	//*****************************************************************************
	// aggiunge alle condizioni di where di una lista ulteriori coindizioni,
	// in base agli eventuali criteri di ricerca impostati dalle funzioni di filtro
	// e ordinamento 
	/**
	* @ignore
	* @access protected
	*/	
	protected function applyFilter()
		{
		if (!is_array($_GET["watable_ff"][$this->name]))
			{
			return;
			}

		$where = "";
		foreach( $_GET["watable_ff"][$this->name] as $idx => $fieldName)
			{
			$col = $this->columns[$fieldName];
			if (!$col || $this->getColumnFilterIndex($col) !== $idx)
				{
				continue;
				}

			$fieldName = $col->aliasOf ? $col->aliasOf : $col->name;
			$operatore = $this->getColumnFilterMode($idx);
			$value = $this->getSqlValue($col, $this->getColumnFilterValue($idx), $operatore);
                        $operatore = $operatore == " start " ? " like " : $operatore;
			$where .= ($where ? " AND " : '') . 
						($col->aliasOf ? $col->aliasOf : $col->name) .
						$operatore . $value;
			}

		if (!$where)
			{
			return;
			}

		$this->isFilterActive = true;
		$this->sqlParsed->where = (!$this->sqlParsed->where ? "WHERE" : $this->sqlParsed->where . " AND") . " $where";
			
		}

	//*****************************************************************************
	// Apllica ulteriori filtri alla query impostati con la ricerca rapida
	/**
	* @ignore
	* @access protected
	*/	
	protected function applyQuickSearch()
		{
		if (!$rr = trim($_GET["watable_qs"][$this->name]))
			{
			return;
			}

		$where = "";
		foreach ($this->columns as $nomeCol => $col)		
			{
			if (!$col->filter)
				{
				continue;
				}

			$where .= ($where ? " OR " : '') . 
						($col->aliasOf ? $col->aliasOf : $col->name) .
						" LIKE " . $this->getSqlValue($col,  $_GET["watable_qs"][$this->name], ' like ');
			}
			
		if ($where)
			{
			$this->sqlParsed->where = (!$this->sqlParsed->where ? "WHERE" : $this->sqlParsed->where . " AND") . " ($where)";
			}
		}

	//*****************************************************************************
	// restituisce eventuali sort order impostati con i criteri di ricerca 
	// impostati dalle funzioni di filtro e ordinamento (sortfilter.php e pagine 
	// relative)
	/**
	* @ignore
	* @access protected
	*/	
	protected function applySortOrder()
		{
	
		if (!is_array($_GET["watable_of"][$this->name]))
			{
			return;
			}

		$myOrder = '';
		foreach( $_GET["watable_of"][$this->name] as $idx => $fieldName)
			{
			$col = $this->columns[$fieldName];
			if (!$col || $this->getColumnSortIndex($col) !== $idx)
				{
				continue;
				}

			$operatore = $this->getColumnSortMode($idx);
			$fieldName = $col->aliasOf ? $col->aliasOf : $col->name;
			$myOrder .= ($myOrder ? ', ' : ' ORDER BY ') . 
								"$fieldName $operatore";
			}
			
		if ($myOrder)
			{
			$this->sqlParsed->order = $myOrder;
			}
		}
	
	//*****************************************************************************
	// restituisce un (uno solo!) eventuale sort order impostato con l'ordinamento
	// rapido, ossia con il click su una intestazione di colonna laddove abilitato
	// OCCHIO: viene in coda ad eventuali ordinamenti fatti col modulo
	// ordinamento/filtro
	/**
	* @ignore
	* @access protected
	*/	
	protected function applyQuickSortOrder()
		{
		$colonnaOrdinamentoRichiesta =  $_GET["watable_qo"][$this->name];
		$col = $this->columns[$colonnaOrdinamentoRichiesta];
		if (!$col || !$col->sort)
			{
			return;
			}

		$fieldName = $col->aliasOf ? $col->aliasOf : $col->name;
		$this->sqlParsed->order = " ORDER BY $fieldName" .
								(strtolower( $_GET["watable_qom"][$this->name]) == "desc" ? " DESC" : '');
			
		}

	//*****************************************************************************
	// restituisce la stringa sql da utilizzare con condizione di limit per la 
	// visualizzazione tabellare
	/**
	* @ignore
	* @access protected
	*/	
	protected function getLimitClause()
		{
		if ($this->listMaxRec == 0)
			{
			return array();
			}
			
		return array($this->listMaxRec, 
						 $_GET["watable_pg"][$this->name] * $this->listMaxRec);
		
		}
		
	//*****************************************************************************
	// costruisce la barra di navigazione dei record delle pagine delle viste
	/**
	* @ignore
	* @access protected
	*/	
	protected function createNavbarData()
		{
	
		// in  $_GET["watable_pg'] c'e' il nr di pagina corrente della navigazione 
		// dei record
		$currPage =  $_GET["watable_pg"][$this->name]?  $_GET["watable_pg"][$this->name] : 0; 		
																	
		$firstRec = ($this->_recordCount  ? ($currPage * $this->listMaxRec) + 1 : 0);
		if ($this->listMaxRec == 0)
			{
			$lastRec = $this->_recordCount;
			$totalePagine = 1;
			}
		else
			{
			$lastRec = 	(($currPage * $this->listMaxRec) + $this->listMaxRec) > $this->_recordCount ?
						$this->_recordCount :
						($currPage * $this->listMaxRec) + $this->listMaxRec;
			$totalePagine = intval($this->_recordCount / $this->listMaxRec) + 
							($this->_recordCount % $this->listMaxRec > 0 ? 1 : 0);
			}
						
		$this->data->navbar = new waTableDataNavbar();
		
		$this->data->navbar->currentPageNr = $currPage;
		$this->data->navbar->totalPageNr = $totalePagine;
		$this->data->navbar->firstRecord = $firstRec;
		$this->data->navbar->lastRecord = $lastRec;
		$this->data->navbar->totalRecordNr = $this->_recordCount;
		
		}
		
	//*****************************************************************************
	// restituisce il textbox per la ricerca rapida
	/**
	* @ignore
	* @access protected
	*/	
	protected function createQuickSearchData()
		{
		$this->data->quickSearch = $_GET["watable_qs"][$this->name];
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getRowID()
		{
		// l'dentificativo della riga e' sempre il campo della prima colonna
		return $this->fromSql() ?
				$this->record->value($this->_colsByCntr[0]->name) :
				$this->record[$this->_colsByCntr[0]->name];
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function fromSql()
		{
		return isset($this->_sql);
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function createColHeadersData()
		{
		$this->data->columnHeaders = array();
		$idx = 0;
		foreach ($this->columns as $col)
			{
			$col->fieldType = !$col->fieldType && $this->fromSql() && $this->recordset ? 
									$this->recordset->fieldType($col->name) :
									$col->fieldType;

			$this->data->columnHeaders[$idx] = new waTableDataColumnHeader();
			$this->data->columnHeaders[$idx]->name = $col->name;
			$this->data->columnHeaders[$idx]->label = $col->label;
			$this->data->columnHeaders[$idx]->alignment = $col->alignment;
			$this->data->columnHeaders[$idx]->show = $col->show;
			$this->data->columnHeaders[$idx]->sort = $col->sort;
			$this->data->columnHeaders[$idx]->filter = $col->filter;
			$this->data->columnHeaders[$idx]->fieldType = $col->fieldType;
			$this->data->columnHeaders[$idx]->format = $col->format;
			$this->data->columnHeaders[$idx]->maxChar = $col->maxChar;
			$this->data->columnHeaders[$idx]->HTMLConversion = $col->HTMLConversion;
			$this->data->columnHeaders[$idx]->link = $col->link;
			$this->data->columnHeaders[$idx]->quickSort = $this->dammiDatiOrdinamentoRapido($col);
			$this->data->columnHeaders[$idx]->decimalDigitsNr = $col->decimalDigitsNr;
			
			$idx++;
			}
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function createRowsData()
		{
		
		$this->data->rows = array();
		$iteraSu = $this->fromSql() ? $this->recordset->records : $this->matrix;
		foreach ($iteraSu as $idx => $this->record)
			{
			if ($this->functionBeforeRow)
				{
				$p = $this->functionBeforeRow;
				if ($p($this) === false)
					{
					continue;
					}
				}
				
			$this->data->rows[$idx] = new waTableDataRow();
			$this->data->rows[$idx]->id = $this->getRowID();
			
			$this->createRowQualifiersData($this->data->rows[$idx]);
			$this->createRowCellsData($this->data->rows[$idx]);
				
			if ($this->functionAfterRow)
				{
				$p = $this->functionAfterRow;
				$p($this);
				}
			}
			
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function createRowCellsData(waTableDataRow $out)
		{
		$out->cells = array();
		$idx = 0;
		foreach ($this->columns as $col)
			{
			if ($col->computeFunction)
			// e' una colonna che viene valorizzata dal chiamante;
			// gli passiamo la tabella e ilrecord corrente, sa lui cosa farsene
				{
				$out->cells[$idx] = call_user_func($col->computeFunction, $this);
				}
			else
				{
				$out->cells[$idx] = $this->format($col);
				}

			if ($col->totalize)
				{
				if (!$col->totalizer)
					{
					$col->totalizer = 0;
					}
				$col->totalizer += ($this->fromSql() ?
											$this->record->value($col->name) :
											$this->record[$col->name]);
				$this->_hasTotals = true;
				}
				
			$idx++;
			}
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function createTotalsRowData()
		{
		if (!$this->_hasTotals)
			{
			// se la tabella ?? vuota, non sappiamo se vuole la riga dei totali o meno
			foreach ($this->columns as $col)
				{
				if ($col->totalize)
					{
					$this->_hasTotals = true;
					break;
					}
				}
			
			}
			
		if (!$this->_hasTotals)
			{
			return;
			}

		$this->data->totalsRow = new waTableDataTotalsRow();
		$this->data->totalsRow->cells = array();
		$idx = 0;
		foreach ($this->columns as $col)
			{
			$this->data->totalsRow->cells[$idx] = "";
			if ($col->totalize)
				{
				$this->data->totalsRow->cells[$idx] = $this->formatVal($col, $col->totalizer);
				}
			$idx++;
			}
			
		}
		
	//***************************************************************************
	/**
	* exportCSV
	* @ignore
	*
	*/
	protected function exportCSV()
		{
		$buffer = $comma = '';
		foreach ($this->columns as $col)
			{
			if ($col->show)
				{
				$buffer .= $comma . '"' . $col->label . '"';
				$comma = ',';
				}
			}
		$buffer .= "\n";
							
		// si legge da db a blocchi di 100 righe, in modo da non sforare il 
		// memory limit
		while ($this->readNextExportBlock())
			{
			foreach ($this->recordset->records as $this->record)
				{
				$comma = '';
				foreach ($this->columns as $col)
					{
					if ($col->show)
						{
						$buffer .= $comma;
						$buffer .= '"';
						if (!empty($col->computeFunction))
							{
							$tocat = call_user_func($col->computeFunction, $this);
							}
						elseif ($this->record->recordset->fieldType($col->name) == waDB::DATE &&
								$this->record->value($col->name) != 0)
							{
							$tocat = date("Y-m-d", $this->record->value($col->name));
							}
						elseif ($this->record->recordset->fieldType($col->name) == waDB::DATETIME &&
								$this->record->value($col->name) != 0)
							{
							$tocat = date("Y-m-d H:i:s", $this->record->value($col->name));
							}
						else
							{
							$tocat = $this->record->value($col->name);
							}
						$buffer .= str_replace('"', '""', $tocat) . '"';
						$comma = ',';
						}

					}
				$buffer .= "\n";
				}
			}
			
		$nomefile = $this->lettersNumbersOnly($this->title) . date("_YmdHis") . ".csv";
		header("Pragma: ");
		header("Expires: Fri, 15 Aug 1980 18:15:00 GMT");
		header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0, false");
		header('Content-Length: '. strlen($buffer));
		header("Content-Disposition: attachment; filename=\"$nomefile\";" );
		header("Content-Type: application/force-download");
		header("Content-Transfer-Encoding: binary");
			
		exit($buffer);		
		
		}
		
	//***************************************************************************
	/**
	* exportCSVFromMatrix
	* @ignore
	*
	*/
	protected function exportCSVFromMatrix()
		{
		$buffer = $comma = '';
		foreach ($this->columns as $col)
			{
			if ($col->show)
				{
				$buffer .= $comma . '"' . $col->label . '"';
				$comma = ',';
				}
			}
		$buffer .= "\n";
							
                foreach ($this->matrix as $this->record)
                        {
                        $comma = '';
                        foreach ($this->columns as $col)
                                {
                                if ($col->show)
                                        {
                                        $buffer .= $comma;
                                        $buffer .= '"';
                                        
                                        if (!empty($col->computeFunction))
                                                {
                                                $tocat = call_user_func($col->computeFunction, $this);
                                                }
                                        else
                                                {
                                                $tocat = $this->format($col);
                                                }
                                        $buffer .= str_replace('"', '""', $tocat) . '"';
                                        $comma = ',';
                                        }

                                }
                        $buffer .= "\n";
                        }
			
		$nomefile = $this->lettersNumbersOnly($this->title) . date("_YmdHis") . ".csv";
		header("Pragma: ");
		header("Expires: Fri, 15 Aug 1980 18:15:00 GMT");
		header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0, false");
		header('Content-Length: '. strlen($buffer));
		header("Content-Disposition: attachment; filename=\"$nomefile\";" );
		header("Content-Type: application/force-download");
		header("Content-Transfer-Encoding: binary");
			
		exit($buffer);		
		
		}
		
	//***************************************************************************
	/**
	* exportXLS 
	* @ignore
	*
	*/
	protected function exportXLS()
		{
		date_default_timezone_set('UTC');
		
		$objPHPExcel = new \PHPExcel();
		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		
		$colIdx = 0;
		foreach ($this->columns as $col)
			{
			if ($col->show)
				{
				$sheet->setCellValueByColumnAndRow($colIdx, 1, $col->label);
				$sheet->getStyleByColumnAndRow($colIdx, 1)->getFont()->setBold(true);
				$colIdx++;
				}
			}

		$rowIdx = 2;
		// si legge da db a blocchi di 100 righe, in modo da non sforare il 
		// memory limit
		while ($this->readNextExportBlock())
			{
			foreach ($this->recordset->records as $this->record)
				{
				$colIdx = 0;
				foreach ($this->columns as $col)
					{
					if ($col->show)
						{
						if (!empty($col->computeFunction))
							{
							$sheet->setCellValueExplicitByColumnAndRow($colIdx, $rowIdx, call_user_func($col->computeFunction, $this));
							}
						elseif ($this->recordset->fieldType($col->name) == waDB::DATE && $this->record->value($col->name))
							{
							$sheet->setCellValueByColumnAndRow($colIdx, $rowIdx,  \PHPExcel_Shared_Date::PHPToExcel($this->record->value($col->name)));
							$sheet->getStyleByColumnAndRow($colIdx, $rowIdx)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
							}
						elseif ($this->recordset->fieldType($col->name) == waDB::DATETIME && $this->record->value($col->name))
							{
							$sheet->setCellValueByColumnAndRow($colIdx, $rowIdx,  \PHPExcel_Shared_Date::PHPToExcel($this->record->value($col->name)));
							$sheet->getStyleByColumnAndRow($colIdx, $rowIdx)->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
							}
						elseif ($this->recordset->fieldType($col->name) == waDB::INTEGER)
							{
							$sheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $this->record->value($col->name));
							}
						elseif ($this->recordset->fieldType($col->name) == waDB::DECIMAL)
							{
							$sheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $this->record->value($col->name));
							}
						else
							{
							$sheet->setCellValueExplicitByColumnAndRow($colIdx, $rowIdx, $this->record->value($col->name));
							}

						$colIdx++;
						}
					}
				$rowIdx++;
				}
			}

		// Redirect output to a client???s web browser (Excel5)
		$filename = $this->lettersNumbersOnly($this->title) . date("_YmdHis") . ".xls";
		header('Content-Type: application/vnd.ms-excel');
		header("Content-Disposition: attachment;filename=\"$filename\"");
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

		exit();
		}		
		
	//***************************************************************************
	/**
	* exportPDF
	* @ignore
	*
	*/
	protected function exportPDF()
		{
		require_once $this->pdfClassFile;

		$pdf = new $this->pdfClassName($this);
		$pdf->export();
		}
		
	//***************************************************************************
	/**
	* exportPDFFromMatrix
	* @ignore
	*
	*/
	protected function exportPDFFromMatrix()
		{
		require_once $this->pdfClassFile;

		$pdf = new $this->pdfClassName($this);
		$pdf->exportFromMatrix();
		}
		
	//***************************************************************************
	/**
	 * in fase di esportazione, legge il blocco di record successivo (non li 
	 * leggiamo tutti in una volta altrimenti rischiamo l'overflow)
	 * @ignore
	*/
	public function readNextExportBlock()
		{
		static $indiceBloccoRecord = 0;

		unset($this->recordset);
		$this->recordset = new waRecordset($this->dbConnection);
		$sqlParsed = $this->sqlParsed;
		$sql = "$sqlParsed->select $sqlParsed->from $sqlParsed->where $sqlParsed->group $sqlParsed->having $sqlParsed->order";
		$this->recordset->read($sql, waTable::NR_REC_EXPORT_BLOCK, $indiceBloccoRecord * waTable::NR_REC_EXPORT_BLOCK);
		if ($this->recordset->errorNr())
			{
			// non dovrebbe avere senso....
			exit("errore db");
			} 
		$indiceBloccoRecord++;
		
		return $this->recordset->records;
		
		}
		
	//***************************************************************************
	/**
	 * @ignore
	*/
	public function lettersNumbersOnly($inputString)
		{
		$toret = '';
		for ($i = 0; $i < strlen($inputString); $i++)
			{
			$ord = ord(substr($inputString, $i, 1));
			if (($ord >= ord('a') && $ord <= ord('z')) ||
				($ord >= ord('A') && $ord <= ord('Z')) ||
				// vengono accettati nel name anche meno 
				// e undescore
				($ord >= ord('0') && $ord <= ord('9')) ||
				$ord == ord('-') ||
				$ord == ord('_'))
				$toret .= chr($ord);
			}
		return $toret;
		}
		
	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function setView()
		{
		if ($this->view)
			{
			return;
			}

		// non ?? stato definito un view-object: andiamo con quello di default
		include_once  __DIR__ . "/uis/watable_default/view/watable.php";
		$this->view = new \waLibs\views\waTable\waTable_default\waTableView();
		}

	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getMyPath()
		{
		if ($_SESSION[get_class() . "_path"])
			{
			// ce l'eravamo gi?? salvata in precedenza
			return $_SESSION[get_class() . "_path"];
			}
		
		if (strpos(__FILE__, "\\") !== false)
			{
			// siamo sotto windows
			$thisFile = strtolower(str_replace("\\", "/", __FILE__));
			$dr = strtolower(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']));
			}
		else
			{
			$thisFile = __FILE__;
			$dr = $_SERVER['DOCUMENT_ROOT'];
			}
		
		if (substr($dr, -1) == "/")
			{
			$dr = substr($dr, 0, -1);
			}
			
		if ($dr != substr($thisFile, 0, strlen($dr)))
			{
			// symlink! cerchiamo la path relativa alla document root
			$relative = "/" . basename(dirname(dirname($thisFile))) . "/" . basename(dirname($thisFile));
			$dirIterator = new \RecursiveDirectoryIterator($dr, \RecursiveDirectoryIterator::FOLLOW_SYMLINKS);
			$iterator = new \ParentIterator($dirIterator);
			$directories = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST, \RecursiveIteratorIterator::CATCH_GET_CHILD);

			foreach ($directories as $path => $object)
				{
				$path = str_replace("\\", "/", $path);
				if (strpos($path, $relative) !== false)
					{
					$toret = substr($path, strlen($dr));
					break;
					}
				}
			}
		else
			{
			$toret = substr(dirname($thisFile), strlen($dr));
			}

		return $_SESSION[get_class() . "_path"] = $toret;		
		}
	
//***************************************************************************
	}	// fine classe waTable
