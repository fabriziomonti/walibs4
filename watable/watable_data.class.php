<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include delle sotto-strutture dati di cui è composta la classe
* @ignore
*/
include_once __DIR__ . "/watable_data_filter_mode.class.php";
include_once __DIR__ . "/watable_data_filter.class.php";
include_once __DIR__ . "/watable_data_order_mode.class.php";
include_once __DIR__ . "/watable_data_order.class.php";
include_once __DIR__ . "/watable_data_action.class.php";
include_once __DIR__ . "/watable_data_navbar.class.php";
include_once __DIR__ . "/watable_data_column_header.class.php";
include_once __DIR__ . "/watable_data_row.class.php";
include_once __DIR__ . "/watable_data_totals_row.class.php";

//***************************************************************************
//****  classe waTableData ***************************************************
//***************************************************************************
/**
* waTableData
*
* struttura dati da passare al view-object
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableData 
	{
	/**
	 * nome della tabella
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * URI della pagina
	 * 
	 * @var string
	 */
	public $uri;
	
	/**
	 * path relativa alla document_root della libreria waTable
	 * 
	 * @var string
	 */
	public $waTablePath;
	
	/**
	 * titolo della tabella
	 * 
	 * @var string
	 */
	public $title;
	
	/**
	 * flag di selezione esclusiva
	 * 
	 * @var bool
	 */
	public $exclusiveSelection;
	
	/**
	 * pagina di visualizzazione/modifica singolo record
	 * 
	 * @var string
	 */
	public $formPage;
	
	/**
	 * lista delle modalità di filtro applicapili {@link waTableDataFilterMode}
	 * 
	 * @var array
	 */
	public $filterModes;
	
	/**
	 * lista dei filtri applicati {@link waTableDataFilter}
	 * 
	 * @var array
	 */
	public $filters;
	
	/**
	 * lista delle modalità di ordinamento applicapili {@link waTableDataOrderMode}
	 * 
	 * @var array
	 */
	public $orderModes;
	
	/**
	 * lista dei filtri applicati {@link waTableDataOrder}
	 * 
	 * @var array
	 */
	public $orders;
	
	/**
	 * stringa di ricerca rapida
	 * 
	 * @var string
	 */
	public $quickSearch;
	
	/**
	 * lista delle azioni su pagina {@link waTableDataAction}
	 * 
	 * @var array
	 */
	public $pageActions;
	
	/**
	 * lista delle azioni su record {@link waTableDataAction}
	 * 
	 * @var array
	 */
	public $recordActions;
	
	/**
	 * dati della barra di navigazione {@link waTableDataNavbar}
	 * 
	 * @var waTableDataNavbar
	 */
	public $navbar;
	
	/**
	 * lista delle intestazioni di colonna {@link waTableDataColumnHeader}
	 * 
	 * @var array
	 */
	public $columnHeaders;
	
	/**
	 * lista delle righe {@link waTableDataRow}
	 * 
	 * @var array
	 */
	public $rows;
	
	/**
	 * riga dei totali {@link waTableDataTotalsRow}
	 * 
	 * @var waTableDataTotalsRow
	 */
	public $totalRows;
	
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language;
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale;
	
	}
	
