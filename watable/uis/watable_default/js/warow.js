/**
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
 * classe che contiene proprietà e metodi di una riga appartenente a un oggetto
 * di classe {@link waTable}
 * 
 * @class waRow
 */
var waRow = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	
	/**
	 * oggetto di classe {@link waTable} a cui la riga appartiene
	 * @type waTable
	 * @memberof waRow
	 */
	table		: null,
	
	/**
	 * identificativo della riga; corrisponde alla chiave univoca che deve
	 * sempre essere contenuta nella prima colonna della tabella
	 * @type string
	 * @memberof waRow
	 */
	id			: '',
	
	/**
	 * oggetto HTML corrispondente al tag TR che contiene la riga
	 * @type HTML_TR_object
	 * @memberof waRow
	 */
	obj			: null,
	
	/**
	 * dizionario contenente i valori non formattati di tutte le
	 * celle della riga 
	 * @type object
	 * @memberof waRow
	 */
	fields		: null,
	
	/**
	 * valore boolean che indica se la riga è attualmente selezionata (click)
	 * o meno
	 * @type boolean
	 * @memberof waRow
	 */
	selected	: false,

	//-------------------------------------------------------------------------
	/**
	 * inizializzazione (costruttore)
	 * 
	 * @param {waTable} table valorizza la proprietà {@link table}
	 * @param {string} id valorizza la proprietà {@link id}
	 * @param {object} fields valorizza la proprietà {@link fields}
	 * @memberof waRow
	 */
	initialize: function(table, id, fields) 
		{
		// definizione iniziale delle proprieta'
		this.table = table;
		this.id = id;
		this.obj = document.getElementById("row_" + this.table.name + "_" + id);
		this.fields = fields;
		this.table.rows[this.id] = this;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * seleziona/deseleziona la riga
	 * 
	 * @param {boolean} yesNo flag di selezione
	 * @memberof waRow
	 */
	select: function(yesNo) 
		{
		if (yesNo && this.table.exclusiveSelection)
			this.table.selectAll(false);
		this.selected = yesNo;
		this.render();
		},
		
	//-------------------------------------------------------------------------
	/**
	 * inverte lo stato di selezione della riga
	 * @memberof waRow
	 */
	changeStatus: function() 
		{
		this.select(!this.selected);
		},
		
	//-------------------------------------------------------------------------
	/**
	 * 
	 * @ignore
	 * @memberof waRow
	 */
	render: function() 
		{
		this.obj.className = this.selected ? "selected" : '';
		},
		
	//-------------------------------------------------------------------------
	/**
	 * restituisce l'oggetto HTML TD della colonna data
	 * 
	 * @param {string} columnName nome della colonna
	 * @memberof waRow
	 */
	getCell: function(columnName) 
		{
		// cerchiamo l'ordinale della colonna
		var rigaIntestazioni = document.getElementById(this.table.name + "_headers");
		var i = 0;
		for (; i < rigaIntestazioni.cells.length; i++)
			{
			if (rigaIntestazioni.cells[i].id == this.table.name + "_" + columnName)
				break;
			}
			
		return this.obj.cells[i];
		},
		
	//-------------------------------------------------------------------------
	/**
	 * restituisce il contenuto HTML (innerHTML) del TD della colonna data
	 * 
	 * @param {string} columnName nome della colonna
	 * @memberof waRow
	 */
	getCellContent: function(columnName) 
		{
		var cella = this.getCell(columnName);
		if (!cella)
			return false;
			
		return cella.innerHTML;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * modifica il contenuto HTML (innerHTML) del TD della colonna data con il
	 * valore dato
	 * 
	 * @param {string} columnName nome della colonna
	 * @param {string} newContent valore da impostare
	 * @memberof waRow
	 */
	setCellContent: function(columnName, newContent) 
		{
		var cella = this.getCell(columnName);
		if (!cella)
			return false;
			
		cella.innerHTML = newContent;
		return true;
		},
		
  	//---------------------------------------------------------------------------
	/**
	 * metodo invocato a fronte di richiesta di azione
  	 * "Details" (ossia la visualizzazione del dettaglio della riga).
	 * 
	 * Per default, l'azione apre la pagina del modulo (proprietà formPage
	 * di waTable) passandole in GET l'identificativo della riga e il codice 
	 * operazione di vsualizzazione (una delle const di waForm)
	 * @memberof waRow
	 */
	details: function ()
		{
		this.table.action_Details(this.id);
		},
			
  	//---------------------------------------------------------------------------
	/**
	 * metodo invocato a fronte di richiesta di azione
  	 * "Edit" (ossia la modifica della riga).
	 * 
	 * Per default, l'azione apre la pagina del modulo (proprietà formPage
	 * di waTable) passandole in GET l'identificativo della riga e il codice 
	 * operazione di modifica (una delle const di waForm)
	 * @memberof waRow
	 */
	edit: function ()
		{
		this.table.action_Edit(this.id);
		},
			
  	//---------------------------------------------------------------------------
	/**
	 * metodo invocato a fronte di richiesta di azione
  	 * "Delete" (ossia la cancellazione della riga).
	 * 
	 * Per default, l'azione apre la pagina del modulo (proprietà formPage
	 * di waTable) passandole in GET l'identificativo della riga e il codice 
	 * operazione di eliminazione (una delle const di waForm)
	 * @memberof waRow
	 */
	delete: function ()
		{
		this.table.action_Delete(this.id);
		}
			

	}
);
