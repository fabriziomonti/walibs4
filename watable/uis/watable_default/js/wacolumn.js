/**
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waColumn: contiene attributi e metodi relativi a una colonna di una
* tabella di classe {@link waTable}.
* 
* @class waColumn
*/
var waColumn = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	
	/**
	 * oggetto di classe {@link waTable} a cui la colonna appartiene
	 * @memberof waColumn
	 * @type waTable
	 */
	table			: null,
	
	/**
	 *nome della colonna
	 * @memberof waColumn
	 * @type string
	 */
	name			: '',
	
	/**
	 * intestazione della colonna
	 * @memberof waColumn
	 * @type  string
	 */
	label			: '',
	
	/**
	 * tipo del campo; corrisponde alla proprietà fieldType della
	 * classe waColumn in PHP, la quale a sua volta discende (se la tabella è
	 * associata a un recordset) dal risultato del metodo fieldType di waRecordset 
	 * @type string
	 * @memberof waColumn
	 */
	fieldType			: '',
	
	//-------------------------------------------------------------------------
	/**
	 * inizializzazione (costruttore)
	 * 
	 * @param {waTable} table valorizza la proprietà {@link table}
	 * @param {string} name valorizza la proprietà {@link name}
	 * @param {string} label valorizza la proprietà {@link label}
	 * @param {string} fieldType valorizza la proprietà {@link fieldType}
	 * @memberof waColumn
	 */
	initialize: function(table, name, label, fieldType) 
		{
		// definizione iniziale delle proprieta'
		this.table = table;
		this.name = name;
		this.label = label;
		this.fieldType = fieldType;
		
		this.table.columns[this.name] = this;
		
		}
		
	}
);
