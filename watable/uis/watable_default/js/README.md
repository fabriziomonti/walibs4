<h1>waLibs / waTable</h1>
<table>
	<tr><td style='font-weight: bold'>Version</td><td>4.0</td></tr>
	<tr><td style='font-weight: bold'>Author</td><td>F.Monti</td></tr>
	<tr><td style='font-weight: bold'>Copyright</td><td>(c) 2007-2016 <a href='http://www.webappls.com'>WebAppls</a> Bologna, Italy</td></tr>
	<tr><td style='font-weight: bold'>License</td><td><a href='http://www.gnu.org/licenses/gpl.html'>GPLv3</a></td></tr>

</table>

Classi per la gestione di una waTable.