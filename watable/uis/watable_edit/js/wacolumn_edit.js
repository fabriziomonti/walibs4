/**
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waColumn: contiene attributi e metodi relativi a una colonna di una
* table di classe {@link waTable}.
* 
* I metodi di questa classe vengono di fatto usati solo internamente e solo
* in caso di editing 
* 
* @class waColumn
*/
var waColumn_edit = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waColumn,
	
	//-------------------------------------------------------------------------
	// proprieta'
	
	/**
	 * tipo del controllo di input; corrisponde alla proprietà inputTipo della
	 * classe waColumn in PHP
	 * @type string
	 */
	inputType			: '',

	/**
	 * indica se la colonna, in fase di input, debba essere considerata readonly
	 * (in questa versione della classe non gestito)
	 * @type boolean
	 */
	readOnly		: false,

	/**
	 * indica se la colonna, in fase di input, debba essere considerata mandatory
	 * @type boolean
	 */
	mandatory	: false,
	
	//-------------------------------------------------------------------------
	/**
	 * inizializzazione (costruttore)
	 * 
	 * @param {waTable} table valorizza la proprietà {@link table}
	 * @param {string} name valueszza la proprietà {@link name}
	 * @param {string} label valorizza la proprietà {@link label}
	 * @param {string} fieldType valorizza la proprietà {@link fieldType}
	 * @param {string} inputType valorizza la proprietà {@link inputType}
	 * @param {boolean} mandatory valorizza la proprietà {@link mandatory}
	 */
	initialize: function(table, name, label, fieldType, inputType, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(table, name, label, fieldType);
		this.inputType = inputType;
		this.mandatory = mandatory == 1 ? true : false;
		
		},
		
	//-------------------------------------------------------------------------
	/**
	 * alla digitazione di un tasto sul controllo di input di tipo integer viene 
	 * richiamato questo evento
	 * @ignore
	 */
	integer_onkeyup: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		var re = /^[0-9]*$/;
		if (!re.test(ctrl.value)) 
			ctrl.value = ctrl.value.replace(/[^0-9]/g,"");	
		},
		
	//-------------------------------------------------------------------------
	// all'accesso al controllo di input di tipo currency viene richiamato questo 
	// evento
	currency_onfocus: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		ctrl.value = ctrl.value.replace(/\./g,"");
		},
		
	//-------------------------------------------------------------------------
	// alla digitazione di un tasto sul controllo di input di tipo currency viene 
	// richiamato questo evento
	currency_onkeyup: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		ctrl.value = ctrl.value.replace(/\./g,",");
		var re = /^[0-9-',']*$/;
		if (!re.test(ctrl.value)) 
			ctrl.value = ctrl.value.replace(/[^0-9-',']/g,"");	
		},
		
	//-------------------------------------------------------------------------
	// formatta il valore nel controllo di tipo currency alla perdita del fuoco
	currency_onblur: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (ctrl.value == '')
			return;
			
		var elems = new Array();
		if (ctrl.value.indexOf(",") >= 0)
			elems = ctrl.value.split(",");
		else if (ctrl.value.indexOf(".") >= 0)
			elems = ctrl.value.split(".");
		else
			elems[0] = ctrl.value;
		var interi = elems[0] == '' ? 0 : elems[0];
		var decimali = elems[1] ? elems[1] : '';
		for (i = interi.length - 3; i > 0; i -= 3)
			interi = interi.substring (0 , i) + "." + interi.substring (i);
	    if (decimali.length > 2)
	    	decimali = decimali.substr(0, 2);
		for (i = decimali.length; i < 2; i++)
	      decimali += "0";
		ctrl.value = interi + "," + decimali;
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input generico
	verifyInput_text: function(rowId) 
		{
			
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (this.mandatory && !ctrl.value)
			return "id " + rowId + " - " + this.label + ": field is mandatory\n";
		
		// se il campo e' stato modificato accendiamo il checkbox di modifica
		// del record
		if (this.table.rows[rowId].fields[this.name] != ctrl.value)
			{
			this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
			this.table.rows[rowId].fields[this.name] = ctrl.value;
			}
			
		return '';
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input logico (checkbox)
	verifyInput_boolean: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (this.mandatory && !ctrl.checked)
			return "id " + rowId + " - " + this.label + ": field is mandatory\n";
		
		// se il campo e' stato modificato accendiamo il checkbox di modifica
		// del record
		if (this.table.rows[rowId].fields[this.name] != (ctrl.checked ? "1" : "0"))
			{
			this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
			this.table.rows[rowId].fields[this.name] = ctrl.checked  ? "1" : "0";
			}
			
		return '';
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input di tipo currency
	verifyInput_currency: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (this.mandatory && !ctrl.value)
			return "id " + rowId + " - " + this.label + ": field is mandatory\n";
		
		// se il campo e' stato modificato accendiamo il checkbox di modifica
		// del record
		var valore_cmp = ctrl.value.replace(/\./g,"");
		valore_cmp = valore_cmp.replace(/\,/g,".");
		if (this.table.rows[rowId].fields[this.name] != valore_cmp)
			{
			this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
			this.table.rows[rowId].fields[this.name] = valore_cmp;
			}
			
		return '';
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input date
	verifyInput_date: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (this.mandatory && !ctrl.value)
			return "id " + rowId + " - " + this.label + ": field is mandatory\n";

		if (ctrl.value)
			{
			var year = ctrl.value.substr(6, 4);
			var month = ctrl.value.substr(3, 2);
			var day = ctrl.value.substr(0, 2);
			var date = new Date(year, month - 1, day);
			day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
			month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
			year = date.getFullYear();
			if (day + "/" + month + "/" + year != ctrl.value)
				return "id " + rowId + " - " + this.label + ": invalid date format (dd/mm/yyyy)\n";
			// se il campo e' stato modificato accendiamo il checkbox di modifica
			// del record; la date nei "fields" e' in formato epoch php
			if (date.getTime() != this.getInputValue_date(rowId))
				{
				this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
				this.table.rows[rowId].fields[this.name] = year + "-" + month  + "-" + day;
				}
			}
		else if (this.table.rows[rowId].fields[this.name])
			{
			this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
			this.table.rows[rowId].fields[this.name] = '';
			}
			
		return '';
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input dataora
	verifyInput_datetime: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (this.mandatory && !ctrl.value)
			return "id " + rowId + " - " + this.label + ": field is mandatory\n";

		if (ctrl.value)
			{
			var year = ctrl.value.substr(6, 4);
			var month = ctrl.value.substr(3, 2);
			var day = ctrl.value.substr(0, 2);
			var hour = ctrl.value.substr(11, 2);
			var min = ctrl.value.substr(14, 2);
			var sec = ctrl.value.substr(17, 2).length ? ctrl.value.substr(17, 2) : false;
			var date = new Date(year, month - 1, day, hour, min, sec);
			day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
			month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
			year = date.getFullYear();
			hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			sec = sec ? (date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds()) : sec;
			
			if (day + "/" + month + "/" + year + " " + hour + ":" + min + (sec ? ":" + sec : "") != ctrl.value)
				return "id " + rowId + " - " + this.label + ": invalid date-time format (dd/mm/yyyy hh:mm" + (sec ? ":ss" : "") + ")\n";
			
			// se il campo e' stato modificato accendiamo il checkbox di modifica
			// del record; la date nei "fields" e' in formato epoch php
			if (date.getTime() != this.getInputValue_datetime(rowId))
				{
				this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
				this.table.rows[rowId].fields[this.name] = year + "-" + month  + "-" + day + " " + hour + ":" + min + ":" + (sec ? sec : "00");
				}				
			}
		else if (this.table.rows[rowId].fields[this.name])
			{
			this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
			this.table.rows[rowId].fields[this.name] = '';
			}
			
		return '';
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input hour
	verifyInput_time: function(rowId) 
		{
		var ctrl = this.table.obj.elements[this.name + "[" + rowId + "]"];
		if (this.mandatory && !ctrl.value)
			return "id " + rowId + " - " + this.label + ": field is mandatory\n";

		if (ctrl.value)
			{
			var hour = ctrl.value.substr(0, 2);
			var min = ctrl.value.substr(3, 2);
			var sec = ctrl.value.substr(6, 2).length ? ctrl.value.substr(6, 2) : false;
			var date = new Date(1980, 0, 1, hour, min, sec);
			hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			sec = sec ? (date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds()) : sec;
			if (hour + ":" + min + (sec ? ":" + sec : "") != ctrl.value)
				return "id " + rowId + " - " + this.label + ": invalid time format (hh:mm" + (sec ? ":ss" : "") + "\n";

			// se il campo e' stato modificato accendiamo il checkbox di modifica
			// del record; la date nei "fields" e' in formato epoch php
			if (date.getTime() != this.table.rows[rowId].fields[this.name] * 1000)
				{
				this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
				this.table.rows[rowId].fields[this.name] = hour + ":" + min + ":" + (sec ? sec : "00");
				}				
			}
		else if (this.table.rows[rowId].fields[this.name])
			{
			this.table.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = true;
			this.table.rows[rowId].fields[this.name] = '';
			}
			
		return '';
		},
		
	//-------------------------------------------------------------------------
	// verifica il contenuto di un controllo di input 
	verifyInput: function(rowId) 
		{
		// se la colonna non ha input, non facciamo ovviamente niente
		if (!this.inputType)
			return '';

		// se e' gia' stata richiesta la cancellazione della riga, inutile 
		// controllare la validita' dell'input'
		if (this.table.obj.elements["watable_input_del_chk[" + rowId + "]"] &&
			this.table.obj.elements["watable_input_del_chk[" + rowId + "]"].checked)
			return '';
		
		var verifyFncName = "verifyInput_" + this.inputType.toLowerCase().substr(("WATABLE_INPUT_").length);
		if (this[verifyFncName])
			return this[verifyFncName](rowId);
		else
			return this.verifyInput_text(rowId);
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore (applicativo) di un controllo di input
	getInputValue: function(rowId) 
		{
		// se la colonna non ha input, non facciamo ovviamente niente
		if (!this.inputType)
			return '';

		var getFncName = "getInputValue_" + this.inputType.toLowerCase().substr(("WATABLE_INPUT_").length);
		if (this[getFncName])
			return this[getFncName](rowId);
		else
			return this.table.rows[rowId].fields[this.name]
		},
		
	//-------------------------------------------------------------------------
	getInputValue_date: function(rowId) 
		{
		var string_val = this.table.rows[rowId].fields[this.name];

		if (!string_val)
			{
			return false;
			}
			
		return new Date( string_val.substr(0, 4), string_val.substr(5, 2) - 1, string_val.substr(8, 2));
		},
		
	//-------------------------------------------------------------------------
	getInputValue_datetime: function(rowId) 
		{
		var string_val = this.table.rows[rowId].fields[this.name];

		if (!string_val)
			{
			return false;
			}
			
		return new Date( string_val.substr(0, 4), string_val.substr(5, 2) - 1, string_val.substr(8, 2), string_val.substr(11, 2), string_val.substr(14, 2));
		},
		
	//-------------------------------------------------------------------------
	getInputValue_time: function(rowId) 
		{
		var string_val = this.table.rows[rowId].fields[this.name];

		if (!string_val)
			{
			return false;
			}
			
		return new Date(1980, 0, 1, string_val.substr(0, 2), string_val.substr(3, 2), string_val.substr(6, 2));
		}
		
	}
);
