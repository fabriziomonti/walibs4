/**
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waTable: contiene attributi e metodi relativi a una table waLibs.
* 
* La struttura di questa classe prevede che:
*
* <ul>
* <li>
*	per ogni table venga creato un oggetto di classe waTable con name 
* 	uguale a quello della table (proprietà waTable::name in PHP) e aggiunto 
* 	come nuova proprietà dell'oggetto document. Questa operazione è svolta
* 	automaticamente dall'XSLT
*	<li>ogni azione, su riga o meno, invoca un metodo dell'oggetto del punto 
* 	precedente, metodo il cui name è così composto:
* 	<p>
* 	action_[table_name]_[action_name]
*	</p>
* 	alle azioni su riga, verrà passato come parametro del metodo 
* 	l'identificativo univoco della riga stessa
*	<li> date la struttura esposta, affinchè il metodo relativo all'azione possa  
* 	essere eseguito è necessario che il metodo sia implementato da parte della
* 	applicazione che usa la table, e che la sua
* 	implementazione venga associata (ossia: fatta appartenere) all'oggetto di 
* 	classe waTable creato nel	primo punto
* 	<li>per le azioni standard (Nuovo/Vedi/Modifica/Elimina) l'associazione del
* 	metodo come descritto nel punto precedente è già svolta dalla classe base 
* 	che associa al name del metodo comprensivo di [table_name] la propria
* 	implementazione di default dei metodi/azioni; è naturalmente possibile fare 
* 	overload di questa implementazione di default implementando i metodi con i
* 	criteri descritti al punto precedente
* 	<li>analogamente a quanto descritto per le azioni, anche eventuali link
* 	presenti nelle celle verranno risolti dall'XSLT con la chiamata ad un metodo
* 	il cui name è così composto:
* 	<p>
* 	link_[table_name]_[column_name]
*	</p>
* 	a cui verrà passato come parametro del metodo l'identificativo univoco della 
* 	riga stessa. Sarà sempre compito dell'applicazione che usa la table 
* 	implementare il metodo e associarlo (ossia: farlo appartenere) all'oggetto 
* 	di classe waTable creato nel	primo punto.
* </ul>
* Per quanto tutto ciò possa apparire complesso, la UI di default di 
* waApplicazione contiene già al suo interno un meccanismo automatico per fare
* tutto questo e renderlo disponibile in modo semplice al programmatore.
* 
* @class waTable
*/
var waTable_edit = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waTable,
	
	//-------------------------------------------------------------------------
	// proprieta'
	
	//-------------------------------------------------------------------------
	// implements


	//-------------------------------------------------------------------------
	/**
	 * metodo invocato quando l'utente richiede il submit del form (intera 
	 * table)
	 */
	action_Submit: function ()
		{
		var msg = '';
		
		for (var rowId in this.rows)
			{
			for (var colName in this.columns)
				msg += this.columns[colName].verifyInput(rowId);
			}
		if (msg !== '')
			{
			this.alert(msg);
			return false;
			}
		
		var proxy = this;
		this.confirm
			(
			"Confirm submission?",
			function () 
				{
				proxy.obj.submit();
				}
			);
			
		return false;
		},
			
  	//--------------------------------------------------------------------------
	// evento scatenato quando l'utente select il checkbox della cancellazione
	// della riga
	event_CheckBoxDelete_onclick: function (rowId)
		{
		var del_chk = this.obj.elements["watable_input_del_chk[" + rowId + "]"];
		var ctrl = null;
		
		for (var colName in this.columns)
			{
			if (this.columns[colName].inputType)
				{
				// la colonna ha un controllo di input associato
				ctrl = this.obj.elements[this.columns[colName].name + "[" + rowId + "]"];
				ctrl.disabled = del_chk.checked;
				}
			}
			
		}


	}
);
