(function (jQuery, window) {

	jQuery.fn.contextMenu = function (settings) {

		return this.each(function () {

			// Open context menu
			jQuery(this).on("contextmenu", function (e) {
				jQuery("ul.dropdown-menu").hide();

				if (jQuery(e.target).is("a")) {
					return;
				}
				
				// return native menu if pressing control
				if (e.ctrlKey)
					return;

				//open menu
				var $menu = jQuery(settings.menuSelector);
				$menu.data("invokedOn", jQuery(e.target));
				if ($menu.find("li").length)
					$menu.show();
				$menu.css({
					position: "absolute",
					left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
					top: getMenuPosition(e.clientY, 'height', 'scrollTop')
				});
				$menu.off('click');
				$menu.on('click', 'a', function (e) {
					$menu.hide();

					var $invokedOn = $menu.data("invokedOn");
					var $selectedMenu = jQuery(e.target);

					settings.menuSelected.call(this, $invokedOn, $selectedMenu);
				});

				return false;
			});

			//make sure menu closes on any click
			jQuery('body').click(function () {
				jQuery(settings.menuSelector).hide();
			});
		});

		function getMenuPosition(mouse, direction, scrollDir) {
			var win = jQuery(window)[direction](),
					scroll = jQuery(window)[scrollDir](),
					menu = jQuery(settings.menuSelector)[direction](),
					position = mouse + scroll;

			// opening menu would pass the side of the page
			if (mouse + menu > win && menu < mouse)
				position -= menu;

			return position;
		}

	};
})(jQuery, window);

