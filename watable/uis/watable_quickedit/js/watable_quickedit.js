/**
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waTable: contiene attributi e metodi relativi a una table waLibs.
* 
* La struttura di questa classe prevede che:
*
* <ul>
* <li>
*	per ogni table venga creato un oggetto di classe waTable con name 
* 	uguale a quello della table (proprietà waTable::name in PHP) e aggiunto 
* 	come nuova proprietà dell'oggetto document. Questa operazione è svolta
* 	automaticamente dall'XSLT
*	<li>ogni azione, su riga o meno, invoca un metodo dell'oggetto del punto 
* 	precedente, metodo il cui name è così composto:
* 	<p>
* 	action_[table_name]_[action_name]
*	</p>
* 	alle azioni su riga, verrà passato come parametro del metodo 
* 	l'identificativo univoco della riga stessa
*	<li> date la struttura esposta, affinchè il metodo relativo all'azione possa  
* 	essere eseguito è necessario che il metodo sia implementato da parte della
* 	applicazione che usa la table, e che la sua
* 	implementazione venga associata (ossia: fatta appartenere) all'oggetto di 
* 	classe waTable creato nel	primo punto
* 	<li>per le azioni standard (Nuovo/Vedi/Modifica/Elimina) l'associazione del
* 	metodo come descritto nel punto precedente è già svolta dalla classe base 
* 	che associa al name del metodo comprensivo di [table_name] la propria
* 	implementazione di default dei metodi/azioni; è naturalmente possibile fare 
* 	overload di questa implementazione di default implementando i metodi con i
* 	criteri descritti al punto precedente
* 	<li>analogamente a quanto descritto per le azioni, anche eventuali link
* 	presenti nelle celle verranno risolti dall'XSLT con la chiamata ad un metodo
* 	il cui name è così composto:
* 	<p>
* 	link_[table_name]_[column_name]
*	</p>
* 	a cui verrà passato come parametro del metodo l'identificativo univoco della 
* 	riga stessa. Sarà sempre compito dell'applicazione che usa la table 
* 	implementare il metodo e associarlo (ossia: farlo appartenere) all'oggetto 
* 	di classe waTable creato nel	primo punto.
* </ul>
* Per quanto tutto ciò possa apparire complesso, la UI di default di 
* waApplicazione contiene già al suo interno un meccanismo automatico per fare
* tutto questo e renderlo disponibile in modo semplice al programmatore.
* 
* @class waTable
*/
var watable_quickedit = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waTable_edit,
	
	//-------------------------------------------------------------------------
	// proprieta'
	
	//-------------------------------------------------------------------------
	// implements


	//-------------------------------------------------------------------------
	//-----  funzionalità quick-edit  -----------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	// mostra un errore in fase di rpc e mantiene il focus sul control
	showRPCError: function(msg, control) 
		{
		this.alert(msg);
		if (control)
			setTimeout(function(){control.focus();}, 5); 
		return null;
			
		},
		
	//-------------------------------------------------------------------------
	/**
	 * effettua una chiamata RPC verso il server.
	 * 
	* gli argomenti da passare al metodo sono posizionali 
	* <ol>
	* <li>name funzione/metodo PHP da richiamare lato server
	* <li>parametro 1 da passare alla funzione/metodo
	* <li>parametro 2 da passare alla funzione/metodo
	* <li>parametro n da passare alla funzione/metodo...
	* </ol>
	* 
	* caso speciale: se il name funzione/metodo e' watable_rpc_quick_update, 
	* allora la chiamata e' gestita internamente dalla classe PHP waTable
	* (non chiama in causa l'applicazione); 
	* in questo caso il secondo parametro e' una stringa di coppie chiave/valore
	* che la classe PHP ricevera' in POST e dara' in pasto all'XSLT di input, 
	* e si comportera' esattamente come avesse ricevuto il post in fase di editing.
	* Questa funzionalita' e' utilizzata quando si vuole consolidare immediatamente 
	* su DB un dato all'uscita da un control, o la cancellazione di un record 
	* senza ricaricare la pagina, o la creazione di un nuovo record senza ricaricare
	* la pagina
	 */
	RPC: function(callback, action, rowId, columnName, value) 
		{
		var toPost = {};
		toPost.watable_rpc = 1;
		toPost.watable_name = this.name;
		toPost.watable_rpc_action = action;
		toPost.watable_rpc_row_id = rowId;
		toPost.watable_rpc_column_name = columnName;
		toPost.watable_rpc_value = value;
			
		callback = jQuery.proxy(callback, this);
		jQuery.ajax
			(
				{
				url			: document.location.href,
				type		: 'POST',
				dataType	: 'json',
				data		: toPost,
				success		: function (response, textStatus, jqXHR) 
								{
								callback(response, rowId);
								},
				error		: function(jqXHR, textStatus, errorThrown)
								{
								callback(null, rowId);
								},
				timeout	: 20000
				}
			);
	
		},

	//-------------------------------------------------------------------------
	/**
	 * all'evento onblur su un control di edit submit tramite RPC i dati 
	 * modificati al server
	 * 
	 * @param {string} columnName name della column/campo a cui appartiene la cella
	 * @param {string} rowId identificativo della riga a cui appartiene la cella
	 */
	action_quickEdit: function(columnName, rowId) 
		{
		var column = this.columns[columnName];
		var control = this.obj.elements[column.name + "[" + rowId + "]"];
		var msg = column.verifyInput(rowId) ;
		if (msg != '')
			return this.showRPCError(msg, control);

		// se il control non e' stato modificato e' inutile andarlo ad aggiornare
		if (!this.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked)
			return true;

		var value = column.inputType == 'boolean' ? (control.checked ? '1' : '0') : control.value;
		this.RPC(this.gotQuickEditResponse, "watable_rpc_quick_edit", rowId, column.name, value);
		},
		
	//-------------------------------------------------------------------------
	/**
	 */
	gotQuickEditResponse: function(response, rowId) 
		{
		if (!response)
			{
			return this.showRPCError("RPC system error");
			}
		if (response.watable_rpc_response != "WATABLE_RPC_OK")
			{
			return this.showRPCError("RPC application error:\n" + response.watable_rpc_message);
			}
			
		this.obj.elements["watable_input_mod_chk[" + rowId + "]"].checked = false;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * richiede al server l'eliminazione di un record tramite RPC
	 * 
	 * @param {string} rowId identificativo della riga a cui appartiene la cella
	 */
	action_quickDelete: function(rowId) 
		{
		var proxy = this;
		this.confirm
			(
			"Confirm deletion?",
			function () 
				{
				proxy.RPC(proxy.gotQuickDeleteResponse, "watable_rpc_quick_delete", rowId);
				}
			);
		},
		
	//-------------------------------------------------------------------------
	/**
	 */
	gotQuickDeleteResponse: function(response, rowId) 
		{
		if (!response)
			{
			return this.showRPCError("RPC system error");
			}
		if (response.watable_rpc_response != "WATABLE_RPC_OK")
			{
			return this.showRPCError("RPC application error:\n" + response.watable_rpc_message);
			}
		jQuery("#row_" + this.name + "_" + rowId).hide();
		},
		
	//-------------------------------------------------------------------------
	/**
	 * richiede al server la creazione di un record tramite RPC
	 * 
	 */
	action_quickNew: function() 
		{
		this.RPC(this.gotQuickNewResponse, "watable_rpc_quick_new");
		},
		
	//-------------------------------------------------------------------------
	/**
	 */
	gotQuickNewResponse: function(response) 
		{
		if (!response)
			{
			return this.showRPCError("RPC system error");
			}
		if (response.watable_rpc_response != "WATABLE_RPC_OK")
			{
			return this.showRPCError("RPC application error:\n" + response.watable_rpc_message);
			}
		
		var rowId = response.watable_rpc_data;	// identificativo della riga che viene creata
		
		var newRow = jQuery("<div />").append(jQuery("#row_" + this.name + "____xxx___").clone()).html();		
		newRow = newRow.replace(/___xxx___/g, rowId);
		jQuery("#row_" + this.name + "____xxx___").after(newRow);
		jQuery("#row_" + this.name + "_" + rowId).show();
		
		new waRow(this, rowId, {});
		return rowId;
		}
		

	}
);
