<?php 
namespace waLibs\views\waTable\waTable_actions_context_left_click;
use stdClass;

include_once __DIR__ . "/../../watable_actions_context/view/watable.php";
	
//******************************************************************************
class waTableView extends \waLibs\views\waTable\waTable_actions_context\waTableView
	{
	
	//**************************************************************************
	protected function setJavascriptLink()
		{
		\waLibs\views\waTable\waTable_default\waTableView::setJavascriptLink();
		?>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_actions_context_left_click/js/watable_actions_context_left_click.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	}
//******************************************************************************


