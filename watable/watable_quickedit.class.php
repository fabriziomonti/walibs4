<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* @ignore
*/
include_once __DIR__ . "/watable_edit.class.php";


//***************************************************************************
//****  classe waTable_quick_edit **************************************************
//***************************************************************************
/**
* waTable_quick_edit
*
* estensione della classe {@link waTable} contenente le informazioni
 * aggiuntive per la gestione del quick-editing, ossia l'immediato consolidamento
 * su db a fronte di perdita focus di un controllo di input contenuto nella 
 * tabella
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTable_quickedit extends waTable_edit
	{
	
	/**
	* istanza applicazione a cui la tabella appartiene
	 * 
	 * eventuale istanza di una applicazione (waApplication o sua estensione o 
	 * simili) all'interno della quale la tabella è stata generata.
	 * <br/><br/>
	 * La valorizzazione di questa proprietà ha senso solo quando si intende
	 * effettuare una chiamata RPC ad un metodo che appartiene all'applicazione
	*
	* @var object
	*/	
	public $application = null;
	
	//***************************************************************************
	/**
	 * legge eventuali valori in input quando la tabella e' usata come modulo
	*/
	public function getInputValues()
		{
		if ($_POST["watable_name"] != $this->name)
		// il post (se c'e') non e' relativo a questa istanza della tabella
			{
			return;
			}

		// eventuali parametri di una chiamata RPC applicativa non ha senso che 
		// vengano macinati dall'XSLT; se e' una chiamata RPC applicativa per 
		// questa istanza della tabella, usiamo i parametri prelevandoli
		// direttamente dal POST e usciamo
		if (!$_POST["watable_rpc"])
			{
			return parent::getInputValues();
			}

		$this->record = $this->getRecToUpdate($_POST["watable_rpc_row_id"]);
		if (!$this->record)
			{
			$this->RPCResponse (waTable::RPC_KO, "db error");
			}
			
		if ($_POST["watable_rpc_action"] == "watable_rpc_quick_edit")
			{
			$this->createRawInputData();
			$this->setView();
			$rawValue = $this->view->transformInput($this->data);
			$colName = $_POST["watable_rpc_column_name"];
			$method_name = "convertInput_" . strtolower(substr($this->columns[$colName]->inputType, strlen("WATABLE_INPUT_")));
			if (method_exists($this, $method_name))
				{
				$this->record->$colName = call_user_func(array($this, $method_name), $rawValue);
				}
			else
				{
				$this->record->$colName = $this->convertInput_text($rawValue);
				}
			}
		
		elseif ($_POST["watable_rpc_action"] == "watable_rpc_quick_delete")
			{
			$this->record->delete();
			}
		
		elseif ($_POST["watable_rpc_action"] == "watable_rpc_quick_new")
			{
			// questo serve per fregare righedb, altrimenti se non trova nessun 
			// dato cambiato non effettua neppure l'insert...
			$this->record->insertValue(0, '0');
			}

		else
			{
			$this->RPCResponse (waTable::RPC_KO, "function unknown");
			}
		
		// segnaliamo all'applicazione che è avvenuto input 
		$this->input = true;
		}
		
	//***************************************************************************
	/**
	 * salva il record modificato in getInputValues
	 * 
	 * l'eventuale esito di errore viene ritornato direttamente al client
	 * tramite json, non all'applicazione
	 * 
	*/
	public function save($consolidate = false)
		{
		$this->record->recordset->save();
		if ($this->record->recordset->dbConnection->errorNr())
			{
			$this->RPCResponse (waTable::RPC_KO, "db error");
			}

		$this->RPCResponse (waTable::RPC_OK, "", $this->record->recordset->dbConnection->lastInsertedId());
		
		}
		
	//***************************************************************************
	/**
	 * @return waRecord 
	 * @ignore
	*/
	protected function getRecToUpdate($recordId)
		{
		// se non e' gia' stato creato il recordset della tabella, ne creiamo 
		// comunque uno vuoto; questo ci serve per avere informazioni sulle
		// colonne e quindi sulla chiave primaria
		if (!$this->recordset)
			{
			if (!$this->fromSql())
				{
				return false;
				}
			$this->dbConnection = $this->dbConnection ? $this->dbConnection :
								wadb_getConnection($this->_dbConfigFile);
			if ($this->dbConnection->errorNr())
				{
				return false;
				}
			$this->recordset = new waRecordset($this->dbConnection);
			$this->recordset->read($this->_sql, 0);
			}
		
		// per regola la prima colonna della waTable e' sempre LA chiave 
		// primaria	(la doppia indirezione e' per avere il case corretto del
		// name campo)
		$pk = $this->recordset->fieldName($this->recordset->fieldIndex($this->_colsByCntr[0]->name));
		$pkTable = $this->recordset->columns[strtoupper($pk)]["table"];
		
		// creiamo la stringa sql per creare il recordset
		$sql = "SELECT * FROM $pkTable where $pk=" . $this->dbConnection->sqlString($recordId);
		$rs = new waRecordset($this->dbConnection);
		$rs->read($sql, 1);
		if ($rs->errorNr())
			{
			return false;
			}
		if (!$rs->records && $recordId)
			{
			return false;
			}
		return $rs->records ? $rs->records[0] : $rs->add();
		}
		
	//***************************************************************************
	/**
	* invia al client una risposta a RPC in formato  XML
	*
	* @ignore
	*/
	public function RPCResponse($esito = waTable::RPC_OK, $messaggio = '', $datiRisposta = null) 
		{
		$retval = new stdClass();
		$retval->watable_rpc_response = $esito;
		$retval->watable_rpc_message = $messaggio;
		$retval->watable_rpc_data = $datiRisposta;
		$retval = json_encode($retval, defined("JSON_PRETTY_PRINT") ? JSON_PRETTY_PRINT : 0);
		header("Content-Type: application/json; charset=utf-8");
		exit($retval);
		
		}
		
//***************************************************************************
	}	// fine classe waTable_edit
