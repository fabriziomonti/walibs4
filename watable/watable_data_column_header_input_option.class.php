<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataColumnHeaderInputOption ***************************
//***************************************************************************
/**
* waTableDataColumnHeaderInputOption
*
* struttura dati da passare al view-object: specifiche per le opzioni di un 
 * controllo di input (select, radio...)
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataColumnHeaderInputOption 
	{
	/**
	 * valore dell'opzione
	 * 
	 * @var string
	 */
	public $value;

	/**
	 * testo dell'opzione
	 * 
	 * @var string
	 */
	public $text;

	}
	
