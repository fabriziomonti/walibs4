<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include della classe waColumn_edit
* @ignore
*/
include_once(__DIR__ . "/wacolumn_edit.class.php");

/**
* @ignore
*/
include_once __DIR__ . "/watable.class.php";


//***************************************************************************
//****  classe waTable_edit **************************************************
//***************************************************************************
/**
* waTable_edit
*
* estensione della classe {@link waTable} contenente le informazioni
 * aggiuntive per la gestione dell'editing
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTable_edit extends waTable
	{
	
	/**
	* matrice dei campi modificati da un'eventuale azione di input
	*
	* @var array
	*/	
	public $input = array();
	
	//***************************************************************************
	/**
	* Aggiunge una colonna alla tabella
	*
	* per la spiegazione approfondita dei parametri si veda le proprieta'
	* della classe {@link waColumn}.
	*
	* @param string $name name colonna
	* @param string $label intestazione colonna
	* @param boolean $show flag di visualizzazione
	* @param boolean $sort flag di ordinamento
	* @param boolean $filter flag di filtro
	* @param integer $alignment alignment colonna
	* @param integer $format format contenuto cella
	* @param integer $decimalDigitsNr nr. decimali da mostrare in caso di format float
	* @param integer $maxChar nr.massimo caratteri visualizzabili dalla cella
	* @param boolean $link indica che il contenuto della cella e' destinato a contenere un link
	* @param string $computeFunction funzione personalizzata di calcolo del valore da mostrare nella cella
	* @param boolean $totalize flag di totalizzazione colonna
	* @param string $aliasOf eventuale name campo di cui la colonna e' alias
	* @param boolean $noWrap indica se usare o meno il wrap all'interno delle celle della colonna
	* @param boolean $HTMLConversion indica se convertire o meno il valore trovato in corrispondenza del campo affinche' venga mostrato, di eventuali sequenze HTML significative, il codice oppure il rendering
	* @return waColumn istanza dell'oggetto {@link waColumn} che viene aggiunta
	* all'array {@link colonne}; e' naturalmente possibile modificare gli attributi
	* di questa istanza a piacimento.
	*/
	public function addColumn(	$name, 
								$label, 
								$show = true,
								$sort = true, 
								$filter = true, 
								$alignment = '', 
								$format = '',
								$decimalDigitsNr = 2,
								$maxChar = waTable::CELL_MAX_CHAR,
								$link = false,
								$computeFunction = '',
								$totalize = false,
								$aliasOf = '',
								$noWrap = false,
								$HTMLConversion = true
							)

		{
		$index = count($this->columns);
		$this->columns[$name] = new waColumn_edit();
		$this->columns[$name]->name = $name;
		$this->columns[$name]->label = $label;
		$this->columns[$name]->show = $show;
		$this->columns[$name]->sort = $this->fromSql() ? $sort : false;
		$this->columns[$name]->filter = $this->fromSql() ? $filter : false;
		$this->columns[$name]->alignment = $alignment;
		$this->columns[$name]->format = $format;
		$this->columns[$name]->decimalDigitsNr = $decimalDigitsNr;
		$this->columns[$name]->maxChar = $maxChar;
		$this->columns[$name]->link = $link;
		$this->columns[$name]->computeFunction = $computeFunction;
		$this->columns[$name]->totalize = $totalize;
		$this->columns[$name]->aliasOf = $aliasOf;
		$this->columns[$name]->noWrap = $noWrap;
		$this->columns[$name]->HTMLConversion = $HTMLConversion;
                
		$this->_colsByCntr[$index] = &$this->columns[$name];
		return $this->columns[$name];
		}

	//***************************************************************************
	/**
	* costruisce la struttura dati da passare al {@link view view-object} per
	 * ottenere la trasformazione dell'input
	*
	* @access protected
	* @ignore
	*/
	protected function createRawInputData()
		{
		$this->data = new waTableData();
		$this->data->name = $this->name;
		$this->data->uri = $this->getURI();
		$this->data->waTablePath = $this->_currPath;
		$this->data->title = $this->title;
		$this->data->exclusiveSelection = $this->exclusiveSelection;
		$this->data->formPage = $this->formPage;
		
		// headers della tabella (unica cosa che servirebbe?)
		$this->createColHeadersData();
			
		} 

	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/
	protected function createColHeadersData()
		{
		parent::createColHeadersData();
		
		$idx = 0;
		foreach ($this->columns as $col)
			{
			
			if ($col->inputType)
				{
				$this->data->columnHeaders[$idx]->input = new waTableDataColumnHeaderInput();
				$this->data->columnHeaders[$idx]->input->type = $col->inputType;
				$this->data->columnHeaders[$idx]->input->mandatory = $col->inputMandatory;
				
				$col->fieldMaxLen = !$col->fieldMaxLen && $this->fromSql() && $this->recordset ? 
										$this->recordset->fieldMaxLength($col->name) :
										$col->fieldMaxLen;
				
				$this->data->columnHeaders[$idx]->input->fieldMaxLen = $col->fieldMaxLen;

				if (is_array($col->inputOptions))
					{
					$this->data->columnHeaders[$idx]->input->options = array();
					$oidx = 0;
					foreach ($col->inputOptions as $val => $text)
						{
						$this->data->columnHeaders[$idx]->input->options[$oidx] = new waTableDataColumnHeaderInputOption();
						$this->data->columnHeaders[$idx]->input->options[$oidx]->value = $val;
						$this->data->columnHeaders[$idx]->input->options[$oidx]->text = $text;
						$oidx++;
						}
					}
				}
								
			$idx++;
			}
		}
		
	//***************************************************************************
	/**
	 * legge eventuali valori in input quando la tabella e' usata come modulo
	*/
	public function getInputValues()
		{
		if ($_POST["watable_name"] != $this->name)
		// il post (se c'e') non e' relativo a questa istanza della tabella
			{
			return;
			}

		$this->createRawInputData();
			
		// se non è stato passato definisce l'oggetto view di default
		$this->setView();
		$rawInputData = $this->view->transformInput($this->data);
			
		if (!$rawInputData)
			{
			return;
			}

		$this->setInputData($rawInputData);
		}
		
	//***************************************************************************
	/**
	 * -
	 * 
	* @ignore
	* @access protected
	*/
	protected function setInputData($rawInputData)
		{
		// converte tutti i valori ricevuti in input nel formato utilizzabile
		// dall'applicazione e li mette nella proprieta' $input
		foreach ($rawInputData as $id_riga => $valori)
			{
			foreach ($valori as $fieldName => $value)
				{
				if ($fieldName == "watable_delete")
					{
					$this->input[$id_riga][$fieldName] = true;
					}
				else
					{
					$method_name = "convertInput_" . strtolower(substr($this->columns[$fieldName]->inputType, strlen("WATABLE_INPUT_")));
					if (method_exists($this, $method_name))
						{
						$this->input[$id_riga][$fieldName] = call_user_func(array($this, $method_name), $value);
						}
					else
						{
						$this->input[$id_riga][$fieldName] = $this->convertInput_text($value);
						}
					}
				}
			}
		}
		
	//***************************************************************************
	/**
	 * -
	 * 
	 * restituisce true se il recordset della tabella e' da aggiornare a fronte
	 * di eventuale input
	*/
	public function isToUpdate()
		{
		return $this->input ? true : false;
		}
		
	//***************************************************************************
	/**
	 * salva nel recordset i nuovi valori derivanti da eventuale input
	 * 
	* I campi che che vengono valorizzati
	 * sono unicamente quelli che appartengono alla tabella a cui appartiene
	 * il campo chiave primaria
	 * 
	 * @param boolean $consolidate : se true effettua anche il consolidamento
	 * sulla base dati; altrimenti si limita a valorizzare i waRecord
	 * 
	 * @return boolean : false = errore; true = ok
	*/
	public function save($consolidate = false)
		{
		// se non e' gia' stato creato il recordset della tabella, ne creiamo 
		// comunque uno vuoto; questo ci serve per avere informazioni sulle
		// columns e quindi sulla chiave primaria
		if (!$this->recordset)
			{
			if (!$this->fromSql())
				{
				return false;
				}
			$this->dbConnection = $this->dbConnection ? $this->dbConnection :
								wadb_getConnection($this->_dbConfigFile);
			if ($this->dbConnection->errorNr())
				{
				return false;
				}
			$this->recordset = new waRecordset($this->dbConnection);
			$this->recordset->read($this->_sql, 0);
			}
		
		// per regola la prima colonna della waTable e' sempre LA chiave 
		// primaria	(la doppia indirezione e' per avere il case corretto del
		// name campo)
		$pk = $this->recordset->fieldName($this->recordset->fieldIndex($this->_colsByCntr[0]->name));
		$tabellaPK = $this->recordset->columns[strtoupper($pk)]["table"];
		
		// creiamo la stringa sql per creare il recordset
		$sql = "SELECT * FROM $tabellaPK";
		
		// ciclo delle modifiche/cancellazioni; cerchiamo di racchiudere tutto 
		// in una transazione, anche se non e' detto che la tabella sul db le 
		// supporti
		$this->dbConnection->beginTransaction();
		foreach ($this->input as $id_riga => $valori)
			{
			$rs = new waRecordset($this->dbConnection);
			// leggiamo il record relativo alla sola chiave primaria
			$rs->read("$sql WHERE $pk=" . $this->dbConnection->sqlString($id_riga), 1);
			if (!$rs->records)
				{
				return false;
				}
			if ($valori["watable_delete"])
				{
				$rs->records[0]->delete();
				}
			else
				{
				foreach ($valori as $nome_campo => $value)
				// non facciamo controllisull'esistenza del campo, perche'
				// se il name campo non esiste nel recordset di destinazione
				// la scrittura non ha nessun effetto
					{
					$rs->records[0]->insertValue($nome_campo, $value);
					}
				}
			if ($consolidate && !$rs->save())
				{
				return false;
				}
			}
		$this->dbConnection->commitTransaction();
			
		return true;
		}
		
	//***************************************************************************
	/**
	* converte un valore di tipo testo cosi' come arrivato dal post nel formato 
	 * utilizzabile dall'applicazione
	*
	* @ignore
	*/
	protected function convertInput_text($value) 
		{
		return $value;
		}
		
	//***************************************************************************
	/**
	* converte un valore di tipo data cosi' come arrivato dal post nel formato 
	 * utilizzabile dall'applicazione
	*
	* @ignore
	*/
	protected function convertInput_date($value) 
		{
		if (@preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $value, $parts))
			{
			return mktime(0, 0, 0, $parts[2], $parts[3], $parts[1]);
			}

		return false;
		}
		
	//***************************************************************************
	/**
	* converte un valore di tipo dataora cosi' come arrivato dal post nel formato 
	 * utilizzabile dall'applicazione
	*
	* @ignore
	*/
	protected function convertInput_datetime($value) 
		{
		if (@preg_match('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', $value, $parts))
			{
			return mktime($parts[4], $parts[5], $parts[6], $parts[2], $parts[3], $parts[1]);
			}

		return false;
		}
		
	//***************************************************************************
	/**
	* converte un valore di tipo ora cosi' come arrivato dal post nel formato 
	 * utilizzabile dall'applicazione
	*
	* @ignore
	*/
	protected function convertInput_time($value) 
		{
		if (@preg_match('/^(\d{2}):(\d{2}):(\d{2})$/', $value, $parts))
			{
			return mktime($parts[1], $parts[2], $parts[3], 1, 1, 1980);
			}

		return false;
		}
		
	//***************************************************************************
	/**
	* converte un valore di tipo valuta cosi' come arrivato dal post nel formato 
	 * utilizzabile dall'applicazione
	*
	* @ignore
	*/
	protected function convertInput_currency($value) 
		{
		if ($value === '')
			{
			return null;
			}
		return floatval($value);
		}
		
	//***************************************************************************
	}	// fine classe waTable_edit
