<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;
use stdClass;

/**
* -
* include della classe waColumn
* @ignore
*/
include_once(__DIR__ . "/wacolumn.class.php");

//***************************************************************************
//****  classe waColumn_edit ****************************************************
//***************************************************************************
/**
* waColumn_edit
*
* estensione della classe {@link waColumn} contenente le informazioni
 * aggiuntive per la gestione dell'editing
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waColumn_edit extends waColumn
	{
	
	/**
	* -
	*
	* indica che tipo di input type  utilizzare lato client nel caso in cui la tabella venga utilizzata 
	* per effettuare editing direttamente, senza passare da un waForm. Ovviamente questo tipo 
	* di tabella deve essere renderizzata da un {@link waTable::$view view-object} in grado di:
	* - gestire il submit dei dati
	* - effettuare al momento del submit una validazione dei dati inseriti
	* Altrettanto ovviamente, lato server occorre scrivere il codice per recepire le modifiche effettuate
	* tramite i controlli di input dall'operatore.
	* 
	* Esiste una codifica predefinita per i {@link waTable::INPUT_TEXTAREA tipi} che è possibile trovare all'interno delle const
	 * del package, e che si consiglia di utilizzare per ovvi motivi di mutua
	 * leggibilità del codice. 
	* 
	* Non esiste il default per questa proprieta' (si suppone che non tutte le colonne debbano essere editabili,
	* quindi il programmatore valorizzera' la proprieta' solo laddove necessita di editing)
	* 
	* @var string
	*/	
	public $inputType = "";
	
	/**
	* inputObbligatorio
	*
	* nel caso in cui {@link $inputType} sia valorizzato, indica se il controllo di input deve essere 
	* obbligatoriamente valorizzato o meno
	* 
	* @var boolean
	*/	
	public $inputMandatory = false;
	
	/**
	* inputOptions
	*
	* nel caso in cui {@link $inputType} sia valorizzato, indica le opzioni che l'input puo' assumere
	* (tipicamente in caso di tipo = selezione o opzione). La proprieta' e' un array associativo in cui la chiave di
	* ogni elemento e' il valore dell'opzione, e il valore dell'elemento e' il testo da selezionare
	* 
	* @var array()
	*/	
	public $inputOptions = "";

	/**
	* fieldMaxLength
	*
	* indica la lunghezza massima del campo contenuto nella colonna (non e' il numero massimo di caratteri che la colonna deve presentare, 
	* corrispondente alla proprieta' {@link $maxChars}; e' la lunghezza massima del campo su db, concetto ben diverso). 
	* Questa informazione puo' essere utile qualora si intenda fare
	* editing direttamente tramite la tabella, e quindi in congiunzione con l'utilizzo esplicito di {@link $inputType}.
	* 
	* In caso di recordset letto da base dati, per default questa proprieta' riporta il risultato di waRecordset::fieldMaxLength
	* (e' ovviamente possibile sovrascrivere questo valore e forzarlo). In caso di recordset generato da array sara'
	* compito del programmatore valorizzare opportunamente questa proprieta', qualora servisse lato UI.
	* 
	* Non esiste il default per questa proprieta'
	* 
	* @var string
	*/	
	public $fieldMaxLen = "";
	
//***************************************************************************
	}	// fine classe waColumn_edit


