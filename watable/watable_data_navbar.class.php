<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataNavbar *****************************************
//***************************************************************************
/**
* waTableDataNavbar
*
* struttura dati da passare al view-object: navbar
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataNavbar 
	{
	/**
	 * nr. pagina corrente (0 based)
	 * 
	 * @var int
	 */
	public $currentPageNr;

	/**
	 * nr. totale pagine
	 * 
	 * @var int
	 */
	public $totalPageNr;

	/**
	 * nr. progressivo del primo record della pagina
	 * 
	 * @var int
	 */
	public $firstRecord;

	/**
	 * nr. progressivo dell'ultimo record della pagina
	 * 
	 * @var int
	 */
	public $lastRecord;

	/**
	 * nr. totale dei record (di tutte le pagine)
	 * 
	 * @var int
	 */
	public $totalRecordNr;

	}
	
