<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataOrder *********************************************
//***************************************************************************
/**
* waTableDataOrder
*
* struttura dati da passare al view-object: struttura di un ordinamento
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataOrder 
	{
	/**
	 * indice dell'ordinamento
	 * 
	 * @var int
	 */
	public $index;

	/**
	 * nome della colonna su cui ordinare
	 * 
	 * @var string
	 */
	public $field;
		
	/**
	 * modo con cui operare l'ordinamento {@link waTableDataOrderMode::value}
	 * 
	 * @var string
	 */
	public $mode;
	}
	
