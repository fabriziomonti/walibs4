<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/watable_data_column_header_input.class.php";


//***************************************************************************
//****  classe waTableDataColumnHeader **************************************
//***************************************************************************
/**
* waTableDataColumnHeader
*
* struttura dati da passare al view-object: dati dell'intestazione di una colonna
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataColumnHeader 
	{
	/**
	 * nome
	 * 
	 * @var string
	 */
	public $name;

	/**
	 *  etichetta
	 * 
	 * @var string
	 */
	public $label;

	/**
	 * allineamento (vedi waTable::ALIGN_*)
	 * 
	 * @var string
	 */
	public $alignment;

	/**
	 * visibilità della colonna
	 * 
	 * @var bool
	 */
	public $show;

	/**
	 * ordinabilità della colonna
	 * 
	 * @var bool
	 */
	public $sort;

	/**
	 * filtrabilità della colonna
	 * 
	 * @var bool
	 */
	public $filter;

	/**
	 * tipo campo su db (si vedano le define di waDB)
	 * 
	 * @var string
	 */
	public $fieldType;

	/**
	 * formattazione della colonna (vedi waTable::FMT_*)
	 * 
	 * @var string
	 */
	public $format;

	/**
	 * nr massimo di caratteri che può mostrare la colonna
	 * 
	 * @var int
	 */
	public $maxChar;

	/**
	 * la colonna deve convertire i caratteri HTML
	 * 
	 * @var bool
	 */
	public $HTMLConversion;

	/**
	 * la colonna conterrà un link
	 * 
	 * @var bool
	 */
	public $link;

	/**
	 * sulla colonna si può eseguire ordinamento rapido
	 * 
	 * @var bool
	 */
	public $quickSort;

	/**
	 * eventuale nr di decimali da usare in formattazione
	 * 
	 * @var bool
	 */
	public $decimalDigitsNr;

	/**
	 * eventuale struttura dati per gestione input
	 * 
	 * @var waTableDataColumnHeaderInput
	 */
	public $input;

	}
	
