<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataTotalsRow *****************************************
//***************************************************************************
/**
* waTableDataTotalsRow
*
* struttura dati da passare al view-object: dati della riga dei totali della 
* tabella
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataTotalsRow 
	{
	/**
	 * array dei valori contenuti nelle celle; ogni elemento dell'array corrisponde
	 * posizionalmente alla colonna definita in waTableData::columnHeaders
	 * 
	 * @var mixed
	 */
	public $cells;

	}
	
