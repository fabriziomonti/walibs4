<?php
/**
* -
*
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waTableDataFilter ********************************************
//***************************************************************************
/**
* waTableDataFilter
*
* struttura dati da passare al view-object: struttura di un filtro di ricerca
* 
* @package waTable
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTableDataFilter 
	{
	/**
	 * indice del filtro
	 * 
	 * @var int
	 */
	public $index;

	/**
	 * nome della colonna su cui filtrare
	 * 
	 * @var string
	 */
	public $field;
		
	/**
	 * modo con cui operare il filtro {@link waTableDataFilterMode::value}
	 * 
	 * @var string
	 */
	public $mode;
		
	/**
	 * valore con cui operare il filtro
	 * 
	 * @var string
	 */
	public $value;
		
	}
	
