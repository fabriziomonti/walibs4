//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
 * classe waEmail: controllo di input di un indirizzo email
 * 
 * @class waEmail
 * @extends waControl
 */
var waEmail = new Class({
    //-------------------------------------------------------------------------
    // extends
    Extends: waControl,

    //-------------------------------------------------------------------------
    // proprieta'
    type: 'email',

    //-------------------------------------------------------------------------
    formatVerify: function () {
        if (this.obj.value == '') {
            return true;
        }
        
        // per battere pari con RFC5322 e FILTER_VALIDATE_EMAIL escludiamo gli indirizzi ".@" 
        if (this.obj.value.indexOf(".@") !== -1) {
            return false;
        }
        
        //var emailPattern = /^(\w+(?:\.\w+)*)@((?:\w+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var emailPattern = /^[\w-\.]{1,}\@([\da-zA-Z-]{2,}\.){1,}[\da-zA-Z-]{2,4}$/i;
        return (emailPattern.test(this.obj.value) ? true : false);

    }

});
