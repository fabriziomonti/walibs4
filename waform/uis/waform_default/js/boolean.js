//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waBoolean: input HTML di tipo si/no, renderizzato con un checkbox.
* 
* Il controllo applicativo assume sempre i valori 0/1
* 
* @class waBoolean
* @extends waControl
*/
var waBoolean = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'boolean',
	
	//-------------------------------------------------------------------------
	get: function() 
		{
		return this.obj.checked ? 1 : 0;
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(valore) 
		{
		this.obj.checked = valore ? 1 : 0;
		},
		
	//-------------------------------------------------------------------------
	mandatoryVerify: function() 
		{
		if (!this.mandatory)
			return true;
		return this.obj.checked;
		}
		
	}
);
