//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waDateTime: controllo per l'input di una data/ora
* 
* @class waDateTime
* @extends waDa
*/
var waDateTime = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waDate,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'datetime'
	
	}
);
