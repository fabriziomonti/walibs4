//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waTime: controllo per l'input di una ora
* 
* @class waTime
* @extends waControl
*/
var waTime = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waDate,
	
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	get: function() 
		{
		var retval = false;
		
		try 
			{
			var tmp = jQuery(this.pickerSelector).data("DateTimePicker").date().toDate();
			retval = new Date(1980, 0, 1, tmp.getHours(), tmp.getMinutes(), tmp.getSeconds());
			}
		catch (e)
			{
			}
			
		return retval;
		}
	
	}
);
