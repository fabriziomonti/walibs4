//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waControl: contiene attributi e metodi relativi a un controllo
* HTML generico.
* 
* Da questa classe derivano tutti gli altri tipi di controllo del package.
* 
* 
* @class waControl
*/
var waControl = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	
	/**
	 * oggetto di classe {@link waForm} a cui il controllo appartiene
	 * @type waForm
	 * @memberof waButton
	 */
	form			: null,
	
	/**
	 * name del controllo
	 * @type string
	 * @memberof waControl
	 */
	name			: '',
	
	/**
	 * tipo del controllo; viene definito da ogni sottoclasse
	 * @type string
	 * @memberof waControl
	 */
	type			: '',
	
	/**
	 * value del controllo
	 * @type string
	 * @memberof waControl
	 */
	value			: '',
	
	/**
	 * indica se il controllo deve essere visible o meno
	 * @type boolean
	 * @memberof waControl
	 */
	visible			: false,
	
	/**
	 * indica se il controllo deve essere readonly o meno
	 * @type boolean
	 * @memberof waControl
	 */
	readOnly		: false,
	
	/**
	 * indica se il controllo deve essere mandatory o meno
	 * @type boolean
	 * @memberof waControl
	 */
	mandatory	: false,
	
	/**
	 * ogetto HTML contenente il il controllo fisico (questa classe rappresenta
	 * il controllo logico/applicativo; qualche volta i due concetti coincidono,
	 * ma non sempre).
	 * @type HTML_input_object
	 * @memberof waControl
	 */
	obj				: null,
	
	/**
	 * selettore jQuery dell'oggetto HTML o del contenitore degli oggetti HTML
	 * che compongono il controllo
	 * 
	 * @memberof waControl
	 */
	selector		: '',
	
	//-------------------------------------------------------------------------
	/**
	 * inizializzazione (costruttore)
	 * 
	 * @param {waForm} form valorizza la proprietà {@link form}
	 * @param {string} name valorizza la proprietà {@link name}
	 * @param {string} value valorizza la proprietà {@link value}
	 * @param {boolean} visible valorizza la proprietà {@link visible}
	 * @param {boolean} readOnly valorizza la proprietà {@link readOnly}
	 * @param {boolean} mandatory valorizza la proprietà {@link mandatory}
	 * @memberof waControl
	 */
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.form = form;
		this.name = name;

		this.value = value;
		this.visible = visible * 1;
		this.readOnly = readOnly * 1;
		this.mandatory = mandatory * 1;
		this.obj = this.form.obj.elements[this.name];
		if (this.type == 'label' || this.type == 'frame')
			this.form.labels[this.name] = this;
		else
			this.form.controls[this.name] = this;
		
		this.selector = '#' + form.name + '_' + this.name;
		},
		
	//-------------------------------------------------------------------------
	/**
	* inserisce un valore applicativo nel controllo
	* 
	* @param {mixed} value valore da inserire nel controllo
	 * @memberof waControl
	*/
	set: function(value) 
		{
		this.obj.value = value;
		},
		
	//-------------------------------------------------------------------------
	/**
	* restituisce il  valore applicativo contenuto nel controllo
	 * @memberof waControl
	*/
	get: function() 
		{
		return this.obj.value;
		},
		
	//-------------------------------------------------------------------------
	/**
	* rende visible/invisible un controllo
	* 
	* @param {boolean} yesNo flag di visibilità
	* @param {boolean} labelToo flag che indica che il metodo deve agire 
	*	anche sull'eventuale label associata al controllo
	 * @memberof waControl
	*/
	show: function(yesNo, labelToo) 
		{
		this.visible = yesNo;
		this.render();
		if (labelToo && this.form.labels[this.name])
			this.form.labels[this.name].show(yesNo, false);
		},
		
	//-------------------------------------------------------------------------
	/**
	* abilita/disabilita un controllo
	* 
	* @param {boolean} yesNo flag di abilitazione
	* @param {boolean} labelToo flag che indica che il metodo deve agire 
	*	anche sull'eventuale label associata al controllo
	 * @memberof waControl
	*/
	enable: function(yesNo, labelToo) 
		{
		this.readOnly = !yesNo;
		this.render();
		if (labelToo && this.form.labels[this.name])
			this.form.labels[this.name].enable(yesNo, false);
		},
		
	//-------------------------------------------------------------------------
	/**
	 * rende obbligatorio/non obbligatorio un controllo
	* 
	* @param {boolean} yesNo flag di obbligatoreità
	* @param {boolean} labelToo flag che indica che il metodo deve agire 
	*	anche sull'eventuale label associata al controllo
	 * @memberof waControl
	 */
	setMandatory: function(yesNo, labelToo) 
		{
		this.mandatory = yesNo;
		this.render();
		if (labelToo && this.form.labels[this.name])
			this.form.labels[this.name].setMandatory(yesNo, false);
		},
		
	//-------------------------------------------------------------------------
	/**
	* a seconda dello stato /definisce la classe css di un controllo (visible, 
	* readOnly, mandatory) 
	 * @memberof waControl
	*/
	render: function() 
		{
		this.visible ? jQuery(this.selector).show() : jQuery(this.selector).hide();
		
		this.obj.disabled = this.readOnly;
		
		if (this.readOnly)
			jQuery(this.selector).addClass("disabled");
		else
			jQuery(this.selector).removeClass("disabled");
		if (this.mandatory)
			jQuery(this.selector).addClass("required");
		else
			jQuery(this.selector).removeClass("required");
		
		},
		
	//-------------------------------------------------------------------------
	/**
	 * verifica che un controllo obbligatoio sia stato valorizzato
	 * @return (boolean)
	 * @memberof waControl
	 * 
	 */
	mandatoryVerify: function() 
		{
		if (!this.mandatory)
			return true;
		return this.get() ? true : false;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * verifica che un controllo sia valorizzato correttamente
	 * @return (boolean)
	 * @memberof waControl
	 */
	formatVerify: function() 
		{
		return true;
		},
		
	//-------------------------------------------------------------------------
	// se la classe ha degli eventi predefiniti, una volta chiamato l'event
	// predefinito occorre innescare anche l'eventuale codice
	// definito dal programmatore dell'applicazione per l'event 
	applicationEvent: function(event, eventName, eventObj) 
		{
		if (this[eventName])
			{
			var obj = eventObj ? eventObj : this.obj;
			obj[eventName] = this[eventName];
			var result;
			if (typeof event == "undefined")
				result = obj[eventName]();
			else
				result = obj[eventName](event);
			obj[eventName] = this["my" + eventName];
			return result;
			}
		},
		
	//-------------------------------------------------------------------------
	// associa un event al controllo; se c'e' un event predefinito lo
	// parcheggia nell'apposita variabile
	addEvent: function (eventName, event)
		{
		if (this["my" + eventName])
			this[eventName] = event;
		else
			this.obj[eventName] = event;
		},
	
	//-------------------------------------------------------------------------
	/**
	 * verifica se un controllo fisico appartiene al controllo applicativo
	* 
	* @param {object} obj oggetto input HTML 
	 * @return (boolean)
	 * @memberof waControl
	 */
	isMine: function (obj)
		{
		return obj.name == this.obj.name;
		},
	
	//-------------------------------------------------------------------------
	/**
	 * trim (che non sempre c'e'....)
	 * @return (string)
	 * @memberof waControl
	 */
	trim: function(str) 
		{
		return str.replace(/(^\s*)|(\s*$)/g, "");
		},
		
	//-------------------------------------------------------------------------
	// simula un event sul controllo
	simulateEvent: function(eventType) 
		{
		var event;
		if (document.createEvent) 
			{
			event = document.createEvent("HTMLEvents");
			event.initEvent(eventType, true, true);
			this.obj.dispatchEvent(event);
			} 
		else 
			{
			event = document.createEventObject();
			event.eventType = eventType;
			this.obj.fireEvent("on" + event.eventType, event);
			}
		
		}
		
		
	}
);
