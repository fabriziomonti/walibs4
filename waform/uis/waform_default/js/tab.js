//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waTab: div HTML con bordi e un'etichetta che simula un box 
* all'interno del quale possono essere raggruppati (graficamente e basta!)
* diversi controlli
* 
* @class waTab
* @extends waControl
*/
var waTab = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'tab',
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.obj = document.getElementById(this.form.name + "_"+ this.name);
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(value) 
		{
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	get: function() 
		{
		},
		
	//-------------------------------------------------------------------------
	// a seconda dello stato definisce la classe css di un controllo
	render: function() 
		{
		this.visible ? jQuery("#" + this.obj.id).show() : jQuery("#" + this.obj.id).hide();
		if (this.readOnly)
			{
			this.obj.className += " waform_disabled";
			}
		else
			{
			this.obj.className = this.obj.className.replace("waform_disabled", "");
			}
		if (this.mandatory)
			{
			this.obj.className += " waform_mandatory";
			}
		else
			{
			this.obj.className = this.obj.className.replace("waform_mandatory", "");
			}
		}
			
	}
);
