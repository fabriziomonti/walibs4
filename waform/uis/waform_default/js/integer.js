//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
 * classe waInteger: input HTML destinato a contenere un numero intero
 * 
 * @class waInteger
 * @extends waControl
 */

var waInteger = new Class({
    //-------------------------------------------------------------------------
    // extends
    Extends: waControl,

    //-------------------------------------------------------------------------
    // proprieta'
    type: 'integer',
    signed: false,
    
    myonkeyup: function onkeyup(event) {this.form.waForm.controls[this.name].onKeyUp(event);},
    onkeyup: false,

    //-------------------------------------------------------------------------
    //initialization
    initialize: function (form, name, value, visible, readOnly, mandatory) {
        // definizione iniziale delle proprieta'
        this.parent(form, name, value, visible, readOnly, mandatory);
        this.signed = this.obj.signed ? this.obj.signed : this.signed;
        this.obj.onkeyup = this.myonkeyup;

    },

    //-------------------------------------------------------------------------
    onKeyUp: function (event) {
        if (this.signed) {
            return this.onSignedKeyUp(event);
        }

        var re = /^[0-9]*$/;
        if (!re.test(this.obj.value)) {
            this.obj.value = this.obj.value.replace(/[^0-9]/g, "");
        }
        this.applicationEvent(event, "onkeyup");
    },

    //-------------------------------------------------------------------------
    onSignedKeyUp: function (event) {

        var toTest = this.obj.value;
        var sign = "";
        var testSign = toTest.substring(0, 1);
        if (testSign === '+' || testSign === '-') {
            sign = testSign;
            toTest = toTest.substring(1);
        }

        var re = /^[0-9]*$/;
        if (!re.test(toTest)) {
            toTest = toTest.replace(/[^0-9]/g, "");
        }
        this.obj.value = sign + toTest;
        this.applicationEvent(event, "onkeyup");
    },

    //-------------------------------------------------------------------------
    mandatoryVerify: function () {
        if (!this.mandatory) {
            return true;
        }
        return this.trim(this.obj.value) != '';
    },

    //-------------------------------------------------------------------------
    formatVerify: function () {
        if ((this.obj.value * 2 / 2) == (this.obj.value + '')) {
            return true;
        }
        return false;
    }

});
