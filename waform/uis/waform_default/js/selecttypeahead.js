//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waSelectTypeahead: select con autocompletion ajax
* 
* @class waSelectTypeahead
* @extends waControl
*/
var waSelectTypeahead = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type			: 'selecttypeahead',
	
	idObj			: null,
	idSelector		: "",
	myonchange		: function (event) {void(0)},
	
	firstCallToOnchange	: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
			
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		
		this.idObj = this.obj;
		this.idSelector = this.selector;
		this.obj = this.form.obj.elements[this.name + "_typeahead"];
		this.selector += "_typeahead";
		this.options = [];
		jQuery(this.selector).typeahead
			(
				{
				source: function (pattern, callback) {document[form.name].controls[name].queryRPC(pattern, callback)}, 
				autoSelect: true,
				items: 10,
				delay:	300,
				minLength: 0,
				afterSelect: function (item)
					{
					document[form.name].controls[name].afterSelect(item);
					}
				}
			); 		
	
		jQuery(this.selector).off("change").on("change", function(event) {document[form.name].controls[name].onChange(event)});
		jQuery(this.selector).off("dblclick").on("dblclick", function(event) {document[form.name].controls[name].onDblclick(event)});
		},
		
	//-------------------------------------------------------------------------
	set: function(idValue, nameValue) 
		{
		this.idObj.value = idValue;
		this.obj.value = nameValue;
		},
		
	//-------------------------------------------------------------------------
	get: function() 
		{
		return this.idObj.value * 1;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * l'evento change viene chiamato:
	 * - se viene selezionato un elemento (prima che venga triggerato l'evento 
	 *		afterSelect della classe typeahead)
	 *	- alla normale change del controllo di testo, anche se non corrisponde
	 *		a un elemento selezionabile
	 *		
	 *	per questo motivo ritardiamo la chiamata a una eventuale onchange
	 *	applicativa, in modo che possa recepoire eventuali veri cambiamenti 
	 *	apportati da typeahead
	 *	
	 * @param {type} event
	 * @ignore
	 */
	onChange: function(event) 
		{
		this.idObj.value = "";
		var formName = this.form.name;
		var ctrlName = this.name;
		this.firstCallToOnchange = true;
		setTimeout
			(
			function() 
				{
				document[formName].controls[ctrlName].onChangeAfterSelect(event);
				}, 
			100
			);
		},
		
	//-------------------------------------------------------------------------
	onChangeAfterSelect: function(event) 
		{
		// sa dio per quale strana rientranza, l'evento applicativo viene 
		// chaimato due volte quando c'è un elemento selezionato, quindi usiamo
		// un flag per sincronizzarci
		if (!this.firstCallToOnchange)
			{
			return;
			}
		this.firstCallToOnchange = false;
		
		if (this.obj.value !== "" && this.idObj.value === "")
			{
			// se abbiamo un testo ma non un valore, proviamo a vedere 
			// se il testo è uguale a una delle opzioni
			for (var i = 0; i < this.options.length; i++)
				{
				if (this.options[i].name.toLowerCase() === this.obj.value.toLowerCase())
					{
					// il testo è uguale a una delle opzioni: valoriziamo il
					// valore con l'id corrispondente al testo
					this.idObj.value = this.options[i].id;
					this.obj.value = this.options[i].name;
					break;
					}
				}
			}

		if (this.idObj.value === "")
			{
			// se il valore è vuoto, allora svuotiamo anche il testo
			this.obj.value = "";
			}

		// chiamiamo un eventuale evento applicativo associato al controllo
		this.applicationEvent(event, "onchange");
			
		},
		
	//-------------------------------------------------------------------------
	afterSelect: function(item) 
		{
		this.idObj.value = item.id;
		},
		
	//-------------------------------------------------------------------------
	onDblclick: function(event) 
		{
		jQuery(this.selector).typeahead('lookup').focus();			
		},
		
	//-------------------------------------------------------------------------
	queryRPC: function(pattern, callback) 
		{
		jQuery("#" + this.form.obj.id).spin();			

		// oltre al pattern, passiamo tutti i valori contenuti nel form
		var params = {};
		for (var li in this.form.controls)
			{
			if (this.form.controls[li].type != 'button')
				params[li] = this.form.controls[li].get();
			}
			
		var data = 
			[
			"waform_rpc_inner_control_function",
			this.name,
			pattern,
			JSON.stringify(params)
			];
		var toPost = {};
		toPost.waform_action = 'WAFORM_ACTION_RPC';
		toPost.waform_form_name = this.form.name;
		toPost.waform_rpc_function = "rpcReload";
		toPost.waform_rpc_data = JSON.stringify(data);
			
		var proxy = this;
		jQuery.ajax
			(
				{
				url			: document.location.href,
				type		: 'POST',
				dataType	: 'json',
				data		: toPost,
				timeout		: 20000,
				success		: function (response, textStatus, jqXHR) 
								{
								jQuery("#" + proxy.form.obj.id).spinStop();			
								proxy.gotQueryRPCResponse(response, callback);
								},
				error		: function(jqXHR, textStatus, errorThrown)
								{
								jQuery("#" + proxy.form.obj.id).spinStop();			
								return proxy.form.showRPCError("System error during RPC:\n" + textStatus + " - " + errorThrown);
								},
				}
			);
	
		},
		
	//-------------------------------------------------------------------------
	render: function() 
		{
		this.parent();
		this.idObj.disabled = this.readOnly;
		},
		
	//-------------------------------------------------------------------------
	gotQueryRPCResponse: function(response, callback) 
		{
		if (response.waform_rpc_response != "WAFORM_RPC_OK")
			{
			return this.form.showRPCError("Application server error during RPC:\n" + response.waform_rpc_message);
			}
			
		callback(response.waform_rpc_data);
		}
		
		
	}
);
