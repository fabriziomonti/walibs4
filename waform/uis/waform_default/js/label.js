//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waLabel: label HTML tipicamente associata a un controllo di
* input col medesimo nome
* 
* @class waLabel
* @extends waControl
*/
var waLabel = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'label',
	helpLink: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.obj = document.getElementById("lbl_" + this.form.name + "_"+ this.name);
		this.selector = "#lbl_" + this.form.name + "_"+ this.name;
		
		this.helpLink = document.getElementById("hlplink_" + this.form.name + "_"+ this.name);
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(valore) 
		{
		if (this.helpLink)
			this.helpLink.innerHTML = valore;
		else
			this.obj.innerHTML = valore;
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	get: function() 
		{
		if (this.helpLink)
			return this.helpLink.innerHTML;
		return this.obj.innerHTML;
		}
		
	
	}
);
