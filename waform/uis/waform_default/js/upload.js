//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waUpload: controllo composito per la gestione di caricamento,
* visualizzazione, eliminazione di un file
* 
* @class waUpload
* @extends waControl
*/
var waUpload = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'upload',
	
	/**
	 * oggetto HTML fisico (checkbox) utilizzato per com unicare al server che è 
	 * stata richiesta l'eliminazione del file
	 * @ignore
	 * @type HTML_object
	 */
	deleteBoolean: null,
	
	/**
	 * oggetto HTML fisico (div) che contiene il tutto
	 * @type HTML_object
	 * @ignore
	 */
	container: null,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.deleteBoolean = this.form.obj.elements["waform_booleandeletefile_" + this.form.name + "_" + this.name];
		this.container = document.getElementById("waform_upload_container_" + this.form.name + "_" + this.name);
			
		jQuery(this.selector).fileinput(this.getConfigOptions());		
		
		var proxy = this;
		jQuery(this.selector).on
			(
			'fileclear', 
			function(event) 
				{
				proxy.deleteBoolean.checked = true;
				}
			);		
		
		jQuery(this.selector).on
			(
			'fileloaded', 
			function(event) 
				{
				proxy.deleteBoolean.checked = false;
				}
			);		
		
		},
		
	//-------------------------------------------------------------------------
	/**
	 * @ignore
	*/
	getConfigOptions: function() 
		{
		var options = 
			{
			language: this.form.language,
			showCaption: false,
			showUpload: false,
			showRemove: !this.readOnly,
			showBrowse: !this.readOnly
			};
			
		if (this.obj.showPage)
			{
			var ext = this.value.substr(-4).toLowerCase();
			if (ext == ".jpg" || ext == "jpeg" || ext == ".png" || ext == ".gif")
				options.initialPreview = ["<img src='" + this.obj.showPage + "'>"];
			else
				options.initialPreview =	[	"<div class='file-preview-text' onclick='var w=window.open(\"" + this.obj.showPage + "\")'>" +
													"<h2><i class='glyphicon glyphicon-file'></i></h2>" +
													this.value + 
												"</div>"
											];
				
			options.initialPreviewConfig =	[
												{
												caption: this.value,
												showDrag: false,
												size: parseFloat(this.obj.fileSize)
												}
											];
			};

		return options;
		},
		
	//-------------------------------------------------------------------------
	/**
	* a seconda dello stato /definisce la classe css di un controllo(visible, 
	* readOnly, mandatory) renderizza il controllo
	 * @ignore
	*/
	render: function() 
		{
		this.visible ? jQuery("#" + this.container.id).show() : jQuery("#" + this.container.id).hide();
		this.obj.disabled = this.readOnly;
		var containerSelector = "#" + this.container.id;
		jQuery(containerSelector + " button.fileinput-remove-button").prop("disabled", this.readOnly);
		
		if (this.readOnly)
			jQuery(containerSelector).addClass("disabled");
		else
			jQuery(containerSelector).removeClass("disabled");
		if (this.mandatory)
			jQuery(containerSelector).addClass("required");
		else
			jQuery(containerSelector).removeClass("required");
		
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(value) 
		{
		}
		

	
	
	}
);
