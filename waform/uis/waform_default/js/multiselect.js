//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waMultiSelect: select con possibilità di selezioni multiple
* 
* @class waMultiSelect
* @extends waSelect
*/
var waMultiSelect = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waSelect,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'multiselect',
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		// in questo caso l'obj è il div che contiene i checkbox
		this.obj = document.getElementById(form.name + "_" + name);
		
		},
	
	//-------------------------------------------------------------------------
	get: function() 
		{
		var retval = new Array();
		var selCntr = 0;
		
		for (var i= 0; i < this.form.obj.elements.length; i++)
			{
			if (this.form.obj.elements[i].name.substr(0, this.name.length + 1) == this.name + "[" &&
				this.form.obj.elements[i].checked)
				{
				retval[selCntr] = this.form.obj.elements[i].name.slice(this.name.length + 1, -1);
				selCntr++;
				}
			}

		return retval;
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(value) 
		{
		for (var i= 0; i < this.form.obj.elements.length; i++)
			{
			if (this.form.obj.elements[i].name.substr(0, this.name.length + 1) == this.name + "[")
				this.form.obj.elements[i].checked = false;
			}
		for (var c = 0; c < value.length; c++)
			{
			if (this.form.obj.elements[this.name + "[" + value[c] + "]"])
				this.form.obj.elements[this.name + "[" + value[c] + "]"].checked = true;
			}
		},
		
	//-------------------------------------------------------------------------
	empty: function() 
		{
		this.obj.innerHTML = '';
		},
		
	//-------------------------------------------------------------------------
	fill: function(values, toSelect) 
		{
		for (var key in values)
			{
			if (typeof values[key] != 'undefined')
				{
				this.obj.innerHTML += "<div>\n" +
										"\t<input type='checkbox'" +
											" name='" + this.name + "[" + key + "]'" +
											" id='" + this.form.name + "_" + this.name + key + "'" + 
											(values[key] == toSelect ? " checked='checked'" : "") + " />\n" + 
										"\t<div>\n" + 
										"\t\t<label for='" + this.form.name + "_" + this.name + key + "'>" + values[key] + "</label>\n" + 
										"\t</div>\n" + 
										"</div>\n";
				}
			}

		},
		
	//-------------------------------------------------------------------------
	mandatoryVerify: function() 
		{
		if (!this.mandatory)
			return true;
		return this.get().length > 0;
		},
		
	
	//-------------------------------------------------------------------------
	render: function() 
		{
		this.parent();
		
		for (var i= 0; i < this.form.obj.elements.length; i++)
			{
			if (this.form.obj.elements[i].name.substr(0, this.name.length + 1) == this.name + "[")
				this.form.obj.elements[i].disabled = this.readOnly;
			}
			
		if (this.readOnly)
			jQuery(this.selector + " label").addClass("disabled");
		else
			jQuery(this.selector + " label").removeClass("disabled");
		
		},
		
	//-------------------------------------------------------------------------
	// verifica se un controllo fisico appartiene al controllo logico
	isMine: function (obj)
		{
		return obj.name.substr(0, this.name.length + 1) == this.name + "[";
		},
	
	//-------------------------------------------------------------------------
	// associa un event al controllo
	addEvent: function (eventName, event)
		{
		for (var i= 0; i < this.form.obj.elements.length; i++)
			{
			if (this.form.obj.elements[i].name.substr(0, this.name.length + 1) == this.name + "[")
				this.form.obj.elements[i][eventName] = event;
			}
		},
	
	//-------------------------------------------------------------------------
	// verifica se un controllo fisico appartiene al controllo logico
	enableItem: function (itemId, isEnabled)
		{
		if (this.readOnly)
			{
			return;
			}
			
		var selector = this.selector + itemId;
		if (isEnabled)
			{
			jQuery(selector).removeClass("disabled");
			jQuery(selector).attr("disabled", false);
			jQuery("label[for='" + selector.substr(1) + "']").removeClass("disabled");
			}
		else
			{
			jQuery(selector).attr('checked', false);
			jQuery(selector).addClass("disabled");
			jQuery(selector).attr("disabled", true);
			jQuery("label[for='" + selector.substr(1) + "']").addClass("disabled");
			}
			
		}
	
	}
);
