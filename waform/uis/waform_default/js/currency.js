//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waCurrency: controllo per l'input di numero decimale
* 
* @class waCurrency
* @extends waControl
*/
var waCurrency = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type		: 'currency',
	decimalNr	: 2,
	myonkeyup	: function onkeyup(event) {this.form.waForm.controls[this.name].onKeyUp(event);},
	myonfocus	: function onfocus(event) {this.form.waForm.controls[this.name].onFocus(event);},
	myonblur	: function onblur(event) {this.form.waForm.controls[this.name].onBlur(event);},
	onkeyup		: false,
	onfocus		: false,
	onblur		: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.decimalNr = this.obj.decimalNr ? this.obj.decimalNr : this.decimalNr;
		this.obj.onkeyup = this.myonkeyup;
		this.obj.onfocus = this.myonfocus;
		this.obj.onblur = this.myonblur;
		
		},
		
	//-------------------------------------------------------------------------
	onKeyUp: function(event) 
		{
		this.obj.value = this.obj.value.replace(/\./g,",");
		var re = /^[0-9-',']*$/;
		if (!re.test(this.obj.value)) 
			this.obj.value = this.obj.value.replace(/[^0-9-',']/g,"");	
		var elems = this.obj.value.split(",");
		if (elems.length > 2)
			{
			this.obj.value = elems[0] + "," + elems[1];
			for (var i = 2; i < elems.length; i++)
				this.obj.value += elems[i];
			}
		this.applicationEvent(event, "onkeyup");
			
		},
		
	//-------------------------------------------------------------------------
	onFocus: function(event) 
		{
		this.unformat();
		this.applicationEvent(event, "onfocus");
		},
		
	//-------------------------------------------------------------------------
	onBlur: function(event) 
		{
		this.format();
		this.applicationEvent(event, "onblur");
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	get: function() 
		{
		var val = this.obj.value;
		val = val.replace(/\./g,"");
		val = val.replace(/\,/g,".");
		return this.roundFloat(val);
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(value) 
		{
		this.obj.value = this.roundFloat(value);
		this.format();
		},
		
	//-------------------------------------------------------------------------
	roundFloat: function (nr)
		{
		var exp = Math.pow(10, this.decimalNr);
		return Math.round(nr * exp) / exp;
		},
		
	//-------------------------------------------------------------------------
	// formatta il valore nel controllo
	format: function() 
		{
		if (this.obj.value == '')
			return;
			
		var elems = new Array();
		if (this.obj.value.indexOf(",") >= 0)
			elems = this.obj.value.split(",");
		else if (this.obj.value.indexOf(".") >= 0)
			elems = this.obj.value.split(".");
		else
			elems[0] = this.obj.value;
		var interi = elems[0] == '' ? 0 : elems[0];
		var decimali = elems[1] ? elems[1] : '';
		for (i = interi.length - 3; i > 0; i -= 3)
			interi = interi.substring (0 , i) + "." + interi.substring (i);
	    if (decimali.length > this.decimalNr)
	    	decimali = decimali.substr(0, this.decimalNr);
		for (i = decimali.length; i < this.decimalNr; i++)
	      decimali += "0";
		this.obj.value = interi + "," + decimali;
                
            // il segno meno incasina in qualche caso
            if (this.obj.value.indexOf("-.") === 0) {
                this.obj.value = "-" + this.obj.value.substring(2);
            }
                
                
		},
		
	//-------------------------------------------------------------------------
	// deformatta il valore nel controllo
	unformat: function() 
		{
		this.obj.value = this.obj.value.replace(/\./g,"");
		},
		
	//-------------------------------------------------------------------------
	mandatoryVerify: function() 
		{
		if (!this.mandatory)
			return true;
		return this.trim(this.obj.value) != '';
		}
		
	}
);




	