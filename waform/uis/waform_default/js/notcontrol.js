//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waNotControl: elemento arbitrario all'interno di un modulo
* 
* @class waNotControl
* @extends waControl
*/
var waNotControl = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'notcontrol',
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.obj = document.getElementById(this.form.name + "_" + name);
		this.obj.name = this.form.name + "_" + name;
		
		},
		
	//-------------------------------------------------------------------------
	set: function(value) 
		{
		this.obj.innerHTML = value;
		},
		
	//-------------------------------------------------------------------------
	get: function() 
		{
		return this.obj.innerHTML;
		},
		
	//-------------------------------------------------------------------------
	render: function() 
		{
		this.parent();
		this.obj.className = "waform_notcontrol" + (this.mandatory ? " waform_mandatory" : '');
		}
		
	
	
	}
);
