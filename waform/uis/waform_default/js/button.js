//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waButton: contiene attributi e metodi relativi a un bottone HTML
* 
* 
* @class waButton
* @extends waControl
*/
var waButton = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type		: 'button',
	
	/**
	 * flag che indica che la pressione di questo bottone richiede abort
	 * della sessione di editing sul modulo
	 * @type boolean
	 * @memberof waButton
	 */
	cancel		: false,
	
	/**
	 * flag che indica che la pressione di questo button richiede al server
	 * l'deletezione del record attualmente in editing
	 * @memberof waButton
	 * @type boolean
	 */
	delete		: false,
	
	/**
	 * flag che indica che la pressione di questo button richiede al server
	 * il submit del record attualmente in editing
	 * @memberof waButton
	 * @type boolean
	 */
	submit		: false,	
	
	myonclick	: function onclick(event) {this.form.waForm.controls[this.name].onClick(event);},
	onclick		: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		if (!this.obj)
			// pugnuetta di safari coi button....
			this.obj = this.form.obj.children(this.name);
			
		this.cancel = this.obj.cancel ? this.obj.cancel : 0;
		this.delete = this.obj.delete ? this.obj.delete : 0;
		this.submit = this.obj.submit ? this.obj.submit : 0;
		this.obj.onclick = this.myonclick;
			
		},
		
	//-------------------------------------------------------------------------
	/**
	* inserisce un valore applicativo nel controllo
	* 
	* @param {mixed} value valore da inserire nel controllo
	 * @memberof waControl
	*/
	set: function(value) 
		{
		this.parent(value);
		jQuery(this.selector).text(value);
		},
		
	//-------------------------------------------------------------------------
	onClick: function(event) 
		{
		if (this.cancel)
			{
			this.form.cancel();
			}
		else if (this.delete)
			{
			this.form.delete();
			}
		else if (this.submit)
			{
			this.form.submit();
			}

		this.applicationEvent(event, "onclick");
		}
		
	
	}
);
