//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waCaptcha: controllo di input di un codice di controllo captcha
* 
* @class waCaptcha
* @extends waControl
*/
var waCaptcha = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type	: 'captcha',
	imgObj	: null,		// e' l'immagine contenent la chiave
		
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.imgObj = document.getElementById(this.form.name + "_captcha_img_" + this.name);
		
		},
		
	//-------------------------------------------------------------------------
	// a seconda dello stato definisce la classe css di un controllo
	render: function() 
		{
		// oggetto principale
		this.parent();

		// immagine
		this.visible ? jQuery("#" + this.imgObj.id).show() : jQuery("#" + this.imgObj.id).hide();
		}
		

	}
);
