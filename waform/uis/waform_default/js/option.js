//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waOption: controllo di scelta mediante radio button
* 
* @class waOption
* @extends waControl
*/

var waOption = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'option',
	
	container	: null,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.container = document.getElementById("waform_radio_container_" + form.name + "_" + name);
		
		this.valueBeforeShow = value;
		var proxy = this;
		jQuery(this.pickerSelector).on("dp.show", function(event) {proxy.onWidgetShow(event);});
		jQuery(this.pickerSelector).on("dp.hide", function(event) {proxy.onWidgetHide(event);});
		this.myonchange = function(event) {proxy.onChange(event);};
		this.obj.onchange = this.myonchange;
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	get: function() 
		{
		for (var i = 0; i < this.obj.length; i++)
			{
			if (this.obj[i].checked)
				return this.obj[i].value;
			}
		return false;
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(value) 
		{
		for (var i = 0; i < this.obj.length; i++)
			this.obj[i].checked = this.obj[i].value == value;
		},
		
	//-------------------------------------------------------------------------
	// associa un event al controllo
	addEvent: function (eventName, event)
		{
		for (var i = 0; i < this.obj.length; i++)
			this.obj[i][eventName] = event;
		},
	
	//-------------------------------------------------------------------------
	// verifica se un controllo fisico appartiene al controllo logico
	isMine: function (obj)
		{
		for (var i = 0; i < this.obj.length; i++)
			return obj.name == this.obj[i].name;
		},
	
	//-------------------------------------------------------------------------
	// a seconda dello stato definisce la classe css di un controllo
	render: function() 
		{
			
		this.visible ? jQuery("#" + this.container.id).show() : jQuery("#" + this.container.id).hide();
			
		for (var i = 0; i < this.obj.length; i++)
			this.obj[i].disabled = this.readOnly;
			
		var className = (this.mandatory ? "waform_mandatory" : '');
		for (var i = 0; i < this.obj.length; i++)
			this.obj[i].className = className;
								
		}
	
	}
);
