//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waMultiSelect: select con possibilità di selezioni multiple
* 
* @class waMultiSelect
* @extends waSelect
*/
var waMultiSelect = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waSelect,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'multiselect',
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.obj = this.form.obj.elements[this.name + "[]"];
		
		},
	
	//-------------------------------------------------------------------------
	get: function() 
		{
		var retval = new Array();
		var selCntr = 0;
		for (var i= 0; i < this.obj.options.length; i++)
			{
			if (this.obj.options[i].selected)
				{
				retval[selCntr] = this.obj.options[i].value;
				selCntr++;
				}
			}

		return retval;
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(value) 
		{
		for (var i= 0; i < this.obj.options.length; i++)
			this.obj.options[i].selected = false;
		for (var c = 0; c < value.length; c++)
			{
			for (i= 0; i < this.obj.options.length; i++)
				this.obj.options[i].selected = this.obj.options[i].value == value[c];
			}
		},
		
	//-------------------------------------------------------------------------
	mandatoryVerify: function() 
		{
		if (!this.mandatory)
			return true;
		return this.get().length > 0;
		}
		
	
	}
);
