//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
 * classe che gestisce le proprietà e i metodi di un modulo waLibs.
 * 
 * 
* La struttura di questa classe prevede che:
*
* <ul>
* <li>
*	per ogni modulo venga creato un oggetto di classe waForm con nome 
* 	uguale a quello dell modulo (proprietà waModulo::nome in PHP) e aggiunto 
* 	come nuova proprietà dell'oggetto document. Questa operazione è svolta
* 	automaticamente dal view-object
*	<li>
*	Il view-object non associa alcun evento ad alcun controllo (se non quello
*	dei bottoni di submit implicitamente previsto dall'HTML). Sarà compito della
*	applicazione creare le funzioni di gestione degli eventi e associarli ai
*	controlli contenuti nel modulo. La UI di default di waApplication
*	contiene già al suo interno un meccanismo automatico per fare ciò.
* </ul>
* 
* @class waForm
 */
var waForm = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	
	/**
	 * nome del modulo
	 * @type string
	 * @memberof waForm
	 */
	name				: '',
	
	/**
	 * flag che deve essere valorizzato dai bottoni di submit quando si desidera 
	 * eliminare un record (bottoni con proprietà {@link waButton.delete} = 
	 * true); viene invocato il submit ma non vengono eseguiti 
	 * controlli formali o di obbligatorietà
	 * @type boolean
	 * @deprecated 
	 * @memberof waForm
	 */
	submitTypeDelete	: false,	
	
	/**
	 * flag che deve essere valorizzato dai bottoni quando si desidera 
	 * abortire la sessione di editing (bottoni con proprietà {@link waButton.delete} = 
	 * true); non vengono eseguiti controlli formali o di obbligatorietà
	 * @type boolean
	 * @deprecated 
	 * @memberof waForm
	 */
	submitTypeCancel	: false,	

	/**
	* lingua utilizzata
	*
	 * @type string
	 * @memberof waForm
	*/	
	language : 'it',
	
	/**
	* locale utilizzato
	*
	 * @type string
	 * @memberof waForm
	*/	
	locale : 'it-IT',

	/**
	 * dizionario dei controlli di input (no labels, no cornici) contenuti nel 
	 * modulo.
	 * 
	 * La chiave di ogni elemento è il nome del controllo.
	 * 
	 * @type object
	 * @memberof waForm
	 */
	controls			: {},

	/**
	 * dizionario dei controlli di tipo etichetta o cornice contenuti nel modulo.
	 * 
	 * La chiave di ogni elemento è il nome dell'etichetta.
	 * 
	 * @type object
	 * @memberof waForm
	 */
	labels			: {},

	/**
	 * oggetto form HTML fisico a cui fa riferimento il modulo
	 * 
	 * @type object
	 * @memberof waForm
	 */
	obj					: null,
	
//	applicazione		: null,		// eventuale istanza dell'applicazione (e' lei stessa a valorizzare questa proprieta')
	http_request 		: false,
	rpc_error 			: 'waform_rpc_error',
	
	//-------------------------------------------------------------------------
	// implements

	//-------------------------------------------------------------------------
	/**
	 * inizializzazione (costruttore)
	 * 
	 * @param {string} name valorizza la proprietà {@link name}
	 * @memberof waForm
	 */
	initialize: function(name) 
		{
		this.name = name;
		this.obj = document.getElementById(this.name);
		this.obj.waForm = this;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * restituisce l'array dei campi che violano il requisito di obbligatorietà
	 *  
	 * @return {array} ogni elemento dell'array contiene, se disponibile, la label 
	 *	del controllo; altrimenti il nome del controllo
	 * @memberof waForm
	 */
	getMandatoryViolations: function() 
		{
		var retval = [];
		for (var controlName in this.controls)
			{
			if (!this.controls[controlName].mandatoryVerify())
				{
				retval.push(this.labels[controlName] ? this.labels[controlName].get() : controlName);
				}
			}
			
		return retval;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * verifica che tutti i controlli siano stati valorizzati con valori
	 * compatibili col tipo del controllo e restituisce l'array dei campi che 
	 * violano il requisito
	 *  
	 * @return {array} ogni elemento dell'array contiene, se disponibile, la label 
	 *	del controllo; altrimenti il nome del controllo
	 * @memberof waForm
	 */
	getFormatViolations: function() 
		{
		var retval = [];
		for (var controlName in this.controls)
			{
			if (!this.controls[controlName].formatVerify())
				{
				retval.push(this.labels[controlName] ? this.labels[controlName].get() : controlName);
				}
			}
			
		return retval;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * richiesta di cancel
	 * 
	 * @memberof waForm
	 */
	cancel: function() 
		{
		this.obj.waform_action.value = 'WAFORM_ACTION_CANCEL';
		this.obj.submit();
		},
		
	//-------------------------------------------------------------------------
	/**
	 * richiesta di delete
	 * 
	 * @memberof waForm
	 */
	delete: function() 
		{
		var proxy = this;
		this.confirm("Confirm deletion?", function() {proxy.gotDeleteConfirm();});
		},
		
	//-------------------------------------------------------------------------
	/**
	 * conferma di delete
	 * 
	 * @ignore
	 * @memberof waForm
	 */
	gotDeleteConfirm: function() 
		{
		this.obj.waform_action.value = 'WAFORM_ACTION_DELETE';
		this.obj.submit();
		},
		
	//-------------------------------------------------------------------------
	/**
	 * submit del modulo
	 * 
	 * @memberof waForm
	 */
	submit: function() 
		{
		var violations = this.getFormatViolations();
		if (violations.length)
			{
			var msg = '';
			for (var i = 0; i < violations.length; i++)
				{
				msg += "Invalid value in " +  violations[i] + ".\n";
				}

			this.alert(msg);
			return false;
			}
			
		violations = this.getMandatoryViolations();
		if (violations.length)
			{
			var msg = '';
			for (var i = 0; i < violations.length; i++)
				{
				msg += violations[i] + " is required.\n";
				}
			this.alert(msg);
			return false;
			}
			
		var proxy = this;
		this.confirm("Confirm submission?", function() {proxy.gotSubmitConfirm();});
		},
		
	//-------------------------------------------------------------------------
	/**
	 * conferma di submit
	 * 
	 * @ignore
	 * @memberof waForm
	 */
	gotSubmitConfirm: function() 
		{
		this.obj.waform_action.value = 'WAFORM_ACTION_EDIT';
		this.obj.submit();
		},
		
	//-------------------------------------------------------------------------
	/**
	 * placeholder per ridefinizione di alert asincrona
	 * 
	 * @param {string} msg testo del messaggio
	 * @memberof waForm
	 */ 
	alert: function(msg) 
		{
		alert(msg);
		},
		
	//-------------------------------------------------------------------------
	/**
	 * placeholder per ridefinizione di confirm asincrona
	 * 
	 * @param {string} msg testo del messaggio
	 * @param {function} callback - viene richiamata solo a fronte di confirm
	 * @param {object} callbackOptions opzioni da passare a callback
	 * @memberof waForm
	 */ 
	confirm: function(msg, callback, callbackOptions) 
		{
		if (confirm(msg))
			{
			callback(callbackOptions);
			}
		},
		
	//-------------------------------------------------------------------------
	// mostra un eventuale errore durante rpc
	showRPCError: function(msg) 
		{
		this.alert(msg);
		return this.rpc_error;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * effettua una chiamata RPC verso il server.
	 * 
	* gli argomenti da passare al metodo sono posizionali 
	* <ol>
	* <li>nome funzione/metodo PHP da richiamare lato server
	* <li>parametro 1 da passare alla funzione/metodo
	* <li>parametro 2 da passare alla funzione/metodo
	* <li>parametro n da passare alla funzione/metodo...
	* </ol>
	* 
	* torna ciò che ritorna il server
	 * @memberof waForm
	 */
	RPC: function() 
		{
		jQuery("#" + this.name).spin(null, "modal");			
			
		var toPost = {};
		toPost.waform_action = 'WAFORM_ACTION_RPC';
		toPost.waform_form_name = this.name;
		toPost.waform_rpc_function = arguments[0];
		var data = [];
		for (var i = 1; i < arguments.length; i++)
			data[data.length] = arguments[i];
		toPost.waform_rpc_data = JSON.stringify(data);
			
		var proxy = this;
		var esito = null;
		jQuery.ajax
			(
				{
				url			: document.location.href,
				type		: 'POST',
				dataType	: 'json',
				data		: toPost,
				async		: false,
				timeout		: 20000,
				success		: function (response, textStatus, jqXHR) 
								{
								jQuery("#" + proxy.name).spinStop();
								if (response.waform_rpc_response != "WAFORM_RPC_OK")
									{
									return proxy.showRPCError("Application server error during RPC:\n" + response.waform_rpc_message);
									}
								esito = response.waform_rpc_data;
								},
				error		: function(jqXHR, textStatus, errorThrown)
								{
								jQuery("#" + proxy.name).spinStop();
								return proxy.showRPCError("System error during RPC:\n" + textStatus + " - " + errorThrown);
								},
				}
			);
	
		return esito;
		}
	}
			
);
