//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waDate: controllo l'input di una data
* 
* @class waDate
* @extends waControl
*/
var waDate = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type			: 'date',
	pickerSelector	: '',
	valueBeforeShow	: null,
	myonchange		: function (event) {void(0);},
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		this.pickerSelector = '#' + form.name + '_' + this.name + '_datetimepicker';
		
		this.valueBeforeShow = value;
		var proxy = this;
		jQuery(this.pickerSelector).on("dp.show", function(event) {proxy.onWidgetShow(event);});
		jQuery(this.pickerSelector).on("dp.hide", function(event) {proxy.onWidgetHide(event);});
		this.myonchange = function(event) {proxy.onChange(event);};
		this.obj.onchange = this.myonchange;
		},
		
	//-------------------------------------------------------------------------
	onWidgetShow: function(event) 
		{
		// se il campo non è inizializzato la show lo inizializza 
		// automaticamente, quindi poi non possiamo identificare il cambiamento
		this.valueBeforeShow = this.valueBeforeShow === "" ? "" : this.obj.value;
		},	
		
	//-------------------------------------------------------------------------
	// al momento della chiusura del widget, se il valore è cambiato, 
	// triggeriamo change
	onWidgetHide: function(event) 
		{
		if (this.obj.value === this.valueBeforeShow)
			return;
			
		this.valueBeforeShow = this.obj.value;
		this.simulateEvent("change");
		},	
		
	//-------------------------------------------------------------------------
	onChange: function(event) 
		{
		this.valueBeforeShow = this.obj.value === "" ? "" : this.valueBeforeShow;
		// occorre lasciare il tempo al dp di valorizzare correttamente il campo
		var proxy = this;
		setTimeout(function() {proxy.applicationEvent(event, "onchange");}, 20);
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	get: function() 
		{
		var retval = false;
		
		try 
			{
			retval = jQuery(this.pickerSelector).data("DateTimePicker").date().toDate();		
			}
		catch (e)
			{
			}
			
		return retval;
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	set: function(date) 
		{
		jQuery(this.pickerSelector).data("DateTimePicker").date(date);
		},
		
	//-------------------------------------------------------------------------
	render: function() 
		{
		this.parent();
		this.visible ? jQuery(this.pickerSelector).show() : jQuery(this.pickerSelector).hide();
		},
		
		
	}
);
