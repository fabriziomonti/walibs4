<?php 
namespace waLibs\views\waForm\waForm_default;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waDateTimeView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlDateTime $data)
		{
		parent::transform($data);
		$control_id = $this->form->name . "_" . $this->name . "_datetimepicker";
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group date col-xs-12 col-sm-4 col-md-3 col-lg-3' id='<?=$control_id?>'>
				<input 
					type='text' 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: center; <?=$this->getControlStyle()?>'
				>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>		
			</div>

			<script type="text/javascript">
				jQuery(function () 
					{
					jQuery('#<?=$control_id?>').datetimepicker
						(
							{
							locale: '<?=$this->form->locale?>',
							format: 'L LT',
							date: new Date("<?=$data->value?>"),
							widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
							}
						);
					}
				);

			</script>
		
		</div>		
		
		<?php
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlDateTime $data)
		{
		
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		$value = trim($_POST[$data->name]);
		if (!$value)
			{
			return false;
			}
			
		if ($this->form->locale == "en_US")
			{
			// è solo ad esempio...
			$retval = substr($value, 6, 4) . "-" .
						substr($value, 0, 2) . " " .
						substr($value, 3, 2) . "-" .
						substr($value, 11, 2) . ":" .
						substr($value, 14, 2) . ":";
			}
		else
			{
			// locale default: it_IT
			$retval = substr($value, 6, 4) . "-" .
						substr($value, 3, 2) . "-" .
						substr($value, 0, 2) . " " .
						substr($value, 11, 2) . ":" .
						substr($value, 14, 2) . ":";
			}
		
		$retval .= $data->showSeconds ? substr($value, 17, 2) : "00";
		
		return $retval;
		}
		
	//**************************************************************************
	}
//******************************************************************************


