<?php 
namespace waLibs\views\waForm\waForm_default;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waDateView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControl $data)
		{
		parent::transform($data);
		$control_id = $this->form->name . "_" . $this->name . "_datetimepicker";
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group date col-xs-12 col-sm-3 col-md-2 col-lg-2' id='<?=$control_id?>'>
				<input 
					type='text' 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: center; <?=$this->getControlStyle()?>'
				>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>		
			</div>

			<script type="text/javascript">
				jQuery(function () 
					{
					jQuery('#<?=$control_id?>').datetimepicker
						(
							{
							locale: '<?=$this->form->locale?>',
							format: 'L',
							date: new Date("<?=$data->value?>"),
							widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
							}
						);
					}
				);

			</script>
		
		</div>		
		
		<?php
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControl $data)
		{
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		$value = trim($_POST[$data->name]);
		if ($this->form->locale == "en_US")
			{
			// è solo ad esempio...
			return $value ? 
							substr($value, 6, 4) . "-" .
							substr($value, 0, 2) . "-" .
							substr($value, 3, 2)
						: false;
			}
		
		// locale default: it_IT
		return $value ? 
						substr($value, 6, 4) . "-" .
						substr($value, 3, 2) . "-" .
						substr($value, 0, 2)
					: false;
		}
		
	//**************************************************************************
	}
//******************************************************************************


