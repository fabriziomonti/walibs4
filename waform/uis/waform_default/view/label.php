<?php 
namespace waLibs\views\waForm\waForm_default;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waLabelView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlLabel $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if ($this->labelHaveControl())
			{
			?>
			<div 
				class='form-group'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
			>
			<?php
			}
			
		?>
		<label 
			id='lbl_<?=$this->form->name?>_<?=$this->name?>' 
			<?=$this->getControlAttributes()?> 
			style='<?=$this->getControlStyle()?>'
			class='<?=$this->getControlClass()?>'
		><?=$this->value?></label>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlLabel $data)
		{
		return null;
		}
		
	//**************************************************************************
	}
//******************************************************************************


