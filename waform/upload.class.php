<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waUpload *******************************************************
//***************************************************************************
/**
* waUpload
*
* classe per la gestione di un controllo complesso che permette di:
* - caricare un file (upload)
* - scaricare/vedere inline il file caricato
* - sostituire un file
* - eliminare un file precedentemente caricato
 * 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waUpload extends waControl
	{
	
	/**
	* URL per visualizzazione file
	*
	* e' l'URL di una pagina tramite cui e' possibile 
	* prendere visione del file
	* Se valorizzato, dovra' anche contenere tutti i parametri per accedere 
	* al file corretto (la classe non aggiunge nulla).
	* @var string
	*/
	public $showPage		=	"";
	
	/**
	* indica se visualizzare il nome del file precedentemente caricato
	*
	* Del file viene ovviamente mostrato solamente il basename.
	* @var boolean
	*/
	public $showFileName	= 	true;
	
	/**
	* size del file in Kb
	*
	* Se valorizzato, viene mostrato il size del file accanto al nome 
	* del file precedentemente caricato.
	* @var integer
	*/
	public $fileSize		=	0;

	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'upload';
	
	/**
	* - 
	*
	* array destinato a contenere i valori di input del modulo, valorizzato
	* a fronte di chiamata a {@link waForm::leggiValoriIngresso}
	* @ignore
	* @var array
	*/	
	var $input	= null;
	
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	 * @return waFormDataControlUpload
	*/
	function get()
		{
		$retval = parent::get();
		$retval->basename = basename($this->value);
		return $retval;
		}

	//****************************************************************************************
	/**
	* Restituisce il basename del file. 
	* 
	* Attenzione:
	*
	* o se e' stata richiesta la eliminazione, torna una stringa vuota
	* o se si e' verificato un errore durante il caricamento torna false
	* o se il campo non e' stato valorizzato dall'utente torna il valore con cui il controllo e' stato inizializzato in fase di creazione, sia esso derivante da DB o valore di default (ossia: il valore non deve cambiare)
	* o se non si e' verificato nessuno dei casi precedenti, ritorna il basename del file selezionato dall'utente
	*
	* @ignore
	* @return mixed il timestamp della data se valorizzata correttamente; altrimenti FALSE
	*/
	function input2inputValue($valueIn)
		{
		if ($valueIn === null)
			{
			return $this->inputValue = null;
			}
			
		// ci salviamo i values dell'array perche' poi ci serviranno per il salvataggio del file
		// e altrimenti vanno persi
		$this->input = $valueIn;
		
		if ($valueIn->delete)
			{
			return $this->inputValue = '';
			}
		if ($valueIn->error != 0 && $valueIn->error != 4)
			{
			return $this->inputValue = false;
			}
		if (!$valueIn->name)
			{
			// se non vogliono l'eliminazione del file e il nome e' vuoto allora 
			// il valore rimane quello di partenza
			return $this->inputValue = $this->value;
			}

		// controllo sulla lunghezza del nome del file se deve essere salvato su db
		if ($this->dbBound && $this->form->recordset)
			{
			$maxlen = $this->form->recordset->fieldMaxLength($this->name);
			if (strlen($valueIn->name) > $maxlen)
				{
				return $this->inputValue = substr($valueIn->name, 0, $maxlen - 5) . "." . pathinfo($valueIn->name, PATHINFO_EXTENSION);
				}
			}
			
		return $this->inputValue = $valueIn->name;
		}

	//****************************************************************************************
	/**
	* Restituisce il path del file temporaneo caricato dall'utente
	*
	* Si usa in fase di ricezione dei dati, non durante il display del modulo.
	*
	* @return string 
	*/
	public function getTmpValue()
		{
		return $this->input->tmp_name;
		}
		
	//****************************************************************************************
	/**
	* Restituisce true se l'utente ha richiesto la eliminazione del file esistente
	*
	* Si usa in fase di ricezione dei dati, non durante il display del modulo.
	*
	* @return boolean 
	*/
	public function isToDelete()
		{
		return $this->input->delete;

		}
		
	//****************************************************************************************
	/**
	* Restituisce true se c'e' effettivamente qualcosa da salvare
	*
	* Si usa in fase di ricezione dei dati, non durante il display del modulo.
	* 
	* @return boolean 
	*/
	public function isToSave()
		{
		return $this->input->tmp_name ? true : false;

		}
		
	//****************************************************************************************
	/**
	* salva il file nella destinazione richiesta
	*
	* Si usa in fase di ricezione dei dati, non durante il display del modulo.
	*
	* @param string $destinazione puo' essere sia un nome completo di file, sia il nome di una 
	* directory; in questo secondo caso, il nome del file sara' quello del file temporaneo
	* attribuito dall'engine PHP
	* @return boolean true = ok
	*/
	public function saveFile($destination)
		{
		return move_uploaded_file($this->input->tmp_name, $destination);
		}
		
	//****************************************************************************************
	/**
	* ritorna il codice di errore avvenuto durante il caricamento o false in caso di nessun errore
	*
	* Si usa in fase di ricezione dei dati, non durante il display del modulo.
	*
	* @return mixed false in caso di nessun errore; altrimenti il codice errore generato da PHP
	* durante il caricamento
	*/
	public function getUploadError()
		{
		return $this->input->error == 0 || $this->input->error == 4 ? false : $this->input->error;
		}

	}	// fine classe waUpload



