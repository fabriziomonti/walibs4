<?php

/**
 * -
 *
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */

namespace waLibs;

/**
 * @ignore
 */
include_once(__DIR__ . "/text.class.php");

//***************************************************************************
//****  classe waInteger *******************************************************
//***************************************************************************
/**
 * waInteger
 *
 * classe per la gestione dei controlli di tipo integer. 
 *
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */
class waInteger extends waText {

    /**
     * @ignore
     * @access protected
     */
    protected $type = 'integer';

    /**
     * nr. massimo di caratteri accettabili dal controllo
     *
     * laddove possibile, la classe provvedera' a desumere dal database questa informazione
     * @var integer
     */
    public $maxChars = 10;

    /**
     * flag che indica se il numero deve accettare segno o meno
     *
     * @var boolean
     */
    public $signed = false;

    //****************************************************************************************

    /**
     * Restituisce il numero inputato in formato  integer
     *
     * Si usa in fase di ricezione dei dati, non
     * durante la costruzione della form.
     *
     * @ignore
     * @return int
     */
    function input2inputValue($valueIn) {
        if ($valueIn === null) {
            return $this->inputValue = null;
        }

        return $this->inputValue = intval($valueIn);
    }

}

// fine classe waInteger
