<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include della struttura dati da passare al view object
* @ignore
*/
include_once __DIR__ . "/waform_data.class.php";

//***************************************************************************
//****  interfaccia i_waFormView ********************************************
//***************************************************************************
/**
* i_waFormView
*
* interfaccia che tutti i {@link waForm::view view-object} utiizzati da waForm
* devono rispettare
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
interface i_waFormView
{
	/**
	 * trasforma i dati passati in input nella UI prevista dal view-object
	 * 
	 * @param stdClass $data i dati da trasformare
	 */
    public function transform(waFormData $data);

	/**
	 * trasforma i dati ricevuti in input dalla UI nel formati previsti dai
	 * diversi controlli waForm
	 * 
	 * @param stdClass $data i dati da trasformare
	 */
	public function transformInput(waFormData $data);
}