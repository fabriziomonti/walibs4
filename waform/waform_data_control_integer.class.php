<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control_text.class.php";

//***************************************************************************
//****  classe waFormDataControlInteger **************************************
//***************************************************************************
/**
* waFormDataControlInteger
*
* struttura dati di un controllo di tipo integer 
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlInteger extends waFormDataControlText {

    /**
    * flag che indica se il numero deve accettare segno o meno
    *
    * @var boolean
    */
    public $signed		= false;


    
}
	
