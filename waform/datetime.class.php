<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/date.class.php");

//***************************************************************************
//****  classe waDateTime *******************************************************
//***************************************************************************
/**
* waDateTime
*
* classe per la gestione dei controlli di tipo datetime. 

* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waDateTime extends waDate
	{
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'datetime';
	
	/**
	* intervallo minuti
	*
	* indica che intervallo di minuti e' selezionabile 
	* @var integer
	*/
	public $minuteInterval = 1;
	
	/**
	* indica se mostrare il controllo anche per i secondi
	*
	* @var boolean
	*/
	public $showSeconds	= FALSE;
	
	/**
	* intervallo secondi
	*
	* indica che intervallo di secondi e' selezionabile (se presente il controllo
	* dei secondi)
	* @var integer
	*/
	public $secondInterval = 1;
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	 * @return waFormDataControl
	*/
	function get()
		{
		$retval = parent::get();
		$retval->value = $this->value ? date("Y-m-d H:i:s", $this->value) : '';
		return $retval;
		}

	//****************************************************************************************
	/**
	* Restituisce la data inputata in formato timestamp
	*
	* Si usa in fase di ricezione dei dati, non
	* durante la costruzione della form.
	*
	* @ignore
	* @return mixed il timestamp della data se valorizzata correttamente; altrimenti NULL
	*/
	function input2inputValue($valueIn)
		{
		if ($valueIn === null)
			{
			return $this->inputValue = null;
			}

		if (!preg_match('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', $valueIn, $parts))
			{
			return $this->inputValue = false;
			}

		if (!checkdate((int) $parts[2], (int) $parts[3], (int) $parts[1]))
			{
			return $this->inputValue = false;
			}
			
		return $this->inputValue = mktime($parts[4], $parts[5], $parts[6], $parts[2], $parts[3], $parts[1]);

		}

	}	// fine classe waDateTime
