<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waFormDataControl ********************************************
//***************************************************************************
/**
* waFormDataControl
*
* struttura dati di un controllo
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControl 
	{
	/**
	 * nome del controllo
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * tipo del controllo
	 * 
	 * @var string
	 */
	public $type;
		
	/**
	 * classe del controllo
	 * 
	 * @var string
	 */
	public $class;
		
	/**
	 * visibilità controllo
	 * 
	 * @var bool
	 */
	public $visible;
		
	/**
	 * abilitazione
	 * 
	 * @var bool
	 */
	public $readOnly;
		
	/**
	 * obbligatorietà
	 * 
	 * @var bool
	 */
	public $mandatory;
		
	/**
	 * valore
	 * 
	 * @var mixed
	 */
	public $value;
		
	/**
	 * il controllo è contenitore
	 * 
	 * @var boolean
	 */
	public $isContainer;
		
	/**
	 * nome di eventuale container del controllo
	 * 
	 * @var string
	 */
	public $container;
		
	}
	
