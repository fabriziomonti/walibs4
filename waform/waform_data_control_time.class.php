<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlTime ************************************
//***************************************************************************
/**
* waFormDataControlTime
*
* struttura dati di un controllo di tipo time
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlTime extends waFormDataControl
	{
	/**
	* intervallo minuti
	*
	* @var integer
	*/
	public $minuteInterval;
	
	/**
	* indica se mostrare il controllo anche per i secondi
	*
	* @var boolean
	*/
	public $showSeconds;
	
	/**
	* intervallo secondi
	*
	* @var integer
	*/
	public $secondInterval;
	

	}
	
