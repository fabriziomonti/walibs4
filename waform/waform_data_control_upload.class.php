<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlUpload **************************************
//***************************************************************************
/**
* waFormDataControlUpload
*
* struttura dati di un controllo di tipo upload
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlUpload extends waFormDataControl
	{
	/**
	* URL per visualizzazione file
	*
	* @var string
	*/
	public $showPage;
	
	/**
	* indica se visualizzare il nome del file precedentemente caricato
	*
	* @var boolean
	*/
	public $showFileName;
	
	/**
	* size del file in Kb
	*
	* @var integer
	*/
	public $fileSize;

	/**
	* basename del file
	*
	* @var string
	*/
	public $basename;
	
	}
	
