<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlMultiSelect *********************************
//***************************************************************************
/**
* waFormDataControlMultiSelect
*
* struttura dati di un controllo di tipo multiselect
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlMultiSelect extends waFormDataControl
	{
	/**
	* array chiave/valore dei valori selezionabili.
	*
	* @var array
	*/
	public $list;
	
	/**
	* array dei valori non selezionabili.
	*
	* @var array
	*/
	public $notSelectableList;
	
		
	}
	
