<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control_text.class.php";

//***************************************************************************
//****  classe waFormDataControlTaxCode *********************************
//***************************************************************************
/**
* waFormDataControlTaxCode
*
* struttura dati di un controllo di tipo taxcode
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlTaxCode extends waFormDataControlText
	{
	/**
	 * flag di gestione CF 
	 * 
	 * @var boolean
	 */
	public $manageCF = true;

	/**
	 * flag di gestione PI 
	 * 
	 * @var boolean
	 */
	public $managePI = false;

	}
	
