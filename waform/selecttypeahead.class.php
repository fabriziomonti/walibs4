<?php

/**
 * -
 *
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */

namespace waLibs;

use stdClass;

/**
 * @ignore
 */
include_once(__DIR__ . "/select.class.php");

//***************************************************************************
//****  classe waSelectTypeahead *******************************************************
//***************************************************************************
/**
 * waSelectTypeahead
 *
 * classe per la gestione di una select, ossia una lista all'interno
 * della quale e' possibile selezionare un solo elemento.
 * 
 * diversamente dalla select classica, questo controllo gestisce l'autocompletion
 * tramite chiamata RPC (ajax) al server.
 * 
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */
class waSelectTypeahead extends waControl {

	/**
	 * array dei valori selezionabili.
	 *
	 * La lista e' un array 
	 * associativo in cui la chiave dell'elemento e' l'identificativo della
	 * riga (tipicamente un record di una tabella in relazione) e il valore
	 * dell'elemento e' la descrizione (text) da porre in corrispondenza della
	 * riga.
	 *
	 * La lista puo' essere creata, in alternativa al passaggio esplicito,
	 * a partire da una query da sottoporre al DB: vedi {@link $sql}.
	 * @var array
	 */
	public $list = array();

	/**
	 * query SQL per la creazione della lista dei valori selezionabili.
	 *
	 * @var string
	 */
	public $sql = '';

	/**
	 * eventuale funzione/metodo custom da richiamare in caso di ricaricamento via RPC;
	 * se non definita, verrà richiamata la funzione standard che si basa sulla
	 * query sql prevista da {@link $sql)
	 * 
	 * per richiamare una funzione occorre valorizzare il parametro con il nome 
	 * della funzione
	 * 
	 * per richiamare un metodo, occorre valorizzare il parametro con un array
	 * il cui primo elemento conterrà l'oggetto a cui il metodo appartiene, e
	 * nel secondo elemento il nome del metodo da invocare
	 * 
	 * alla funzione/metodo sarà passato 1 solo parametro: un array in cui
	 * o il primo elemento contiene il pattern da ricercare
	 * o il secondo elemento contiene un oggetto stdClass contenente tutti i 
	 * 		valori inputati al momento della chiamata nel form; in questo modo
	 * 		è possibile restringere applicativamnte la query sulla base delle
	 * 		scelte operato dall'utente lato client
	 * 
	 * il metodo deve ritornare un array associativo in cui la chiave 
	 * dell'elemento e' l'identificativo della riga (tipicamente un record di 
	 * una tabella in relazione) e il valore dell'elemento e' la descrizione 
	 * (text) da mostrare in corrispondenza della riga.
	 * 
	 *
	 * @var mixed (string | array)
	 */
	public $rpcFunction = '';

	/**
	 * @ignore
	 * @access protected
	 */
	protected $type = 'selecttypeahead';

	//***************************************************************************

	/**
	 * @ignore
	 * @return waFormDataControlSelectTypeahead
	 */
	function get() {
		$retval = parent::get();
		$retval->text = $this->getText();

		return $retval;
	}

	//***************************************************************************

	/**
	 * @access protected
	 * @ignore
	 */
	function getText() {

		if ($this->value) {
			if ($this->sql && $this->form->recordset) {
				return $this->getTextFromDb();
			} elseif ($this->list) {
				return $this->list[$this->value];
			}
		}

		return "";
	}

	//***************************************************************************

	/**
	 * @access protected
	 * @ignore
	 */
	function getTextFromDb() {

		$dbconn = $this->form->recordset->dbConnection;
		$rs = new waRecordset($dbconn);
		$rs->read($this->sql, 0);

		// cerchiamo i nomi del campi chiave
		$keyFieldName = $rs->fieldName(0);
		$tableName = $rs->tableName(0);
		if ($tableName) {
			$keyFieldName = "$tableName.$keyFieldName";
		}
		$sqlClauses = $dbconn->splitSql($this->sql);
		$whereOrAnd = $sqlClauses->where ? "and" : "where";
		$sql = "$sqlClauses->select $sqlClauses->from $sqlClauses->where " .
				" $whereOrAnd $keyFieldName=" . $dbconn->sqlString($this->value) .
				" $sqlClauses->group $sqlClauses->order";

		$righe = $rs->read($sql, 1);
		if (!$righe) {
			return "";
		}

		// restituiamo il valore dell'ultimo campo (il descrittore)
		return $righe[0]->value($rs->fieldNr() - 1);
	}

	//***************************************************************************

	/**
	 * @access protected
	 * @ignore
	 */
	function rpcReload($params) {
		$pattern = $params[0];

		$retval = array();
		if ($this->rpcFunction) {
			$formValues = json_decode($params[1]);
			if (is_string($this->rpcFunction) && function_exists($this->rpcFunction)) {
				$retval = call_user_func($this->rpcFunction, $pattern, $formValues);
			} elseif (is_array($this->rpcFunction) && method_exists($this->rpcFunction[0], $this->rpcFunction[1])) {
				$retval = call_user_func($this->rpcFunction, $pattern, $formValues);
			}
		} 
		elseif ($this->sql && $this->form->recordset) {
			$retval = $this->rpcReloadFromDb($pattern);
		} 
		elseif ($this->list) {
			$retval = $this->rpcReloadFromList($pattern);
		}

		return $retval;
	}

	//***************************************************************************

	/**
	 * @access protected
	 * @ignore
	 */
	function rpcReloadFromList($pattern) {
		$retval = array();
		
		foreach ($this->list as $key => $value) {
			if (stripos($value, $pattern) !== false) {
				$item = new stdClass();
				$item->id = $key;
				$item->name = $value;
				$retval [] = $item;
			}
		}
		
		
		return $retval;
	}

	//***************************************************************************

	/**
	 * @access protected
	 * @ignore
	 */
	function rpcReloadFromDb($pattern) {
		$retval = array();

		$dbconn = $this->form->recordset->dbConnection;
		$rs = new waRecordset($dbconn);
		$rs->read($this->sql, 0);

		// cerchiamo i nomi del campo descrittore
		$textFieldName = $rs->fieldName($rs->fieldNr() - 1);
		$sqlClauses = $dbconn->splitSql($this->sql);
		$havingOrAnd = stripos($sqlClauses->where, " having ") === false ? "having" : "and";
		$sql = "$sqlClauses->select $sqlClauses->from $sqlClauses->where " .
				" $havingOrAnd $textFieldName like " . $dbconn->sqlString("%$pattern%") .
				" $sqlClauses->group $sqlClauses->order";

		$righe = $rs->read($sql, 10);
		if (!$righe) {
			return $retval;
		}

		foreach ($righe as $riga) {
			$item = new stdClass();
			// costruisco la chiave dell'elemento; la chiave e' composta da tutti i campi
			// ritornati dalla query, separati da "|", ad eccezione dell'ultimo che e' la
			// descrizione da mettere a video. Nella grande maggioranza dei casi avro' solo
			// 2 campi: la chiave di relazione e la descrizione. Gli altri campi, se presenti,
			// servono a ritornare attributi del record selezionato all'applicazione
			$key = $riga->value(0);
			for ($i = 1; $i < ($rs->fieldNr() - 1); $i++) {
				$key .= "|" . $riga->value($i);
			}
			$item->id = $key;
			$item->name = $riga->value($rs->fieldNr() - 1);
			$retval [] = $item;
		}

		return $retval;
	}

}

// fine classe waSelectTypeahead

