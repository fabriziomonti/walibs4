<?php

/**
 * -
 *
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */

namespace waLibs;

/**
 * @ignore
 */
include_once(__DIR__ . "/text.class.php");

//***************************************************************************
//****  classe waEmail ************************************************
//***************************************************************************
/**
 * waEmail
 *
 * classe per la gestione dei controlli destinati a contenere un indirizzo email. 
 * E' un normale {@link waText} dal quale si differenzia solo per il tipo, in 
 * modo da permetterne il riconoscimento lato view-object e di conseguenza le
 * relative procedure di controllo
 * 
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */
class waEmail extends waText {

    /**
     * @ignore
     * @access protected
     * @return waFormDataControlText
     */
    protected $type = 'email';

    //****************************************************************************************

    /**
     * Restituisce il valore se valido, altrimenti null
     *
     * Si usa in fase di ricezione dei dati, non
     * durante la costruzione della form.
     *
     * @ignore
     * @return string
     */
    function input2inputValue($valueIn) {

        if ($valueIn === null) {
            return $this->inputValue = null;
        }

        if ($valueIn === '') {
            return $this->inputValue = "";
        }

        if (!filter_var($valueIn, FILTER_VALIDATE_EMAIL)) {
            return $this->inputValue = "";
        }



        return $this->inputValue = $valueIn;
    }

    //****************************************************************************************
}

// fine classe waEmail
