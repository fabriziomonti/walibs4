<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waBoolean *******************************************************
//***************************************************************************
/**
* waBoolean
*
* classe per la gestione dei controlli di tipo si/no (checkbox, ecc.)
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waBoolean extends waControl
	{
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'boolean';
	
	}	// fine classe waBoolean








