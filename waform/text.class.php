<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waText ******************************************************
//***************************************************************************
/**
* waText
*
* classe per la gestione di un controllo text generico.
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waText extends waControl
	{
	
	/**
	* larghezza in caratteri del controllo
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var integer
	*/
	public $displayChars		= 50;
	
	/**
	* nr. massimo di caratteri accettabili dal controllo
	*
	* laddove possibile, la classe provvedera' a desumere dal database questa 
	 * informazione
	* @var integer
	*/
	public $maxChars		= 255;

	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'text';
	
	//***************************************************************************
	/**
	* @ignore
	 * @return waFormDataControlText
	*/
	function get()
		{
		if ($this->dbBound && $this->form->recordset)
			{
			$this->maxChars = $this->form->recordset->fieldMaxLength($this->name);
			}

		return parent::get();
		}


	}	// fine classe waText







