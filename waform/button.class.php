<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waButton *******************************************************
//***************************************************************************
/**
* waButton
*
* classe per la gestione dei bottoni
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waButton extends waControl
	{
	/**
	* indica che il bottone deve provocare submit
	* @var boolean
	*/
	public $submit	= true;
	
	/**
	* indica che il bottone deve provocare abort
	* @var boolean
	*/
	public $cancel	= false;
	
	/**
	* indica che il bottone deve provocare la cancellazione di un record
	* @var boolean
	*/
	public $delete	= false;

	/**
	* un bottone per sua natura non ha mai la corrispondenza con un campo del db
	* 
	* @var boolean
	*/
	public $dbBound		= false;
	
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'button';
	
	}	// fine classe waButton







