<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlTextArea ************************************
//***************************************************************************
/**
* waFormDataControlTextArea
*
* struttura dati di un controllo di tipo textarea
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlTextArea extends waFormDataControl
	{
	/**
	* larghezza in caratteri del controllo
	*
	* @var integer
	*/
	public $columns			= 50;
	
	/**
	* altezza in caratteri del controllo
	*
	* @var integer
	*/
	public $rows			= 3;

	/**
	* nr. massimo di caratteri accettabili dal controllo
	*
	* @var integer
	*/
	public $maxChars		= '';

	}
	
