<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlContainer ***************************************
//***************************************************************************
/**
* waFormDataControlContainer
*
* struttura dati di un controllo di tipo container
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlContainer extends waFormDataControl
	{
	/**
	* indica se il controllo è un contenitore di altri controlli
	* 
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var boolean
	*/
	public $isContainer	= true;

	/**
	* indica se la container debba fungere anche da meccanismo di
	* accesso all'help online
	*
	* @var boolean
	*/
	public $help;
	
	}
	
