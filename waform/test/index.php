<?php
error_reporting(E_ERROR|E_WARNING);
include("../waform.class.php");

// file contenente i parametri di connessione al database
define ("FILE_CONFIG_DB", dirname(__FILE__) . "/dbconfig.inc.php");

// main flow ******

// costruiamo i moduli che andranno gestiti nella pagina
$form0 = dammiModulo("form0");
//$form1 = dammiModulo("form1");

// verifichiamo quale e' l'azione da compiere 
if ($form0->isToDelete())
	doDelete($form0);
elseif ($form0->isToUpdate())
	doUpdate($form0);
//elseif ($form1->isToDelete())
//	doDelete($form1);
else
	// se non e' stata richiesta un'operazione, allora mostriamo i moduli per 
	// permettere l'input utente
	showPage($form0, $form1);
	
	
//*****************************************************************************
function showPage(waLibs\waForm $form0)
	{
	
	if ($_GET['json'])
		exit($form0->showJSON());
	
	// intestazione della pagina html...
	$outputBuffer = creaTop();

	// output del modulo
	$outputBuffer .= $form0->show(true);
//	$outputBuffer .= $form1->show(true);

	// inclusione delle procedure di controllo specifiche di questa pagina
	// che verranno utilizzate in interfaccia utente
	$outputBuffer .=  "<script type='text/javascript' src='ui/js/index.js'></script>\n";
	
	// chiusura della pagina html
	$outputBuffer .= creaFooter();

	// risistema il codice html rendendolo "strict"
	$outputBuffer = fammeloStrict($outputBuffer);
	
	// output del buffer contenente la pagina 
	exit($outputBuffer);
	
	}

//*****************************************************************************
function creaTop()
	{
	// intestazione della pagina html...
	header("Content-Type: text/html; charset=UTF-8");
	$buffer = "
				<!DOCTYPE html>
				<html>
					<head>
						<meta charset='utf-8'>
						<meta http-equiv='X-UA-Compatible' content='IE=edge'>
						<meta name='viewport' content='width=device-width, initial-scale=1'>
						<title>waForm Test</title>

					</head>
					<body>
					<div class='container-fluid'>
				
				";
	
	return $buffer;
	}
	
//*****************************************************************************
function creaFooter()
	{
	return "</div></body></html>";
	}
	
//*****************************************************************************
function fammeloStrict($buffer)
	{
	// spostiamo eventuali "<link ...>" nell'head per essere "strict"
	list ($pre_head, $resto) = explode("<head>", $buffer, 2);
	list ($head, $resto) = explode("</head>", $resto, 2);
	list ($tra_head_e_body, $body) = explode("<body", $resto, 2);
	while (true)
		{
		list ($prima, $dopo) = explode("<link ", $body, 2);
		if (!$dopo)
			break;
		list ($link, $dopo) = explode(">", $dopo, 2);
		$head .= "<link $link" . (substr(rtrim($link), -1) == "/" ? '' : " /") . ">\n";
		$body = "$prima$dopo";
		}

	$buffer = "$pre_head\n<head>\n$head</head>$tra_head_e_body\n<body$body";

	return $buffer;
	}

//*****************************************************************************
/**
 * 
 * @param type $nome
 * @return waLibs\waForm
 */
function dammiModulo($nome)
	{
	// creazione del modulo...
	$modulo = new waLibs\waForm();
	$modulo->name = $nome;
	
	// creazione del recorset da associare al modulo
	$dbconn = waLibs\wadb_getConnection(FILE_CONFIG_DB);
	if ($dbconn->errorNr()) exit("Errore connetti: " . $dbconn->errorNr() . " - " . $dbconn->errorMessage() . "<hr>");
  	$sql = "SELECT * FROM corsi WHERE id_corso=" . $dbconn->sqlInteger($_GET['id_corso']);
	$recordset = new waLibs\waRecordset($dbconn);
	$recordset->read($sql, $nrRighe);
	if ($dbconn->errorNr()) exit("Errore read: " . $dbconn->errorNr() . " - " . $dbconn->errorMessage() . "<hr>");

	// associazione del recordset al modulo...
	$modulo->recordset = $recordset;
	
	// definizioni delle proprieta' del modulo...
 	$modulo->fieldNameRecId = 'id_corso';
	
 	// inserimento dei controlli all'interno del modulo
	$ctrl = $modulo->addSelect("id_organismo", "Organismo", false, true);
		$ctrl->list = dammiLista("organismi", "id_organismo", "nome");
	$ctrl = $modulo->addSelect("id_amministrazione", "Sigla", false, true);
		$ctrl->list = dammiLista("amministrazioni", "id_amministrazione", "sigla");
 	$ctrl = $modulo->addText("rifpa", "Rif. P.A.", false, true);
 	$ctrl = $modulo->addText("nome", "Titolo corso");
 	$modulo->addInteger("nr_ore", "Monte ore complessivo");
 	$ctrl = $modulo->addDate("data_inizio", "Data inizio");
		$ctrl->value = time();
 	$ctrl = $modulo->addDateTime("data_fine", "Data fine");
 	$ctrl = $modulo->addCurrency("importo", "Importo complessivo");
	
	$ctrl = $modulo->addItem("test_non_controllo");
		$ctrl->dbBound = false;
		$ctrl->value = file_get_contents("lorem.txt");
	
	$ctrl = $modulo->addTextArea("area", "area");
		$ctrl->dbBound = false;
	$ctrl = $modulo->addBoolean("check", "check");
		$ctrl->dbBound = false;
		
	$ctrl = $modulo->addMultiSelect("multiselect", "multiselect");
		$ctrl->dbBound = false;
		$ctrl->list = array (5 => "cinque",10 => "dieci" , 20 => "venti",30 => "trenta" , 40 => "quaranta", 50 => "cinquanta" , 60 => "sessanta");
		$ctrl->value = array (10, 30, 50);
		$ctrl->notSelectableList = array (20, 60);
		
	$ctrl = $modulo->addUpload("file", "file");
		$ctrl->dbBound = false;
		$ctrl->value = "jethro.pdf";
 	$ctrl = $modulo->addTime("soloora", "ora test");
		$ctrl->dbBound = false;
	
	// bottoni del modulo
	$butt = $modulo->addButton("btnInvia", "REGISTRA");
  	if ($_GET['id_corso'])
  		{
		$butt2 = $modulo->addButton("btnElimina", "ELIMINA");
		$butt2->delete = true;
  		}
	
	setButton($modulo, "cmdAbilita", "abilita", 0);
	setButton($modulo, "cmdDisabilita", "disabilita", 1);
	setButton($modulo, "cmdObbligatorio", "obbligatori", 2);
	setButton($modulo, "cmdNonObbligatorio", "non obblig.", 3);
	setButton($modulo, "cmdVisualizza", "visualizza", 4);
	setButton($modulo, "cmdNascondi", "nascondi", 5);
	
//exit($modulo->mostraXML());	
	// lettura di eventuali dati in input di cui il modulo potrebbe essere
	// destinatario
	$modulo->getInputValues();
			
	return $modulo;
	}
	
//*****************************************************************************
function setButton(waLibs\waForm $modulo, $nome, $valore, $pos)
	{
	
	$butt = $modulo->addButton($nome, $valore);
	$butt->submit = false;
	}
	
//*****************************************************************************
function dammiLista($table, $idFieldName, $descriptionFieldName, $where = '')
	{
	$dbconn = waLibs\wadb_getConnection(FILE_CONFIG_DB);
	if ($dbconn->errorNr())
		exit("Errore connetti: " . $dbconn->errorNr() . " - " . $dbconn->errorMessage() . "<hr>");
		
	$recordset = new waLibs\waRecordset($dbconn);
	$sql = "SELECT * FROM $table $where ORDER BY $descriptionFieldName";
	$recordset->read($sql);
	if ($dbconn->errorNr())
		exit("Errore read: " . $dbconn->errorNr() . " - " . $dbconn->errorMessage() . "<hr>");
		
	$retval = array();
	foreach ($recordset->records as $riga)
		$retval[$riga->value($idFieldName)] = $riga->value($descriptionFieldName);
		
	return $retval;
	}


//*****************************************************************************
// funzione RPC che viene richiamata dal client
function fammiUnaRPC($id_amministrazione)
	{
	return dammiLista("corsi", "id_corso", "nome", " WHERE id_amministrazione='$id_amministrazione'");
	}
	
//*****************************************************************************
// inserimento/modifica su db del record gestito tramite il modulo
function doUpdate(waLibs\waForm $modulo)
	{
	$esito = $modulo->checkMandatory();
	if (!$esito) exit("mancano campi obbligatori!<hr>");
	$esito = $modulo->save(true);
	if (!$esito) exit("Errore inserimento/modifica: " . $modulo->recordset->dbConnection->errorNr() . " - " . $modulo->recordset->dbConnection->errorMessage() . "<hr>");

	$recid = $_GET['id_corso'] ? $_GET['id_corso'] : $esito;

	echo "Operazione eseguita correttamente<p>" .
				"<a href=''>torna al modulo vuoto</a><br>" .
				"<a href='?id_corso=$recid'>torna in modifica sul record</a><br>";	

	}

//*****************************************************************************
// eliminazione da db del record gestito tramite il modulo
function doDelete($modulo)
	{
	if (!$modulo->delete(true))
		exit("Errore eliminazione: " . $modulo->recordset->dbConnection->errorNr() . " - " . $modulo->recordset->dbConnection->errorMessage() . "<hr>");
	
	echo "Operazione eseguita correttamente<p>" .
				"<a href=''>torna al modulo</a>";	

	}
	
	
	
	


