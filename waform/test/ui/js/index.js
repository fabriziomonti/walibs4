//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// proprieta'
	tables		: {},
	forms		: {},
	tablesIdx	: [],
	formsIdx	: [],
	// proprieta' valorizzate e utilizzabili nel caso in cui la page gestisca una sola table/form
	table 	: null,		// prima o unica table passata al costruttore
	form 		: null,		// primo o unico form passato al costruttore

	//-------------------------------------------------------------------------
	//initialization
	initialize: function(tables, forms) 
		{
		// definizione iniziale delle proprieta'
		if (tables)
			{
			if (tables instanceof Array)	
				{
				this.tablesIdx = tables;
				for (var i = 0; i < tables.length; i++)
					{
					this.tables[tables[i].name] = tables[i];
					this.tables[tables[i].name].application = this;
					}
				this.table = this.tablesIdx[0];
				}
			else
				this.table = this.tablesIdx[0] = this.tables[tables.name] = tables;
			}

		if (forms)
			{
			if (forms instanceof Array)	
				{
				this.formsIdx = forms;
				for (var i = 0; i < forms.length; i++)
					{
					this.forms[forms[i].name] = forms[i];
					this.forms[forms[i].name].application = this;
					}
				this.form = this.formsIdx[0];
				}
			else
				this.form = this.formsIdx[0] = this.forms[forms.name] = forms;
			}
		
		// collegamento tra il form e le azioni non comprese da standard
		// pone gli eventi nello scope dell'applicazione 
		this.bindFormEvents();
		},
	
	//-------------------------------------------------------------------------
	// funzione richiamata automaticamente dalla bindFormEvents
	applicationEvent: function (event)
		{
		// siamo nello scope dell'oggetto fisico che ha innescato l'evento;
		// vogliamo andare nello scope di questo (this) oggetto;
		// cerchiamo qual'e' il controllo a cui appartiene l'oggetto che ha
		// scatenato l'evento, in modo da ricostruire il nome del metodo da
		// richiamare
		
		var nomeMetodo = "event_on" + (window.event || event).type + "_";
		
		// verifichiamo se non sia un evento del form
		if (this.elements)
			return this.waForm.application[nomeMetodo + this.name + "_" + this.name](event);

		var nomeControllo = '';
		for (nomeControllo in this.form.waForm.controls)
			{
			if (this.form.waForm.controls[nomeControllo].isMine(this))
				break;
			}
			
		nomeMetodo += this.form.waForm.name + "_" + this.form.waForm.controls[nomeControllo].name;
		return this.form.waForm.application[nomeMetodo](event);
		},
		
	//-------------------------------------------------------------------------
	// collegamento tra il form e le azioni non comprese da standard
	// pone gli eventi nello scope dell'applicazione 
	bindFormEvents: function ()
		{
		var elems = [];
		var nomeEvento = '';
		var nomeControllo = '';
		var nomeMetodo = '';
		var nomeform = '';
			
		for (nomeMetodo in this)
			{
			if (typeof(this[nomeMetodo]) == 'function' && nomeMetodo.substr(0, ("event_").length) == "event_")
				{
				elems = nomeMetodo.split("_", 3);
				nomeEvento = elems[1];
				for (nomeform in this.forms)
					{
					if (nomeMetodo.substr(0, ("event_" + nomeEvento + "_" + nomeform + "_").length) == 
												"event_" + nomeEvento + "_" + nomeform + "_")
						{
						nomeControllo = nomeMetodo.substr(("event_" + nomeEvento + "_" + nomeform + "_").length);
						if (this.forms[nomeform].controls[nomeControllo])
							this.forms[nomeform].controls[nomeControllo].addEvent(nomeEvento, this.applicationEvent);
						else if (nomeform == nomeControllo)
							// e' stato richiesto un evento sull'intero form (verosimilmente l'overload di onsubmit)
							this.forms[nomeform].obj[nomeEvento] = this.applicationEvent;
						}
					}
				}
			}
		},

	//-------------------------------------------------------------------------
	// verifica se un oggetto dictionary ha elementi al suo interno oppure no
	emtpyDictionary: function (dictionary)
		{
		if (jQuery.isArray(dictionary) && dictionary.length == 0)
			return true;
		
		for (var li in dictionary)
			return false;
		
		return true;
		},
		
	//-------------------------------------------------------------------------
	event_onchange_form0_id_amministrazione: function (event)
		{
		
		var esito = this.forms.form0.RPC("fammiUnaRPC", this.forms.form0.controls["id_amministrazione"].get());
		if (this.emtpyDictionary(esito))
			this.alert("L'RPC dice che non esistono corsi per l'id_amministrazione " + 
					this.forms.form0.controls["id_amministrazione"].get());
		else
			{
			var msg = "Lista dei corsi gestiti dall' id_amministrazione " + 
						this.forms.form0.controls["id_amministrazione"].get() + 
						" ottenuta tramite RPC" + "\n\n";
			for (var li in esito)
				msg += li + " - " + esito[li] + "\n";
		
			this.alert(msg);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form0_cmdAbilita: function (event)
		{
		for (var li in this.forms.form0.controls)
			{
			if (this.forms.form0.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form0.controls[li].enable(true, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form0_cmdDisabilita: function (event)
		{
		for (var li in this.forms.form0.controls)
			{
			if (this.forms.form0.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form0.controls[li].enable(false, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form0_cmdObbligatorio: function (event)
		{
		for (var li in this.forms.form0.controls)
			{
			if (this.forms.form0.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form0.controls[li].setMandatory(true, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form0_cmdNonObbligatorio: function (event)
		{
		for (var li in this.forms.form0.controls)
			{
			if (this.forms.form0.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form0.controls[li].setMandatory(false, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form0_cmdVisualizza: function (event)
		{
		for (var li in this.forms.form0.controls)
			{
			if (this.forms.form0.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form0.controls[li].show(true, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form0_cmdNascondi: function (event)
		{
			
		for (var li in this.forms.form0.controls)
			{
			if (this.forms.form0.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form0.controls[li].show(false, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onchange_form1_id_amministrazione: function (event)
		{
		var esito = this.forms.form1.RPC("fammiUnaRPC", this.forms.form1.controls["id_amministrazione"].get());
		if (this.emtpyDictionary(esito))
			this.alert("L'RPC dice che non esistono corsi per l'id_amministrazione " + 
					this.forms.form1.controls["id_amministrazione"].get());
		else
			{
			var msg = "Lista dei corsi gestiti dall' id_amministrazione " + 
						this.forms.form1.controls["id_amministrazione"].get() + 
						" ottenuta tramite RPC" + "\n\n";
			for (var li in esito)
				msg += li + " - " + esito[li] + "\n";
		
			this.alert(msg);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form1_cmdAbilita: function (event)
		{
		for (var li in this.forms.form1.controls)
			{
			if (this.forms.form1.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form1.controls[li].enable(true, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form1_cmdDisabilita: function (event)
		{
		for (var li in this.forms.form1.controls)
			{
			if (this.forms.form1.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form1.controls[li].enable(false, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form1_cmdObbligatorio: function (event)
		{
		for (var li in this.forms.form1.controls)
			{
			if (this.forms.form1.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form1.controls[li].setMandatory(true, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form1_cmdNonObbligatorio: function (event)
		{
		for (var li in this.forms.form1.controls)
			{
			if (this.forms.form1.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form1.controls[li].setMandatory(false, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form1_cmdVisualizza: function (event)
		{
		for (var li in this.forms.form1.controls)
			{
			if (this.forms.form1.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form1.controls[li].show(true, true);
			}
		},
	
	//-------------------------------------------------------------------------
	event_onclick_form1_cmdNascondi: function (event)
		{
		for (var li in this.forms.form1.controls)
			{
			if (this.forms.form1.controls[li].name.substr(0, 3) != "cmd")
				this.forms.form1.controls[li].show(false, true);
			}
		}
	
			
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
var forms = [];
forms.push(document.form0);
//forms.push(document.form1);
var page = new waPage(null, forms);




