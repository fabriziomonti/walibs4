<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waControl ****************************************************
//***************************************************************************
/**
* waControl
*
* classe generica che definisce le proprieta' comuni a tutti i controlli
* di un form
 * 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waControl
	{
	/**
	* form a cui il controllo appartiene
	* @var waForm
	*/
	public $form;
	
	/**
	* nome del controllo
	* @var string
	*/
	public $name			= '';
	
	/**
	* valore del controllo
	*
	* il valore assume significati diversi a seconda del controllo. Alcuni esempi:
	* - {@link waText}, {@link waTextArea}, {@link waCurrency}, {@link waDate}, 
	*   {@link waDateTime}, {@link waUpload} -> e' il valore di default, che puo' essere 
	*   ricoperto da cio'che viene trovato sul db
	* - {@link waLabel}, {@link waButton}, {@link waFrame} -> 
	*   e' la caption
	* - {@link waSelect}/{@link waOption} -> viene selezionato l'elemento corrispondente al valore passato,
	*   purche' l'array degli elementi selezionabili (ancorche' proveniente da una 
	*   query) lo contenga
	* - {@link waBoolean} -> 1 se checked - 0 o vuoto altrimenti
	* - {@link waMultiSelect}, lista -> e' un array contenente i valori degli elementi selezionati
	* @var mixed
	*/
	public $value			= '';
	
	/**
	* indica se il controllo e' obbligatorio o meno
	*
	* per etichette e cornici ha senso piu' che altro per la renderizzazione
	* @var boolean
	*/
	public $mandatory 		= false;

	/**
	* indica se il controllo e' abilitato o meno
	*
	* si presti attenzione che tipicamente tutti i controlli solaLettura sono disabilitati,
	* e in quanto tali in HTML non passano il proprio valore alla pagina di destinazione.
	*
	* @var boolean
	*/
	public $readOnly 		= false;
	
	/**
	* indica se il controllo e' associato ad un campo di un record; per default
	* lo è
	*
	* il campo del record dovra' avere il medesimo nome del controllo,
	* case-insensitive
	* 
	* @var boolean
	*/
	public $dbBound		= true;
	
	/**
	* indica se il controllo e' visibile o meno
	*
	* a differenza della proprieta' {@link $readOnly}, il valore di un
	* controllo hidden, in HTML,  viene passato alla pagina di destinazione.
	* 
	* @var boolean
	*/
	public $visible		= true;

	/**
	* valore ritornato dalla fase di input
	* @var string
	*/
	public $inputValue	= null;
	
	/**
	* flag che indica che non e' stato ritornato alcun valore dalla fase di input,
	 * ossia che il controllo era disabilitato al momento del submit
	 * 
	* @var boolean
	*/
	public $noInputReturned = false;
	
	/**
	* indica se il controllo è un contenitore di altri controlli
	* 
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var boolean
	*/
	public $isContainer	= false;

	/**
	* nome di eventuale container al quale il controllo appartiene
	* 
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var string
	*/
	public $container	= '';

	/**
	 * tipo del controllo (valorizzato automaticamente dalla classe)
	 * 
	* @access protected
	*/
	protected $type			= '';
	
	/**
	 * nome della clase del controllo (valorizzato automaticamente dalla classe)
	 * 
	* @access protected
	*/
	protected $class			= '';
	
	/**
	 * indica se il controllo e' di input o un etichetta/cornice
	 * 
	* @access protected
	*/
	protected $isInput			= true;
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* costruttore
	*
	* inizializza il controllo.
	* @param waForm $form oggetto di classe {@link waForm} a cui il controllo appartiene
	* @param string $nome nome del controllo; vedi {@link $nome}
	* @param string $valore valore di default del controllo; vedi {@link $valore}
	*/
	public function __construct(waForm $form, $name = '', $value = '')
		{
		$this->form = $form;
		$this->name = $name;
		$this->value = $value;
		$form->controls[] = $this;
		
		if ($this->isInput)
			{
			$form->inputControls[$name] = $this;
			$form->inputControlsNum[] = $this;
			}
		else
			{
			$form->labels[$name] = $this;
			$form->labelsNum[] = $this;
			}

		}
		
	//***************************************************************************
	/**
	* @access protected
	* @ignore
	*/	
	protected function getCurrentPath()
		{
		return $this->form->getMyPath();
		}
		
	//***************************************************************************
	/**
	* @ignore
	 * @return waFormDataControl
	*/	
	function get()
		{
		// determiniamo la classe dell'oggetto dati destinato al view-object
		$class = new \ReflectionClass($this);
		$dataClassName = $className = $class->getShortName();
		$dataClassName = "\\waLibs\\waFormDataControl" . substr($dataClassName, 2);
		if (class_exists($dataClassName))
			{
			$retval = new $dataClassName();
			}
		else
			{
			$dataClassName = $class->getParentClass()->getShortName();
			if ($dataClassName == "waControl")
				{
				$dataClassName = "\\waLibs\\waFormDataControl";
				}
			else
				{
				$dataClassName = "\\waLibs\\waFormDataControl" . substr($dataClassName, 2);
				}
			$retval = new $dataClassName();
			}

		foreach ($retval as $property => $chissene)
			{
			$retval->$property = $this->$property;
			}
		$retval->class = $className;
		
		return $retval;
		}
	
	//***************************************************************************
	/**
	* @ignore
	*/	
	function setInitialValue()
		{
		// se la form e' in bind con un record, ed il record e' valorizzato, prelevo
		// il valore dal record; altrimenti il valore e' quello di default
		if ($this->dbBound && $this->form->record)
			{
			$this->value = $this->form->record->value($this->name);
			}
		}
	
	//***************************************************************************
	/**
	 * converte il valore proveniente dal post nel valore logico del controllo
	* @ignore
	*/	
	function input2inputValue($inValue)
		{
		return $this->inputValue = $inValue;
		}
	
	//***************************************************************************
	/**
	* verificaObbligo
	* @ignore
	*
	*/
	function checkMandatory()
		{
		return !$this->mandatory || 
				($this->inputValue !== null && $this->inputValue !== '' && $this->inputValue !== false);
		}

	//***************************************************************************
	/**
	 * salva sul campo del record il valore di input
	* @ignore
	*/	
	function input2record()
		{
		if (!$this->dbBound || !$this->form->record || $this->inputValue === null)
			{
			return;
			}
		$this->form->record->insertValue($this->name, $this->inputValue);
		}
	
	}	// fine classe waControl





