<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waFrame *******************************************************
//***************************************************************************
/**
* waFrame
*
* classe per la creazione di una frame che raggruppa (graficamente) piu' controlli.
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFrame extends waControl
	{
	/**
	* indica se il controllo è un contenitore di altri controlli
	* 
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var boolean
	*/
	public $isContainer	= true;

	/**
	* indica se la label della frame debba fungere anche da meccanismo di
	* accesso all'help online
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var boolean
	*/
	public $help			= false;

	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'frame';

	/**
	 * indica se il controllo e' di input o un etichetta/frame
	 * 
	* @access protected
	*/
	protected $isInput			= false;
	
	//***************************************************************************
	/**
	* @ignore
	*/	
	function setInitialValue()
		{
		}
	
	//***************************************************************************
	/**
	* verificaObbligo
	* @ignore
	*
	*/
	function checkMandatory()
		{
		return true;
		}

	//***************************************************************************
	/**
	 * salva sul campo del record il valore di input
	* @ignore
	*/	
	function input2record()
		{
		}
	

	}	// fine classe waFrame


