<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlCurrency ************************************
//***************************************************************************
/**
* waFormDataControlCurrency
*
* struttura dati di un controllo di tipo currency
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlCurrency extends waFormDataControl
	{
	/**
	 * nr interi ammessi
	 * 
	 * @var int
	 */
	public $integerNr;

	/**
	 * nr decimali ammessi
	 * 
	 * @var int
	 */
	public $decimalNr;
		
	/**
	* il controllo rimane vuoto se il valore da mostrare e' zero
	* 
	* @var boolean
	*/
	var $emptyOnZero;

	}
	
