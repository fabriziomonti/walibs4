<?php
/**
* -
*
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */
namespace waLibs;

/**
 * @ignore
 */
include_once(__DIR__ . "/text.class.php");

//***************************************************************************
//****  classe waCaptcha *******************************************************
//***************************************************************************
/**
 * waCaptcha
 *
 * classe per la gestione del submit di un codice di controllo, al fine di
 * verificare la presenza fisica di un operatore ed evitare le azioni dei 
 * "bot". 
 * 
 * Il controllo può funzionare solo se la sessione è attiva!
 * 
 * @package waForm
 * @version 4.0
 * @author F.Monti
 * @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 */
class waCaptcha extends waText
	{

	/**
	 * @ignore
	 * @access protected
	 */
	protected $type = 'captcha';

	/**
	 * per sua natura il controllo è obbligatorio
	 */
	public $mandatory = true;

	/**
	 * per sua natura il controllo non ha corrispondenza sul db
	 */
	public $dbBound = false;

	/**
	* larghezza in caratteri del controllo
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var integer
	*/
	public $displayChars		= 5;

	/**
	* nr. massimo di caratteri accettabili dal controllo
	*
	* @var integer
	*/
	public $maxChars		= 5;

	//***************************************************************************
	/**
	 * @access protected
	 * @ignore
	 * @return waFormDataControlText
	 */
	function get()

		{
		// creiamo la stringa che andrà mostrata nell'immagine e la
		// salviamo in sessione
		$elems = explode(" ", microtime());
		$chiave = chr((substr($elems[1], -1) + ord('a'))) . substr($elems[0], 2, 4) . substr($elems[1], -3);
		$chiave = substr($chiave, 0, -1) . chr(substr($chiave, -1) + ord('l'));
		// sostituisco un'eventuale 0 con 1 in modo da non potersi 
		// confondere con la lettera "o" maiuscola
		$chiave = str_replace("0", "1", substr($chiave, 0, $this->maxChars));

		// il valore in questo caso è la chiave del parametro di sessione 
		// che nasconde il vero valore
		$this->value = microtime(true) * rand(2, 5);
		$_SESSION["WAFORM_CAPTCHA_CODE_$this->value"] = $chiave;

		return parent::get();
		}

	//***************************************************************************
	/**
	 * converte il valore proveniente dal post nel valore logico del controllo
	* @ignore
	*/	
	function input2inputValue($valueIn)
		{

		if (is_object($valueIn))
			{
			$this->inputValue = strtolower($_SESSION["WAFORM_CAPTCHA_CODE_$valueIn->k"]) == strtolower($valueIn->v);
			}

		return $this->inputValue;
		}

	//****************************************************************************************
	}

// fine classe waCaptcha
