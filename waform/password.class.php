<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/text.class.php");

//***************************************************************************
//****  classe waPassword *******************************************************
//***************************************************************************
/**
* waPassword
*
* classe per la gestione dei controlli di tipo password. 
 * 
* E' un normale {@link waTesto} dal quale si differenzia solo per il tipo, in 
* modo da permetterne il riconoscimento lato client e di conseguenza le
* relative procedure di controllo
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waPassword extends waText
	{
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'password';

	}	// fine classe waPassword

