<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waTime *******************************************************
//***************************************************************************
/**
* waTime
*
* classe per la gestione dei controlli di tipo time.
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTime extends waControl
	{
	/**
	* intervallo minuti
	*
	* indica che intervallo di minuti e' selezionabile 
	* @var integer
	*/
	public $minuteInterval = 1;
	
	/**
	* indica se mostrare il controllo anche per i secondi
	*
	* @var boolean
	*/
	public $showSeconds	= FALSE;
	
	/**
	* intervallo secondi
	*
	* indica che intervallo di secondi e' selezionabile (se presente il controllo
	* dei secondi)
	* @var integer
	*/
	public $secondInterval = 1;
	
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'time';

	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	 * @return waFormDataControlTime
	*/
	function get()
		{
		$retval = parent::get();
		$retval->value = $this->value ? date("H:i:s", $this->value) : '';
		return $retval;
		}

	//****************************************************************************************
	/**
	* Restituisce l'ora inputata in formato timestamp
	*
	 * @ignore
	* @return mixed il timestamp dell'ora se valorizzata correttamente 
	* <B>con spiazzamento della data al 01/01/1980!!!</B>; altrimenti null
	*/
	function input2inputValue($valueIn)
		{
		if ($valueIn === null)
			{
			return $this->inputValue = null;
			}

		if (!preg_match('/^(\d{2}):(\d{2}):(\d{2})$/', $valueIn, $parts))
			{
			return $this->inputValue = false;
			}

		return $this->inputValue = mktime($parts[1], $parts[2], $parts[3], 1,1,1980);
		}

	}	// fine classe waTime


