<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waLabel *******************************************************
//***************************************************************************
/**
* waLabel
*
* classe per la creazione di una label
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waLabel extends waControl
	{
	/**
	* indica se la label debba fungere anche da meccanismo di
	* accesso all'help online
	*
	* @var boolean
	*/
	public $help			= true;
	
	/**
	* nr. massimo di caratteri accettabili dal controllo a cui la label si riferisce
	*
	* laddove possibile, la classe provvedera' a desumere dal database questa 
	 * informazione
	* @var integer
	*/
	public $maxChars;

	/**
	* @access protected
	* @ignore
	*/
	protected $type			= 'label';
	
	/**
	 * indica se il controllo e' di input o un label/cornice
	 * 
	* @access protected
	*/
	protected $isInput			= false;
	

	//***************************************************************************
	/**
	* @access protected
	* @ignore
	 * @return waFormDataControlLabel
	*/
	function get()
		{
		if ($this->form->recordset)
			{
			$this->maxChars = $this->form->recordset->fieldMaxLength($this->name);
			}

		return parent::get();

		}

	//***************************************************************************
	/**
	* @ignore
	*/	
	function setInitialValue()
		{
		}
	
	//***************************************************************************
	/**
	* verificaObbligo
	* @ignore
	*
	*/
	function checkMandatory()
		{
		return true;
		}

	//***************************************************************************
	/**
	 * salva sul campo del record il valore di input
	* @ignore
	*/	
	function input2record()
		{
		}
	

	}	// fine classe waLabel
