<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waCurrency *******************************************************
//***************************************************************************
/**
* waCurrency
*
* classe per la gestione dei controlli di tipo decimale. 
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waCurrency extends waControl
	{
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'currency';
	
	/**
	* numero digit interi
	* @var integer
	*/
	var $integerNr		= 5;
	
	/**
	* numero digit decimali
	* @var integer
	*/
	var $decimalNr		= 2;

	/**
	* il controllo rimane vuoto se il valore da mostrare e' zero; altrimenti viene 
	* mostrato "0,00"
	* @var boolean
	*/
	var $emptyOnZero	= true;

	//****************************************************************************************
	/**
	* Restituisce il numero decimale inputato in formato float
	*
	* Si usa in fase di ricezione dei dati, non
	* durante la costruzione della form.
	*
	* @ignore
	* @return float
	*/
	function input2inputValue($valueIn)
		{

		if ($valueIn === null)
			{
			return $this->inputValue = null;
			}

		return $this->inputValue = round(floatval($valueIn), $this->decimalNr);
		}

	}	// fine classe waCurrency





