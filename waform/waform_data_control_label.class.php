<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlLabel ***************************************
//***************************************************************************
/**
* waFormDataControlLabel
*
* struttura dati di un controllo di tipo label
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlLabel extends waFormDataControl
	{
	/**
	* indica se la label debba fungere anche da meccanismo di
	* accesso all'help online
	*
	* @var boolean
	*/
	public $help;
	
	/**
	* nr. massimo di caratteri accettabili dal controllo a cui la label si riferisce
	*
	* @var integer
	*/
	public $maxChars;


	}
	
