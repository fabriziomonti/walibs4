<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include dell'interfaccia i_waFormView
* @ignore
*/
include_once  __DIR__ . "/i_waFormView.interface.php";

/**
* -
* include del package waDb per l'accesso al database
* @ignore
*/
include_once(__DIR__ . "/../wadb/wadb.class.php");



//***************************************************************************
//****  classe waForm **************************************************
//***************************************************************************
/**
* waForm
*
* classe per la gestione di una form standard, con prelevamento, opzionale,
* dei dati da un record waRecord contenuto in un oggetto waRecordset. 
* La form e' solo il contenitore che contiene
* i vari controlli definiti dalle specifiche classi.
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waForm
	{
	//codici operazione
	/**
	* codice dell'operazione di visualizzazione dettaglio record
	*/
	const ACTION_DETAILS =		'WAFORM_ACTION_DETAILS';

	/**
	* codice dell'operazione di inserimento record
	*/
	const ACTION_INSERT =		'WAFORM_ACTION_INSERT';

	/**
	* codice dell'operazione di modifica record
	*/
	const ACTION_EDIT =		'WAFORM_ACTION_EDIT';

	/**
	* codice dell'operazione di eliminazione record
	*/
	const ACTION_DELETE =		'WAFORM_ACTION_DELETE';

	/**
	* codice dell'operazione di annullamento (abort)
	*/
	const ACTION_CANCEL =		'WAFORM_ACTION_CANCEL';

	/**
	* codice dell'operazione di chiamata RPC
	*/
	const ACTION_RPC =			'WAFORM_ACTION_RPC';

	/**
	* definizione del codice OK per rpc
	* 
	*/
	const RPC_OK =	'WAFORM_RPC_OK';

	/**
	* definizione del codice NON OK per rpc
	* 
	*/
	const RPC_KO =	'WAFORM_RPC_KO';

	/**
	* chiave del codice operazione nella query string
	*/
	const ACTION_KEY =		'waform_action';

	/**
	* view-object del form
	*
	* l'oggetto view è è quello che implementa l'interfaccia i_waFormView
	* e al quale saranno passati i dati per la costruzione dell'interfaccia 
	* utente
	* 
	* se non definito viene usato il default 
	* 
	* \waLibs\views\waForm\waForm_default\waFormView
	* 
	* contenuto in uis/waform_default/view/waform.php
	* 
	* @var i_waFormView
	*/	
	public 	$view = null;
	
	/**
	* pagina di destinazione (action HTML) del modulo
	* @var string
	*/	
	public $destinationPage		= '';
	
	/**
	* nome del modulo
	* @var string
	*/	
	public $name 			= 'waForm';

	/**
	 * titolo del modulo che puo' essere utilizzato come si vuole nel {@link view view-object}
	 *
	 * @var string
	 */
	public $title;
		
	/**
	* recordset
	*
	* eventule oggetto di classe waRecordset che contiene il record associato 
	* al modulo. Il recordset sara' generato dall'applicazione e passato alla
	* classe come proprieta'; il record preso in considerazione sara' sempre 
	* il primo della lista, qualora il recordset contenesse piu' record
	* @var waRecordset
	*/	
	public $recordset;
											
	/**
	* chiave univoca del record
	*
	* il valore di questo campo, se definito e se il record e' valorizzato, viene 
	* inviato al {@link view view-object}; questa informazione viene utilizzata 
	 * a piacere nella UI
	* @var string
	*/	
	public $fieldNameRecId = '';

	/**
	* campo identificativo di editing per simulazione lock optimistic
	*
	* il valore di questo campo, se definito e se il record e' valorizzato, viene 
	* inviato nel {@link view view-object};  questa informazione viene 
	 * utilizzata a piacere nella UI

	 * E' pensato per simulare un lock-optimistic: la pagina di
	* destinazione potra' cosi' controllare sul proprio record se
	* il campo e' stato modificato tra il momento in cui il record e'
	* stato letto e il momento in cui questo viene scritto, mediante chiamata al
	 * metoto {@link getModId}; ovvio che
	* l'applicazione deve provvedere a modificare questo campo su base dati
	* (tipicamente un timestamp) ogni volta che il record viene 
	* modificato
	* @var string
	*/	
	public $fieldNameModId = '';

	/**
	* oggetto di classe non conosciuta da waForm che contiene l'applicazione
	*
	* in fase di RPC, e' possibile chiamare un metodo di questo oggetto anziche'
	* una funzione procedurale
	* @var string
	*/	
	public $application	= null;
	
	/**
	* - 
	*
	* array destinato a contenere i valori di input del modulo, valorizzato
	* a fronte di chiamata a {@link getInputValues}
	* @var array
	*/	
	public $input	= null;
	
	/**
	* insieme di tutti i controlli (di input e etichette) che compongono il
	 * modulo.
	 * 
	 * L'array e' di tipo numerico (non un dizionario, quindi) perche' al 
	 * proprio interno puo' contenere elementi di nome uguale (tipicamente una
	 * etichetta e un controllo di input) e perche' l'ordine di creazione dei
	 * controlli (e quindi l'indice numerico) e' quello che sara' passato al
	 * {@link view view-object} per il rendering

	 * @var array 
	*/	
	public $controls		= array();

	/**
	 * -
	 * 
	 * array associativo (dizionario) contenente l'insieme dei controlli di 
	 * input (non etichette e cornici, quindi) presenti nel modulo; ogni 
	 * elemento e' identificato dal nome del controllo di input
	 * 
	 * @var array
	 */
	public $inputControls	= array();
	
	/**
	 * -
	 * 
	 * array associativo (dizionario) contenente l'insieme delle etichette (e cornici)
	 * presenti nel modulo; ogni elemento e' identificato dal nome dell'
	 * etichetta/cornice
	 * 
	 * @var array
	 */
	public $labels = array();
	
	/**
	 * -
	 * 
	 * array a chiave numerica contenente l'insieme dei controlli di 
	 * input presenti nel modulo, in ordine di inserimento
	 * 
	 * @var array
	 */
	public $inputControlsNum	= array();
	
	/**
	 * -
	 * 
	 * array a chiave numerica contenente l'insieme delle etichette (e cornici)
	 * presenti nel modulo, in ordine di inserimento
	 * 
	 * @var array
	 */
	public $labelsNum = array();
	
	/**
	 * -
	 * 
	 * containera  aperti
	 * 
	 */
	public $openedContainers = [];
	
	/**
	 * -
	 * 
	 * oggetto di classe waRecord che contiene la prima riga dell'array
	 * waRecordset::records ; si presti attenzione al fatto che la proprietà risulterà
	 * valorizzata solo a fronte di chiamata a {@link getInputValues}
	* @var waRecord
	*/	
	public $record;
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language = 'it';
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale = 'it-IT';
	
	/**
	* struttura dati che verrà passata al {@link view view-object}
	*
	* @ignore
	*/	
	var	$data; 
	
	/**
	* in fase di input viene valorizzato con quanto trovato nel campo {@link $fieldNameModId}
	* al momento della creazione dell'output 
	*
	* @ignore
	*/	
	var	$modIdValue; 
	
	/**
	* identifica il tipo di operazione (aggiorna/elimina/abbandona) che viene 
	 * richiesta dalla UI; 
	*
	* @ignore
	*/	
	protected	$requestedInputAction; 
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* costruttore
	*
	* inizializza l modulo.
	* @param string $destinationPage pagina di destinazione del modulo
	 * (action HTML); per
	* default e' la stessa pagina in cui il modulo viene costruito (compresi i
	* parametri di query-string)
	* @param object $application oggetto di classe non predeterminata (ma
	* verosimilmente derivata dalla classe waApplicazione) all'interno del quale cercare i
	* metodi invocati con modalita' RPC qualora il metodo non fosse disponbile
	* proceduralmente. Con questo valore viene valorizzata la proprietà 
	 * {@link $application)
	* @return void 
	*/
	public function __construct($destinationPage = null, $application = null)
		{
		$this->application = $application;
		$this->destinationPage = empty($destinationPage) ? 
									$_SERVER['REQUEST_URI'] :
									$destinationPage;
		
		// inclusione automatica delle classi del package
		$classes = glob(__DIR__ . "/*.class.php");
		foreach ($classes as $class)
			{
			if (basename($class) != basename(__FILE__))
				{
				include_once($class);
				}
			}
		}

	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	 * include eventuali classi estese per i controlli specifici 
	 * dell'applicazione
	 * 
	 * @param string $extensionDir directory contenente le estensioni
	* @return void 
	 */
	public function loadExtensions($extensionDir)
		{
		$classes = glob($extensionDir . "/*.class.php");
		foreach ($classes as $class)
			{
			include_once($class);
			}
		
		}

	//***************************************************************************
	/**
	* show
	*
	* effettua la
	 * trasformazione mediante il {@link view view-object} e ne produce 
	 * l'output. Deve essere ovviamente invocato al termine della costruzione 
	 * del modulo.
	 * 
	* @param boolean $return se false, allora viene immediatamente effettuato
	* l'output del modulo; altrimenti la funzione ritorna il buffer di output 
	* del modulo stesso
	* @return void|string
	*/
	public function show($return = false)
		{
		$this->createData();
		ob_start();

		// se non è stato passato definisce l'oggetto view di default
		$this->setView();
		$this->view->transform($this->data);
		
		if ($return)
			{
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
			}	
			
		ob_end_flush();
		} 

	//***************************************************************************
	/**
	* ritorna l'oggetto contenente i dati da inviare al view-object
	 * 
	 * da usare in fase di debug per ispezionare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	* 
	* @return waFormData
	*/
	public function getData()
		{
		$this->createData();
		return $this->data;
		}
		
	//***************************************************************************
	/**
	* da usare in fase di debug per mostrare i dati che vengono
	 * passati al {@link view view-object} anziche' l'HTML generato
	* dal {@link view view-object} stesso
	 * 
	* @return void
	*/
	public function showJSON()
		{
		$this->createData();
		header("Content-Type: application/json; charset=utf-8");			
		echo json_encode($this->data, JSON_PRETTY_PRINT);
		}
		
	//***************************************************************************
	/**
	* costruisce la struttura dati da passare al {@link view view-object}
	*
	* @access protected
	* @ignore
	*/
	protected function createData()
		{
			
		$this->data = new waFormData();
		$this->data->name = $this->name;
		$this->data->waFormPath = $this->getMyPath();
		$this->data->title = $this->title;
		$this->data->uri = $_SERVER['REQUEST_URI'];
		$this->data->destinationPage = $this->destinationPage;
		$this->data->recId = new waFormDataFieldId();
		$this->data->recId->name = $this->fieldNameRecId;
		$this->data->recId->value = $this->record && $this->fieldNameModId ?
											$this->record->value($this->fieldNameRecId) :
											'';
		$this->data->modId = new waFormDataFieldId();
		$this->data->modId->name = $this->fieldNameModId;
		$this->data->modId->value = $this->record && $this->fieldNameModId ?
											$this->record->value($this->fieldNameModId) :
											'';
		$this->data->language = $this->language;
		$this->data->locale = $this->locale;

		$this->data->controls = array();
		foreach ($this->controls as $ctrl)
			{
			$this->data->controls[] = $ctrl->get();
			}
		}
		
	//**********************************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function RPCExecute()
		{			
		// $_POST['waform_payload'] contiene i dati applicativi, sotto forma di array json encoded
		$params = $_POST["waform_rpc_data"] ? json_decode($_POST["waform_rpc_data"]) : array();
		
		if ($params[0] === "waform_rpc_inner_control_function")
			{
			// è una chiamata per una rpc gestita internamente da uno dei 
			// controlli
			array_shift($params);
			$ctrlName = array_shift($params);
			$functionName = $_POST['waform_rpc_function'];
			$response = $this->inputControls[$ctrlName]->$functionName($params);
			}
		elseif (function_exists($_POST['waform_rpc_function']))
			{
			$response = call_user_func_array($_POST['waform_rpc_function'], $params);
			}
		elseif (method_exists($this->application, $_POST['waform_rpc_function']))
			{
			$response = call_user_func_array(array($this->application, $_POST['waform_rpc_function']), $params);
			}
		else
			{
			$this->RPCResponse(waForm::RPC_KO, "RPC function not found: $_POST[waform_rpc_function]");
			}

		$this->RPCResponse (waForm::RPC_OK , "", $response);
		}
		
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function RPCResponse($response = waForm::RPC_OK, $message = '', $data = null) 
		{
		$retval["waform_rpc_response"] = $response;
		$retval["waform_rpc_message"] = $message;
		$retval["waform_rpc_data"] = $data;
		header("Content-Type: application/json; charset=utf-8");			
		exit(json_encode($retval, JSON_PRETTY_PRINT));
		}
		
	//***************************************************************************
	/**
	* -
	*
	* restituisce il tipo di operazione richiesta per la pagina (una delle defines
	* {@link WAFORM_OPE_VIS_DETTAGLIO}, {@link WAFORM_OPE_INSERIMENTO}, 
	* {@link WAFORM_OPE_MODIFICA}, {@link WAFORM_OPE_ELIMINA})
	*	
	 * @ignore 
	* @return integer
	*/
	function getAction()
		{
		return $_GET[waForm::ACTION_KEY];
		}

	//***************************************************************************
	/**
	*
	* @ignore
	*/
	function objectToArray($d) 
		{
		if (is_object($d))
		// Gets the properties of the given object
		// with get_object_vars function
			{
			$d = get_object_vars($d);
			}

		if (is_array($d)) 			
			{
			if ($d)
				{
				return array_map(array($this, "objectToArray"), $d);
				}
			else
				{
				return '';
				}
			}
			
		// Return array
		return $d;
		}
	
	//***************************************************************************
	/**
	* getInputValues
	 * 
	 * e' il primo metodo da invocare una volta costruito il modulo;
	 *  invocando questo metodo, infatti,  e' possibile
	 * prendere decisioni sul comportamento che l'applicazione deve assumere, a 
	 * seconda dell'operativita' dell'utente. Tipicamente avremo una situazione 
	 * di questo tipo:
	* <code>
	 * // chiamata al metodo dell'applicazione che costruisce il modulo
	 * // popolandolo con i controlli e associandovi un record waDB
	 * $this->form = $this->creaModulo(); 
	 * 
	 * // chiamata al presente metodo che legge eventuali values in input
	 * $this->form->getInputValues();
	 * 
	 * // verifica se e' stato richiesto dall'utente l'abort dell'operazione
	 * // di editing del record 
	 * if ($this->form->isTocancel())
	 *	   $this->response();
	 * 
	 * // verifica se e' stato richiesto dall'utente la cancellazione del record
	 * // in editing 
	 * elseif ($this->form->isToDelete())
	 *	   $this->eliminaRecord();
	 * 
	 * // verifica se e' stato effettuato il submit da parte dell'utente, che 
	 * // quindi desidera consolidare (inserire/modificare)i dati inputati nel 
	 * // modulo; i dati, gia' normalizzati, potranno essere prelevati dall'
	 * // array associativo {@link input}, oppure tramite la proprieta' 
	 * // {@link waControl::$inputValue inputValue} di ogni controllo, 
	 * // o ancora utilizzare il metodo {@link save}
	 * // per copiare direttamente i dati del modulo nel record waRecord
	 * elseif ($this->form->isToUpdate())
	 *	   $this->aggiornaRecord();
	 * 
	 * // poiche' non e' stata effettuata alcuna azione di input da parte 
	 * // dell'utente, produciamo l'output del modulo
	 * else
	 *	   $this->mostraPagina();
	* </code>

	 * 
	 * @return void
	*
	*/
	public function getInputValues()
		{
		// valorizziamo il record, se esistente
		if ($this->recordset)
			{
			$this->record = $this->recordset->records[0];
			}

		// dobbiamo definire il valore iniziale (di default o letto da DB) di
		// ogni controllo di input
		foreach ($this->inputControls as $ctrl)
				{
				$ctrl->setInitialValue();
				}

		// bicio_??? spostare sopra?
		// i parametri dati arrivano sempre in post 
		if ($_POST["waform_form_name"] != $this->name)
		// il post (se c'e') non e' relativo a questa istanza del modulo
			{
			return;
			}

		$this->requestedInputAction = $_POST[waForm::ACTION_KEY];
		// eventuali parametri di una chiamata RPC non ha senso che vengano 
		// macinati dall'XSLT; se e' una chiamata RPC per questa istanza del
		// modulo, usiamo i parametri prelevandoli direttamente dal POST e
		// usciamo
		if ($this->requestedInputAction == waForm::ACTION_RPC)
			{
			// e' una chiamata rpc del modulo; eseguiamo la funzione rpc ed usciamo.
			$this->RPCExecute();
			}

		// c'e' stata richiesta una modifica dati (potrebbe anche essere una
		// eliminazione del record; lo vediamo dopo); valorizziamo ogni
		// controllo di input con i dati ricevuti dalla UI
		$this->modIdValue = $_POST[$this->fieldNameModId] ? $_POST[$this->fieldNameModId] : $this->modIdValue;
		
		$this->createData();
		// se non è stato passato definisce l'oggetto view di default
		$this->setView();
		$jsonBulkInput = $this->view->transformInput($this->data);
		
		foreach($jsonBulkInput->controls as $idx => $bulkCtrl)
			{
			$ctrl = $this->controls[$idx];
			$var = $ctrl->input2inputValue($bulkCtrl->inputValue);
			if ($var !== null)
				{
				$this->input[$ctrl->name] = $var;
				}
			}
		}
		
	//***************************************************************************
	/**
	* -
	*
	 * verifica se i controlli definiti obbligatori in fase di costruzione del
	 * modulo sono stati tutti correttamente valorizzati
	 * 
	 * @return boolean
	*/
	public function checkMandatory()
		{
		foreach($this->controls as $ctrl)
			{
			if (!$ctrl->checkMandatory())
				{
				return false;
				}
			}
		return true;
		}

	//***************************************************************************
	/**
	* -
	*
	 * qualora sia stato implementata la gestione del ModId (proprieta' 
	 * {@link fieldNameModId} e relativa gestione all'interno della UI) 
	 * restituisce il valore che e' stato letto in fase di costruzione del 
	 * modulo all'interno del record. In questo modo e' possibile confrontarlo
	 * col valore presente all'interno del record al momento dell'aggiornamento
	 * e verificare cosi' se si e' verificata una violazione di lock
	 * 
	 * @return mixed (si consiglia di utilizzare un timestamp)
	*/
	public function getModId()
		{
		return $this->modIdValue;
		}

	//***************************************************************************
	/**
	* -
	*
	* salva automaticamente tutti i valori di input dei controlli nel corrispondente
	* campo del record associato al modulo. I campi che che vengono valorizzati
	 * sono unicamente quelli che appartengono alla tabella a cui appartiene
	 * il primo campo trovato nel record che è chiave primaria
	 * 
	 * @param boolean $consolidate : se true effettua anche il consolidamento
	 * sulla base dati; altrimenti si limita a valorizzare il waRecord
	 * 
	 * @return boolean|integer : false = errore; true = ok; se $consolida e
	 * il record e' frutto di un nuovo inserimento, torna l'identificativo
	 * del record inserito
	*/
	public function save($consolidate = false)
		{
		// valorizziamo il record, se esistente
		if (!$this->recordset)
			{
			return false;
			}

		if (!$this->recordset->records[0])
			{
			$this->record = $this->recordset->add();
			$new = true;
			}
		else
			{
			$this->record = $this->recordset->records[0];
			}

		$tabellaPK = $this->recordset->tableName($this->recordset->primaryKey());
		if (!$tabellaPK)
			{
			return false;
			}

		foreach($this->controls as $ctrl)
			{
			// lavoriamo solo sui campi che appartengono alla tabella della chiave primaria
			if ($this->recordset->tableName($ctrl->name) == $tabellaPK)
				{
				$ctrl->input2record();
				}
			}
			
		if ($consolidate)
			{
			$this->recordset->save();
			if ($this->recordset->dbConnection->errorNr())
				{
				return false;
				}
			if ($new)
				{
				return $this->recordset->dbConnection->lastInsertedId();
				}
			}
			
		return true;
		}
		
	//***************************************************************************
	/**
	* provede all'eliminazione del record associato al modulo, se presente
	 * 
	 * @param boolean $consolidate : se true effettua anche il consolidamento
	 * sulla base dati; altrimenti si limita a valorizzare il waRecord
	 * 
	 * attenzione: il record deve contenere un solo campo chiave primaria, 
	 * altrimenti il metodo non viene eseguito e viene tornato il valore false
	 * 
	 * @return boolean : false = errore ; true = ok ; 
	 * 
	*/
	public function delete($consolidate = false)
		{
		// valorizziamo il record, se esistente
		if (!$this->recordset || !$this->recordset->records[0])
			{
			return false;
			}

		// verifichiamo che stiano richiedendo l'eliminazione di un record con
		// una sola chiave primaria
		$nrPK = 0;
		foreach($this->recordset->columns as $col)
			{
			// lavoriamo solo sui campi che appartengono alla tabella della chiave primaria
			$nrPK += $col['primaryKey'] ? 1 : 0;
			}
		if ($nrPK != 1)
			{
			return false;
			}

		$this->record = $this->recordset->records[0];
		$this->record->delete();
		if ($consolidate)
			{
			$this->recordset->save();
			if ($this->recordset->dbConnection->errorNr())
				{
				return false;
				}
			}
			
		return true;
		}
		
	//***************************************************************************
	/**
	* verifica se e' stata richiesta da UI l'aggiornameto/inserimento del record
	 * 
	 * @return boolean
	 * 
	*/
	public function isToUpdate()
		{
		return $this->requestedInputAction == waForm::ACTION_INSERT || 
					$this->requestedInputAction == waForm::ACTION_EDIT;
		}

	//***************************************************************************
	/**
	* verifica se e' stata richiesta l'eliminazione del record (da UI o da 
	 * programma)
	 * 
	 * @return boolean
	 * 
	*/
	public function isToDelete()
		{
		return $this->getAction() == waForm::ACTION_DELETE || 
				$this->requestedInputAction == waForm::ACTION_DELETE;
		}

	//***************************************************************************
	/**
	* verifica se e' stata richiesto l'annullamento (abort) dell'editing
	 * 
	 * @return boolean
	 * 
	*/
	public function isToCancel()
		{
		return $this->requestedInputAction == waForm::ACTION_CANCEL;
		}

		
	//***************************************************************************
	/**
	* verifica se e' stata richiesta la sola visualizzazione del form
	 * 
	 * @return boolean
	 * 
	*/
	public function isReadOnly()
		{
		return $this->getAction() == waForm::ACTION_DETAILS;
		}

	//**************************************************************************
	/**
	 * aggiunge un controllo e relativa etichetta al modulo
	 * 
	 * @param string	$class nome della classe del controllo che si intende creare
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 *
	 * @return object	oggetto controllo di input della classe data
	 */
	
	public function addGeneric($class, $name, $label, $readOnly = false, $mandatory = false)
		{

		$lblctrl = new waLabel($this , $name, $label);
		$lblctrl->readOnly = $readOnly;
		$lblctrl->mandatory = $mandatory;
		$class = __NAMESPACE__ . "\\$class";
		$ctrl = new $class($this, $name);
		$ctrl->readOnly = $readOnly;
		$ctrl->mandatory = $mandatory;
		
		if ($this->openedContainers)
			{
			$this->addControlToContainer($this->openedContainers[count($this->openedContainers) - 1], $ctrl);
			}
		
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo TextArea, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waTextArea
	 */
	public function addTextArea($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waTextArea', $name, $label, 
											$readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo bottone all'interno del modulo
	 * 
	 * @param string	$name nome del controllo che si intende creare
	 * @param string	$value valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * 
	 * @return waButton
	 */
	public function addButton($name, $value, $readOnly = false)
		{
			
		$ctrl = new waButton($this, $name, $value);
		$ctrl->readOnly = $readOnly;
		
		if ($this->openedContainers)
			{
			$this->addControlToContainer($this->openedContainers[count($this->openedContainers) - 1], $ctrl);
			}
		
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo captcha, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waCaptcha
	 */
	public function addCaptcha($name, $label, $readOnly = false, $mandatory = true)
		{
		$ctrl = $this->addGeneric('waCaptcha', $name, $label,  $readOnly, $mandatory);
											
											
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo caricafile, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waUpload
	 */
	public function addUpload($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waUpload', $name, $label, 
											$readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo cfpi, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waTaxCode
	 */
	public function addTaxCode($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waTaxCode', $name, $label, 
											$readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo data, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waDate
	 */
	public function addDate($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waDate', $name, $label, 
											$readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo datetime, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waDateTime
	 */
	public function addDateTime($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waDateTime', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * -
	 * 
	 * alias di {@link addNotControl}
	 * 
	 * @param string	$name nome del controllo 
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * @return waNotControl
	 */
	public function addItem($name, $readOnly = false, $mandatory = false)
		{
		return $this->addNotControl($name, $readOnly, $mandatory);
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo email, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waEmail
	 */
	public function addEmail($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waEmail', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo intero, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waInteger
	 */
	public function addInteger($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waInteger', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo logico, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waBoolean
	 */
	public function addBoolean($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waBoolean', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo multiselezione, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waMultiSelect
	 */
	public function addMultiSelect($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waMultiSelect', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo noncontrollo, senza alcuna etichetta, 
	 * all'interno del modulo
	 * 
	 * @param string	$name nome del controllo 
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waNotControl
	 */
	public function addNotControl($name, $readOnly = false, $mandatory = false)
		{
		$ctrl = new waNotControl($this, $name);
		$ctrl->readOnly = $readOnly;
		$ctrl->mandatory = $mandatory;
		if ($this->openedContainers)
			{
			$this->addControlToContainer($this->openedContainers[count($this->openedContainers) - 1], $ctrl);
			}
		
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo opzione, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waOption
	 */
	public function addOption($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waOption', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo ora, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waTime
	 */
	public function addTime($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waTime', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo password, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waPassword
	 */
	public function addPassword($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waPassword', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo selezione, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waSelect
	 */
	public function addSelect($name, $label, $readOnly = false, $mandatory = false) 
		{
		$ctrl = $this->addGeneric('waSelect', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo selezione, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waSelectTypeahead
	 */
	public function addSelectTypeahead($name, $label, $readOnly = false, $mandatory = false) 
		{
		$ctrl = $this->addGeneric('waSelectTypeAhead', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo testo, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waText
	 */
	public function addText($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waText', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo valuta, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$name nome del controllo e dell'etichetta che si intende creare
	 * @param string	$label valore dell'etichetta del controllo
	 * @param boolean	$readOnly true se il controllo va creato in sola lettura
	 * @param boolean	$mandatory true se il controllo va creato obbligatorio
	 * 
	 * @return waCurrency
	 */
	public function addCurrency($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = $this->addGeneric('waCurrency', $name, $label, $readOnly, $mandatory);
		return $ctrl;
		}
		
	//**************************************************************************
	/**
	 * apre un waFrame ; tutti i controlli aggiunti fino alla chiusura del 
	 * waFrame saranno aggiunti all'interno di questo container
	 * 
	 * 
	 * @return waFrame
	 */
	public function openFrame($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = new waFrame($this, $name, $label);
		if ($this->openedContainers)
			{
			$this->addControlToContainer($this->openedContainers[count($this->openedContainers) - 1], $ctrl);
			}
		$this->openedContainers[] = $ctrl;
		return $ctrl;
		}
		
	//**************************************************************************
	/**
	 * chiude un waFrame
	 * 
	 */
	public function closeFrame()
		{
		array_pop($this->openedContainers);
		}
		
	//**************************************************************************
	/**
	 * apre un waTab ; tutti i controlli aggiunti fino alla chiusura del 
	 * waTab saranno aggiunti all'interno di questo container
	 * 
	 * @return waTab
	 */
	public function openTab($name, $label, $readOnly = false, $mandatory = false)
		{
		$ctrl = new waTab($this, $name, $label);
		if ($this->openedContainers)
			{
			$this->addControlToContainer($this->openedContainers[count($this->openedContainers) - 1], $ctrl);
			}
		$this->openedContainers[] = $ctrl;
		return $ctrl;
		}
		
	//**************************************************************************
	/**
	 * chiude un waTab
	 * 
	 */
	public function closeTab()
		{
		array_pop($this->openedContainers);
		}
		
	//**************************************************************************
	/**
	 * aggiunge il controllo dato a un altro controllo di tipo container
	 * 
	 * @param waControl $container container (frame, tab, ...)
	 * @param waControl $ctrl controllo da aggiungere
	 * @param boolean	$labelToo se cercare e aggiungere anche l'etichetta 
	 * 
	 * @return void
	 */
	public function addControlToContainer(waControl $container, waControl $ctrl, $labelToo = true)
		{
		$ctrl->container = $container->name;
		if ($labelToo && $this->labels[$ctrl->name])
			{
			$this->labels[$ctrl->name]->container = $container->name;
			}
		}
		
	//***********************************************************************
	/**
	* -
	*
	* magic method: restituisce, se esiste, il valore di input (!) del controllo
	 * individuato da $nomeControllo
	 * 
	 * è uno shortcut equivalente a invocare le proprietà
	 * 
	 * - waForm::input[$nomeControllo] 
	 * - waForm::controlliInput[$nomeControllo]->valoreInput
	 * 
	 * Attenzione a situazioni particolari in cui un modulo potrebbe contenere 
	 * controlli con lo stesso nome di una delle proprietà della classe (title, name, ecc.):
	 * in questo caso ciò che viene restituito è il valore della proprietà,
	 * non del controllo
	 * 
	* @param string $nomeControllo nome del controllo di input contenuto nel modulo
	* @return mixed valore del controllo immesso in input
	*/ 
	public function __get($ctrlName)
		{
		if ($this->inputControls[$ctrlName])
			{
			return $this->inputControls[$ctrlName]->inputValue;
			}
			
		}
		
	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function setView()
		{
		if ($this->view)
			{
			return;
			}

		// non è stato definito un view-object: andiamo con quello di default
		include_once  __DIR__ . "/uis/waform_default/view/waform.php";
		$this->view = new \waLibs\views\waForm\waForm_default\waFormView();
		}

	//***********************************************************************
	/**
	* @ignore
	* @access protected
	*/	
	protected function getMyPath()
		{
		if ($_SESSION[get_class() . "_path"])
			{
			// ce l'eravamo già salvata in precedenza
			return $_SESSION[get_class() . "_path"];
			}
		
		if (strpos(__FILE__, "\\") !== false)
			{
			// siamo sotto windows
			$thisFile = strtolower(str_replace("\\", "/", __FILE__));
			$dr = strtolower(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']));
			}
		else
			{
			$thisFile = __FILE__;
			$dr = $_SERVER['DOCUMENT_ROOT'];
			}
		
		if (substr($dr, -1) == "/")
			{
			$dr = substr($dr, 0, -1);
			}
			
		if ($dr != substr($thisFile, 0, strlen($dr)))
			{
			// symlink! cerchiamo la path relativa alla document root
			$relative = "/" . basename(dirname(dirname($thisFile))) . "/" . basename(dirname($thisFile));
			$dirIterator = new \RecursiveDirectoryIterator($dr, \RecursiveDirectoryIterator::FOLLOW_SYMLINKS);
			$iterator = new \ParentIterator($dirIterator);
			$directories = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST, \RecursiveIteratorIterator::CATCH_GET_CHILD);

			foreach ($directories as $path => $object)
				{
				$path = str_replace("\\", "/", $path);
				if (strpos($path, $relative) !== false)
					{
					$toret = substr($path, strlen($dr));
					break;
					}
				}
			}
		else
			{
			$toret = substr(dirname($thisFile), strlen($dr));
			}

		return $_SESSION[get_class() . "_path"] = $toret;		
		}
	
	}	// fine classe waForm






