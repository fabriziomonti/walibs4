<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waOption *******************************************************
//***************************************************************************
/**
* waOption
*
* classe per la gestione dei controlli di tipo radio-button, ossia una lista 
* all'interno della quale e' possibile selezionare un solo elemento.
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waOption extends waControl
	{
	
	/**
	* array dei valori selezionabili.
	*
	* La lista e' un array 
	* associativo in cui la chiave dell'elemento e' il valore dell'option
	* (tipicamente la chiave primaria un record di una tabella in relazione) e 
	* il valore dell'elemento e' la descrizione (label) da porre in 
	* corrispondenza dell'option.
	*
	* La lista puo' essere creata, in alternativa al passaggio esplicito,
	* a partire da una query da sottoporre al DB: vedi {@link $sql}.
	* @var array
	*/
	public $list			= array();
	
	/**
	* query SQL per la creazione della lista dei valori selezionabili.
	*
	* Tramite questa query, la classe accedera' al DB e con il risultato della
	* query creera' la lista.
	* Il primo campo sara' la chiave dell'elemento, l'ultimo campo sara'
	* la descrizione dell'elemento.
	* E' possibile definire una query che restituisce piu' di 2 campi; in
	* questo caso, la classe concatenera' tutti i campi, separati da "|" 
	* ad eccezione dell'ultimo (descrizione); questa stringa concatenata
	* diventera' la chiave di ogni elemento. In questo modo e' possibile
	* passare all'applicazione, per ogni option selezionabile, anche ulteriori
	* informazioni relative alla option selezionata, oltre all'identificativo
	* univoco.
	* @var string
	*/
	public $sql	= '';
	
	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'option';
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	 * @return waFormDataControlOption
	*/
	function get()
		{
		// se mi viene passata una query, costruisco la lista sulla
		// base della query
		if ($this->sql)
			{
			$this->list = $this->BuildListFromDBQuery();
			}

		return parent::get();
		}

	//***************************************************************************
	/**
	* @access protected
	* @ignore
	*/
	protected function BuildListFromDBQuery()
		{

		// se mi viene passata una query, costruisco la lista sulla
		// base della query

		if (empty($this->form->recordset))
		// se l'applicazione non ha messo in bind un recordset alla form, non
		// abbiamo le informazioni per connetterci al db
			{
			return (array());
			}

		$rs = new waRecordset($this->form->recordset->dbConnection);
		$righe = $rs->read($this->sql);
		if ($rs->errorNr())
			{
			return (array());
			}

		$list = array();
		foreach ($righe as $riga)
			{
			// costruisco la chiave dell'elemento; la chiave e' composta da tutti i campi
			// ritornati dalla query, separati da "|", ad eccezione dell'ultimo che e' la
			// descrizione da mettere a video. Nella grande maggioranza dei casi avro' solo
			// 2 campi: la chiave di relazione e la descrizione. Gli altri campi, se presenti,
			// servono a ritornare attributi del record selezionato all'applicazione
			$key = $riga->value(0);
			for ($i = 1; $i < ($rs->fieldNr() - 1); $i++)
				{
				$key .= "|" . $riga->value($i);
				}
			$list[$key] = $riga->value($rs->fieldNr() - 1);
			}

		return $list;
		}
		

	}	// fine classe waOption

