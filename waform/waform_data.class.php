<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* -
* include delle sotto-strutture dati 
* @ignore
*/
include_once __DIR__ . "/waform_data_field_id.class.php";
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormData ***************************************************
//***************************************************************************
/**
* waFormData
*
* struttura dati da passare al view-object
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormData 
	{
	/**
	 * nome del menu
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * URI della pagina
	 * 
	 * @var string
	 */
	public $uri;
	
	/**
	 * path relativa alla document_root della libreria waForm
	 * 
	 * @var string
	 */
	public $waFormPath;
	
	/**
	 * pagina di destinazione del modulo (action)
	 * 
	 * @var string
	 */
	public $destinationPage;
	
	/**
	 * titolo del modulo
	 * 
	 * @var string
	 */
	public $title;
	
	/**
	 * identificativo del record
	 * 
	 * @var waFormDataFieldId
	 */
	public $recId;
	
	/**
	 * identificativo di ultima modifica del record
	 * 
	 * @var waFormDataFieldId
	 */
	public $modId;
	
	/**
	 * array dei controlli {@link waFormDataControl}
	 * 
	 * @var array
	 */
	public $controls;
	
	
	/**
	* lingua utilizzata
	*
	* @var string
	*/	
	public $language;
	
	/**
	* locale utilizzato
	*
	* @var string
	*/	
	public $locale;
	
	}
	
