<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlSelectTypeahead *****************************
//***************************************************************************
/**
* waFormDataControlSelectTypeahead
*
* struttura dati di un controllo di tipo select
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlSelectTypeahead extends waFormDataControl
	{
	/**
	 * testo da mostrare inizialmente
	 * 
	 * @var string
	 */
	public $text;

	}
	
