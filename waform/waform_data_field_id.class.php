<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

//***************************************************************************
//****  classe waFormDataFieldId ********************************************
//***************************************************************************
/**
* waFormDataFieldId
*
* struttura dati di un campo identificativo del record (chiave primaria o 
 * identificativo di ultima modifica)
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataFieldId 
	{
	/**
	 * nome del campo identificativo del record
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * valore del campo identificativo del record
	 * 
	 * @var string
	 */
	public $value;
		
	}
	
