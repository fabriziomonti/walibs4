<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once __DIR__ . "/waform_data_control.class.php";

//***************************************************************************
//****  classe waFormDataControlButton **************************************
//***************************************************************************
/**
* waFormDataControlButton
*
* struttura dati di un controllo di tipo button
* 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waFormDataControlButton extends waFormDataControl
	{
	/**
	 * il bottone è di tipo submit
	 * 
	 * @var bool
	 */
	public $submit;
		
	/**
	 * il bottone è di tipo cancel
	 * 
	 * @var bool
	 */
	public $cancel;
		
	/**
	 * il bottone è di tipo delete
	 * 
	 * @var bool
	 */
	public $delete;
		
	}
	
