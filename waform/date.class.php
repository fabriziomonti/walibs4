<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waData *******************************************************
//***************************************************************************
/**
* waData
*
* classe per la gestione dei controlli di tipo data. 
 * 
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waDate extends waControl
	{
	
	/**
	* @ignore
	* @access protected
	*/
	protected $type				= 'date';
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	* @access protected
	 * @return waFormDataControl
	*/
	function get()
		{
		$retval = parent::get();
		$retval->value = $this->value ? date("Y-m-d", $this->value) : '';
		return $retval;
		}

	//**************************************************************************
	/**
	* Restituisce la data inputata in formato timestamp
	*
	* Si usa in fase di ricezione dei dati, non
	* durante la costruzione della form.
	*
	* @ignore
	* @return mixed il timestamp della data se valorizzata correttamente; altrimenti NULL
	*/
	function input2inputValue($valueIn)
		{
		if ($valueIn === null)
			{
			return $this->inputValue = null;
			}

		if (!preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $valueIn, $parts))
			{
			return $this->inputValue = false;
			}

		if (!checkdate((int) $parts[2], (int) $parts[3], (int) $parts[1]))
			{
			return $this->inputValue = false;
			}

		return $this->inputValue = mktime(0,0,0, $parts[2], $parts[3], $parts[1]);
		}

	}	// fine classe waData






