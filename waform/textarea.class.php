<?php
/**
* -
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/

namespace waLibs;

/**
* @ignore
*/
include_once(__DIR__ . "/control.class.php");

//***************************************************************************
//****  classe waTextArea *******************************************************
//***************************************************************************
/**
* waTextArea
*
* classe per la gestione di un controllo textarea.
*
* @package waForm
* @version 4.0
* @author F.Monti
* @copyright (c) 2007-2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
class waTextArea extends waControl
	{
	/**
	* larghezza in caratteri del controllo
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var integer
	*/
	public $columns			= 50;
	
	/**
	* altezza in caratteri del controllo
	*
	* questa informazione viene utilizzata a piacere nella UI
	* @var integer
	*/
	public $rows			= 3;

	/**
	* nr. massimo di caratteri accettabili dal controllo
	*
	* @var integer
	*/
	public $maxChars		= '';

	/**
	* @ignore
	* @access protected
	*/
	protected $type			= 'textarea';
	
	}	// fine classe waTextArea

	